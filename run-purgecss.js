/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

const exec = require('child_process').exec;

/**
 * or you can refer to https://dev.to/dylanvdmerwe/reduce-angular-style-size-using-purgecss-to-remove-unused-styles-3b2k
 * for full comparision script to show before and after results
 * */

console.log('Run PurgeCSS...');

exec(
  'purgecss --config ./purgecss.config.js',
  function (error, stdout, stderr) {
    console.log('PurgeCSS completed Yay');
  }
);