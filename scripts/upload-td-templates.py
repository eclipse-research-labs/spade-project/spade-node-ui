import os
import boto3
import dotenv
from botocore.exceptions import NoCredentialsError

# S3 configuration
S3_URL = "fsn1.your-objectstorage.com"
S3_ID = "LIYKS5YF4WSGD6EPC1K2"
S3_PASS = "f5HGrt2gNOEndKRiim71TVcyv2Vgnw1qOCEpf334"
BUCKET_NAME = "spade2"
# S3_URL = "s3.joralmi.eu"
# S3_ID = "X53EwKNy3s2TWb3bRK9W"
# S3_PASS = "0ra1kp33xknwtgZMmJKlQZOwgZmfGrNdEt6oJBzJ"
# BUCKET_NAME = "spadetds"

s3_client = boto3.client('s3', endpoint_url=f'https://{S3_URL}', aws_access_key_id=S3_ID, aws_secret_access_key=S3_PASS)
# s3_client = boto3.client('s3', endpoint_url=f'https://{S3_URL}', disable_signing=True)
# s3_client = boto3.client('s3', config=Config(signature_version=UNSIGNED))

def upload_to_s3(file_name, bucket, object_name=None):
    try:
        s3_client.upload_file(file_name, bucket, object_name or file_name)
        print(f"Upload Successful: {file_name}")
    except FileNotFoundError:
        print(f"The file was not found: {file_name}")
    except NoCredentialsError:
        print("Credentials not available")

def clear_bucket(bucket):
    try:
        response = s3_client.list_objects_v2(Bucket=bucket)
        if 'Contents' in response:
            for item in response['Contents']:
                s3_client.delete_object(Bucket=bucket, Key=item['Key'])
            print(f"All contents deleted from bucket: {bucket}")
        else:
            print(f"No contents found in bucket: {bucket}")
    except s3_client.exceptions.NoSuchKey:
        print("No such key found in the bucket")
    except NoCredentialsError:
        print("Credentials not available")

def main():
    templates_folder = 'templates'
    clear_bucket(BUCKET_NAME)
    for root, dirs, files in os.walk(templates_folder):
        for file in files:
            file_path = os.path.join(root, file)
            upload_to_s3(file_path, BUCKET_NAME)

if __name__ == "__main__":
    main()
