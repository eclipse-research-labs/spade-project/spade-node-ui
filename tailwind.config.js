/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

/* refer for more details https://tailwindcss.com/docs/configuration */

module.exports = {
  content: ['src/**/*.{html,js, jsx, tsx, ts}'],
  darkMode: 'media',
  theme: {
    fontFamily: {
      primary: ['sans-serif'],
      secondary: ['Open Sans'],
    },
    borderRadius: {
      none: '0',
      tiny: '4px',
      sm: '8px',
      DEFAULT: '12px',
      md: '16px',
      lg: '24px',
      full: '9999px',
    },
    minWidth: {
      0: '0',
      '1/4': '25%',
      '1/2': '50%',
      '3/4': '75%',
      full: '100%',
    },
    fontSize: {
      tiny: '8px',
      xs: '10px',
      sm: '12px',
      base: '14px',
      md: '16px',
      lg: '18px',
      xl: '24px',
      xxl: '28px',
    },
    colors: {
      transparent: 'transparent',
      white: '#fff',
      black: '#000000',
      grey: '#CCCCCC',
      lightGrey: '#F8F9FA',
      dialogGrey: '#f5f5f5',
      borderGrey: '#DEE2E6',
      whitesmoke: '#f3f3f3',
      info: '#3b82f6',
      yellow: '#f2b619',
      success: '#277d26',
      danger: '#ff3a3a',
      warning: '#f69e0d',
      skyBlue: '#14a6e7',
      grassGreen: '#379e2e',
      grassGreen: '#379e2e',
      darkOrange: '#f5911f',
      cobalt: '#002240',
      primary: {
        50: '#f2fbf3',
        100: '#f2fbf3',
        200: '#e1f7e3',
        300: '#c4eec8',
        400: '#96df9d',
        500: '#5bc566',
        600: '#2c8d37',
        700: '#266f2e',
        800: '#225929',
        900: '#1e4924',
        950: '#0b280f',
      },
      secondary: {
        50: '#f3f6fb',
        100: '#e3eaf6',
        200: '#cedcef',
        300: '#adc4e3',
        400: '#85a6d5',
        500: '#688ac9',
        600: '#5571bb',
        700: '#4a60ab',
        800: '#41508c',
        900: '#384570',
        950: '#2b324f',
      },
      text: {
        50: '#f7f7f7',
        100: '#e3e3e3',
        200: '#c8c8c8',
        300: '#a4a4a4',
        400: '#818181',
        500: '#666666',
        600: '#515151',
        700: '#484848',
        800: '#383838',
        900: '#313131',
      },
    },
    extend: {
      width: {
        '1/10': '10%',
        '1.5/10': '15%',
        '3/10': '30%',
        '3.5/10': '35%',
        '4.5/10': '45%',
        '5.5/10': '55%',
        '6.5/10': '65%',
        '7/10': '70%',
        '8.5/10': '85%',
        '9/10': '90%',
        '9.5/10': '95%',
      },
      height: {
        '1/10': '10%',
        '1.5/10': '15%',
        '3/10': '30%',
        '3.5/10': '35%',
        '4.5/10': '45%',
        '5.5/10': '55%',
        '6.5/10': '65%',
        '7/10': '70%',
        '8.5/10': '85%',
        '9/10': '90%',
        '9.5/10': '95%',
      },
      boxShadow: {
        big: '0 5px 30px -24px rgba(0, 0, 0, 0.3)',
        small: '0 2px 10px -7px rgba(0, 0, 0, 0.3)',
      },
    },
  },
  variants: {},
  plugins: [],
  corePlugins: {
    outline: false,
  }
}
