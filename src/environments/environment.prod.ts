/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

export const environment = {
  production: true,
  ccUrl: 'https://catalogue.spade.bavenir.eu',
  s3url: 'https://s3.joralmi.eu',
  northBoundUrl: 'https://localhost:443',
  southBoundUrl: 'https://localhost:444',
  marketplaceUrl: 'https://marketplace.spade.bavenir.eu',
  kcConfig: {
    url: 'https://auth.spade.bavenir.eu',
    realm: 'CC-Dev',
    clientId: 'cc-dev',
  },
  prefixUrl: '',
}
