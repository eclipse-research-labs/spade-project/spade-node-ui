/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

export const environment = {
  production: false,
  ccUrl: 'http://localhost:4000',
  s3url: 'https://s3.joralmi.eu',
  northBoundUrl: 'https://localhost:443',
  southBoundUrl: 'https://localhost:444',
  marketplaceUrl: 'http://localhost:10000',
  kcConfig: {
    url: 'https://auth.jupiter.bavenir.eu',
    realm: 'CC-Dev',
    clientId: 'cc-dev',
  },
  prefixUrl: '',
}
