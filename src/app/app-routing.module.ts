/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { userGuard } from '@core/guards/user.guard'

const routes: Routes = [
  { path: '', loadChildren: () => import('./features/intro/intro.module').then((m) => m.IntroModule) },
  { path: 'auth', loadChildren: () => import('./features/auth/auth.module').then((m) => m.AuthModule) },
  {
    path: 'marketplace',
    canActivate: [userGuard.watch],
    loadChildren: () => import('./features/marketplace/marketplace.module').then((m) => m.MarketplaceModule),
  },
  {
    path: 'nodes',
    canActivate: [userGuard.watch],
    loadChildren: () => import('./features/nodes/nodes.module').then((m) => m.NodesModule),
  },
  {
    path: 'my-node',
    loadChildren: () => import('./features/my-node/my-node.module').then((m) => m.MyNodeModule),
  },
  {
    path: 'my-org',
    canActivate: [userGuard.watch],
    loadChildren: () => import('./features/my-org/my-org.module').then((m) => m.MyOrgModule),
  },
  { path: 'detail', loadChildren: () => import('./features/detail/detail.module').then((m) => m.DetailModule) },
  {
    path: 'contract-creator',
    canActivate: [userGuard.watch],
    data: { roles: [] },
    loadChildren: () =>
      import('./features/contract-creator/contract-creator.module').then((m) => m.ContractCreatorModule),
  },
  {
    path: 'td-editor',
    loadChildren: () => import('./features/td-editor/td-editor.module').then((m) => m.TdEditorModule),
  },
  {
    path: 'not-found',
    loadChildren: () => import('./features/not-found/not-found.module').then((m) => m.NotFoundModule),
  },
  {
    path: 'playground',
    canActivate: [userGuard.watch],
    loadChildren: () => import('./features/playground/playground.module').then((m) => m.PlaygroundModule),
  },
  { path: '**', redirectTo: 'not-found' },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
