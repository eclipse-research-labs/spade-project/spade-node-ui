/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, HostListener, NgZone } from '@angular/core'
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog'
import { Router } from '@angular/router'
import { isString } from 'src/app/utils'

@Component({
  selector: 'app-td-editor',
  templateUrl: './td-editor.component.html',
  styleUrls: ['./td-editor.component.scss'],
  providers: [DialogService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TdEditorComponent {
  ref: DynamicDialogRef | undefined

  constructor(private _dialogService: DialogService, private _router: Router, private _zone: NgZone) {}

  @HostListener('window:message', ['$event'])
  onMessage(event: MessageEvent) {
    const data = event.data
    if (data) {
      this._zone.run(() => {
        this._handle(data)
      })
    }
  }

  private _handle(data: any) {
    if (isString(data)) {
      if (data === 'close') {
        this._router.navigateByUrl('/home/my-node')
      }
    } else {
      // if (data && data.name && data.td) {
      //   this.ref = this._dialogService.open(CreateItemComponent, {
      //     width: '900px',
      //     height: 'calc(100vh - 80px)',
      //     header: (data.name ?? 'Item') + "'s Thing Desctiption",
      //     data: { td: data.td },
      //   })
      // }
    }
  }
}
