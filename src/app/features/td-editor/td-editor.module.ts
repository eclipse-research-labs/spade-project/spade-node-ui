/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { TdEditorRoutingModule } from './td-editor-routing.module'
import { TdEditorComponent } from './td-editor.component'
import { NgLetDirective } from 'ng-let'

@NgModule({
  declarations: [TdEditorComponent],
  imports: [CommonModule, TdEditorRoutingModule, NgLetDirective],
})
export class TdEditorModule {}
