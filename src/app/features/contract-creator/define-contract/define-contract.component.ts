/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { Router } from '@angular/router'
import { ContractCreatorService } from '@core/services/contract-creator/contract-creator.service'

@Component({
  selector: 'app-define-contract',
  templateUrl: './define-contract.component.html',
  styleUrl: './define-contract.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DefineContractComponent {
  constructor(private _contractCreatorService: ContractCreatorService, private _router: Router) {}

  continue() {
    if (!this.disabled) {
      this._router.navigateByUrl(
        `${this._contractCreatorService.url}/${
          this._contractCreatorService.selectTargets
            ? 'select-targets'
            : this._contractCreatorService.selectAssignees
            ? 'select-assignees'
            : 'send-contract'
        }`
      )
    }
  }

  get read() {
    return this._contractCreatorService.read
  }

  set read(value: boolean) {
    this._contractCreatorService.read = value
  }

  get write() {
    return this._contractCreatorService.write
  }

  set write(value: boolean) {
    this._contractCreatorService.write = value
  }

  get subscribe() {
    return this._contractCreatorService.subscribe
  }

  set subscribe(value: boolean) {
    this._contractCreatorService.subscribe = value
  }

  get disabled() {
    return (
      !this._contractCreatorService.read &&
      !this._contractCreatorService.write &&
      !this._contractCreatorService.subscribe
    )
  }
}
