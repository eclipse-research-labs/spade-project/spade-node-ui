/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, HostListener, OnInit } from '@angular/core'
import { MenuItem } from 'primeng/api'
import { Router } from '@angular/router'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { ContractCreatorService } from '@core/services/contract-creator/contract-creator.service'

@Component({
  selector: 'app-contract-creator',
  templateUrl: './contract-creator.component.html',
  styleUrl: './contract-creator.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContractCreatorComponent implements OnInit {
  menuItems: MenuItem[] | undefined

  constructor(
    private _contractCreatorService: ContractCreatorService,
    private _router: Router,
    private _myNodeService: MyNodeService
  ) {}

  ngOnInit(): void {
    if (!this._contractCreatorService.newContracts) {
      this._router.navigateByUrl(`/my-node/online/${this._myNodeService.node?.nodeId}/items`, { replaceUrl: true })
    }
    this.menuItems = [
      {
        label: 'Define Contract',
        routerLink: `${this._contractCreatorService.url}/define-contract`,
      },
      ...(this._contractCreatorService.selectTargets
        ? [
            {
              label: 'Select Targets',
              routerLink: `${this._contractCreatorService.url}/select-targets`,
            },
          ]
        : []),
      ...(this._contractCreatorService.selectAssignees
        ? [
            {
              label: 'Select Assignee',
              routerLink: `${this._contractCreatorService.url}/select-assignees`,
            },
          ]
        : []),
      {
        label: 'Send Contracts',
        routerLink: `${this._contractCreatorService.url}/send-contract`,
      },
    ]
  }

  onClose() {
    this._router.navigateByUrl(
      `my-node/online/${this._myNodeService.node?.nodeId}/${
        this._contractCreatorService.selectAssignees ? 'items' : 'discovery'
      }`,
      { replaceUrl: true }
    )
    this._contractCreatorService.clear()
  }

  @HostListener('window:beforeunload', ['$event']) unloadHandler(event: Event) {
    let result = confirm('Any unsaved progress will be lost. Are you sure you want to reload the page?')
    if (!result) {
      event.preventDefault()
    }
  }
}
