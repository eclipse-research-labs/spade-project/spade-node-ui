/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { ContractCreatorRoutingModule } from './contract-creator-routing.module'
import { ContractCreatorComponent } from './contract-creator.component'
import { DefineContractComponent } from './define-contract/define-contract.component'
import { SelectTargetsComponent } from './select-targets/select-targets.component'
import { SelectAssigneesComponent } from './select-assignees/select-assignees.component'
import { SendContractComponent } from './send-contract/send-contract.component'
import { StatusComponent } from './send-contract/components/status/status.component'
import { OrgPreviewComponent } from '@shared/components/presentation/app-specific/organisation/org-preview/org-preview.component'
import { NodePreviewComponent } from '@shared/components/presentation/app-specific/node/node-preview/node-preview.component'
import { ContractPreviewComponent } from '@shared/components/presentation/app-specific/contract/contract-preview/contract-preview.component'
import { TableSkeletonComponent } from '@shared/components/presentation/reusable/loaders/table-skeleton/table-skeleton.component'
import { SadFaceComponent } from '@shared/components/presentation/reusable/sad-face/sad-face.component'
import { InfoComponent } from '@shared/components/presentation/reusable/info/info.component'
import { ActionButtonComponent } from '@shared/components/presentation/reusable/action-button/action-button.component'
import { NgLetDirective } from 'ng-let'
import { ButtonModule } from 'primeng/button'
import { TreeTableModule } from 'primeng/treetable'
import { TagModule } from 'primeng/tag'
import { StepsModule } from 'primeng/steps'
import { ScrollingModule } from '@angular/cdk/scrolling'

@NgModule({
  declarations: [ContractCreatorComponent, DefineContractComponent, SelectTargetsComponent, SelectAssigneesComponent, SendContractComponent, StatusComponent],
  imports: [
    CommonModule,
    ContractCreatorRoutingModule,
    OrgPreviewComponent,
    NodePreviewComponent,
    ContractPreviewComponent,
    TableSkeletonComponent,
    SadFaceComponent,
    InfoComponent,
    ActionButtonComponent,
    NgLetDirective,
    ButtonModule,
    TreeTableModule,
    TagModule,
    StepsModule,
  ],
})
export class ContractCreatorModule {}
