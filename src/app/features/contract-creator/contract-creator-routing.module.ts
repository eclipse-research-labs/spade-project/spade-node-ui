/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ContractCreatorComponent } from './contract-creator.component'
import { DefineContractComponent } from './define-contract/define-contract.component'
import { SelectTargetsComponent } from './select-targets/select-targets.component'
import { SelectAssigneesComponent } from './select-assignees/select-assignees.component'
import { SendContractComponent } from './send-contract/send-contract.component'
import { onlineGuard } from '@core/guards/online.guard'
import { contractCreatorGuard } from '@core/guards/contract-creator.guard'

const routes: Routes = [
  {
    path: ':nodeId',
    component: ContractCreatorComponent,
    canActivate: [onlineGuard.watch, contractCreatorGuard.watch],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'define-contract',
      },
      {
        path: 'define-contract',
        component: DefineContractComponent,
      },
      {
        path: 'select-targets',
        component: SelectTargetsComponent,
      },
      {
        path: 'select-assignees',
        component: SelectAssigneesComponent,
      },
      {
        path: 'send-contract',
        component: SendContractComponent,
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContractCreatorRoutingModule {}
