/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { NodeWithOwner } from '@core/models/node.model'
import { ContractCreatorService } from '@core/services/contract-creator/contract-creator.service'
import { TreeNode, TreeTableNode } from 'primeng/api'

@Component({
  selector: 'app-select-assignees',
  templateUrl: './select-assignees.component.html',
  styleUrl: './select-assignees.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectAssigneesComponent implements OnInit {
  private _selectedNodes: NodeWithOwner[] = []

  constructor(private _contractCreatorService: ContractCreatorService, private _router: Router) {}

  async ngOnInit(): Promise<void> {
    await this._contractCreatorService.initAssigneeCandidates()
  }

  continue() {
    if (!this.disabled) {
      this._contractCreatorService.assignees = [...this._selectedNodes]
      this._router.navigateByUrl(`${this._contractCreatorService.url}/send-contract`)
    }
  }

  goBack() {
    this._router.navigateByUrl(
      `${this._contractCreatorService.url}/${
        this._contractCreatorService.selectTargets ? 'select-targets' : 'define-contract'
      }`,
      {
        replaceUrl: true,
      }
    )
  }

  selectionChange(nodes: TreeTableNode<TreeNode> | TreeTableNode<TreeNode>[] | null) {
    if (!nodes) {
      this._selectedNodes = []
    } else if (nodes instanceof Array) {
      this._selectedNodes = nodes
        .filter((node) => this.isNode(node.data))
        .map((element) => element.data as NodeWithOwner)
    } else if (this.isNode(nodes.data)) {
      this._selectedNodes = [nodes.data as NodeWithOwner]
    }
  }

  isOrg(data: any) {
    if (data && data['cid']) {
      return true
    }
    return false
  }

  isNode(data: any) {
    if (data && data['nodeId']) {
      return true
    }
    return false
  }

  get initializing$() {
    return this._contractCreatorService.initializing$
  }

  get assigneeCandidatesTree() {
    return this._contractCreatorService.assigneeCandidatesTree
  }

  get disabled() {
    return this._selectedNodes.length <= 0
  }
}
