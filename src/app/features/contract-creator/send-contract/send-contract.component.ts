/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { Router } from '@angular/router'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { ContractRequestStatus } from '@core/enums/contract.enum'
import { ContractCreatorService } from '@core/services/contract-creator/contract-creator.service'

@Component({
  selector: 'app-send-contract',
  templateUrl: './send-contract.component.html',
  styleUrl: './send-contract.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SendContractComponent {
  sendingDone = false

  constructor(
    private _contractCreatorService: ContractCreatorService,
    private _router: Router,
    private _myNodeService: MyNodeService,
    private _snackBar: SnackBarService
  ) {
    this._init()
  }

  private _init() {
    this._contractCreatorService.initContracts()
  }

  async sendContracts() {
    try {
      await this._contractCreatorService.sendContracts()
      this._snackBar.showSuccess('All contracts sent')
    } finally {
      this.sendingDone = true
    }
  }

  async resendFailedContracts() {
    try {
      await this._contractCreatorService.sendContracts(ContractRequestStatus.FAILED)
      this._snackBar.showSuccess('All failed contracts resent')
    } finally {
      this.sendingDone = true
    }
  }

  goBack() {
    this._router.navigateByUrl(
      `${this._contractCreatorService.url}/${
        this._contractCreatorService.selectAssignees
          ? 'select-assignees'
          : this._contractCreatorService.selectTargets
          ? 'select-targets'
          : 'define-contract'
      }`,
      {
        replaceUrl: true,
      }
    )
  }

  onFinish() {
    this._contractCreatorService.clear()
    this._router.navigateByUrl(
      `my-node/online/${this._myNodeService.node?.nodeId}/${
        this._contractCreatorService.selectAssignees ? 'items' : 'discovery'
      }`,
      { replaceUrl: true }
    )
  }

  isOrg(data: any) {
    if (data && data['cid']) {
      return true
    }
    return false
  }

  isNode(data: any) {
    if (data && data['nodeId']) {
      return true
    }
    return false
  }

  isContract(data: any) {
    if (data['policyId']) {
      return true
    }
    return false
  }

  get loading$() {
    return this._contractCreatorService.loading$
  }

  get contractsTree() {
    return this._contractCreatorService.contractsTree
  }

  get hasFailed() {
    return this._contractCreatorService.hasFailed
  }

  get defaultStatus(): ContractRequestStatus {
    return ContractRequestStatus.READY
  }
}
