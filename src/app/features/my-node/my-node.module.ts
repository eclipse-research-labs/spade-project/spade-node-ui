/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { MyNodeRoutingModule } from './my-node-routing.module'
import { MyNodeComponent } from './my-node.component'
import { AdapterDescriptionComponent } from '@shared/components/presentation/app-specific/adapter/adapter-description/adapter-description.component'
import { HeaderComponent } from '@shared/components/smart/header/header.component'
import { SideNavComponent } from '@shared/components/presentation/reusable/side-nav/side-nav.component'
import { EuContribComponent } from '@shared/components/presentation/app-specific/eu-contrib/eu-contrib.component'
import { NgLetDirective } from 'ng-let'
import { ConsumptionsComponent } from '@shared/components/smart/consumptions/consumptions.component'
import { AppStatusComponent } from '@shared/components/smart/app-status/app-status.component'

@NgModule({
  declarations: [MyNodeComponent],
  imports: [
    CommonModule,
    MyNodeRoutingModule,
    AdapterDescriptionComponent,
    HeaderComponent,
    SideNavComponent,
    AppStatusComponent,
    EuContribComponent,
    NgLetDirective,
    ConsumptionsComponent,
  ],
  exports: [],
})
export class MyNodeModule {}
