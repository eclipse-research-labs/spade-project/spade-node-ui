/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog'
import { CredentialComponent } from '../../../shared/components/smart/credential/credential.component'
import { applyLocalTimezone, inflect } from 'src/app/utils'
import { UserRole } from '@core/enums/user.enum'

@Component({
  selector: 'app-credentials',
  templateUrl: './credentials.component.html',
  styleUrl: './credentials.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DialogService],
})
export class CredentialsComponent {
  ref: DynamicDialogRef | undefined

  constructor(private _myNodeService: MyNodeService, private _dialogService: DialogService) {}

  newCredential() {
    this.ref = this._dialogService.open(CredentialComponent, {
      header: 'New Credential',
    })
  }

  convertTtl(creation: Date, ttl: number) {
    const milliseconds = ttl * 1000
    const expires = new Date(applyLocalTimezone(creation).getTime() + milliseconds)
    const today = new Date()
    const hoursDelta = (expires.getTime() - today.getTime()) / 3600000
    const daysDelta = Math.round(hoursDelta / 24)
    const underOneDay = Math.floor(hoursDelta / 24) == 0
    return [
      expires,
      hoursDelta <= 0
        ? 'expired'
        : `expires in: ${underOneDay ? inflect(hoursDelta, '<1 hour', '1 hour', hoursDelta + ' hours') : inflect(daysDelta, '<1 day', '1 day', daysDelta + ' days')}`,
    ]
  }

  get credentials$() {
    return this._myNodeService.credentials$
  }

  get initializing$() {
    return this._myNodeService.initializing$
  }

  get admin() {
    return UserRole.ADMIN
  }

  get nodeOperator() {
    return UserRole.NODE_OPERATOR
  }
}
