/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { CredentialsRoutingModule } from './credentials-routing.module'
import { CredentialsComponent } from './credentials.component'
import { CredentialComponent } from '../../../shared/components/smart/credential/credential.component'
import { OptionsComponent } from './components/options/options.component'
import { ExpirationComponent } from './components/expiration/expiration.component'
import { RoleCheckerComponent } from '@shared/components/smart/role-checker/role-checker.component'
import { InfoComponent } from '@shared/components/presentation/reusable/info/info.component'
import { TableSkeletonComponent } from '@shared/components/presentation/reusable/loaders/table-skeleton/table-skeleton.component'
import { LongIdComponent } from '@shared/components/presentation/reusable/long-id/long-id.component'
import { SadFaceComponent } from '@shared/components/presentation/reusable/sad-face/sad-face.component'
import { ApplyLocalTzPipe } from '@shared/pipes/apply-local-tz.pipe'
import { CopyToClipboardDirective } from '@shared/directives/copy-to-clipboard.directive'
import { ConfirmationDialogComponent } from '@shared/components/presentation/reusable/confirmation-dialog/confirmation-dialog.component'
import { ReactiveFormsModule } from '@angular/forms'
import { NgLetDirective } from 'ng-let'
import { TableModule } from 'primeng/table'
import { ButtonModule } from 'primeng/button'
import { MenuModule } from 'primeng/menu'
import { TagModule } from 'primeng/tag'
import { InputNumberModule } from 'primeng/inputnumber'
import { RadioButtonModule } from 'primeng/radiobutton'
import { DropdownModule } from 'primeng/dropdown'

@NgModule({
  declarations: [CredentialsComponent, OptionsComponent, ExpirationComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CredentialsRoutingModule,
    RoleCheckerComponent,
    InfoComponent,
    TableSkeletonComponent,
    LongIdComponent,
    SadFaceComponent,
    ApplyLocalTzPipe,
    CopyToClipboardDirective,
    ConfirmationDialogComponent,
    NgLetDirective,
    TableModule,
    ButtonModule,
    MenuModule,
    TagModule,
    InputNumberModule,
    RadioButtonModule,
    DropdownModule,
    CredentialComponent
  ],
})
export class CredentialsModule {}
