/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { CredentialsApiService } from '@core/api/modules/credentials'
import { BaseService } from '@core/services/base'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { firstValueFrom, take } from 'rxjs'

@Injectable()
export class OptionsService extends BaseService {
  constructor(private _credentialApiService: CredentialsApiService, private _myNodeService: MyNodeService) {
    super()
  }

  async deleteCredential(clientId: string) {
    await this.loadApiWrapper(this._deleteCredential(clientId))
    this._myNodeService.updateCredentials([...this._myNodeService.credentials.filter((element) => element.clientid != clientId)])
  }

  private async _deleteCredential(clientId: string) {
    const sbUrl = this._myNodeService.sbUrl
    await firstValueFrom(this._credentialApiService.deleteCredential(sbUrl, clientId).pipe(take(1)))
  }
}
