/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core'
import { ConfirmationService, MenuItem } from 'primeng/api'
import { OptionsService } from './options.service'
import { Credential } from '@core/models/credentials.model'
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog'
import { CredentialComponent } from '../../../../../shared/components/smart/credential/credential.component'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { UserRole } from '@core/enums/user.enum'
import { UserService } from '@core/services/user/user.service'

@UntilDestroy()
@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrl: './options.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ConfirmationService, OptionsService],
})
export class OptionsComponent implements OnInit {
  @Input() credential!: Credential

  ref: DynamicDialogRef | undefined

  options!: MenuItem[]

  private _authorized = true

  constructor(
    private _confirmationService: ConfirmationService,
    private _service: OptionsService,
    private _userService: UserService,
    private _dialogService: DialogService,
    private _snackBar: SnackBarService
  ) {}

  ngOnInit(): void {
    this._initOptions()
    this._listenForUserChange()
  }

  refreshCredential(event: Event) {
    this.ref = this._dialogService.open(CredentialComponent, {
      header: 'Refresh Credential',
      data: {
        credential: this.credential
      },
    })
  }

  deleteCredential(event: Event) {
    this._confirmationService.confirm({
      target: event.target as EventTarget,
      header: 'Delete Credential?',
      acceptLabel: 'Delete',
      rejectLabel: 'Cancel',
      acceptIcon: 'fa fa-trash-can',
      rejectIcon: 'pi pi-times',
      acceptButtonStyleClass: 'p-button-text p-button-danger',
      rejectButtonStyleClass: 'p-button-text p-button-secondary',
      accept: async () => {
        await this._deleteCredential()
      },
    })
  }

  private async _deleteCredential() {
    await this._service.deleteCredential(this.credential.clientid)
    this._snackBar.showSuccess('Credential deleted')
  }

  private _listenForUserChange() {
    this._userService.user$.pipe(untilDestroyed(this)).subscribe((user) => {
      this._authorized = user?.roles.includes(UserRole.ADMIN) || user?.roles.includes(UserRole.NODE_OPERATOR) || false
      this._initOptions()
    })
  }

  private _initOptions() {
    this.options = [
      {
        label: 'Settings',
        items: [
          {
            label: 'Refresh',
            icon: 'pi pi-refresh',
            disabled: !this._authorized,
            tooltipOptions: {
              appendTo: 'body',
              tooltipPosition: 'right',
              tooltipLabel: 'Insufficient role to refresh credentials',
              disabled: this._authorized,
            },
            command: (e) => {
              const event = e.originalEvent
              if (event) {
                this.refreshCredential(event)
              }
            },
          },
          {
            label: 'Delete',
            icon: 'fa fa-trash-can',
            disabled: !this._authorized,
            tooltipOptions: {
              appendTo: 'body',
              tooltipPosition: 'right',
              tooltipLabel: 'Insufficient role to delete credentials',
              disabled: this._authorized,
            },
            command: (e) => {
              const event = e.originalEvent
              if (event) {
                this.deleteCredential(event)
              }
            },
          },
        ],
      },
    ]
  }

  get loading$() {
    return this._service.loading$
  }
}
