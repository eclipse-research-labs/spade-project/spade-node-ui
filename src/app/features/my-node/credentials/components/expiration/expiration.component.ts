/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core'
import { Credential } from '@core/models/credentials.model'
import { applyLocalTimezone, inflect, removeLocalTimezone } from 'src/app/utils'

@Component({
  selector: 'app-expiration',
  templateUrl: './expiration.component.html',
  styleUrl: './expiration.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExpirationComponent implements OnInit, OnChanges {
  @Input() credential!: Credential

  ttl!: [Date, string]

  ngOnInit(): void {
    this._convertTtl()
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.credential?.currentValue?.ttl != changes.credential?.previousValue?.ttl) {
      this._convertTtl()
    }
  }

  private _convertTtl() {
    const created = new Date(this.credential.created)
    const ttl = this.credential.ttl
    const milliseconds = ttl * 1000
    const expires = new Date(created.getTime() + milliseconds)
    const today = removeLocalTimezone(new Date())
    const hoursDelta = (expires.getTime() - today.getTime()) / 3600000
    const daysDelta = hoursDelta / 24
    const underOneDay = Math.floor(hoursDelta / 24) == 0
    const hoursRound = Math.round(hoursDelta)
    const daysRound = Math.round(daysDelta)
    this.ttl = [
      expires,
      hoursDelta <= 0
        ? 'expired'
        : `expires in ${underOneDay ? inflect(hoursRound, 'less then hour', '1 hour', hoursRound + ' hours') : inflect(daysRound, '<1 day', '1 day', daysRound + ' days')}`,
    ]
  }

  get systemTimezone() {
    return Intl.DateTimeFormat().resolvedOptions().timeZone
  }
}
