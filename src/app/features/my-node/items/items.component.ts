/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Item, ItemWithIdentity } from '@core/models/item.model'
import { MasterService } from '@core/services/master/master.service'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog'
import { AddItemComponent } from './components/add-item/add-item.component'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { Filter } from '@shared/models/filter-items.model'
import { resolveItemType } from 'src/app/utils'
import { ContractAccessType } from '@core/enums/contract.enum'
import { UserService } from '@core/services/user/user.service'
import { UserRole } from '@core/enums/user.enum'

@UntilDestroy()
@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DialogService],
})
export class ItemsComponent implements OnInit {
  defaultPicked?: Item
  filteredItems: Item[] = []
  selectedItems: ItemWithIdentity[] = []

  constructor(
    private _router: Router,
    private _userService: UserService,
    private _myNodeService: MyNodeService,
    private _masterService: MasterService,
    private _dialogService: DialogService,
    private _cd: ChangeDetectorRef
  ) {}

  private _ref: DynamicDialogRef | undefined

  filter: Filter | null = null

  async ngOnInit(): Promise<void> {
    this.myItems$.pipe(untilDestroyed(this)).subscribe(() => {
      this._loadDefautPicked()
      this._applyFilter()
      this.selectedItems = this.selectedItems.filter((element) => this.myItems.some((item) => item.oid == element.oid))
    })
  }

  search = (value: string): Item[] => {
    return this.myItems.filter((element) => {
      const parsedVal = value.toLowerCase()
      return element.oid.toLowerCase().includes(parsedVal) || element.title.toLowerCase().includes(parsedVal)
    })
  }

  onChange(item: Item) {
    this._myNodeService.removeFromNew(item)
    this._router.navigateByUrl(`${this.url}/${item.oid}`)
  }

  onFilterChange(filter: Filter | null) {
    this.filter = filter
    this._applyFilter()
    this._cd.detectChanges()
  }

  onSelectionChange(items: Item[]) {
    this.selectedItems = items.map((element) => ({ ...element, organisation: this._userService.user?.organisation }))
  }

  hasExigentContracts(item: Item) {
    return this._myNodeService.hasExigentContracts(item.oid)
  }

  private _loadDefautPicked() {
    const split = this._router.url.split(`${this.url}/`)
    if (split[1]) {
      const oid = split[1]
      this.defaultPicked = this.myItems.find((element) => element.oid == oid)
    }
  }

  private _applyFilter() {
    let items = [...this.myItems]
    const types = this.filter?.itemTypes
    if (types && types.length > 0) {
      items = items.filter((element) => {
        const type = resolveItemType(element['@type'])
        if (type) {
          return types.some((element) => element.id == type.id)
        }
        return false
      })
    }
    this.filteredItems = items
    this._cd.detectChanges()
  }

  onTryAgain() {
    this._masterService.reloadApp()
  }

  addItem() {
    this._ref = this._dialogService.open(AddItemComponent, {
      styleClass: 'p-dialog-fullscreen',
      showHeader: false,
    })
  }

  isNew(item: Item) {
    return this._myNodeService.isNewItem(item)
  }

  get url() {
    return `/my-node/${this.brokerState}/${this.nodeState}/items`
  }

  get brokerState() {
    return this._myNodeService.brokerState
  }

  get nodeState() {
    return this._myNodeService.nodeState
  }

  get initializing$() {
    return this._myNodeService.initializing$
  }

  get errorInitializing$() {
    return this._myNodeService.errorInitializing$
  }

  get myItems$() {
    return this._myNodeService.myItems$
  }

  get myItems() {
    return this._myNodeService.myItems
  }

  get accessType() {
    return ContractAccessType.GRANT
  }

  get online() {
    return this._myNodeService.online
  }

  get admin() {
    return UserRole.ADMIN
  }

  get nodeOperator() {
    return UserRole.NODE_OPERATOR
  }
}
