/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core'
import { Item } from '@core/models/item.model'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { ConfirmationService, MenuItem } from 'primeng/api'
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog'
import { ChangePrivacyComponent } from '../../../../../shared/components/smart/item/change-privacy/change-privacy.component'
import { ItemOptionsService } from './item-options.service'
import { UserService } from '@core/services/user/user.service'
import { UserRole } from '@core/enums/user.enum'

@UntilDestroy()
@Component({
  selector: 'app-item-options',
  templateUrl: './item-options.component.html',
  styleUrl: './item-options.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ConfirmationService, DialogService, ItemOptionsService],
})
export class ItemOptionsComponent implements OnInit, OnChanges {
  @Input() item!: Item

  options!: MenuItem[]

  private _authorized = true

  showTD = false
  td!: string

  ref: DynamicDialogRef | undefined

  constructor(
    private _service: ItemOptionsService,
    private _userService: UserService,
    private _snackBar: SnackBarService,
    private _confirmationService: ConfirmationService,
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.item?.currentValue?.id != changes.item?.previousValue?.id) {
      this.td = JSON.stringify(this.item ?? '', null, 2)
      this._initOptions()
    }
  }

  ngOnInit(): void {
    this.td = JSON.stringify(this.item ?? '', null, 2)
    this._initOptions()
    this._listenForUserChange()
  }

  changePrivacy(event: Event) {
    this.ref = this._dialogService.open(ChangePrivacyComponent, {
      header: 'Change privacy',
      data: {
        item: this.item,
      },
    })
    this.ref.onClose.pipe(untilDestroyed(this)).subscribe((_) => {
      this._cd.markForCheck()
    })
  }

  deleteItem(event: Event) {
    this._confirmationService.confirm({
      target: event.target as EventTarget,
      header: 'Delete item?',
      acceptLabel: 'Delete',
      rejectLabel: 'Cancel',
      acceptIcon: 'fa fa-trash-can',
      rejectIcon: 'pi pi-times',
      acceptButtonStyleClass: 'p-button-text p-button-danger',
      rejectButtonStyleClass: 'p-button-text p-button-secondary',
      accept: async () => {
        await this._deleteItem()
      },
    })
  }

  async _deleteItem() {
    const oid = this.item?.oid
    if (oid) {
      await this._service.deleteItem(oid)
      this._snackBar.showSuccess('Item deleted')
    }
  }

  private _listenForUserChange() {
    this._userService.user$.pipe(untilDestroyed(this)).subscribe((user) => {
      this._authorized = user?.roles.includes(UserRole.ADMIN) || user?.roles.includes(UserRole.NODE_OPERATOR) || false
      this._initOptions()
    })
  }

  private _initOptions() {
    this.options = [
      {
        label: 'Details',
        items: [
          {
            label: 'Show TD',
            icon: 'fa fa-code',
            command: (e) => {
              const event = e.originalEvent
              if (event) {
                this.showTD = true
              }
            },
          },
        ],
      },
      {
        label: 'Settings',
        items: [
          {
            label: 'Change Privacy',
            icon: 'fa fa-eye',
            disabled: !this._authorized,
            tooltipOptions: {
              appendTo: 'body',
              tooltipPosition: 'left',
              tooltipLabel: "Insufficient role to change item's privacy",
              disabled: this._authorized,
            },
            command: (e) => {
              const event = e.originalEvent
              if (event) {
                this.changePrivacy(event)
              }
            },
          },
          {
            label: 'Delete',
            icon: 'fa fa-trash-can',
            disabled: !this._authorized,
            tooltipOptions: {
              appendTo: 'body',
              tooltipPosition: 'left',
              tooltipLabel: 'Insufficient role to delete the item',
              disabled: this._authorized,
            },
            command: (e) => {
              const event = e.originalEvent
              if (event) {
                this.deleteItem(event)
              }
            },
          },
        ],
      },
    ]
  }

  get loading$() {
    return this._service.loading$
  }
}
