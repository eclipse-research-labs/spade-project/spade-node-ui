/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { ItemsApiService } from '@core/api/modules/items'
import { BaseService } from '@core/services/base'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { firstValueFrom, take } from 'rxjs'

@Injectable()
export class ItemOptionsService extends BaseService {
  constructor(private _myNodeService: MyNodeService, private _itemsApi: ItemsApiService) {
    super()
  }

  public async deleteItem(oid: string) {
    await this.loadApiWrapper(this._deleteItem(oid))
    this._myNodeService.updateMyItems(this._myNodeService.myItems.filter((element) => element.oid != oid))
  }

  private async _deleteItem(oid: string) {
    await firstValueFrom(this._itemsApi.deleteItem(this._myNodeService.sbUrl, oid).pipe(take(1)))
  }
}
