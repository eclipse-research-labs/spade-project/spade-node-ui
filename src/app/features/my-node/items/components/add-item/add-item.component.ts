/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { DynamicDialogRef } from 'primeng/dynamicdialog'
import { AddItemService } from './add-item.service'

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrl: './add-item.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [AddItemService],
})
export class AddItemComponent {
  constructor(private _dialogRef: DynamicDialogRef) {}

  close() {
    this._dialogRef.close()
  }
}
