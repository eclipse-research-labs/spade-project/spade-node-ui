/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { AddItemService } from '../add-item.service'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { DynamicDialogRef } from 'primeng/dynamicdialog'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { FormControl, Validators } from '@angular/forms'

@UntilDestroy()
@Component({
  selector: 'app-from-json',
  templateUrl: './from-json.component.html',
  styleUrl: './from-json.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FromJsonComponent implements OnInit {
  disabledCode: boolean = true

  codeControl = new FormControl('', {
    updateOn: 'change',
    validators: [Validators.required],
  })

  constructor(private _service: AddItemService, private _snackBar: SnackBarService, private _dialogRef: DynamicDialogRef) {}

  ngOnInit(): void {
    this.codeControl.valueChanges.pipe(untilDestroyed(this)).subscribe((value) => {
      const valid = this.codeControl.valid
      if (this.disabledCode && valid) {
        this.disabledCode = false
      } else if (!this.disabledCode && !valid) {
        this.disabledCode = true
      }
    })
  }

  async createItemFromJson() {
    if (this.codeControl.valid) {
      await this._createItem(this.codeControl.value!)
    }
  }

  private async _createItem(td: string) {
    try {
      await this._service.addItem(td)
      this._snackBar.showSuccess('New item created')
      this._dialogRef.close()
    } catch (e) {
      this._snackBar.showError('Item could not be created. See the console for more information.')
    }
  }

  get loading$() {
    return this._service.loading$
  }
}
