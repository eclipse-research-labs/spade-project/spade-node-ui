/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { ItemsApiService } from '@core/api/modules/items'
import { BaseService } from '@core/services/base'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { firstValueFrom, take } from 'rxjs'

@Injectable()
export class AddItemService extends BaseService {
  constructor(private _myNodeService: MyNodeService, private _itemsApi: ItemsApiService) {
    super()
  }

  public async addItem(td: string) {
    const item = await this.loadApiWrapper(this._addItem(td))
    this._myNodeService.addToNew(item)
    this._myNodeService.updateMyItems([...this._myNodeService.myItems, item])
  }

  private async _addItem(td: string) {
    return await firstValueFrom(this._itemsApi.createItem(this._myNodeService.sbUrl, JSON.parse(td)).pipe(take(1)))
  }
}
