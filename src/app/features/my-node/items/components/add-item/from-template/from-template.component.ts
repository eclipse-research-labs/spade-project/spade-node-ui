/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { FromTemplateService } from './from-template.service'
import { Item } from '@core/models/item.model'

@Component({
  selector: 'app-from-template',
  templateUrl: './from-template.component.html',
  styleUrl: './from-template.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [FromTemplateService],
})
export class FromTemplateComponent implements OnInit {

  selectedTemplate?: Item

  constructor(private _service: FromTemplateService) {}

  async ngOnInit(): Promise<void> {
    await this._service.init()
  }

  selectTemplate(template: Item) {
    this.selectedTemplate = template
  }

  get initializing$() {
    return this._service.initializing$
  }

  get templates$() {
    return this._service.templates$
  }
}
