/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { Item, PropertyWithKey } from '@core/models/item.model'
import { inflect } from 'src/app/utils'

@Component({
  selector: 'app-prop-count',
  templateUrl: './prop-count.component.html',
  styleUrl: './prop-count.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PropCountComponent {
  @Input() template!: Item

  show = false

  prop(key: string): PropertyWithKey | undefined {
    const props = this.template.properties
    if (props) return { key, ...props[key] }
    return undefined
  }

  get inflectPropCount() {
    const count = this.properties.length
    return inflect(count, 'No properties', '1 property', `${count} properties`)
  }

  get properties() {
    return Object.keys(this.template.properties ?? {})
  }

  get noProperties() {
    return this.properties.length <= 0
  }
}
