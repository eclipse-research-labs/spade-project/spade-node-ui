/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { GeolocationApiService } from '@core/api/modules/geolocation'
import { Feature } from '@core/models/geolocation.model'
import { ContextClass, Item, ItemType, PropertyWithKey } from '@core/models/item.model'
import { AutoCompleteCompleteEvent } from 'primeng/autocomplete'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'
import { inflect, isString, resolveItemType } from 'src/app/utils'
import { v4 as uuidv4 } from 'uuid'
import { AddItemService } from '../../add-item.service'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { DynamicDialogRef } from 'primeng/dynamicdialog'

@Component({
  selector: 'app-template-detail',
  templateUrl: './template-detail.component.html',
  styleUrl: './template-detail.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TemplateDetailComponent implements OnInit {
  @Input() template!: Item
  @Output() onBack: EventEmitter<Event> = new EventEmitter()

  propNum!: string
  type!: ItemType

  authors = false
  version = false
  location = false
  license = false

  createItemForm!: FormGroup

  private _suggestions$ = new BehaviorSubject<Feature[]>([])

  suggestions$ = this._suggestions$.asObservable()

  constructor(
    private _service: AddItemService,
    private _snackBar: SnackBarService,
    private _dialogRef: DynamicDialogRef,
    private _geolocationApi: GeolocationApiService
  ) {}

  ngOnInit(): void {
    this._initForm()
  }

  private _initForm() {
    const num = Object.keys(this.template.properties ?? {}).length
    this.propNum = num + ' ' + inflect(num, 'properties', 'property', 'properties')
    this.type = resolveItemType(this.template['@type'])
    this.createItemForm = new FormGroup({
      name: new FormControl('', { validators: [Validators.required] }),
      link: new FormControl('', { validators: [Validators.required] }),
      description: new FormControl(this.template.description ?? '', { validators: [Validators.required] }),
      authors: new FormControl('', { validators: [Validators.required] }),
      version: new FormControl('', { validators: [Validators.required] }),
      location: new FormControl<Feature | null>(null, {
        updateOn: 'change',
        validators: [Validators.required],
      }),
      license: new FormControl('', { validators: [Validators.required] }),
      licenseHolder: new FormControl('', { validators: [Validators.required] }),
    })
    if (!this.template.links) {
      this.createItemForm.controls['link']?.disable()
    }
    this.createItemForm.controls['authors']?.disable()
    this.createItemForm.controls['version']?.disable()
    this.createItemForm.controls['location']?.disable()
    this.createItemForm.controls['license']?.disable()
    this.createItemForm.controls['licenseHolder']?.disable()
  }

  addAuthors() {
    this.authors = true
    this.createItemForm.controls['authors']?.enable()
  }

  addVersion() {
    this.version = true
    this.createItemForm.controls['version']?.enable()
  }

  addLocation() {
    this.location = true
    this.createItemForm.controls['location']?.enable()
  }

  addLicense() {
    this.license = true
    this.createItemForm.controls['license']?.enable()
    this.createItemForm.controls['licenseHolder']?.enable()
  }

  removeAuthors() {
    this.authors = false
    this.createItemForm.controls['authors']?.disable()
  }

  removeVersion() {
    this.version = false
    this.createItemForm.controls['version']?.disable()
  }

  removeLocation() {
    this.location = false
    this.createItemForm.controls['location']?.disable()
  }

  removeLicense() {
    this.license = false
    this.createItemForm.controls['license']?.disable()
    this.createItemForm.controls['licenseHolder']?.disable()
  }

  onBackClick(event: Event) {
    this.onBack.emit()
  }

  async search(event: AutoCompleteCompleteEvent) {
    const text = event.query
    const openroute = await firstValueFrom(this._geolocationApi.autocomplete(text).pipe(take(1)))
    this._suggestions$.next(openroute.features)
  }

  selectedFeatureConversionMethod(feature: Feature) {
    return `${feature.properties.name}, ${feature.properties.country}`
  }

  async createItem() {
    if (this.createItemForm.valid) {
      const td = this._generateTd()
      if (td) {
        try {
          await this._createItem(JSON.stringify(td))
        } catch (e) {
          this._snackBar.showError('Failed to create item')
        }
      } else {
        this._snackBar.showError('Invalid TD')
      }
    }
  }

  private _generateTd() {
    const td = JSON.parse(JSON.stringify(this.template)) as Item

    td.title = this.createItemForm.controls['name'].value as string
    td.links = [{ href: this.createItemForm.controls['link'].value as string, rel: 'external' }]
    td.description = this.createItemForm.controls['description'].value as string

    if (this.authors) td['dct:creator'] = this.createItemForm.controls['authors'].value as string
    if (this.version) td['dct:hasVersion'] = this.createItemForm.controls['version'].value as string

    if (this.location) this._generateLocation(td)
    if (this.license) this._generateLicense(td)

    td.adapterId = uuidv4()

    return td
  }

  private _generateLocation(td: Item) {
    const coordinates = (this.createItemForm.controls['location'].value as Feature | null)?.geometry.coordinates
    if (!coordinates) return

    const lat = coordinates.at(1)
    const long = coordinates.at(0)
    if (!lat || !long) return

    const importsIndex = this._initImport(td)
    if (!importsIndex) return
    ;(td['@context'][importsIndex] as ContextClass)['geo'] = 'http://www.w3.org/2003/01/geo/wgs84_pos#'

    td['geo:location'] = {
      'geo:lat': lat.toString(),
      'geo:long': long.toString(),
    }
  }

  private _generateLicense(td: Item) {
    td['dct:license'] = this.createItemForm.controls['license'].value as string
    td['dct:licenseHolder'] = this.createItemForm.controls['licenseHolder'].value as string
  }

  private _initImport(td: Item): number | undefined {
    const importsIndex = td['@context'].findIndex((element) => !isString(element))
    if (importsIndex < 0) {
      td['@context'].push({} as ContextClass)
      return td['@context'].length - 1
    }
    return importsIndex < 0 ? undefined : importsIndex
  }

  private async _createItem(td: string) {
    try {
      await this._service.addItem(td)
      this._snackBar.showSuccess('New item created')
      this._dialogRef.close()
    } catch (e) {
      this._snackBar.showError('Item could not be created. See the console for more information.')
    }
  }

  get loading$() {
    return this._service.loading$
  }

  get properties(): PropertyWithKey[] {
    const props = this.template.properties
    if (!props) return []
    return Object.keys(props).map((key) => {
      return { key, ...props[key] }
    })
  }
}
