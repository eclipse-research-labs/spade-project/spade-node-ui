/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { Item, PropertyWithKey } from '@core/models/item.model'

@Component({
  selector: 'app-prop',
  templateUrl: './prop.component.html',
  styleUrl: './prop.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PropComponent {
  @Input() item!: Item
  @Input() prop!: PropertyWithKey
}
