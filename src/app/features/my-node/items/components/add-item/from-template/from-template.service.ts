/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { TdCatalogApiService } from '@core/api/modules/td-catalog'
import { Item } from '@core/models/item.model'
import { S3Bucket } from '@core/models/s3-bucket.model'
import { BaseService } from '@core/services/base'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'

@Injectable()
export class FromTemplateService extends BaseService {
  private _templates = new BehaviorSubject<Item[] | null>(null)

  templates$ = this._templates.asObservable()

  constructor(private _tdCatalogApi: TdCatalogApiService) {
    super()
  }

  async init() {
    await this.initApiWrapper(this._init())
  }

  private async _init() {
    const bucket = await this._getBucket()
    const asyncs = []
    const files: Item[] = []
    if (bucket) {
      for (let file of (bucket as S3Bucket).ListBucketResult?.Contents ?? []) {
        const name = file.Key.at(0)
        if (name) asyncs.push(name)
      }
    }
    await Promise.all(
      asyncs.map((element) =>
        this._getFile(element).then((td) => {
          files.push(td)
        })
      )
    )
    this._templates.next(
      files.sort((n1, n2) => {
        if (n1.title > n2.title) {
          return 1
        }

        if (n1.title < n2.title) {
          return -1
        }

        return 0
      })
    )
  }

  private async _getBucket() {
    return await firstValueFrom(this._tdCatalogApi.listObjects().pipe(take(1)))
  }

  private async _getFile(name: string) {
    return await firstValueFrom(this._tdCatalogApi.getObject(name).pipe(take(1)))
  }
}
