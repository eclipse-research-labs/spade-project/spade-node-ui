/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { Item } from '@core/models/item.model'
import { MenuItem } from 'primeng/api'

@Component({
  selector: 'app-template-options',
  templateUrl: './template-options.component.html',
  styleUrl: './template-options.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TemplateOptionsComponent {
  @Input() template!: Item

  options!: MenuItem[]

  showTD = false
  td!: string

  ngOnInit(): void {
    this.td = JSON.stringify(this.template ?? '', null, 2)
    this._initOptions()
  }

  private _initOptions() {
    this.options = [
      {
        label: 'Details',
        items: [
          {
            label: 'Show Template',
            icon: 'fa fa-code',
            command: (e) => {
              const event = e.originalEvent
              if (event) {
                this.showTD = true
              }
            },
          },
        ],
      },
    ]
  }
}
