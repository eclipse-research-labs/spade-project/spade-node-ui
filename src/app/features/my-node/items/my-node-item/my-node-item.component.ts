/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Item, ItemType } from '@core/models/item.model'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { resolveItemType, isString } from 'src/app/utils'
import { UserService } from '@core/services/user/user.service'

@UntilDestroy()
@Component({
  selector: 'app-my-node-item',
  templateUrl: './my-node-item.component.html',
  styleUrl: './my-node-item.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MyNodeItemComponent implements OnInit {
  item?: Item | undefined | null
  type!: ItemType

  hasExigent: boolean = false

  constructor(
    private _myNodeService: MyNodeService,
    private _userService: UserService,
    private _route: ActivatedRoute,
    private _cd: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this._route.paramMap.pipe(untilDestroyed(this)).subscribe((params) => {
      const oid = params.get('oid')
      if (oid) {
        this._loadItem(oid)
      }
    })
    this._myNodeService.myItems$.pipe(untilDestroyed(this)).subscribe((element) => {
      if (this.item) {
        this._loadItem(this.item.oid)
      }
    })
  }

  openTypeUrl() {
    if (this.type.id != 'unknown') {
      window.open(`${this.primaryContextImportUrl}/${this.type.title}`, '_blank')
    }
  }

  private _loadItem(oid: string) {
    const item = this.myItems.find((element) => element.oid === oid)
    // reassigning item to trigger OnChange method of ChangePrivacy component
    this.item = item != undefined ? { ...item } : undefined
    this.type = resolveItemType(this.item?.['@type'])
    this.hasExigent = this._myNodeService.hasExigentContracts(oid)
    this._cd.detectChanges()
  }

  get primaryContextImportUrl(): string | undefined {
    return this.item != undefined && this.item != null
      ? isString(this.item['@context'])
        ? (this.item['@context'] as string)
        : (this.item['@context'].find((element) => isString(element)) as string | undefined)
      : undefined
  }

  get myItems$() {
    return this._myNodeService.myItems$
  }

  get myItems() {
    return this._myNodeService.myItems
  }

  get online() {
    return this._myNodeService.online
  }

  get organisation() {
    return this._userService.user?.organisation
  }

  get nodeState() {
    return this._myNodeService.nodeState
  }
}
