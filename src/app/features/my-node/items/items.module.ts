/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { ItemsRoutingModule } from './items-routing.module'
import { ItemsComponent } from './items.component'
import { MyNodeItemComponent } from './my-node-item/my-node-item.component'
import { IteractionsListComponent } from './components/interactions-list/interactions-list.component'
import { DetailModule } from '@features/detail/detail.module'
import { AddItemComponent } from './components/add-item/add-item.component'
import { ItemOptionsComponent } from './components/item-options/item-options.component'
import { FromFileComponent } from './components/add-item/from-file/from-file.component'
import { FromJsonComponent } from './components/add-item/from-json/from-json.component'
import { FromTemplateComponent } from './components/add-item/from-template/from-template.component'
import { PropCountComponent } from './components/add-item/from-template/prop-count/prop-count.component'
import { TemplateOptionsComponent } from './components/add-item/from-template/template-options/template-options.component'
import { PropComponent } from './components/add-item/from-template/prop/prop.component'
import { TemplateDetailComponent } from './components/add-item/from-template/template-detail/template-detail.component'
import { TypeIconComponent } from '@shared/components/presentation/app-specific/type-icon/type-icon.component'
import { LongIdComponent } from '@shared/components/presentation/reusable/long-id/long-id.component'
import { RoleCheckerComponent } from '@shared/components/smart/role-checker/role-checker.component'
import { SelectionComponent } from '@shared/components/presentation/reusable/selection/selection.component'
import { ItemPreviewComponent } from '@shared/components/presentation/app-specific/item/item-preview/item-preview.component'
import { InfoComponent } from '@shared/components/presentation/reusable/info/info.component'
import { FilterItemsComponent } from '@shared/components/smart/filter-items/filter-items.component'
import { SadFaceComponent } from '@shared/components/presentation/reusable/sad-face/sad-face.component'
import { PropPreviewComponent } from '@shared/components/presentation/app-specific/prop/prop-preview/prop-preview.component'
import { EventPreviewComponent } from '@shared/components/presentation/app-specific/event/event-preview/event-preview.component'
import { ConfirmationDialogComponent } from '@shared/components/presentation/reusable/confirmation-dialog/confirmation-dialog.component'
import { CodeEditorComponent } from '@shared/components/presentation/reusable/inputs/code-editor/code-editor.component'
import { ActionButtonComponent } from '@shared/components/presentation/reusable/action-button/action-button.component'
import { PropInfoComponent } from '@shared/components/presentation/app-specific/prop/prop-info/prop-info.component'
import { TableSkeletonComponent } from '@shared/components/presentation/reusable/loaders/table-skeleton/table-skeleton.component'
import { TdUploadComponent } from '@shared/components/presentation/reusable/inputs/td-upload/td-upload.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgLetDirective } from 'ng-let'
import { ButtonModule } from 'primeng/button'
import { MessageModule } from 'primeng/message'
import { DataViewModule } from 'primeng/dataview'
import { SidebarModule } from 'primeng/sidebar'
import { MenuModule } from 'primeng/menu'
import { AutoCompleteModule } from 'primeng/autocomplete'
import { OverlayPanelModule } from 'primeng/overlaypanel'
import { TabViewModule } from 'primeng/tabview'
import { InputTextModule } from 'primeng/inputtext'
import { InputTextareaModule } from 'primeng/inputtextarea'
import { MenuComponent } from '@shared/components/presentation/reusable/menu/menu/menu.component'
import { MenuLabelComponent } from '@shared/components/presentation/reusable/menu/menu-label/menu-label.component'
import { MenuContentComponent } from '@shared/components/presentation/reusable/menu/menu-content/menu-content.component'
import { MenuItemComponent } from '@shared/components/presentation/reusable/menu/menu-item/menu-item.component'
import { ChangePrivacyComponent } from '@shared/components/smart/item/change-privacy/change-privacy.component'
import { OpenItemContractsComponent } from '@shared/components/smart/item/open-item-contracts/open-item-contracts.component'
import { ItemBulkActionsComponent } from '@shared/components/smart/item/item-bulk-actions/item-bulk-actions.component'

@NgModule({
  declarations: [
    ItemsComponent,
    MyNodeItemComponent,
    IteractionsListComponent,
    AddItemComponent,
    ItemOptionsComponent,
    FromFileComponent,
    FromJsonComponent,
    FromTemplateComponent,
    PropCountComponent,
    TemplateOptionsComponent,
    PropComponent,
    TemplateDetailComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ItemsRoutingModule,
    DetailModule,
    TypeIconComponent,
    LongIdComponent,
    RoleCheckerComponent,
    SelectionComponent,
    ItemPreviewComponent,
    InfoComponent,
    FilterItemsComponent,
    SadFaceComponent,
    PropPreviewComponent,
    EventPreviewComponent,
    ConfirmationDialogComponent,
    CodeEditorComponent,
    ActionButtonComponent,
    PropInfoComponent,
    TableSkeletonComponent,
    TdUploadComponent,
    NgLetDirective,
    ButtonModule,
    MessageModule,
    DataViewModule,
    SidebarModule,
    MenuModule,
    AutoCompleteModule,
    OverlayPanelModule,
    TabViewModule,
    InputTextModule,
    InputTextareaModule,
    MenuComponent,
    MenuItemComponent,
    MenuLabelComponent,
    MenuContentComponent,
    ChangePrivacyComponent,
    OpenItemContractsComponent,
    ItemBulkActionsComponent,
  ],
})
export class ItemsModule {}
