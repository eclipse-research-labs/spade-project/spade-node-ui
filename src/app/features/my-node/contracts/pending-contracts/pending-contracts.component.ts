/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { ContractsService } from '../contracts.service'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { inflect } from 'src/app/utils'
import { MenuItem } from 'primeng/api'
import { ContractIdentity } from '@core/models/contract.model'
import { MyNodeService } from '@core/services/my-node/my-node.service'

@Component({
  selector: 'app-pending-contracts',
  templateUrl: './pending-contracts.component.html',
  styleUrl: './pending-contracts.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PendingContractsComponent {
  selectedContracts: ContractIdentity[] = []

  filterOptions: MenuItem[] = [
    {
      label: 'PENDING YOUR SIGNATURE',
      tooltipOptions: {
        appendTo: 'body',
      },
      command: (e: any) => {
        const event = e.originalEvent
        if (event) {
          this.filterExigent(true)
        }
      },
    },
    {
      label: 'PENDING',
      tooltipOptions: {
        appendTo: 'body',
      },
      command: (e: any) => {
        const event = e.originalEvent
        if (event) {
          this.filterExigent(false)
        }
      },
    },
  ]

  constructor(
    private _service: ContractsService,
    private _snackBarService: SnackBarService,
    private _myNodeService: MyNodeService
  ) {}

  onSelectionChange(contractIdentities: ContractIdentity[]) {
    this.selectedContracts = contractIdentities
  }

  onDownloadChunkOfIdentities(contractIdentities: ContractIdentity[]) {
    this._service.downloadContractIdentities(contractIdentities)
  }

  async acceptSelection() {
    await this._service.acceptContracts(this.selectedContracts)
    this._snackBarService.showSuccess('Contracts accepted')
    this.selectedContracts = []
  }

  async revokeSelection() {
    await this._service.revokeContracts(this.selectedContracts)
    this._snackBarService.showSuccess('Contracts revoked')
    this.selectedContracts = []
  }

  filterExigent(exigent: boolean) {
    this._service.setExigentOnly(exigent)
    this.selectedContracts = []
  }

  clearFilter() {
    this._service.setExigentOnly(null)
    this.selectedContracts = []
  }

  get exigentOnly$() {
    return this._service.exigentOnly$
  }

  get labelAccept() {
    const len = this.selectedContracts.length
    return `Accept ${len > 100 ? '100+' : len} ${inflect(len, 'contracts', 'contract', 'contracts')}`
  }

  get labelReject() {
    const len = this.selectedContracts.length
    return `${this._exigentOnly === null ? 'Revoke / Reject' : this._exigentOnly === true ? 'Reject' : 'Revoke'} ${
      len > 100 ? '100+' : len
    } ${inflect(len, 'contracts', 'contract', 'contracts')}`
  }

  get loading$() {
    return this._service.loading$
  }

  get contractIdentities$() {
    return this._service.pendingContractIdentities$
  }

  private get _exigentOnly() {
    return this._service.exigentOnly
  }
}
