/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { ContractsRoutingModule } from './contracts-routing.module'
import { ContractsComponent } from './contracts.component'
import { ActiveContractsComponent } from './active-contracts/active-contracts.component'
import { PendingContractsComponent } from './pending-contracts/pending-contracts.component'
import { RevokedContractsComponent } from './revoked-contracts/revoked-contracts.component'
import { TableSkeletonComponent } from '@shared/components/presentation/reusable/loaders/table-skeleton/table-skeleton.component'
import { NgLetDirective } from 'ng-let'
import { ButtonModule } from 'primeng/button'
import { TabMenuModule } from 'primeng/tabmenu'
import { MenuModule } from 'primeng/menu'
import { SidebarModule } from 'primeng/sidebar'
import { CodeEditorComponent } from '@shared/components/presentation/reusable/inputs/code-editor/code-editor.component'
import { FormsModule } from '@angular/forms'
import { SpinnerComponent } from '@shared/components/presentation/reusable/loaders/spinner/spinner.component'
import { ContractsListComponent } from '@shared/components/smart/contracts/contracts-list/contracts-list.component'
import { ActiveContractActionsComponent } from '@shared/components/smart/contracts/contract-actions/active-contract-actions/active-contract-actions.component'
import { PendingContractActionsComponent } from '@shared/components/smart/contracts/contract-actions/pending-contract-actions/pending-contract-actions.component'
import { RevokedContractActionsComponent } from '@shared/components/smart/contracts/contract-actions/revoked-contract-actions/revoked-contract-actions.component'
import { TagModule } from 'primeng/tag'

@NgModule({
  declarations: [ContractsComponent, ActiveContractsComponent, PendingContractsComponent, RevokedContractsComponent],
  imports: [
    CommonModule,
    ContractsRoutingModule,
    TableSkeletonComponent,
    NgLetDirective,
    ButtonModule,
    TabMenuModule,
    MenuModule,
    SidebarModule,
    CodeEditorComponent,
    FormsModule,
    SpinnerComponent,
    ContractsListComponent,
    ActiveContractActionsComponent,
    PendingContractActionsComponent,
    RevokedContractActionsComponent,
    TagModule,
  ],
})
export class ContractsModule {}
