/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { ContractsService } from '../contracts.service'
import { MenuItem } from 'primeng/api'
import { inflect } from 'src/app/utils'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { ContractIdentity } from '@core/models/contract.model'

@Component({
  selector: 'app-active-contracts',
  templateUrl: './active-contracts.component.html',
  styleUrl: './active-contracts.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActiveContractsComponent {
  filterOptions: MenuItem[] = [
    {
      label: 'Filter',
      items: [
        {
          label: 'Show TD',
          icon: 'fa fa-code',
          command: (e) => {
            const event = e.originalEvent
            if (event) {
            }
          },
        },
      ],
    },
  ]

  selectedContracts: ContractIdentity[] = []

  constructor(private _service: ContractsService, private _snackBarService: SnackBarService) {}

  onSelectionChange(contractIdentities: ContractIdentity[]) {
    this.selectedContracts = contractIdentities
  }

  onDownloadChunkOfIdentities(contractIdentities: ContractIdentity[]) {
    this._service.downloadContractIdentities(contractIdentities)
  }

  async revokeSelection() {
    await this._service.revokeContracts(this.selectedContracts)
    this._snackBarService.showSuccess('Contracts revoked')
    this.selectedContracts = []
  }

  get loading$() {
    return this._service.loading$
  }

  get contractIdentities$() {
    return this._service.activeContractIdentities$
  }

  get label() {
    const len = this.selectedContracts.length
    return `Revoke ${len > 100 ? '100+' : len} ${inflect(len, 'contracts', 'contract', 'contracts')}`
  }
}
