/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContractsComponent } from './contracts.component';
import { ActiveContractsComponent } from './active-contracts/active-contracts.component';
import { PendingContractsComponent } from './pending-contracts/pending-contracts.component';
import { RevokedContractsComponent } from './revoked-contracts/revoked-contracts.component';

const routes: Routes = [
  {
    path: '',
    component: ContractsComponent,
    children: [
      {
        path: '',
        redirectTo: 'active',
        pathMatch: 'full',
      },
      {
        path: 'active',
        component: ActiveContractsComponent,
      },
      {
        path: 'pending',
        component: PendingContractsComponent,
      },
      {
        path: 'revoked',
        component: RevokedContractsComponent,
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContractsRoutingModule { }
