/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { MenuItem } from 'primeng/api'
import { ContractsService } from './contracts.service'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { Observable } from 'rxjs'
import { ContractIdentity } from '@core/models/contract.model'

interface _MenuItem extends MenuItem {
  indicate?: boolean
  contractIdentities$: Observable<ContractIdentity[]>
}

@UntilDestroy()
@Component({
  selector: 'app-contracts',
  templateUrl: './contracts.component.html',
  styleUrl: './contracts.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ContractsService],
})
export class ContractsComponent implements OnInit {
  items!: _MenuItem[]

  constructor(private _service: ContractsService, private _myNodeService: MyNodeService) {}

  ngOnInit(): void {
    this._initItems()
    this._service.init()
    this._listenForExigentContractsChange()
  }

  private _listenForExigentContractsChange() {
    this._myNodeService.exigentContracts$.pipe(untilDestroyed(this)).subscribe((_) => {
      this._initItems()
    })
  }

  private _initItems() {
    this.items = [
      {
        label: 'Active',
        icon: 'fa fa-file-signature',
        routerLink: `${this.url}/active`,
        contractIdentities$: this._service.activeContractIdentities$
      },
      {
        label: 'Pending',
        icon: 'fa fa-file-signature',
        routerLink: `${this.url}/pending`,
        indicate: this._myNodeService.hasExigentContracts(),
        contractIdentities$: this._service.pendingContractIdentities$
      },
      {
        label: 'Revoked',
        icon: 'fa fa-file-signature',
        routerLink: `${this.url}/revoked`,
        contractIdentities$: this._service.revokedContractIdentities$
      },
    ]
  }

  get initializing$() {
    return this._myNodeService.initializing$
  }

  get url() {
    return `/my-node/${this.brokerState}/${this.nodeState}/contracts`
  }

  get brokerState() {
    return this._myNodeService.brokerState
  }

  get nodeState() {
    return this._myNodeService.nodeState
  }
}
