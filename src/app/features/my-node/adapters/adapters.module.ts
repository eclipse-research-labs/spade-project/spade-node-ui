/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { AdaptersRoutingModule } from './adapters-routing.module'
import { AdaptersComponent } from './adapters.component'
import { ConnectionsTableComponent } from './components/connections-table/connections-table.component'
import { AdapterComponent } from './adapter/adapter.component'
import { AdapterDescriptionComponent } from '@shared/components/presentation/app-specific/adapter/adapter-description/adapter-description.component'
import { TypeIconComponent } from '@shared/components/presentation/app-specific/type-icon/type-icon.component'
import { LongIdComponent } from '@shared/components/presentation/reusable/long-id/long-id.component'
import { SpinnerComponent } from '@shared/components/presentation/reusable/loaders/spinner/spinner.component'
import { SadFaceComponent } from '@shared/components/presentation/reusable/sad-face/sad-face.component'
import { SelectionComponent } from '@shared/components/presentation/reusable/selection/selection.component'
import { AdapterPreviewComponent } from '@shared/components/presentation/app-specific/adapter/adapter-preview/adapter-preview.component'
import { NgLetDirective } from 'ng-let'
import { TableModule } from 'primeng/table'
import { ButtonModule } from 'primeng/button'
import { MessageModule } from 'primeng/message'
import { TabViewModule } from 'primeng/tabview'

@NgModule({
  declarations: [AdaptersComponent, ConnectionsTableComponent, AdapterComponent],
  imports: [
    CommonModule,
    AdaptersRoutingModule,
    AdapterDescriptionComponent,
    TypeIconComponent,
    LongIdComponent,
    SpinnerComponent,
    SadFaceComponent,
    SelectionComponent,
    SadFaceComponent,
    AdapterPreviewComponent,
    NgLetDirective,
    TableModule,
    ButtonModule,
    MessageModule,
    TabViewModule,
  ],
  exports: [],
})
export class AdaptersModule {}
