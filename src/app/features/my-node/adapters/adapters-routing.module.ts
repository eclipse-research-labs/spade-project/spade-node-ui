/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AdaptersComponent } from './adapters.component'
import { AdapterComponent } from './adapter/adapter.component'

const routes: Routes = [
  {
    path: '',
    component: AdaptersComponent,
    children: [
      {
        path: ':id',
        component: AdapterComponent,
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdaptersRoutingModule {}
