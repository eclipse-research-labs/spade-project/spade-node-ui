/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { Router } from '@angular/router'
import { Adapter } from '@core/models/adapter.model'
import { MasterService } from '@core/services/master/master.service'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { inflect } from 'src/app/utils'

@Component({
  selector: 'app-adapters',
  templateUrl: './adapters.component.html',
  styleUrls: ['./adapters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdaptersComponent {
  constructor(private _router: Router, private _masterService: MasterService, private _myNodeService: MyNodeService) {}

  defaultPicked?: Adapter

  async ngOnInit(): Promise<void> {
    const split = this._router.url.split(`${this.url}/`)
    if (split[1]) {
      const split2 = split[1].split('/')
      if (split2[0]) {
        const id = split2[0]
        this.defaultPicked = this.adapters.find((element) => element.adid === id)
      }
    }
  }

  search = (value: string): Adapter[] => {
    return this.adapters.filter((element) => {
      const parsedVal = value.toLowerCase()
      return element.adid.toLowerCase().includes(parsedVal) || element.name.toLowerCase().includes(parsedVal)
    })
  }

  onChange(adapter: Adapter) {
    this._router.navigateByUrl(`${this.url}/${adapter.adid}`)
  }

  onTryAgain() {
    this._masterService.reloadApp()
  }

  inflectCount(n: number) {
    return inflect(n, '0 adapters', '1 adapter', `${n} adapters`)
  }

  get url() {
    return `/my-node/${this.brokerState}/${this.nodeState}/adapters`
  }

  get brokerState() {
    return this._myNodeService.brokerState
  }

  get nodeState() {
    return this._myNodeService.nodeState
  }

  get adapters() {
    return this._myNodeService.adapters
  }

  get adapters$() {
    return this._myNodeService.adapters$
  }

  get initializing$() {
    return this._myNodeService.initializing$
  }

  get errorInitializing$(){
    return this._myNodeService.errorInitializing$
  }

  get errorInitializing(){
    return this._myNodeService.errorInitializing
  }
}
