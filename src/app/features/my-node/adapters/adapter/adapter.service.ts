/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { AdaptersApiService } from '@core/api/modules/adapters'
import { Adapter, AdapterConnection } from '@core/models/adapter.model'
import { BaseService } from '@core/services/base'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'

@Injectable()
export class AdapterService extends BaseService {
  private _adapter = new BehaviorSubject<Adapter | null>(null)
  private _adapterConnections = new BehaviorSubject<AdapterConnection[]>([])

  adapter$ = this._adapter.asObservable()
  adapterConnections$ = this._adapterConnections.asObservable()

  constructor(private _myNodeService: MyNodeService, private _adaptersApi: AdaptersApiService) {
    super()
  }

  async initAdapter(adid: string) {
    const adapter = this._myNodeService.adapters.find((element) => element.adid === adid)
    if (!adapter) {
      return
    }
    this._adapter.next(adapter)
    const connections = await this.initApiWrapper(this._resolveAdapterConnections(adid))
    this._adapterConnections.next(connections)
  }

  private async _resolveAdapterConnections(adid: string): Promise<AdapterConnection[]> {
    return await firstValueFrom(this._adaptersApi.resolveAdapterConnections(this.node!.host, adid).pipe(take(1)))
  }

  get node() {
    return this._myNodeService.node
  }

  get adapter() {
    return this._adapter.value
  }

  get adapterConnections() {
    return this._adapterConnections.value
  }
}
