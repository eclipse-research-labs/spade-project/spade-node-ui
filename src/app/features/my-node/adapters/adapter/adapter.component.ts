/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewEncapsulation } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Adapter, AdapterConnection } from '@core/models/adapter.model'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { Observable } from 'rxjs'
import { AdapterService } from './adapter.service'

@UntilDestroy()
@Component({
  selector: 'app-adapter',
  templateUrl: './adapter.component.html',
  styleUrl: './adapter.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [AdapterService],
})
export class AdapterComponent implements OnInit {
  constructor(private _service: AdapterService, private _route: ActivatedRoute) {}

  ngOnInit(): void {
    this._route.paramMap.pipe(untilDestroyed(this)).subscribe(async (params) => {
      const id = params.get('id')
      if (id) {
        await this._service.initAdapter(id)
      }
    })
  }

  get adapter$() {
    return this._service.adapter$
  }

  get initializing$() {
    return this._service.initializing$
  }

  get adapterConnections$() {
    return this._service.adapterConnections$
  }
}
