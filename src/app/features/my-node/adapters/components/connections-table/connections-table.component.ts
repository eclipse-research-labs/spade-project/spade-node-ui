/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input, SimpleChanges } from '@angular/core'
import { Router } from '@angular/router'
import { AdapterConnection } from '@core/models/adapter.model'
import { inflect } from 'src/app/utils'

@Component({
  selector: 'app-connections-table',
  templateUrl: './connections-table.component.html',
  styleUrl: './connections-table.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConnectionsTableComponent {
  @Input() adapterConnections!: AdapterConnection[]

  connectionsCount: string = '0 connections'

  constructor(private _router: Router) {}

  ngOnInit(): void {
    this._calculateCount()
  }

  ngOnChanges(changes: SimpleChanges): void {
    this._calculateCount()
  }

  // goToConnections(prop: PropertyWithKey) {
  //   this._router.navigateByUrl(`/detail/items/${this.url}/${this.oid}/${prop.key}`)
  // }

  private _calculateCount() {
    const count = this.adapterConnections.length
    this.connectionsCount = inflect(count, '0 connections', '1 connection', `${count} connections`)
  }
}
