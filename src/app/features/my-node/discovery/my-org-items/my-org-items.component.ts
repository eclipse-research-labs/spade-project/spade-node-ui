/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { Router } from '@angular/router';
import { Item } from '@core/models/item.model';
import { DiscoveryService } from '@core/services/discovery/discovery.service';
import { MyNodeService } from '@core/services/my-node/my-node.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Filter } from '@shared/models/filter-items.model';
import { combineLatest, map } from 'rxjs';
import { resolveItemType } from 'src/app/utils';

@UntilDestroy()
@Component({
  selector: 'app-my-org-items',
  templateUrl: './my-org-items.component.html',
  styleUrl: './my-org-items.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyOrgItemsComponent {
  defaultPicked?: Item

  filteredItems: Item[] = []

  filter: Filter | null = null

  constructor(
    private _router: Router,
    private _discoveryService: DiscoveryService,
    private _myNodeService: MyNodeService,
    private _cd: ChangeDetectorRef,
  ) {}

  async ngOnInit(): Promise<void> {
    this.myOrgItems$.pipe(untilDestroyed(this)).subscribe(() => {
      this._loadDefautPicked()
      this._applyFilter()
    })
  }

  search = (value: string): Item[] => {
    return this.myOrgItems.filter((element) => {
      const parsedVal = value.toLowerCase()
      return element.oid.toLowerCase().includes(parsedVal) || element.title.toLowerCase().includes(parsedVal)
    })
  }

  onChange(item: Item) {
    this._router.navigateByUrl(`${this.url}/${item.oid}`)
  }

  onFilterChange(filter: Filter | null) {
    this.filter = filter
    this._applyFilter()
    this._cd.detectChanges()
  }

  private _loadDefautPicked() {
    const split = this._router.url.split(`${this.url}/`)
    if (split[1]) {
      const oid = split[1]
      this.defaultPicked = this.myOrgItems.find((element) => element.oid == oid)
    }
  }

  private _applyFilter() {
    let items = [...this.myOrgItems]
    const types = this.filter?.itemTypes
    const organisations = this.filter?.organisations
    if ((types && types.length > 0) || (organisations && organisations.length > 0)) {
      items = items.filter((element) => {
        const type = resolveItemType(element['@type'])
        const cid = element['SPADE:organisation']?.['@id'].split(':').at(-1)
        if (type && types && types.some((element) => element.id == type.id)) {
          return true
        }
        if (cid && organisations && organisations.some((element) => element.cid == cid)) {
          return true
        }
        return false
      })
    }
    this.filteredItems = items
  }

  onTryAgain() {
    this._discoveryService.init(true)
  }

  get url() {
    return `/my-node/${this.brokerState}/${this.nodeState}/discovery/my-org`
  }

  get brokerState() {
    return this._myNodeService.brokerState
  }

  get nodeState() {
    return this._myNodeService.nodeState
  }

  get initializing$() {
    return combineLatest([this._myNodeService.initializing$, this._discoveryService.initializing$]).pipe(map(([x0, x1]) => x0 || x1))
  }

  get errorInitializing$() {
    return combineLatest([this._myNodeService.errorInitializing$, this._discoveryService.errorInitializing$]).pipe(map(([x0, x1]) => x0 || x1))
  }

  get myOrgItems$() {
    return this._discoveryService.myOrgItems$
  }

  get myOrgItems() {
    return this._discoveryService.myOrgItems
  }
}
