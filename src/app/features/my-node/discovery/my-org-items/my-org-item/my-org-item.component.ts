/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ItemWithIdentity } from '@core/models/item.model';
import { DiscoveryService } from '@core/services/discovery/discovery.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-my-org-item',
  templateUrl: './my-org-item.component.html',
  styleUrl: './my-org-item.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyOrgItemComponent {
  item?: ItemWithIdentity | undefined | null
  
  constructor(private _discoveryService: DiscoveryService, private _route: ActivatedRoute, private _cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    this._route.paramMap.pipe(untilDestroyed(this)).subscribe((params) => {
      const oid = params.get('oid')
      if (oid) {
        this._loadItem(oid)
      }
    })
  }

  private _loadItem(oid: string) {
    this.item = this.myOrgItems.find((element) => element.oid === oid)
    this._cd.markForCheck()
  }
  get myOrgItems$() {
    return this._discoveryService.myOrgItems$
  }

  get myOrgItems() {
    return this._discoveryService.myOrgItems
  }
}
