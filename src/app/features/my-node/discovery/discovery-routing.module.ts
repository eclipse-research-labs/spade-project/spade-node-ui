/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { DiscoveryComponent } from './discovery.component'
import { PartnerItemsComponent } from './partner-items/partner-items.component'
import { MyOrgItemsComponent } from './my-org-items/my-org-items.component'
import { PartnerItemComponent } from './partner-items/partner-item/partner-item.component'
import { MyOrgItemComponent } from './my-org-items/my-org-item/my-org-item.component'

const routes: Routes = [
  {
    path: '',
    component: DiscoveryComponent,
    children: [
      {
        path: '',
        redirectTo: 'partners',
        pathMatch: 'full',
      },
      {
        path: 'partners',
        component: PartnerItemsComponent,
        children: [
          {
            path: ':oid',
            component: PartnerItemComponent,
          },
        ],
      },
      {
        path: 'my-org',
        component: MyOrgItemsComponent,
        children: [
          {
            path: ':oid',
            component: MyOrgItemComponent,
          },
        ],
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiscoveryRoutingModule {}
