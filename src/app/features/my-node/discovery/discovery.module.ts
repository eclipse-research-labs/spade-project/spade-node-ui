/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { DiscoveryRoutingModule } from './discovery-routing.module'
import { DiscoveryComponent } from './discovery.component'
import { ForeignItemComponent } from './components/foreign-item/foreign-item.component'
import { InteractionsListComponent } from './components/interactions-list/interactions-list.component'
import { ItemOptionsComponent } from './components/item-options/item-options.component'
import { PartnerItemsComponent } from './partner-items/partner-items.component'
import { MyOrgItemsComponent } from './my-org-items/my-org-items.component'
import { MyOrgItemComponent } from './my-org-items/my-org-item/my-org-item.component'
import { PartnerItemComponent } from './partner-items/partner-item/partner-item.component'
import { RoleCheckerComponent } from '@shared/components/smart/role-checker/role-checker.component'
import { ItemPreviewComponent } from '@shared/components/presentation/app-specific/item/item-preview/item-preview.component'
import { SelectionComponent } from '@shared/components/presentation/reusable/selection/selection.component'
import { FilterItemsComponent } from '@shared/components/smart/filter-items/filter-items.component'
import { SadFaceComponent } from '@shared/components/presentation/reusable/sad-face/sad-face.component'
import { PropPreviewComponent } from '@shared/components/presentation/app-specific/prop/prop-preview/prop-preview.component'
import { CodeEditorComponent } from '@shared/components/presentation/reusable/inputs/code-editor/code-editor.component'
import { TypeIconComponent } from '@shared/components/presentation/app-specific/type-icon/type-icon.component'
import { LongIdComponent } from '@shared/components/presentation/reusable/long-id/long-id.component'
import { FormsModule } from '@angular/forms'
import { NgLetDirective } from 'ng-let'
import { ButtonModule } from 'primeng/button'
import { MessageModule } from 'primeng/message'
import { TabMenuModule } from 'primeng/tabmenu'
import { DataViewModule } from 'primeng/dataview'
import { SidebarModule } from 'primeng/sidebar'
import { MenuModule } from 'primeng/menu'
import { OpenItemContractsComponent } from '@shared/components/smart/item/open-item-contracts/open-item-contracts.component'
import { ItemBulkActionsComponent } from '@shared/components/smart/item/item-bulk-actions/item-bulk-actions.component'
import { TabViewModule } from 'primeng/tabview'
import { EventPreviewComponent } from '@shared/components/presentation/app-specific/event/event-preview/event-preview.component';
import { EventSubscriptionComponent } from './components/event-subscription/event-subscription.component'
import { SelectButtonModule } from 'primeng/selectbutton';
import { SpinnerComponent } from '@shared/components/presentation/reusable/loaders/spinner/spinner.component'
import { SkeletonModule } from 'primeng/skeleton';

@NgModule({
  declarations: [
    DiscoveryComponent,
    ForeignItemComponent,
    InteractionsListComponent,
    ItemOptionsComponent,
    PartnerItemsComponent,
    MyOrgItemsComponent,
    MyOrgItemComponent,
    PartnerItemComponent,
    EventSubscriptionComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    DiscoveryRoutingModule,
    RoleCheckerComponent,
    ItemPreviewComponent,
    SelectionComponent,
    FilterItemsComponent,
    SadFaceComponent,
    PropPreviewComponent,
    EventPreviewComponent,
    CodeEditorComponent,
    TypeIconComponent,
    LongIdComponent,
    NgLetDirective,
    ButtonModule,
    MessageModule,
    TabMenuModule,
    DataViewModule,
    SidebarModule,
    MenuModule,
    OpenItemContractsComponent,
    ItemBulkActionsComponent,
    TabViewModule,
    SelectButtonModule,
    SpinnerComponent,
    SkeletonModule
  ],
})
export class DiscoveryModule {}
