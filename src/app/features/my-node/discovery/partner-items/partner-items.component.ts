/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { Router } from '@angular/router';
import { ContractAccessType } from '@core/enums/contract.enum';
import { UserRole } from '@core/enums/user.enum';
import { Item } from '@core/models/item.model';
import { NodeWithOwner } from '@core/models/node.model';
import { DiscoveryService } from '@core/services/discovery/discovery.service';
import { MyNodeService } from '@core/services/my-node/my-node.service';
import { UserService } from '@core/services/user/user.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Filter } from '@shared/models/filter-items.model';
import { combineLatest, map } from 'rxjs';
import { resolveItemType } from 'src/app/utils';

@UntilDestroy()
@Component({
  selector: 'app-partner-items',
  templateUrl: './partner-items.component.html',
  styleUrl: './partner-items.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PartnerItemsComponent {
  defaultPicked?: Item

  filteredItems: Item[] = []
  selectedItems: Item[] = []

  filter: Filter | null = null

  constructor(
    private _router: Router,
    private _discoveryService: DiscoveryService,
    private _myNodeService: MyNodeService,
    private _cd: ChangeDetectorRef,
    private _userService: UserService
  ) {}

  async ngOnInit(): Promise<void> {
    this.partnerItems$.pipe(untilDestroyed(this)).subscribe(() => {
      this._loadDefautPicked()
      this._applyFilter()
    })
  }

  search = (value: string): Item[] => {
    return this.partnerItems.filter((element) => {
      const parsedVal = value.toLowerCase()
      return element.oid.toLowerCase().includes(parsedVal) || element.title.toLowerCase().includes(parsedVal)
    })
  }

  onChange(item: Item) {
    this._router.navigateByUrl(`${this.url}/${item.oid}`)
  }

  onSelectionChange(items: Item[]) {
    this.selectedItems = items
  }

  onFilterChange(filter: Filter | null) {
    this.filter = filter
    this._applyFilter()
    this._cd.detectChanges()
  }

  hasExigentContracts(item: Item) {
    return this._myNodeService.hasExigentContracts(item.oid)
  }

  private _loadDefautPicked() {
    const split = this._router.url.split(`${this.url}/`)
    if (split[1]) {
      const oid = split[1]
      this.defaultPicked = this.partnerItems.find((element) => element.oid == oid)
    }
  }

  private _applyFilter() {
    let items = [...this.partnerItems]
    const types = this.filter?.itemTypes
    const organisations = this.filter?.organisations
    if ((types && types.length > 0) || (organisations && organisations.length > 0)) {
      items = items.filter((element) => {
        const type = resolveItemType(element['@type'])
        const cid = element['SPADE:organisation']?.['@id'].split(':').at(-1)
        if (type && types && types.some((element) => element.id == type.id)) {
          return true
        }
        if (cid && organisations && organisations.some((element) => element.cid == cid)) {
          return true
        }
        return false
      })
    }
    this.filteredItems = items
  }

  onTryAgain() {
    this._discoveryService.init(true)
  }

  get url() {
    return `/my-node/${this.brokerState}/${this.nodeState}/discovery/partners`
  }

  get brokerState() {
    return this._myNodeService.brokerState
  }

  get nodeState() {
    return this._myNodeService.nodeState
  }

  get initializing$() {
    return combineLatest([this._myNodeService.initializing$, this._discoveryService.initializing$]).pipe(map(([x0, x1]) => x0 || x1))
  }

  get errorInitializing$() {
    return combineLatest([this._myNodeService.errorInitializing$, this._discoveryService.errorInitializing$]).pipe(map(([x0, x1]) => x0 || x1))
  }

  get partnerItems$() {
    return this._discoveryService.partnerItems$
  }

  get partnerItems() {
    return this._discoveryService.partnerItems
  }

  get partnerOrganisations$() {
    return this._discoveryService.partnerOrganisations$
  }

  get partnerOrganisations() {
    return this._discoveryService.partnerOrganisations
  }

  get userOrganisation() {
    return this._userService.user?.organisation
  }

  get assignees(): NodeWithOwner[] {
    return this._myNodeService.node ? [{ ...this._myNodeService.node, owner: this.userOrganisation! }] : []
  }

  get accessType() {
    return ContractAccessType.REQUEST
  }

  get admin() {
    return UserRole.ADMIN
  }

  get nodeOperator() {
    return UserRole.NODE_OPERATOR
  }
}
