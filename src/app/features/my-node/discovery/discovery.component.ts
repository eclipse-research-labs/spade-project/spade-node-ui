/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { DiscoveryService } from '@core/services/discovery/discovery.service'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { MenuItem } from 'primeng/api'

@Component({
  selector: 'app-discovery',
  templateUrl: './discovery.component.html',
  styleUrl: './discovery.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DiscoveryComponent implements OnInit {
  items!: MenuItem[]

  constructor(private _discoveryService: DiscoveryService, private _myNodeService: MyNodeService) {}

  async ngOnInit(): Promise<void> {
    this.items = [
      {
        label: 'My Partners',
        icon: 'fa fa-cube',
        routerLink: `${this.url}/partners`,
      },
      {
        label: 'My Organisation',
        icon: 'fa fa-cube',
        routerLink: `${this.url}/my-org`,
      },
    ]
    await this._discoveryService.init()
  }

  get url() {
    return `/my-node/${this.brokerState}/${this.nodeState}/discovery`
  }

  get brokerState() {
    return this._myNodeService.brokerState
  }

  get nodeState() {
    return this._myNodeService.nodeState
  }
}
