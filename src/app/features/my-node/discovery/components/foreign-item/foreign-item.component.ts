/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, SimpleChanges } from '@angular/core'
import { ItemType, ItemWithIdentity, PropertyWithKey } from '@core/models/item.model'
import { DiscoveryService } from '@core/services/discovery/discovery.service'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { resolveItemType, levelToPrivacyIcon } from 'src/app/utils'

@Component({
  selector: 'app-foreign-item',
  templateUrl: './foreign-item.component.html',
  styleUrl: './foreign-item.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ForeignItemComponent implements OnChanges {
  @Input() item!: ItemWithIdentity
  @Input() contractsButton: boolean = false

  type!: ItemType

  constructor(private _discoveryService: DiscoveryService, private _myNodeService: MyNodeService, private _cd: ChangeDetectorRef) {}

  ngOnChanges(changes: SimpleChanges): void {
    this._loadItemType()
  }

  private _loadItemType() {
    this.type = resolveItemType(this.item?.['@type'])
    this._cd.markForCheck()
  }

  get partnerItems$() {
    return this._discoveryService.partnerItems$
  }

  get partnerItems() {
    return this._discoveryService.partnerItems
  }

  get privacyIcon() {
    return levelToPrivacyIcon(this.item?.['SPADE:Privacy']?.Level ?? 0)
  }

  get online() {
    return this._myNodeService.online
  }
}
