/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core'
import { Router } from '@angular/router'
import { EventWithKey, Item, PropertyWithKey } from '@core/models/item.model'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { delay, inflect } from 'src/app/utils'
import { DataView } from 'primeng/dataview'

@Component({
  selector: 'app-interactions-list',
  templateUrl: './interactions-list.component.html',
  styleUrl: './interactions-list.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InteractionsListComponent implements OnInit, OnChanges {
  @Input() item!: Item

  @ViewChild('propertiesList') propertiesList?: DataView

  activeIndex: number = 0

  properties: PropertyWithKey[] = []
  events: EventWithKey[] = []

  constructor(private _myNodeService: MyNodeService, private _router: Router) {}

  ngOnInit(): void {
    this._init()
  }

  ngOnChanges(changes: SimpleChanges): void {
    this._init()
    delay(1).then(() => this._scrollToIntercation())
  }

  goToPropConsumption(prop: PropertyWithKey) {
    localStorage.setItem('iid', prop.key)
    this._router.navigateByUrl(`${this.url}/props/${prop.key}`)
  }

  // goToEventSubscription(event: EventWithKey) {
  //   this._router.navigateByUrl(`${this.url}/events/${event.key}`)
  // }

  private _init() {
    this._initProperties()
    this._initEvents()
    this.activeIndex = this.propertiesCount > 0 ? 0 : this.eventsCount > 0 ? 1 : 0
  }

  private _initProperties() {
    this.properties = []
    const props = this.item?.properties
    if (props) {
      const propertyKeys = Object.keys(props)
      propertyKeys.forEach((element) => {
        this.properties.push({
          key: element,
          ...props[element],
        })
      })
    }
  }

  private _initEvents() {
    this.events = []
    const events = this.item?.events
    if (events) {
      const eventKeys = Object.keys(events)
      eventKeys.forEach((element) => {
        this.events.push({
          key: element,
          ...events[element],
        })
      })
    }
  }

  private _scrollToIntercation() {
    const iid = localStorage.getItem('iid')
    if (iid) {
      const index = this.properties.findIndex((element) => element.key === iid)
      if (index >= 0) {
        const element = this.propertiesList?.el?.nativeElement?.children[0]?.children[0]?.children[index]
        if (element) {
          element.scrollIntoView({  block: 'center' })
          localStorage.removeItem('iid')
        }
      }
    }
  }

  get propertiesCountInflected() {
    const count = this.propertiesCount
    return inflect(count, '0 properties', '1 property', `${count} properties`)
  }

  get eventsCountInflected() {
    const count = this.eventsCount
    return inflect(count, '0 events', '1 event', `${count} events`)
  }

  get propertiesCount() {
    return this.properties?.length ?? 0
  }

  get eventsCount() {
    return this.events?.length ?? 0
  }

  get url() {
    return `/detail/items/foreign-items/${this.nodeId}/${this.item.oid}`
  }

  get nodeId() {
    return this._myNodeService.node!.nodeId
  }

  get noIteractions() {
    return this.propertiesCount <= 0 && this.eventsCount <= 0
  }
}
