/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { ItemWithIdentity } from '@core/models/item.model'
import { MenuItem } from 'primeng/api'

@Component({
  selector: 'app-item-options',
  templateUrl: './item-options.component.html',
  styleUrl: './item-options.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemOptionsComponent {
  @Input() item!: ItemWithIdentity

  options: MenuItem[] | undefined = [
    {
      label: 'Details',
      items: [
        {
          label: 'Show TD',
          icon: 'fa fa-code',
          command: (e) => {
            const event = e.originalEvent
            if (event) {
              this.showTD = true
            }
          },
        },
      ],
    },
  ]

  showTD = false
  td!: string

  constructor() {}

  ngOnInit(): void {
    const { organisation: _, ...itemWithoutIdentity } = this.item
    this.td = JSON.stringify(itemWithoutIdentity ?? '', null, 2)
  }
}
