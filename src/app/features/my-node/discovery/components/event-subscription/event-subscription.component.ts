/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core'
import { ItemWithIdentity, EventWithKey } from '@core/models/item.model'
import { EventSubscriptionService } from './event-subscription.service'
import { SelectButtonChangeEvent } from 'primeng/selectbutton'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'

@Component({
  selector: 'app-event-subscription',
  templateUrl: './event-subscription.component.html',
  styleUrl: './event-subscription.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [EventSubscriptionService],
})
export class EventSubscriptionComponent implements OnInit {
  @Input() item!: ItemWithIdentity
  @Input() event!: EventWithKey

  loading = true

  stateOptions: any[] = [
    { label: 'Off', value: false },
    { label: 'On', value: true },
  ]

  constructor(private _service: EventSubscriptionService, private _snackBarService: SnackBarService) {}

  ngOnInit(): void {
    this._service.init(this.item, this.event)
  }

  async onChange(event: SelectButtonChangeEvent) {
    if (event.value) {
      try {
        await this._service.subscribeToEvent(this.item, this.event)
        this._snackBarService.showSuccess('Subscribed to event')
      } catch (e) {
        this._snackBarService.showError('Failed to subscribe to event')
      }
    } else {
      try {
        await this._service.unsubscribeFromEvent(this.item, this.event)
        this._snackBarService.showSuccess('Unsubscribed from event')
      } catch (e) {
        this._snackBarService.showError('Failed to unsubscribe from event')
      }
    }
  }

  get noForms() {
    return !this.event.forms || this.event.forms.length <= 0
  }

  get subscribed() {
    return this._service.subscribed
  }

  set subscribed(value: boolean) {
    this._service.subscribed = value
  }

  get initializing$() {
    return this._service.initializing$
  }

  get loading$() {
    return this._service.loading$
  }
}
