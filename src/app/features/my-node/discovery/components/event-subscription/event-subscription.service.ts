/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { HttpErrorResponse, HttpStatusCode } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { EventsApiService } from '@core/api/modules/events'
import { EventWithKey, ItemWithIdentity } from '@core/models/item.model'
import { BaseService } from '@core/services/base'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { firstValueFrom } from 'rxjs'

@Injectable()
export class EventSubscriptionService extends BaseService {
  subscribed = false

  constructor(private _eventsApiService: EventsApiService, private _myNodeService: MyNodeService) {
    super()
  }

  async init(item: ItemWithIdentity, event: EventWithKey) {
    const subscribers = await this.initApiWrapper(this._getEventSubscribers(item.oid, event.key))
    if (
      subscribers.length > 0 &&
      subscribers.some((nodeId) => nodeId.replace('urn:node:', '') === this._myNodeService.node?.nodeId)
    ) {
      this.subscribed = true
    }
  }

  async subscribeToEvent(item: ItemWithIdentity, event: EventWithKey) {
    try {
      await this.loadApiWrapper(this._subscribeToEvent(item.oid, event.key))
    } catch (e) {
      this.subscribed = false
      throw e
    }
  }

  async unsubscribeFromEvent(item: ItemWithIdentity, event: EventWithKey) {
    try {
      await this.loadApiWrapper(this._unsubscribeFromEvent(item.oid, event.key))
    } catch (e) {
      this.subscribed = true
      throw e
    }
  }

  private async _getEventSubscribers(oid: string, eid: string) {
    return await firstValueFrom(this._eventsApiService.getEventSubscribers(this._myNodeService.sbUrl, oid, eid))
  }

  private async _subscribeToEvent(oid: string, eid: string) {
    return await firstValueFrom(this._eventsApiService.subscribeToEvent(this._myNodeService.sbUrl, oid, eid))
  }

  private async _unsubscribeFromEvent(oid: string, eid: string) {
    return await firstValueFrom(this._eventsApiService.unsubscribeFromEvent(this._myNodeService.sbUrl, oid, eid))
  }
}
