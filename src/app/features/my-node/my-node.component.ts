/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { SideNavDivider, SideNavItem } from '@shared/models/side-nav.model'

@UntilDestroy()
@Component({
  selector: 'app-my-node',
  templateUrl: './my-node.component.html',
  styleUrl: './my-node.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MyNodeComponent implements OnInit {
  nav!: (SideNavItem | SideNavDivider)[]

  constructor(private _myNodeService: MyNodeService) {}

  ngOnInit(): void {
    this._initMenu()
    this._myNodeService.init()
    this._listenForExigentContractsChange()
  }

  private _listenForExigentContractsChange() {
    this._myNodeService.exigentContracts$.pipe(untilDestroyed(this)).subscribe((_) => {
      this._initMenu()
    })
  }

  private _initMenu() {
    this.nav = [
      {
        name: 'Items',
        path: `/my-node/${this.brokerState}/${this.nodeState}/items`,
        icon: 'fa fa-cube',
      },
      {
        name: 'Adapters',
        path: `/my-node/${this.brokerState}/${this.nodeState}/adapters`,
        icon: 'fa fa-plug',
      },
      ...(this._myNodeService.online
        ? [
            {
              name: 'Contracts',
              path: `/my-node/${this.brokerState}/${this.nodeState}/contracts`,
              icon: 'fa fa-file-signature',
              indicate: this._myNodeService.hasExigentContracts(),
            },
            {
              label: 'Remote Infrastructure',
            },
            {
              name: 'Discovery',
              path: `/my-node/${this.brokerState}/${this.nodeState}/discovery`,
              icon: 'fa fa-compass',
            },
            {
              name: 'Credentials',
              path: `/my-node/${this.brokerState}/${this.nodeState}/credentials`,
              icon: 'fa fa-key',
            },
          ]
        : []),
    ]
  }

  get node() {
    return this._myNodeService.node
  }

  get nodeState() {
    return this._myNodeService.nodeState
  }

  get brokerState() {
    return this._myNodeService.brokerState
  }
}
