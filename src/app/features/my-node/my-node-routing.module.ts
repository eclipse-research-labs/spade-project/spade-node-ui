/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { MyNodeComponent } from './my-node.component'
import { userGuard } from '@core/guards/user.guard'
import { onlineGuard } from '../../core/guards/online.guard'
import { offlineGuard } from '../../core/guards/offline.guard'

const routes: Routes = [
  {
    path: '',
    component: MyNodeComponent,
    children: [
      {
        path: '',
        redirectTo: 'online/',
        pathMatch: 'full',
      },
      {
        path: 'online/:nodeId',
        canActivate: [userGuard.watch],
        children: [
          {
            path: '',
            redirectTo: 'items',
            pathMatch: 'full',
          },
          {
            path: 'items',
            loadChildren: () => import('./items/items.module').then((m) => m.ItemsModule),
            canActivate: [onlineGuard.watch],
          },
          {
            path: 'adapters',
            loadChildren: () => import('./adapters/adapters.module').then((m) => m.AdaptersModule),
            canActivate: [onlineGuard.watch],
          },
          {
            path: 'contracts',
            loadChildren: () => import('./contracts/contracts.module').then((m) => m.ContractsModule),
            canActivate: [onlineGuard.watch],
          },
          {
            path: 'discovery',
            loadChildren: () => import('./discovery/discovery.module').then((m) => m.DiscoveryModule),
            canActivate: [onlineGuard.watch],
            data: {
              reuseRoute: true,
            },
          },
          {
            path: 'credentials',
            loadChildren: () => import('./credentials/credentials.module').then((m) => m.CredentialsModule),
            canActivate: [onlineGuard.watch],
          },
        ],
      },
      {
        path: 'offline/:nodeAddress',
        children: [
          {
            path: '',
            redirectTo: 'items',
            pathMatch: 'full',
          },
          {
            path: 'items',
            loadChildren: () => import('./items/items.module').then((m) => m.ItemsModule),
            canActivate: [offlineGuard.watch],
          },
          {
            path: 'adapters',
            loadChildren: () => import('./adapters/adapters.module').then((m) => m.AdaptersModule),
            canActivate: [offlineGuard.watch],
          },
        ],
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyNodeRoutingModule {}
