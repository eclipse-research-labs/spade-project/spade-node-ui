/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { MyNodeService } from '@core/services/my-node/my-node.service'

@Component({
  selector: 'app-nodes',
  templateUrl: './nodes.component.html',
  styleUrl: './nodes.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NodesComponent {
  constructor(private _myNodeService: MyNodeService, private _router: Router) {}

  backToNode() {
    this._router.navigateByUrl(`/my-node/online/${this.node?.nodeId}`)
  }

  get node() {
    return this._myNodeService.node
  }

  get node$() {
    return this._myNodeService.node$
  }
}
