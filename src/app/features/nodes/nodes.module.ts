/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { NodesRoutingModule } from './nodes-routing.module'
import { NodesComponent } from './nodes.component'
import { AddNodeComponent } from './components/add-node/add-node.component'
import { NodeListComponent } from './components/node-list/node-list.component'
import { NodeOptionsComponent } from './components/node-options/node-options.component'
import { HeaderComponent } from '@shared/components/smart/header/header.component'
import { EuContribComponent } from '@shared/components/presentation/app-specific/eu-contrib/eu-contrib.component'
import { ConfirmationDialogComponent } from '@shared/components/presentation/reusable/confirmation-dialog/confirmation-dialog.component'
import { SadFaceComponent } from '@shared/components/presentation/reusable/sad-face/sad-face.component'
import { NodePreviewComponent } from '@shared/components/presentation/app-specific/node/node-preview/node-preview.component'
import { SpinnerComponent } from '@shared/components/presentation/reusable/loaders/spinner/spinner.component'
import { RoleCheckerComponent } from '@shared/components/smart/role-checker/role-checker.component'
import { InfoComponent } from '@shared/components/presentation/reusable/info/info.component'
import { ReactiveFormsModule } from '@angular/forms'
import { NgLetDirective } from 'ng-let'
import { MenuModule } from 'primeng/menu'
import { ButtonModule } from 'primeng/button'
import { MessageModule } from 'primeng/message'
import { InputTextModule } from 'primeng/inputtext'

@NgModule({
  declarations: [NodesComponent, AddNodeComponent, NodeListComponent, NodeOptionsComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NodesRoutingModule,
    HeaderComponent,
    EuContribComponent,
    ConfirmationDialogComponent,
    SadFaceComponent,
    NodePreviewComponent,
    SpinnerComponent,
    RoleCheckerComponent,
    InfoComponent,
    NgLetDirective,
    MenuModule,
    ButtonModule,
    MessageModule,
    InputTextModule,
  ],
})
export class NodesModule {}
