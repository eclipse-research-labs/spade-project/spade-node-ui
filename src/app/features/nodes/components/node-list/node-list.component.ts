/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { MasterService } from '@core/services/master/master.service'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { NodesService } from '@core/services/nodes/nodes.service'
import { Node } from '@core/models/node.model'
import { Router } from '@angular/router'
import { FormControl } from '@angular/forms'
import { Observable, combineLatest, map, startWith } from 'rxjs'

@Component({
  selector: 'app-node-list',
  templateUrl: './node-list.component.html',
  styleUrl: './node-list.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NodeListComponent implements OnInit {
  search = new FormControl<string>('')
  filteredNodes$!: Observable<Node[]>

  constructor(
    private _nodesService: NodesService,
    private _masterService: MasterService,
    private _myNodeService: MyNodeService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.filteredNodes$ = combineLatest([this.nodes$, this.search.valueChanges.pipe(startWith(''))]).pipe(
      map(([nodes, searchQuery]) => this._search(nodes ?? [], searchQuery))
    )
  }

  onTryAgain() {
    this._masterService.reloadApp()
  }

  isNew(node: Node) {
    return this._nodesService.isNewNode(node)
  }

  selectNode(node: Node) {
    this._nodesService.removeFromNew(node)
    this._router.navigateByUrl('/my-node/online/' + node.nodeId).then(() => {
      this._myNodeService.init(true)
    })
  }

  private _search(nodes: Node[], searchQuery: string | null): Node[] {
    if (!searchQuery || searchQuery.length <= 3) {
      return nodes
    }
    return nodes.filter((element) => {
      const nodeId = element.nodeId.toLowerCase()
      const name = element.name.toLowerCase()
      const host = element.host.toLowerCase()
      return nodeId.startsWith(searchQuery) || name.startsWith(searchQuery) || host.startsWith(searchQuery)
    })
  }

  get node$() {
    return this._myNodeService.node$
  }

  get nodes$() {
    return this._nodesService.nodes$
  }

  get myNodeLoading$() {
    return this._nodesService.loading$
  }

  get initializing$() {
    return this._nodesService.initializing$
  }

  get errorInitializing$() {
    return this._nodesService.errorInitializing$
  }
}
