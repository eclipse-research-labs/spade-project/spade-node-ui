/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { NodesApiService } from '@core/api/modules/nodes'
import { BaseService } from '@core/services/base'
import { NodesService } from '@core/services/nodes/nodes.service'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { firstValueFrom, take } from 'rxjs'

@Injectable()
export class NodeOptionsService extends BaseService {
  constructor(private _nodesApiService: NodesApiService, private _nodesService: NodesService, private _snackBar: SnackBarService) {
    super()
  }

  async deleteNode(sbUrl: string, nodeId: string) {
    await this.loadApiWrapper(this._deleteNode(sbUrl, nodeId))
    this._nodesService.updateNodes([...this._nodesService.nodes?.filter((element) => element.nodeId != nodeId) ?? []])
  }

  private async _deleteNode(sbUrl: string, nodeId: string) {
    try {
      await firstValueFrom(this._nodesApiService.disablePlatformMode(sbUrl).pipe(take(1)))
    } catch(e) {
      console.error(`Failed to disable platform mode for node: ${sbUrl}\n${e}`)
      this._snackBar.showWarning('Unable to disable platform mode. The Data Broker will revert to offline mode upon the next restart.')
    }
    await firstValueFrom(this._nodesApiService.removeNode(nodeId).pipe(take(1)))
  }
}
