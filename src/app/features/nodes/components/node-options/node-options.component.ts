/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core'
import { NodeOptionsService } from './node-options.service'
import { ConfirmationService, MenuItem } from 'primeng/api'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { Node } from '@core/models/node.model'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { UserService } from '@core/services/user/user.service'
import { UserRole } from '@core/enums/user.enum'

@UntilDestroy()
@Component({
  selector: 'app-node-options',
  templateUrl: './node-options.component.html',
  styleUrl: './node-options.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ConfirmationService, NodeOptionsService],
})
export class NodeOptionsComponent implements OnInit {
  @Input() node!: Node

  options!: MenuItem[]

  private _authorized = true

  constructor(private _confirmationService: ConfirmationService, private _service: NodeOptionsService, private _userService: UserService, private _snackBar: SnackBarService) {}

  ngOnInit(): void {
    this._initOptions()
    this._listenForUserChange()
  }

  deleteNode(event: Event) {
    this._confirmationService.confirm({
      // target: event.target as EventTarget,
      key: 'delete-node',
      header: 'Remove Data Broker from Organisation?',
      acceptLabel: 'Remove',
      rejectLabel: 'Cancel',
      acceptIcon: 'fa fa-trash-can',
      rejectIcon: 'pi pi-times',
      acceptButtonStyleClass: 'p-button-text p-button-danger',
      rejectButtonStyleClass: 'p-button-text p-button-secondary',
      accept: async () => {
        await this._deleteNode()
      },
    })
  }

  private async _deleteNode() {
    await this._service.deleteNode(this.node.host, this.node.nodeId)
    this._snackBar.showSuccess('Data Broker removed')
  }

  private _listenForUserChange() {
    this._userService.user$.pipe(untilDestroyed(this)).subscribe((user) => {
      this._authorized = user?.roles.includes(UserRole.ADMIN) || user?.roles.includes(UserRole.NODE_OPERATOR) || false
      this._initOptions()
    })
  }

  private _initOptions() {
    this.options = [
      {
        label: 'Settings',
        items: [
          {
            label: 'Remove',
            icon: 'fa fa-trash-can',
            disabled: !this._authorized,
            tooltipOptions: {
              appendTo: 'body',
              tooltipPosition: 'left',
              tooltipLabel: 'Insufficient role to delete the Data Broker',
              disabled: this._authorized,
            },
            command: (e) => {
              const event = e.originalEvent
              if (event) {
                this.deleteNode(event)
              }
            },
          },
        ],
      },
    ]
  }

  get loading$() {
    return this._service.loading$
  }
}
