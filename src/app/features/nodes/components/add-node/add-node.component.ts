/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { UserRole } from '@core/enums/user.enum'

@Component({
  selector: 'app-add-node',
  templateUrl: './add-node.component.html',
  styleUrl: './add-node.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddNodeComponent {

  get admin() {
    return UserRole.ADMIN
  }

  get nodeOperator() {
    return UserRole.NODE_OPERATOR
  }

}
