/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { ItemWithIdentity } from '@core/models/item.model'

@Component({
  selector: 'app-marketplace-item-description',
  templateUrl: './marketplace-item-description.component.html',
  styleUrl: './marketplace-item-description.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MarketplaceItemDescriptionComponent {
  @Input() item!: ItemWithIdentity

  showMore = false

  isEllipsisActive(e: HTMLElement) {
    return e.offsetWidth < e.scrollWidth
  }
}
