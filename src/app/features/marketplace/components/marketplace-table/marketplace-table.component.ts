/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, ContentChild, Input, TemplateRef } from '@angular/core'
import { ItemWithIdentity } from '@core/models/item.model'
import { MarketplaceService } from '@core/services/marketplace/marketplace.service'

@Component({
  selector: 'app-marketplace-table',
  templateUrl: './marketplace-table.component.html',
  styleUrl: './marketplace-table.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MarketplaceTableComponent {
  @Input() data: ItemWithIdentity[] = []
  @Input() emptyMessage?: string

  @ContentChild('preview') preview: TemplateRef<any> | undefined

  constructor(private _marketplaceService: MarketplaceService) {}

  onTryAgain() {
    this._marketplaceService.init(true)
  }

  openLink(item: ItemWithIdentity) {
    const link = item.links?.at(0)?.href
    if (link) {
      window.open(link, '_blank')
    }
  }
}
