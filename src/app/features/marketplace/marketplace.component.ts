/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { MarketplaceService } from '@core/services/marketplace/marketplace.service'
import { MenuItem } from 'primeng/api'

@Component({
  selector: 'app-marketplace',
  templateUrl: './marketplace.component.html',
  styleUrl: './marketplace.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MarketplaceComponent implements OnInit {
  items!: MenuItem[]

  activeIndex: number = 0
  howToUse: boolean = false

  constructor(private _marketplaceService: MarketplaceService) {}

  async ngOnInit(): Promise<void> {
    this.items = [
      {
        label: 'Services',
        icon: 'fa fa-cloud',
        routerLink: '/marketplace/services',
      },
      {
        label: 'Datasets',
        icon: 'fa fa-file-lines',
        routerLink: '/marketplace/datasets',
      },
    ]

    this._marketplaceService.init(true)
  }

  refresh() {
    this._marketplaceService.init(true)
  }

  get initializing$() {
    return this._marketplaceService.initializing$
  }

  get services$() {
    return this._marketplaceService.services$
  }

  get datasets$() {
    return this._marketplaceService.datasets$
  }

  get lastUpdated$() {
    return this._marketplaceService.lastUpdated$
  }
}
