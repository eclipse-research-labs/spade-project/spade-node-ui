/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { MarketplaceRoutingModule } from './marketplace-routing.module'
import { MarketplaceComponent } from './marketplace.component'
import { HeaderComponent } from '@shared/components/smart/header/header.component'
import { EuContribComponent } from '@shared/components/presentation/app-specific/eu-contrib/eu-contrib.component'
import { ServicesComponent } from './services/services.component'
import { DatasetsComponent } from './datasets/datasets.component'
import { TabMenuModule } from 'primeng/tabmenu'
import { NgLetDirective } from 'ng-let'
import { TableSkeletonComponent } from '@shared/components/presentation/reusable/loaders/table-skeleton/table-skeleton.component'
import { MarketplaceTableComponent } from './components/marketplace-table/marketplace-table.component'
import { SadFaceComponent } from '@shared/components/presentation/reusable/sad-face/sad-face.component'
import { ButtonModule } from 'primeng/button'
import { ServicePreviewComponent } from '@shared/components/presentation/app-specific/marketplace/service-preview/service-preview.component'
import { DatasetPreviewComponent } from '@shared/components/presentation/app-specific/marketplace/dataset-preview/dataset-preview.component'
import { ScrollingModule } from '@angular/cdk/scrolling'
import { DialogModule } from 'primeng/dialog'
import { InfoComponent } from '@shared/components/presentation/reusable/info/info.component';
import { MarketplaceItemDescriptionComponent } from './components/marketplace-item-description/marketplace-item-description.component'

@NgModule({
  declarations: [MarketplaceComponent, ServicesComponent, DatasetsComponent, MarketplaceTableComponent, MarketplaceItemDescriptionComponent],
  imports: [
    CommonModule,
    MarketplaceRoutingModule,
    HeaderComponent,
    EuContribComponent,
    TabMenuModule,
    NgLetDirective,
    TableSkeletonComponent,
    SadFaceComponent,
    ButtonModule,
    ServicePreviewComponent,
    DatasetPreviewComponent,
    ScrollingModule,
    DialogModule,
    InfoComponent,
  ],
})
export class MarketplaceModule {}
