/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { Router } from '@angular/router'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { UserService } from '@core/services/user/user.service'

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrl: './landing.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingComponent {
  loading: boolean = false
  visible: boolean = false

  constructor(private _myNodeService: MyNodeService, private _userService: UserService, private _router: Router) {}

  async loginToPlatform() {
    try {
      const redirectUri = '/marketplace/services'
      if (this._userService.isLoggedIn) {
        this._router.navigateByUrl(redirectUri)
      } else {
        this.loading = true
        await this._userService.login(redirectUri)
      }
    } finally {
      this.loading = false
    }
  }

  goBackToEntrypoint() {
    window.open('https://spade.bavenir.eu', '_self')
  }

  goToNodeDocumentation() {
    window.open('https://gitlab.eclipse.org/eclipse-research-labs/spade-project/spade-node', '_blank')
    this.visible = false
  }

  get broker$() {
    return this._myNodeService.broker$
  }

  get keycloakInitialized() {
    return this._userService.keycloakInitialized
  }
}
