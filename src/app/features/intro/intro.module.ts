/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { IntroRoutingModule } from './intro-routing.module'
import { IntroComponent } from './intro.component'

import { LandingComponent } from './landing/landing.component'
import { OnboardingComponent } from './onboarding/onboarding.component'
import { DividerComponent } from '@shared/components/presentation/reusable/divider/divider.component'
import { NgLetDirective } from 'ng-let'
import { ButtonModule } from 'primeng/button'
import { DialogModule } from 'primeng/dialog'

@NgModule({
  declarations: [IntroComponent, LandingComponent, OnboardingComponent],
  imports: [CommonModule, IntroRoutingModule, DividerComponent, NgLetDirective, ButtonModule, DialogModule],
})
export class IntroModule {}
