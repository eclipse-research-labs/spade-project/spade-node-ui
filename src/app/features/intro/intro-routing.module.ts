/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { IntroComponent } from './intro.component'
import { LandingComponent } from './landing/landing.component'
import { OnboardingComponent } from './onboarding/onboarding.component'

const routes: Routes = [
  {
    path: '',
    component: IntroComponent,
    children: [
      {
        path: '',
        component: LandingComponent,
      },
      {
        path: 'onboarding',
        component: OnboardingComponent,
        children: [
          {
            path: 'connect-to-node',
            loadChildren: () => import('./onboarding/connect-to-node/connect-to-node.module').then((m) => m.ConnectToNodeModule),
          },
          {
            path: 'connect-node',
            loadChildren: () => import('./onboarding/connect-node/connect-node.module').then((m) => m.ConnectNodeModule),
          },
          {
            path: 'organisation',
            loadChildren: () => import('./onboarding/create-join-org/create-join-org.module').then((m) => m.CreateJoinOrgModule),
          },
        ],
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IntroRoutingModule {}
