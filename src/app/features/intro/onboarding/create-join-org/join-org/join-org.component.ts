/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core'
import { FormControl, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'

@Component({
  selector: 'app-join-org',
  templateUrl: './join-org.component.html',
  styleUrl: './join-org.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JoinOrgComponent {

  showMore: boolean = false

  constructor(private _router: Router, private _cd: ChangeDetectorRef) {}

  invitation: FormControl<string | null> = new FormControl<string | null>('', {
    updateOn: 'change',
    validators: [Validators.required],
  })

  toogleMore() {
    this.showMore = !this.showMore
  }

  join() {
    const invitation = this.invitation.value
    if (invitation) {
      this._router.navigateByUrl('/onboarding/organisation/join/' + invitation.replaceAll('-', ''))
    }
  }
}
