/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { userGuard } from '@core/guards/user.guard';
import { CreateJoinOrgComponent } from './create-join-org.component';
import { CreateOrgComponent } from './create-org/create-org.component';
import { JoinOrgConfirmComponent } from './join-org-confirm/join-org-confirm.component';
import { JoinOrgComponent } from './join-org/join-org.component';
import { LandingComponent } from './landing/landing.component';

const routes: Routes = [
  {
    path: '',
    component: CreateJoinOrgComponent,
    canActivate: [userGuard.watch],
    data: { roles: [] },
    children: [
      {
        path: '',
        component: LandingComponent,
      },
      {
        path: 'create',
        component: CreateOrgComponent,
      },
      {
        path: 'join',
        component: JoinOrgComponent,
      },
      {
        path: 'join/:invitation',
        component: JoinOrgConfirmComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateJoinOrgRoutingModule { }
