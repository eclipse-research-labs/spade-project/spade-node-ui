/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { CreateJoinOrgService } from '../create-join-org.service'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { encodeSBUrl } from 'src/app/utils'

@UntilDestroy()
@Component({
  selector: 'app-join-org-confirm',
  templateUrl: './join-org-confirm.component.html',
  styleUrl: './join-org-confirm.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JoinOrgConfirmComponent implements OnInit {
  invitation?: string | null | undefined

  constructor(
    private _router: Router,
    private _snackBar: SnackBarService,
    private _route: ActivatedRoute,
    private _service: CreateJoinOrgService,
    private _myNodeService: MyNodeService
  ) {}

  ngOnInit(): void {
    this._route.paramMap.pipe(untilDestroyed(this)).subscribe((params) => {
      this.invitation = params.get('invitation')
      if (this.invitation) {
        this._service.loadOrgFromInvitation(this.invitation)
      }
    })
  }

  async join() {
    if (this.invitation) {
      const name = this.organisation?.name
      try {
        await this._service.joinOrganisation(this.invitation)
        this._snackBar.showSuccess(`You have successfully joined ${name} organisation`)
        const broker = this._myNodeService.broker
        if (broker && !broker.online) {
          this._router.navigateByUrl(`/onboarding/connect-node/${encodeSBUrl(broker.address)}`)
        } else {
          this._router.navigateByUrl('/marketplace/services')
        }
      } catch (e) {
        this._snackBar.showError('Failed to join organisation. See the console for more information.')
      }
    }
  }

  get initializing$() {
    return this._service.initializing$
  }

  get loading$() {
    return this._service.loading$
  }

  get errorInitializing$() {
    return this._service.errorInitializing$
  }

  get errorInitializing() {
    return this._service.errorInitializing
  }

  get organisation() {
    return this._service.organisation
  }
}
