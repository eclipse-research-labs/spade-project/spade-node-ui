/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { CreateJoinOrgService } from './create-join-org.service';

@Component({
  selector: 'app-create-join-org',
  templateUrl: './create-join-org.component.html',
  styleUrl: './create-join-org.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [CreateJoinOrgService]
})
export class CreateJoinOrgComponent {}
