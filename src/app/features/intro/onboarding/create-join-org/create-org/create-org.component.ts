/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { Router } from '@angular/router'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { encodeSBUrl } from 'src/app/utils'
import { CreateJoinOrgService } from '../create-join-org.service'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { GeolocationApiService } from '@core/api/modules/geolocation'
import { Feature } from '@core/models/geolocation.model'
import { AutoCompleteCompleteEvent } from 'primeng/autocomplete'

import getCountryISO2 from 'country-iso-3-to-2'
import { MyNodeService } from '@core/services/my-node/my-node.service'

@Component({
  selector: 'app-create-org',
  templateUrl: './create-org.component.html',
  styleUrl: './create-org.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateOrgComponent {
  organisation = new FormGroup({
    name: new FormControl('', {
      updateOn: 'change',
      validators: [Validators.required],
    }),
    location: new FormControl<Feature | null>(null, {
      updateOn: 'change',
      validators: [Validators.required],
    }),
    businessId: new FormControl('', {
      updateOn: 'change',
    }),
    avatar: new FormControl('', {
      updateOn: 'change',
    }),
    description: new FormControl('', {
      updateOn: 'change',
    }),
  })

  private _suggestions$ = new BehaviorSubject<Feature[]>([])

  suggestions$ = this._suggestions$.asObservable()

  constructor(
    private _router: Router,
    private _snackBar: SnackBarService,
    private _service: CreateJoinOrgService,
    private _geolocationApi: GeolocationApiService,
    private _myNodeService: MyNodeService
  ) {}

  async create() {
    if (this.organisation.valid) {
      const name = this.organisation.get('name')!.value as string
      const { location, countryCode } = parseLocation(this.organisation.get('location')!.value as Feature | null)
      const businessId = this.organisation.get('businessId')!.value as string
      const avatar = this.organisation.get('avatar')!.value as string
      const description = this.organisation.get('description')!.value as string
      try {
        await this._service.createOrganisation({
          name,
          location,
          countryCode,
          businessId,
          avatar,
          description,
        })
        this._snackBar.showSuccess(`You have successfully created a ${name} organisation`)
        const broker = this._myNodeService.broker
        if (broker && !broker.online) {
          this._router.navigateByUrl(`/onboarding/connect-node/${encodeSBUrl(broker.address)}`)
        } else {
          this._router.navigateByUrl('/marketplace/services')
        }
      } catch (e) {
        this._snackBar.showError('Failed to create organisation. See the console for more information.')
      }
    }
  }

  async search(event: AutoCompleteCompleteEvent) {
    const text = event.query
    const openroute = await firstValueFrom(this._geolocationApi.autocomplete(text).pipe(take(1)))
    this._suggestions$.next(openroute.features)
  }

  public selectedFeatureConversionMethod(feature: Feature) {
    return `${feature.properties.name}, ${feature.properties.country}`
  }

  get loading$() {
    return this._service.loading$
  }
}

function parseLocation(feature: Feature | null) {
  const l_name = feature?.properties.name
  const l_country = feature?.properties.country
  const l_country_a = feature?.properties.country_a
  let location!: string
  if (l_name && l_country) {
    location = `${l_name}, ${l_country}`
  } else {
    location = ''
  }
  return { location, countryCode: getCountryISO2(l_country_a) ?? '' }
}
