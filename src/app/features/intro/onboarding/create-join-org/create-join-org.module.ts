/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { CreateJoinOrgRoutingModule } from './create-join-org-routing.module'

import { LandingComponent as CreateJoinOrgLanding } from './landing/landing.component'
import { CreateOrgComponent } from './create-org/create-org.component'
import { JoinOrgComponent } from './join-org/join-org.component'
import { CreateJoinOrgComponent } from './create-join-org.component'
import { JoinOrgConfirmComponent } from './join-org-confirm/join-org-confirm.component'
import { CreateJoinOrgCardComponent } from './components/create-join-org-card/create-join-org-card.component'
import { OrgInfoComponent } from './components/org-info/org-info.component'
import { DividerComponent } from '@shared/components/presentation/reusable/divider/divider.component'
import { SpinnerComponent } from '@shared/components/presentation/reusable/loaders/spinner/spinner.component'
import { OnboardingCardComponent } from '@shared/components/presentation/app-specific/onboarding/onboarding-card/onboarding-card.component'
import { ReactiveFormsModule } from '@angular/forms'
import { NgLetDirective } from 'ng-let'
import { ButtonModule } from 'primeng/button'
import { MessageModule } from 'primeng/message'
import { AutoCompleteModule } from 'primeng/autocomplete'
import { InputTextModule } from 'primeng/inputtext'
import { InputTextareaModule } from 'primeng/inputtextarea'
import { InputMaskModule } from 'primeng/inputmask'

@NgModule({
  declarations: [CreateJoinOrgLanding, CreateOrgComponent, JoinOrgComponent, CreateJoinOrgComponent, JoinOrgConfirmComponent, CreateJoinOrgCardComponent, OrgInfoComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CreateJoinOrgRoutingModule,
    DividerComponent,
    SpinnerComponent,
    OnboardingCardComponent,
    NgLetDirective,
    ButtonModule,
    MessageModule,
    AutoCompleteModule,
    InputTextModule,
    InputTextareaModule,
    InputMaskModule,
  ],
})
export class CreateJoinOrgModule {}
