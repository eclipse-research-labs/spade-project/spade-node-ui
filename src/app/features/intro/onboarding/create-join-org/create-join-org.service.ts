/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { OrganisationApiService } from '@core/api/modules/organisations'
import { UsersApiService } from '@core/api/modules/users'
import { UserRole } from '@core/enums/user.enum'
import { Organisation, OrganisationCreate } from '@core/models/organisation.model'
import { BaseService } from '@core/services/base'
import { UserService } from '@core/services/user/user.service'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'
import { delay } from 'src/app/utils'

@Injectable()
export class CreateJoinOrgService extends BaseService {
  private _organisation = new BehaviorSubject<Organisation | null>(null)

  organisation$ = this._organisation.asObservable()

  constructor(private _orgApi: OrganisationApiService, private _userApi: UsersApiService, private _userService: UserService) {
    super()
  }

  async loadOrgFromInvitation(invitation: string) {
    await this.initApiWrapper(this._loadInvitation(invitation))
  }

  private async _loadInvitation(invitation: string) {
    await delay(2000)
    const org = await firstValueFrom(this._orgApi.getOrganisationFromInvitation(invitation).pipe(take(1)))
    this._organisation.next(org)
  }

  async joinOrganisation(invitation: string) {
    const user = await this.loadApiWrapper(this._joinOrganisation(invitation))
    if (user) this._userService.updateUser(user)
  }

  async createOrganisation(organisation: OrganisationCreate) {
    const user = await this.loadApiWrapper(this._createOrganisation(organisation))
    if (user) this._userService.updateUser(user)
  }

  private async _joinOrganisation(invitation: string) {
    const org = await this._join(invitation)
    const user = this._userService.user
    if (user) {
      user.organisation = org
      user.roles = [UserRole.VIEWER]
    }
    return user
  }

  private async _join(invitation: string) {
    return await firstValueFrom(this._orgApi.joinOrganisation(invitation).pipe(take(1)))
  }

  private async _createOrganisation(data: OrganisationCreate) {
    const org = await this._create(data)
    const user = this._userService.user
    if (user) {
      user.organisation = org
      user.roles = [UserRole.VIEWER, UserRole.NODE_OPERATOR, UserRole.ADMIN]
    }
    return user
  }

  private async _create(data: OrganisationCreate) {
    return await firstValueFrom(this._orgApi.createOrganisation(data).pipe(take(1)))
  }

  get organisation() {
    return this._organisation.value
  }
}
