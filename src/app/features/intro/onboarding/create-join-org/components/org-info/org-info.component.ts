/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, ContentChild, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-org-info',
  templateUrl: './org-info.component.html',
  styleUrl: './org-info.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrgInfoComponent {
  
  @ContentChild('image') image!: TemplateRef<any>
  @ContentChild('name') name!: TemplateRef<any>
  @ContentChild('businessID') businessID!: TemplateRef<any>
  @ContentChild('location') location!: TemplateRef<any>
  @ContentChild('description') description!: TemplateRef<any>
  @ContentChild('action') action!: TemplateRef<any>

  @Input() editable: boolean = false

}
