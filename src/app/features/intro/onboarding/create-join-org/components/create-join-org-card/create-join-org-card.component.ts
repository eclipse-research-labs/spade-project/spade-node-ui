/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, ContentChild, Input, TemplateRef } from '@angular/core'
import { UserService } from '@core/services/user/user.service'

@Component({
  selector: 'app-create-join-org-card',
  templateUrl: './create-join-org-card.component.html',
  styleUrl: './create-join-org-card.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateJoinOrgCardComponent {
  @Input() title!: string
  @Input() wide!: boolean
  @Input() routeOnBack?: string

  @ContentChild('leading') leading?: TemplateRef<any> | undefined | null

  constructor(private _userService: UserService) {}

  async onClose() {
    await this._userService.logout()
  }
}
