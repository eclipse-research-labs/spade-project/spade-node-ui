/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { ConnectToNodeService } from '../connect-to-node.service'

@Component({
  selector: 'app-custom',
  templateUrl: './custom.component.html',
  styleUrl: './custom.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomComponent implements OnInit {

  constructor(private _service: ConnectToNodeService) {}

  ngOnInit(): void {
    this._service.updateBroker(null)
  }
}
