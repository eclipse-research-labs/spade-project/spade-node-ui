/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ConnectToNodeService } from './connect-to-node.service';

@Component({
  selector: 'app-connect-to-node',
  templateUrl: './connect-to-node.component.html',
  styleUrl: './connect-to-node.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ConnectToNodeService]
})
export class ConnectToNodeComponent {

}
