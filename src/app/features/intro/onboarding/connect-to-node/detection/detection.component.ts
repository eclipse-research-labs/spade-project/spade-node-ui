/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { ConnectToNodeService } from '../connect-to-node.service'
import { UserService } from '@core/services/user/user.service'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { encodeSBUrl } from 'src/app/utils'

@Component({
  selector: 'app-detection',
  templateUrl: './detection.component.html',
  styleUrl: './detection.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetectionComponent implements OnInit {
  constructor(private _service: ConnectToNodeService, private _userService: UserService, private _router: Router, private _myNodeService: MyNodeService) {}

  wasAutodetected = false

  async ngOnInit(): Promise<void> {
    await this.runDetection()
  }

  async runDetection() {
    this._myNodeService.clearHealthcheckError()
    await this._service.init()
    this.wasAutodetected = this._service.broker != null
  }

  connect() {
    this._router.navigateByUrl(`/onboarding/connect-node/${encodeSBUrl(this._service.broker?.address ?? '')}`)
  }

  connectToDifferentBroker() {
    if (this.wasAutodetected || this._myNodeService.healthcheckError) {
      this._router.navigateByUrl('/onboarding/connect-to-node/custom-address')
    } else {
      this._service.updateBroker(null)
    }
  }

  get initializing$() {
    return this._service.initializing$
  }

  get broker$() {
    return this._service.broker$
  }

  get isLoggedIn() {
    return this._userService.isLoggedIn
  }

  get healthcheckError$() {
    return this._myNodeService.healthcheckError$
  }

  get healthcheckError() {
    return this._myNodeService.healthcheckError
  }
}
