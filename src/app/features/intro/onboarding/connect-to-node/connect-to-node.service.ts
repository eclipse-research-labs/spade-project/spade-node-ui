/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { DB } from '@core/models/node.model'
import { BaseService } from '@core/services/base'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { BehaviorSubject } from 'rxjs'
import { delay } from 'src/app/utils'

@Injectable()
export class ConnectToNodeService extends BaseService {
  private _broker = new BehaviorSubject<DB | null>(null)

  broker$ = this._broker.asObservable()

  constructor(private _myNodeService: MyNodeService) {
    super()
  }

  async init() {
    const broker = await this.initApiWrapper(this._runDetection())
    this._broker.next(broker)
  }

  async runDetection(address: string) {
    const broker = await this.loadApiWrapper(this._runDetection(address))
    this._broker.next(broker)
  }

  private async _runDetection(address?: string) {
    await delay(1000)
    await this._myNodeService.healthcheck(address)
    return this._myNodeService.broker
  }

  updateBroker(broker: DB | null) {
    this._broker.next(broker)
  }

  get broker() {
    return this._broker.value
  }
}
