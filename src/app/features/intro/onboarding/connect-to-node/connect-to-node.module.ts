/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { ConnectToNodeRoutingModule } from './connect-to-node-routing.module'

import { ConnectToNodeComponent } from './connect-to-node.component'
import { LandingComponent } from './landing/landing.component'
import { DetectionComponent } from './detection/detection.component'
import { CustomComponent } from './custom/custom.component'
import { CustomNodeAddressComponent } from './components/custom-node-address/custom-node-address.component'
import { ConnectToNodeCardComponent } from './components/connect-to-node-card/connect-to-node-card.component'
import { ConnectButtonComponent } from './components/connect-button/connect-button.component'
import { UseOfflineButtonComponent } from './components/use-offline-button/use-offline-button.component'
import { SslErrorComponent } from './components/ssl-error/ssl-error.component'
import { DividerComponent } from '@shared/components/presentation/reusable/divider/divider.component'
import { SpinnerComponent } from '@shared/components/presentation/reusable/loaders/spinner/spinner.component'
import { DataBrokerCardComponent } from '@shared/components/presentation/app-specific/onboarding/data-broker-card/data-broker-card.component'
import { AcceptSslComponent } from '@shared/components/smart/accept-ssl/accept-ssl.component'
import { OnboardingCardComponent } from '@shared/components/presentation/app-specific/onboarding/onboarding-card/onboarding-card.component'
import { FormsModule } from '@angular/forms'
import { NgLetDirective } from 'ng-let'
import { ButtonModule } from 'primeng/button'
import { MessageModule } from 'primeng/message'
import { InputTextModule } from 'primeng/inputtext'

@NgModule({
  declarations: [
    ConnectToNodeComponent,
    LandingComponent,
    DetectionComponent,
    CustomComponent,
    CustomNodeAddressComponent,
    ConnectToNodeCardComponent,
    ConnectButtonComponent,
    UseOfflineButtonComponent,
    SslErrorComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ConnectToNodeRoutingModule,
    DividerComponent,
    SpinnerComponent,
    DataBrokerCardComponent,
    AcceptSslComponent,
    OnboardingCardComponent,
    NgLetDirective,
    ButtonModule,
    MessageModule,
    InputTextModule,
  ],
})
export class ConnectToNodeModule {}
