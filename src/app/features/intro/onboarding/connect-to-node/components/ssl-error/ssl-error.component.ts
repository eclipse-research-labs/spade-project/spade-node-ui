/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-ssl-error',
  templateUrl: './ssl-error.component.html',
  styleUrl: './ssl-error.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SslErrorComponent {

  @Input() address!: string
  
}
