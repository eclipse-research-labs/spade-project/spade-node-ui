/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '@core/services/user/user.service';
import { ConnectToNodeService } from '../../connect-to-node.service';
import { encodeSBUrl } from 'src/app/utils';

@Component({
  selector: 'app-connect-button',
  templateUrl: './connect-button.component.html',
  styleUrl: './connect-button.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConnectButtonComponent {
  constructor(private _service: ConnectToNodeService, private _userService: UserService, private _router: Router) {}

  connect() {
    if (this._service.broker?.online) {
      this._router.navigateByUrl('/marketplace/services')
    } else {
      this._router.navigateByUrl(`/onboarding/connect-node/${encodeSBUrl(this._service.broker?.address ?? '')}`)
    }
  }

  get broker$() {
    return this._service.broker$
  }

  get isLoggedIn() {
    return this._userService.isLoggedIn
  }
}
