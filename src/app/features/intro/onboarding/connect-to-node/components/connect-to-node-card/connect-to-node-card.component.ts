/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { Router } from '@angular/router'
import { UserService } from '@core/services/user/user.service'

@Component({
  selector: 'app-connect-to-node-card',
  templateUrl: './connect-to-node-card.component.html',
  styleUrl: './connect-to-node-card.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConnectToNodeCardComponent {
  @Input() title!: string
  @Input() routeOnBack?: string

  constructor(private _userService: UserService, private _router: Router) {}

  onClose() {
    if (this._userService.isLoggedIn) {
      this._router.navigateByUrl('/marketplace/services')
    } else {
      this._router.navigateByUrl('/')
    }
  }
}
