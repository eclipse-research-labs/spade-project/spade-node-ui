/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ConnectToNodeService } from '../../connect-to-node.service';
import { Router } from '@angular/router';
import { encodeSBUrl } from 'src/app/utils';
import { UserService } from '@core/services/user/user.service';

@Component({
  selector: 'app-use-offline-button',
  templateUrl: './use-offline-button.component.html',
  styleUrl: './use-offline-button.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UseOfflineButtonComponent {
  constructor(private _router: Router, private _service: ConnectToNodeService, private _userService: UserService) {}

  async skip() {
    const address = this._service.broker?.address
    if (address) {
      const redirectUri = `/my-node/offline/${encodeSBUrl(address)}/items`
      if (this._userService.isLoggedIn) {
        await this._userService.logout(redirectUri)
      } else {
        await this._router.navigateByUrl(redirectUri)
      }
    }
  }

  get broker$() {
    return this._service.broker$
  }
}
