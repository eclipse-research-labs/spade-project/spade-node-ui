/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { ConnectToNodeService } from '../../connect-to-node.service'
import { UserService } from '@core/services/user/user.service'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { encodeSBUrl } from 'src/app/utils'

@Component({
  selector: 'app-custom-node-address',
  templateUrl: './custom-node-address.component.html',
  styleUrl: './custom-node-address.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomNodeAddressComponent implements OnInit {
  address = ''

  constructor(private _service: ConnectToNodeService, private _userService: UserService, private _router: Router, private _myNodeService: MyNodeService) {}

  ngOnInit(): void {
    this._myNodeService.clearHealthcheckError()
  }

  async detect() {
    if (this.address) {
      this._myNodeService.clearHealthcheckError()
      await this._service.runDetection(this.address)
    }
  }

  connect() {
    const address = this._service.broker?.address
    if (address) {
      this._router.navigateByUrl(`/onboarding/connect-node/${encodeSBUrl(address)}`)
    }
  }

  get broker$() {
    return this._service.broker$
  }

  get loading$() {
    return this._service.loading$
  }

  get isLoggedIn() {
    return this._userService.isLoggedIn
  }

  get healthcheckError$() {
    return this._myNodeService.healthcheckError$
  }
}
