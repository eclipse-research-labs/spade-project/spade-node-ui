/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { LandingComponent } from './landing/landing.component'
import { ConnectToNodeComponent } from './connect-to-node.component'
import { CustomComponent } from './custom/custom.component'
import { DetectionComponent } from './detection/detection.component'

const routes: Routes = [
  {
    path: '',
    component: ConnectToNodeComponent,
    data: { roles: [] },
    children: [
      {
        path: '',
        component: LandingComponent,
      },
      {
        path: 'detection',
        component: DetectionComponent,
      },
      {
        path: 'custom-address',
        component: CustomComponent,
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConnectToNodeRoutingModule {}
