/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { HttpErrorResponse } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { NodesApiService } from '@core/api/modules/nodes'
import { CerthPayload, DB, DNSCheck } from '@core/models/node.model'
import { BasicAuth } from '@core/models/user.model'
import { BaseService } from '@core/services/base'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { UserService } from '@core/services/user/user.service'
import { BehaviorSubject, defer, firstValueFrom, retry, take, timer } from 'rxjs'
import { encodeSBUrl, parseNodeHost } from 'src/app/utils'

const _homeUrl = '/'
const _onboardingUrl = _homeUrl + 'onboarding'
const _connectNodeUrl = _onboardingUrl + '/connect-node'

const interval = 400

@Injectable()
export class ConnectNodeService extends BaseService {
  private _authRequired = new BehaviorSubject<boolean>(false)
  private _basicAuth = new BehaviorSubject<BasicAuth | undefined>(undefined)
  private _broker = new BehaviorSubject<DB | undefined>(undefined)
  private _name = new BehaviorSubject<string | undefined>(undefined)
  private _domain = new BehaviorSubject<string | undefined>(undefined)
  private _DNSCheck = new BehaviorSubject<DNSCheck | undefined>(undefined)

  name$ = this._name.asObservable()
  domain$ = this._domain.asObservable()
  broker$ = this._broker.asObservable()
  DNSCheck$ = this._DNSCheck.asObservable()
  basicAuth$ = this._basicAuth.asObservable()
  authRequired$ = this._authRequired.asObservable()


  private _baseUrl!: string
  private _authUrl!: string
  private _nameUrl!: string
  private _domainUrl!: string

  constructor(private _nodesApi: NodesApiService, private _myNodeService: MyNodeService, private _userService: UserService, private _router: Router) {
    super()
  }

  async initBroker(address: string) {
    if (address.length > 0 && (!this.broker || this.broker.address != address)) {
      const broker = await this.initApiWrapper(this._initBroker(address))
      if (broker) {
        this._broker.next(broker)
        this._composeConnectUrls()
      }
    }
  }

  private async _initBroker(address: string) {
    await this._myNodeService.healthcheck(address)
    const broker = this._myNodeService.broker
    if (!broker) {
      throw Error('Failed to init broker')
    }
    try {
      await this._testAuth(broker.address)
    } catch (e) {
      if (e instanceof HttpErrorResponse && (e.status == 401 || e.status == 400)) {
        this._authRequired.next(true)
      } else {
        throw e
      }
    }
    return broker
  }

  testAuth() {
    if (this.authRequired && !this.basicAuth) {
      this._router.navigateByUrl(this._authUrl, { replaceUrl: true })
    }
  }

  testName() {
    if (!this.name) {
      this._router.navigateByUrl(this._nameUrl, { replaceUrl: true })
    }
  }

  testDomain() {
    if (!this.domain) {
      this._router.navigateByUrl(this._domainUrl, { replaceUrl: true })
    }
  }

  async authenticate(auth: BasicAuth) {
    await this.loadApiWrapper(this._authenticate(auth))
    this._basicAuth.next(auth)
  }

  async connectNode(name: string, host: string, sbUrl: string) {
    const node = await this.loadApiWrapper(this._connectNode(name, host, sbUrl))
    this._myNodeService.addNode(node)
  }

  updateName(name: string) {
    this._name.next(name)
  }

  updateDomain(domain: string) {
    this._domain.next(domain)
  }

  async dnsCheck(host: string) {
    const check = await this.loadApiWrapper(this._dnsCheck(host))
    this._DNSCheck.next(check)
  }

  private async _authenticate(auth: BasicAuth) {
    await this._testAuth(this.broker!.address, auth)
  }

  private async _dnsCheck(host: string) {
    return await firstValueFrom(this._nodesApi.checkDNS(host).pipe(take(1)))
  }

  private async _testAuth(host: string, auth?: BasicAuth) {
    await firstValueFrom(this._nodesApi.testAuth(host, auth ?? this._myNodeService.basicAuth ?? undefined).pipe(take(1)))
  }

  private _composeConnectUrls() {
    const address = this.broker!.address
    this._baseUrl = _connectNodeUrl + '/' + encodeSBUrl(address)
    this._authUrl = this._baseUrl + '/auth'
    this._nameUrl = this._baseUrl + '/name'
    this._domainUrl = this._baseUrl + '/domain'
  }

  private async _connectNode(name: string, host: string, sbUrl: string) {
    const org = this._userService.user!.organisation!
    const cid = org.cid
    const node = await firstValueFrom(this._nodesApi.registerNode(name, host).pipe(take(1)))
    const nodeId = node.nodeId
    const country = org.countryCode
    host = parseNodeHost(host)
    try {
      const certhPayload: CerthPayload = {
        cid,
        nodeId,
        host,
        country,
      }
      const csr = await firstValueFrom(this._nodesApi.enablePlatformMode(certhPayload, sbUrl, this.basicAuth).pipe(take(1)))
      const cert = await firstValueFrom(this._nodesApi.signMyNodeCert(csr).pipe(take(1)))
      await firstValueFrom(this._nodesApi.sendCert(cert, sbUrl, this.basicAuth).pipe(take(1)))
      await this._waitForBrokerToRestart(host)
      return node
    } catch (e) {
      await firstValueFrom(this._nodesApi.disablePlatformMode(sbUrl).pipe(take(1)))
      throw e
    }
  }

  private async _waitForBrokerToRestart(sbUrl?: string) {
    await firstValueFrom(
      defer(() => this._myNodeService.healthcheck(sbUrl, true))
        .pipe(
          retry({
            count: 10,
            delay: (_, index) => {
              console.log('retrying connection to Data Broker for ' + index + ' time')
              const delay = Math.pow(2, index - 1) * interval
              return timer(delay)
            },
          })
        )
        .pipe(take(1))
    )
  }

  get broker() {
    return this._broker.value
  }

  get authRequired() {
    return this._authRequired.value
  }

  get basicAuth() {
    return this._basicAuth.value
  }

  get name() {
    return this._name.value
  }

  get domain() {
    return this._domain.value
  }

  get DNSCheck() {
    return this._DNSCheck.value
  }
}
