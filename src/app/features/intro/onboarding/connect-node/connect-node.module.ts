/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { ConnectNodeRoutingModule } from './connect-node-routing.module'

import { ConnectNodeComponent } from './connect-node.component'
import { LandingComponent } from './landing/landing.component'
import { NameComponent } from './name/name.component'
import { DomainComponent } from './domain/domain.component'
import { AuthenticateComponent } from './authenticate/authenticate.component'
import { TestDomainComponent } from './domain/components/test-domain/test-domain.component'
import { StartUsingNodeComponent } from './start-using-node/start-using-node.component'
import { ConnectNodeCardComponent } from './components/connect-node-card/connect-node-card.component'
import { InfoComponent } from '@shared/components/presentation/reusable/info/info.component'
import { SpinnerComponent } from '@shared/components/presentation/reusable/loaders/spinner/spinner.component'
import { DataBrokerCardComponent } from '@shared/components/presentation/app-specific/onboarding/data-broker-card/data-broker-card.component'
import { DividerComponent } from '@shared/components/presentation/reusable/divider/divider.component'
import { OnboardingCardComponent } from '@shared/components/presentation/app-specific/onboarding/onboarding-card/onboarding-card.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgLetDirective } from 'ng-let'
import { ButtonModule } from 'primeng/button'
import { MessageModule } from 'primeng/message'
import { PasswordModule } from 'primeng/password'
import { InputTextModule } from 'primeng/inputtext'

@NgModule({
  declarations: [
    ConnectNodeComponent,
    AuthenticateComponent,
    LandingComponent,
    NameComponent,
    DomainComponent,
    TestDomainComponent,
    StartUsingNodeComponent,
    ConnectNodeCardComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ConnectNodeRoutingModule,
    InfoComponent,
    SpinnerComponent,
    DataBrokerCardComponent,
    DividerComponent,
    OnboardingCardComponent,
    NgLetDirective,
    ButtonModule,
    MessageModule,
    PasswordModule,
    InputTextModule,
  ],
})
export class ConnectNodeModule {}
