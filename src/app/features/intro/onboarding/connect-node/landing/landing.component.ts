/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { UserService } from '@core/services/user/user.service'
import { ConnectNodeService } from '../connect-node.service'
import { Router } from '@angular/router'
import { encodeSBUrl } from 'src/app/utils'

const _steps = ['Provide the name of the Broker', 'Provide the domain under which your Broker is accessible']
const _authRequiredSteps = ['Authenticate the Broker', ..._steps]

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrl: './landing.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingComponent {
  constructor(private _service: ConnectNodeService, private _userService: UserService, private _router: Router) {}

  continue() {
    const address = this._service.broker?.address
    const authRequired = this._service.authRequired
    if (address) {
      this._router.navigateByUrl(`/onboarding/connect-node/${encodeSBUrl(address)}/${authRequired ? 'auth' : 'name'}`)
    }
  }

  get user$() {
    return this._userService.user$
  }

  get steps() {
    return this._service.authRequired ? _authRequiredSteps : _steps
  }
}
