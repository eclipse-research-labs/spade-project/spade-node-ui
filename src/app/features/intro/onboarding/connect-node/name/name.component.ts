/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { ConnectNodeService } from '../connect-node.service'
import { Router } from '@angular/router'
import { encodeSBUrl } from 'src/app/utils'

@Component({
  selector: 'app-name',
  templateUrl: './name.component.html',
  styleUrl: './name.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NameComponent implements OnInit {
  name: string = ''

  constructor(private _service: ConnectNodeService, private _router: Router) {}

  ngOnInit(): void {
    this._service.testAuth()
    this.name = this._service.name ?? ''
  }

  continue() {
    const address = this._service.broker?.address
    const name = this.name
    if (address && name && name.length > 0) {
      this._service.updateName(this.name)
      this._router.navigateByUrl(`/onboarding/connect-node/${encodeSBUrl(address)}/domain`)
    }
  }

  get authRequired() {
    return this._service.authRequired
  }
}
