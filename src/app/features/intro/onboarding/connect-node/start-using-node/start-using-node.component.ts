/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '@core/services/user/user.service'
import { Node } from '@core/models/node.model'
import { User } from '@core/models/user.model'
import { Observable } from 'rxjs'
import { MyNodeService } from '@core/services/my-node/my-node.service';
import { ConnectNodeService } from '../connect-node.service';

@Component({
  selector: 'app-start-using-node',
  templateUrl: './start-using-node.component.html',
  styleUrl: './start-using-node.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StartUsingNodeComponent {

  constructor(private _router: Router, private _myNodeService: MyNodeService, private _userService: UserService) {}
  
  startUsingNode() {
    this._router.navigateByUrl(`/my-node/online/${this._myNodeService.node?.nodeId}`)
  }

  get node$(): Observable<Node | null> {
    return this._myNodeService.node$
  }

  get user$(): Observable<User | null> {
    return this._userService.user$
  }

}
