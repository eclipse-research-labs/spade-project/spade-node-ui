/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { ConnectNodeService } from '../connect-node.service'
import { encodeSBUrl } from 'src/app/utils'
import { FormControl, FormGroup, Validators } from '@angular/forms'

@Component({
  selector: 'app-authenticate',
  templateUrl: './authenticate.component.html',
  styleUrl: './authenticate.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuthenticateComponent {
  authForm = new FormGroup({
    username: new FormControl('', { validators: [Validators.required] }),
    password: new FormControl('', { validators: [Validators.required] }),
  })

  constructor(private _service: ConnectNodeService, private _router: Router) {}

  async continue() {
    const address = this._service.broker?.address
    if (address && this.authForm.valid) {
      const username = this.authForm.controls.username.value!
      const password = this.authForm.controls.password.value!
      await this._service.authenticate({
        username,
        password,
      })
      this._router.navigateByUrl(`/onboarding/connect-node/${encodeSBUrl(address)}/name`)
    }
  }

  get loading$() {
    return this._service.loading$
  }

  get errorLoading$() {
    return this._service.errorLoading$
  }
}
