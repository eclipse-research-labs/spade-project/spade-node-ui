/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { userGuard } from '@core/guards/user.guard';
import { AuthenticateComponent } from './authenticate/authenticate.component';
import { ConnectNodeComponent } from './connect-node.component';
import { DomainComponent } from './domain/domain.component';
import { LandingComponent } from './landing/landing.component';
import { NameComponent } from './name/name.component';
import { StartUsingNodeComponent } from './start-using-node/start-using-node.component';

const routes: Routes = [
  {
    path: ':nodeAddress',
    component: ConnectNodeComponent,
    canActivate: [userGuard.watch],
    children: [
      {
        path: '',
        component: LandingComponent,
      },
      {
        path: 'auth',
        component: AuthenticateComponent,
      },
      {
        path: 'name',
        component: NameComponent,
      },
      {
        path: 'domain',
        component: DomainComponent,
      },
      {
        path: 'start',
        component: StartUsingNodeComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConnectNodeRoutingModule { }
