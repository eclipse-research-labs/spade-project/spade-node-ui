/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { ConnectNodeService } from '../connect-node.service'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { encodeSBUrl } from 'src/app/utils'

@Component({
  selector: 'app-domain',
  templateUrl: './domain.component.html',
  styleUrl: './domain.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DomainComponent implements OnInit {
  domain!: string

  private _url = ''

  constructor(private _service: ConnectNodeService, private _router: Router, private _snackBar: SnackBarService) {}

  ngOnInit(): void {
    this._service.testAuth()
    this._service.testName()
    this.domain = this._service.domain ?? ''
  }

  async connect() {
    const name = this._service.name ?? ''
    const domain = this.domain
    const address = this._service.broker?.address
    if (domain && domain.length > 0 && address) {
      this._service.updateDomain(this.domain)
      try {
        await this._service.connectNode(name, domain, address)
        this._snackBar.showSuccess(`You have successfully connected node: ${name}`)
        this._router.navigateByUrl(`/onboarding/connect-node/${encodeSBUrl(address)}/start`)
      } catch (e) {
        this._snackBar.showError('Failed to connect node to organisation. See the console for more information.')
      }
    }
  }

  get authRequired() {
    return this._service.authRequired
  }

  get loading$() {
    return this._service.loading$
  }
}
