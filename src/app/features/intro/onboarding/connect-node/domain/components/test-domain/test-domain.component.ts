/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { ConnectNodeService } from '../../../connect-node.service'

@Component({
  selector: 'app-test-domain',
  templateUrl: './test-domain.component.html',
  styleUrl: './test-domain.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestDomainComponent {
  @Input() domain!: string

  constructor(private _service: ConnectNodeService) {}

  async dnsCheck() {
    await this._service.dnsCheck(this.domain)
  }

  get DNSCheck$() {
    return this._service.DNSCheck$
  }

  get loading$() {
    return this._service.loading$
  }
}
