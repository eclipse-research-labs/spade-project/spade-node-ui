/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, ContentChild, Input, TemplateRef } from '@angular/core'
import { Router } from '@angular/router'
import { UserService } from '@core/services/user/user.service'

@Component({
  selector: 'app-connect-node-card',
  templateUrl: './connect-node-card.component.html',
  styleUrl: './connect-node-card.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConnectNodeCardComponent {
  @Input() title!: string
  @Input() showClose!: boolean
  @Input() routeOnBack?: string

  @ContentChild('pageIndicator') pageIndicator: TemplateRef<any> | undefined

  constructor(private _userService: UserService, private _router: Router) {}

  onClose() {
    if (this._userService.isLoggedIn) {
      this._router.navigateByUrl('/marketplace/services')
    } else {
      this._router.navigateByUrl('/')
    }
  }
}
