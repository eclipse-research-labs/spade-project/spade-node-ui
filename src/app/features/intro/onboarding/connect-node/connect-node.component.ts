/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { ConnectNodeService } from './connect-node.service'
import { ActivatedRoute } from '@angular/router'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { decodeSBUrl } from 'src/app/utils'

@UntilDestroy()
@Component({
  selector: 'app-connect-node',
  templateUrl: './connect-node.component.html',
  styleUrl: './connect-node.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ConnectNodeService],
})
export class ConnectNodeComponent {

  nodeAddress!: string

  constructor(private _activatedRoute: ActivatedRoute, private _service: ConnectNodeService, private _myNodeService: MyNodeService) {
    this._activatedRoute.paramMap.pipe(untilDestroyed(this)).subscribe(async (params) => {
      this.nodeAddress = decodeSBUrl(params.get('nodeAddress') ?? '')
      await this._initBroker()
    })
  }

  async onTryAgain() {
    await this._initBroker()
  }

  private async _initBroker() {
    if (this.nodeAddress) {
      await this._service.initBroker(this.nodeAddress)
    }
  }

  get initializing$() {
    return this._service.initializing$
  }

  get broker$() {
    return this._service.broker$
  }

  get errorInitializing$() {
    return this._service.errorInitializing$
  }

  get healthcheckError$() {
    return this._myNodeService.healthcheckError$
  }
}
