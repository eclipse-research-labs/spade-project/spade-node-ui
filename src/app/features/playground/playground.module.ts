/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { PlaygroundRoutingModule } from './playground-routing.module'
import { PlaygroundComponent } from './playground.component'
import { BigFileTestComponent } from './big-file-test/big-file-test.component'
import { UploadFileComponent } from './big-file-test/components/upload-file/upload-file.component'
import { EuContribComponent } from '@shared/components/presentation/app-specific/eu-contrib/eu-contrib.component'
import { HeaderComponent } from '@shared/components/smart/header/header.component'
import { SpinnerComponent } from '@shared/components/presentation/reusable/loaders/spinner/spinner.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgLetDirective } from 'ng-let'
import { ProgressBarModule } from 'primeng/progressbar'
import { ButtonModule } from 'primeng/button'
import { FileUploadComponent } from '@shared/components/presentation/reusable/inputs/file-upload/file-upload.component'

@NgModule({
  declarations: [PlaygroundComponent, BigFileTestComponent, UploadFileComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PlaygroundRoutingModule,
    EuContribComponent,
    HeaderComponent,
    SpinnerComponent,
    NgLetDirective,
    ProgressBarModule,
    ButtonModule,
    FileUploadComponent
  ],
})
export class PlaygroundModule {}
