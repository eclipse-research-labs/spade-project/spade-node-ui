/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { HttpClient, HttpEventType } from '@angular/common/http'
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core'
import { FormControl, Validators } from '@angular/forms'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { DynamicDialogRef } from 'primeng/dynamicdialog'
import { BehaviorSubject, firstValueFrom, Observable, take } from 'rxjs'

interface _Progress {
  percent$: Observable<number>
  control: BehaviorSubject<number>
}

@UntilDestroy()
@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrl: './upload-file.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UploadFileComponent implements OnInit {
  fileControl = new FormControl<File | null>(null, {
    updateOn: 'change',
    validators: [Validators.required],
  })

  progress: Record<string, _Progress> = {}

  uploading = false
  uploaded = false

  constructor(private _dialogRef: DynamicDialogRef, private _http: HttpClient, private _cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.fileControl.valueChanges.pipe(untilDestroyed(this)).subscribe((file) => this._initProgress(file))
  }

  close() {
    this._dialogRef.close()
  }

  uploadFiles() {
    if (this.fileControl.valid && this.fileControl.value) {
      this.uploading = true
      const file = this.fileControl.value
      const reader = new FileReader()
      reader.readAsArrayBuffer(file)
      reader.onload = async () => {
        if (reader.result) {
          const blob = new Blob([reader.result], { type: file.type })
          await this._uploadFile(file.name, blob)
        }
      }
    }
  }

  private _initProgress(file: File | null) {
    this.progress = {}
    if (!file) return
    const control = new BehaviorSubject(0)
    const percent$ = control.asObservable()
    this.progress[file.name] = {
      control,
      percent$,
    }
  }

  private async _uploadFile(key: string, blob: Blob) {
    const formData = new FormData()
    formData.append('file', blob)
    this._upload(key, formData)
  }

  private _checkIfDone() {
    for (const [key, value] of Object.entries(this.progress)) {
      if (value.control.value < 100) return
    }
    this.uploading = false
    this.uploaded = true
  }

  private _upload(key: string, formData: FormData) {
    this._http
      .put('https://s3.joralmi.eu/default/' + key, formData, { reportProgress: true, observe: 'events' })
      .subscribe({
        next: (event: any) => {
          if (event && event.type === HttpEventType.UploadProgress) {
            this.progress[key].control.next(Math.round((100 * event.loaded) / event.total))
            this._checkIfDone()
            this._cd.markForCheck()
          }
        },
      })
  }

  private async _download() {
    return await firstValueFrom(
      this._http
        .get('https://s3.joralmi.eu/default/', {
          headers: {
            accept: 'text/xml',
            'Content-Type': 'text/xml',
          },
          responseType: 'text',
        })
        .pipe(take(1))
    )
  }
}
