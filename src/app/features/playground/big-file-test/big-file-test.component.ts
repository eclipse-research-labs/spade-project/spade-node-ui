/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog'
import { UploadFileComponent } from './components/upload-file/upload-file.component'
import { BigFileTestService } from './big-file-test.service'

@Component({
  selector: 'app-big-file-test',
  templateUrl: './big-file-test.component.html',
  styleUrl: './big-file-test.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DialogService, BigFileTestService],
})
export class BigFileTestComponent {
  private _ref: DynamicDialogRef | undefined

  constructor(private _dialogService: DialogService) {}

  openUploadFile() {
    this._ref = this._dialogService.open(UploadFileComponent, {
      styleClass: 'p-dialog-fullscreen',
      showHeader: false,
    })
  }
}
