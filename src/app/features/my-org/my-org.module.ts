/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { MyOrgRoutingModule } from './my-org-routing.module'
import { MyOrgComponent } from './my-org.component'
import { RoleCheckerComponent } from '@shared/components/smart/role-checker/role-checker.component'
import { InfoComponent } from '@shared/components/presentation/reusable/info/info.component'
import { HeaderComponent } from '@shared/components/smart/header/header.component'
import { SideNavComponent } from '@shared/components/presentation/reusable/side-nav/side-nav.component'
import { EuContribComponent } from '@shared/components/presentation/app-specific/eu-contrib/eu-contrib.component'
import { NgLetDirective } from 'ng-let'
import { AppStatusComponent } from '@shared/components/smart/app-status/app-status.component'

@NgModule({
  declarations: [MyOrgComponent],
  imports: [
    CommonModule,
    MyOrgRoutingModule,
    RoleCheckerComponent,
    InfoComponent,
    HeaderComponent,
    SideNavComponent,
    AppStatusComponent,
    EuContribComponent,
    NgLetDirective,
  ],
})
export class MyOrgModule {}
