/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { OrganisationsRoutingModule } from './organisations-routing.module'
import { OrganisationsComponent } from './organisations.component'
import { GridSkeletonComponent } from '@shared/components/presentation/reusable/loaders/grid-skeleton/grid-skeleton.component'
import { OrgTileComponent } from '@shared/components/presentation/app-specific/organisation/org-tile/org-tile.component'
import { SadFaceComponent } from '@shared/components/presentation/reusable/sad-face/sad-face.component'
import { NgLetDirective } from 'ng-let'
import { ButtonModule } from 'primeng/button'
import { TagModule } from 'primeng/tag'

@NgModule({
  declarations: [OrganisationsComponent],
  imports: [CommonModule, OrganisationsRoutingModule, GridSkeletonComponent, OrgTileComponent, SadFaceComponent, NgLetDirective, ButtonModule, TagModule],
})
export class OrganisationsModule {}
