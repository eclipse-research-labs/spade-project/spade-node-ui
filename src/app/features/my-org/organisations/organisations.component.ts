/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { Organisation } from '@core/models/organisation.model'
import { MyOrgService } from '@core/services/my-org/my-org.service'
import { PartnershipComponent } from '@shared/components/smart/partnership/partnership.component'
import { MenuItem } from 'primeng/api'
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog'

@Component({
  selector: 'app-organisations',
  templateUrl: './organisations.component.html',
  styleUrl: './organisations.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DialogService],
})
export class OrganisationsComponent {
  items: MenuItem[] = [
    {
      label: 'All',
      icon: 'fa fa-user-group',
    },
    {
      label: 'Partners',
      icon: 'fa fa-handshake-simple',
    },
  ]

  ref: DynamicDialogRef | undefined

  constructor(private _myOrgService: MyOrgService, private _dialogService: DialogService) {}

  openPartnersihp(organisation: Organisation) {
    this.ref = this._dialogService.open(PartnershipComponent, {
      header: 'Partnership',
      data: {
        cid: organisation.cid,
      },
    })
  }

  get organisations$() {
    return this._myOrgService.organisations$
  }

  get initializing$() {
    return this._myOrgService.initializing$
  }
}
