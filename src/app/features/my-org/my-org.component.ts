/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { MyOrgService } from '@core/services/my-org/my-org.service'
import { SideNavDivider, SideNavItem } from '@shared/models/side-nav.model'

@Component({
  selector: 'app-my-org',
  templateUrl: './my-org.component.html',
  styleUrl: './my-org.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MyOrgComponent implements OnInit {
  constructor(private _myOrgService: MyOrgService) {}

  async ngOnInit(): Promise<void> {
    await this._myOrgService.init()
  }

  nav: (SideNavItem | SideNavDivider)[] = [
    // {
    //   name: 'Home',
    //   path: '/my-org/home',
    //   icon: 'house-user',
    // },
    {
      name: 'Users',
      path: '/my-org/users',
      icon: 'fa fa-user-group',
    },
    // {
    //   name: 'Nodes',
    //   path: '/my-org/nodes',
    //   icon: 'circle-nodes',
    // },
    {
      label: 'Collaboration',
    },
    // {
    //   name: 'Items',
    //   path: '/my-org/items',
    //   icon: 'cube',
    // },
    {
      name: 'Organisations',
      path: '/my-org/organisations',
      icon: 'fa fa-house-user',
    },
  ]
}
