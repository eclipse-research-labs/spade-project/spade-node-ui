/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MyOrgService } from '@core/services/my-org/my-org.service';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { InviteUserComponent } from './components/invite-user/invite-user.component';
import { UserRole } from '@core/enums/user.enum';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrl: './users.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DialogService]
})
export class UsersComponent {

  ref: DynamicDialogRef | undefined

  constructor(private _myOrgService: MyOrgService, private _dialogService: DialogService) {}

  inviteUser() {
    this.ref = this._dialogService.open(InviteUserComponent, {
      header: 'Invite a user',
      height: '600px',
      width: '800px'
    })
  }

  get users$() {
    return this._myOrgService.users$
  }

  get initializing$() {
    return this._myOrgService.initializing$
  }

  get admin() {
    return UserRole.ADMIN
  }

}
