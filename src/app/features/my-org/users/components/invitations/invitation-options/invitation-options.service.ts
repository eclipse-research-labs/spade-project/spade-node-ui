/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core';
import { OrganisationApiService } from '@core/api/modules/organisations';
import { Invitation } from '@core/models/invitation.model';
import { BaseService } from '@core/services/base';
import { MyOrgService } from '@core/services/my-org/my-org.service';
import { firstValueFrom, take } from 'rxjs';

@Injectable()
export class InvitationOptionsService extends BaseService {

  constructor(private _orgApi: OrganisationApiService, private _myOrgService: MyOrgService) {
    super()
  }

  async deleteInvitation(invitation: Invitation) {
    await this.loadApiWrapper(this._deleteInvitation(invitation))
    this._myOrgService.updateInvitations(this._myOrgService.invitations.filter((element) => element.code != invitation.code))
  }

  async resendInvitation(invitation: Invitation) {
    const renewedInvitation = await this.loadApiWrapper(this._resendInvitation(invitation))
    this._myOrgService.updateInvitations([renewedInvitation, ...this._myOrgService.invitations.filter((element) => element.code != invitation.code)])
  }

  async _deleteInvitation(invitation: Invitation) {
    await firstValueFrom(this._orgApi.deleteCurrentOrganisationInvitation(invitation.code).pipe(take(1)))
  }

  async _resendInvitation(invitation: Invitation) {
    return await firstValueFrom(this._orgApi.renewCurrentOrganisationInvitation(invitation.code).pipe(take(1)))
  }
}
