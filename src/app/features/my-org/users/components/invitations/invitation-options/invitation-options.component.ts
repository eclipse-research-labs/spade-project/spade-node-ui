/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { InvitationOptionsService } from './invitation-options.service'
import { Invitation } from '@core/models/invitation.model'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { ConfirmationService, MenuItem } from 'primeng/api'
import { DynamicDialogRef } from 'primeng/dynamicdialog'
import { UserService } from '@core/services/user/user.service'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { UserRole } from '@core/enums/user.enum'

@UntilDestroy()
@Component({
  selector: 'app-invitation-options',
  templateUrl: './invitation-options.component.html',
  styleUrl: './invitation-options.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ConfirmationService, InvitationOptionsService],
})
export class InvitationOptionsComponent {
  @Input() invitation!: Invitation

  ref: DynamicDialogRef | undefined

  options!: MenuItem[] | undefined

  private _authorized = true

  constructor(
    private _confirmationService: ConfirmationService,
    private _service: InvitationOptionsService,
    private _userService: UserService,
    private _snackBar: SnackBarService
  ) {}

  ngOnInit(): void {
    this._initOptions()
    this._listenForUserChange()
  }

  async resendInvitation(event: Event) {
    await this._service.resendInvitation(this.invitation)
    this._snackBar.showSuccess('Invitation resend')
  }

  deleteInvitation(event: Event) {
    this._confirmationService.confirm({
      target: event.target as EventTarget,
      header: 'Delete Invitation?',
      acceptLabel: 'Delete',
      rejectLabel: 'Cancel',
      acceptIcon: 'fa fa-trash-can',
      rejectIcon: 'pi pi-times',
      acceptButtonStyleClass: 'p-button-text p-button-danger',
      rejectButtonStyleClass: 'p-button-text p-button-secondary',
      accept: async () => {
        await this._deleteInvitation()
      },
    })
  }

  private async _deleteInvitation() {
    await this._service.deleteInvitation(this.invitation)
    this._snackBar.showSuccess('Invitation deleted')
  }

  private _listenForUserChange() {
    this._userService.user$.pipe(untilDestroyed(this)).subscribe((user) => {
      this._authorized = user?.roles.includes(UserRole.ADMIN) || false
      this._initOptions()
    })
  }

  private _initOptions() {
    this.options = [
      {
        label: 'Settings',
        items: [
          {
            label: 'Resend',
            icon: 'pi pi-refresh',
            disabled: !this._authorized,
            tooltipOptions: {
              appendTo: 'body',
              tooltipPosition: 'right',
              tooltipLabel: 'Insufficient role to resend invitation',
              disabled: this._authorized,
            },
            command: (e) => {
              const event = e.originalEvent
              if (event) {
                this.resendInvitation(event)
              }
            },
          },
          {
            label: 'Delete',
            icon: 'fa fa-trash-can',
            disabled: !this._authorized,
            tooltipOptions: {
              appendTo: 'body',
              tooltipPosition: 'right',
              tooltipLabel: 'Insufficient role to delete invitation',
              disabled: this._authorized,
            },
            command: (e) => {
              const event = e.originalEvent
              if (event) {
                this.deleteInvitation(event)
              }
            },
          },
        ],
      },
    ]
  }

  get loading$() {
    return this._service.loading$
  }
}
