/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { UsersApiService } from '@core/api/modules/users'
import { User } from '@core/models/user.model'
import { BaseService } from '@core/services/base'
import { MyOrgService } from '@core/services/my-org/my-org.service'
import { firstValueFrom, take } from 'rxjs'

@Injectable()
export class ChangeRolesService extends BaseService {
  constructor(private _userApi: UsersApiService, private _myOrgService: MyOrgService) {
    super()
  }

  async changeRoles(user: User, roles: string[]) {
    const updated = await this.loadApiWrapper(this._changeRoles(user, roles))
    this._myOrgService.updateUsers(this._myOrgService.users.map((element) => element.uid == user.uid ? updated: element))
  }

  private async _changeRoles(user: User, roles: string[]) {
    return await firstValueFrom(this._userApi.updateUserRoles(user.uid, roles).pipe(take(1)))
  }
}
