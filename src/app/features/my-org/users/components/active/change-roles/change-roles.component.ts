/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog'
import { User } from '@core/models/user.model'
import { ChangeRolesService } from './change-roles.service'

interface _Role {
  title: string
  description: string
  selected: boolean
}

@Component({
  selector: 'app-change-roles',
  templateUrl: './change-roles.component.html',
  styleUrl: './change-roles.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ChangeRolesService],
})
export class ChangeRolesComponent {
  user!: User

  roles = new Map<string, _Role>([
    ['viewer', { title: 'Viewer', description: 'Plain role with no special privileges', selected: false }],
    ['node_operator', { title: 'Node Operator', description: 'Can register new Data Brokers', selected: false }],
    ['administrator', { title: 'Administrator', description: 'Can manage users and organisation data sharing rules', selected: false }],
  ])

  constructor(private _config: DynamicDialogConfig, private _service: ChangeRolesService, private _snackBar: SnackBarService, private _ref: DynamicDialogRef) {
    this.user = _config.data
    for (const key of this.user.roles) {
      this._setRole(key, true)
    }
  }

  toggleRole(key: string) {
    this._setRole(key, !this.roles.get(key)!.selected)
  }

  private _setRole(key: string, value: boolean) {
    const role = this.roles.get(key)!
    role.selected = value
    this.roles.set(key, role)
  }

  async changeRoles() {
    const roles = Array.from(this.roles.keys()).filter((element) => this.roles.get(element)!.selected)
    await this._service.changeRoles(this.user, roles)
    this._snackBar.showSuccess('Roles updated')
    this._ref.close()
  }

  get loading$() {
    return this._service.loading$
  }
}
