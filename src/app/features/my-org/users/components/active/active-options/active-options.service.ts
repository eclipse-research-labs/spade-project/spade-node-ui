/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { OrganisationApiService } from '@core/api/modules/organisations'
import { User } from '@core/models/user.model'
import { BaseService } from '@core/services/base'
import { MyOrgService } from '@core/services/my-org/my-org.service'
import { firstValueFrom, take } from 'rxjs'

@Injectable()
export class ActiveOptionsService extends BaseService {
  constructor(private _orgApi: OrganisationApiService, private _myOrgService: MyOrgService) {
    super()
  }

  async removeUser(user: User) {
    await this.loadApiWrapper(this._removeUser(user))
    this._myOrgService.updateUsers([...this._myOrgService.users.filter((element) => element.uid != user.uid)])
  }

  private async _removeUser(user: User) {
    await firstValueFrom(this._orgApi.removeCurrentOrganisationUser(user.uid).pipe(take(1)))
  }
}
