/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core'
import { User } from '@core/models/user.model'
import { ActiveOptionsService } from './active-options.service'
import { ConfirmationService, MenuItem } from 'primeng/api'
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog'
import { UserService } from '@core/services/user/user.service'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { ChangeRolesComponent } from '../change-roles/change-roles.component'
import { UserRole } from '@core/enums/user.enum'

@UntilDestroy()
@Component({
  selector: 'app-active-options',
  templateUrl: './active-options.component.html',
  styleUrl: './active-options.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ConfirmationService, DialogService, ActiveOptionsService],
})
export class ActiveOptionsComponent implements OnInit {
  @Input() user!: User

  ref: DynamicDialogRef | undefined

  options!: MenuItem[] | undefined

  private _authorized = true

  constructor(
    private _confirmationService: ConfirmationService,
    private _service: ActiveOptionsService,
    private _userService: UserService,
    private _snackBar: SnackBarService,
    private _dialogService: DialogService,
  ) {}

  ngOnInit(): void {
    this._initOptions()
    this._listenForUserChange()
  }

  changeRoles(event: Event) {
    this.ref = this._dialogService.open(ChangeRolesComponent, {
      header: 'Change User Roles',
      data: this.user,
    })
  }

  removeUser(event: Event) {
    this._confirmationService.confirm({
      target: event.target as EventTarget,
      header: 'Remove User from Organisation?',
      acceptLabel: 'Remove',
      rejectLabel: 'Cancel',
      acceptIcon: 'fa fa-trash-can',
      rejectIcon: 'pi pi-times',
      acceptButtonStyleClass: 'p-button-text p-button-danger',
      rejectButtonStyleClass: 'p-button-text p-button-secondary',
      accept: async () => {
        await this._removeUser()
      },
    })
  }

  private async _removeUser() {
    await this._service.removeUser(this.user)
    this._snackBar.showSuccess('User removed from organisation')
  }

  private _listenForUserChange() {
    this._userService.user$.pipe(untilDestroyed(this)).subscribe(user => {
      this._authorized = user?.roles.includes(UserRole.ADMIN) || false
      this._initOptions()
    })
  }

  private _initOptions() {
    this.options = [
      {
        label: 'Settings',
        items: [
          {
            label: 'Change Roles',
            icon: 'fa fa-user-pen',
            disabled: !this._authorized || this.myUser,
            tooltipOptions: {
              appendTo: 'body',
              tooltipPosition: 'right',
              tooltipLabel: 'Insufficient role to change user roles',
              disabled: this._authorized,
            },
            command: (e) => {
              const event = e.originalEvent
              if (event) {
                this.changeRoles(event)
              }
            },
          },
          {
            label: 'Remove',
            icon: 'fa fa-trash-can',
            disabled: !this._authorized || this.myUser,
            tooltipOptions: {
              appendTo: 'body',
              tooltipPosition: 'right',
              tooltipLabel: 'Insufficient role to remove user',
              disabled: this._authorized,
            },
            command: (e) => {
              const event = e.originalEvent
              if (event) {
                this.removeUser(event)
              }
            },
          },
        ],
      },
    ]
  }

  get myUser() {
    return this._userService.user?.uid == this.user.uid
  }

  get loading$() {
    return this._service.loading$
  }
}
