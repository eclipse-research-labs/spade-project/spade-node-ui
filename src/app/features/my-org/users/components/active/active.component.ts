/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { User } from '@core/models/user.model'
import { MyOrgService } from '@core/services/my-org/my-org.service'
import { UserService } from '@core/services/user/user.service'

@Component({
  selector: 'app-active',
  templateUrl: './active.component.html',
  styleUrl: './active.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActiveComponent {
  constructor(private _myOrgService: MyOrgService, private _userService: UserService) {}

  myUser(user: User) {
    return this._userService.user?.uid == user.uid
  }

  get users$() {
    return this._myOrgService.users$
  }

  get initializing$() {
    return this._myOrgService.initializing$
  }
}
