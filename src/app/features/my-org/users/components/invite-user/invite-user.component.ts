/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { FormGroup, FormControl, Validators, AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms'
import { MyOrgService } from '@core/services/my-org/my-org.service'
import { UntilDestroy } from '@ngneat/until-destroy'
import { DynamicDialogRef } from 'primeng/dynamicdialog'
import { BehaviorSubject } from 'rxjs'
import { inflect } from 'src/app/utils'
import { InviteUserService } from './invite-user.service'

@UntilDestroy()
@Component({
  selector: 'app-invite-user',
  templateUrl: './invite-user.component.html',
  styleUrl: './invite-user.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [InviteUserService],
})
export class InviteUserComponent implements OnInit {
  userForm!: FormGroup

  private _newInvitations$ = new BehaviorSubject<string[]>([])

  newInvitations$ = this._newInvitations$.asObservable()

  constructor(private _service: InviteUserService, private _myOrgService: MyOrgService, private _ref: DynamicDialogRef) {}

  ngOnInit(): void {
    this.userForm = new FormGroup(
      {
        email: new FormControl('', { validators: [Validators.required, Validators.email, this._customValidator()] }),
      },
      { updateOn: 'submit' }
    )
  }

  addInvitation() {
    if (this.userForm.valid) {
      const email = this.userForm.controls.email!.value!
      const invitations = this._newInvitations$.value
      this._newInvitations$.next([email, ...invitations])
      this.userForm.reset()
    }
  }

  countNewInvitations(invitations: string[]) {
    return invitations.reduce((acc, val) => (this._myOrgService.invitations.some((element) => element.email == val) ? acc : acc + 1), 0)
  }

  countRenewedInvitations(invitations: string[]) {
    return invitations.reduce((acc, val) => (this._myOrgService.invitations.some((element) => element.email == val) ? acc + 1 : acc), 0)
  }

  inflectInvitations(count: number) {
    return inflect(count, 'invitations', 'invitation', 'invitations')
  }

  async sendInvitations() {
    await this._service.inviteUsers(this._newInvitations$.value)
    this._ref.close()
  }

  private _customValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const users = this._myOrgService.users
      const newInvitations = this._newInvitations$.value
      const email = control.value as string
      const userExists = users.some((user) => user.email == email)
      const invitationExists = newInvitations.some((invitation) => invitation == email)
      return userExists || invitationExists
        ? {
            userExists,
            invitationExists,
          }
        : null
    }
  }

  get loading$() {
    return this._service.loading$
  }
}
