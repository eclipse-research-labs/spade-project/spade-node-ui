/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { OrganisationApiService } from '@core/api/modules/organisations'
import { BaseService } from '@core/services/base'
import { MyOrgService } from '@core/services/my-org/my-org.service'
import { firstValueFrom, take } from 'rxjs'

@Injectable()
export class InviteUserService extends BaseService {
  constructor(private _orgApi: OrganisationApiService, private _myOrgService: MyOrgService) {
    super()
  }

  async inviteUsers(emails: string[]) {
    const invitations = await this.loadApiWrapper(this._inviteUsers(emails))
    this._myOrgService.updateInvitations(invitations)
  }

  private _inviteUsers(emails: string[]) {
    return firstValueFrom(this._orgApi.sendCurrentOrganisationInvitations(emails).pipe(take(1)))
  }
}
