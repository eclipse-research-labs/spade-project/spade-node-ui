/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { UsersRoutingModule } from './users-routing.module'
import { UsersComponent } from './users.component'
import { ActiveComponent } from './components/active/active.component'
import { InvitationsComponent } from './components/invitations/invitations.component'
import { InviteUserComponent } from './components/invite-user/invite-user.component'
import { ChangeRolesComponent } from './components/active/change-roles/change-roles.component'
import { ActiveOptionsComponent } from './components/active/active-options/active-options.component'
import { InvitationOptionsComponent } from './components/invitations/invitation-options/invitation-options.component'
import { RoleCheckerComponent } from '@shared/components/smart/role-checker/role-checker.component'
import { InfoComponent } from '@shared/components/presentation/reusable/info/info.component'
import { SadFaceComponent } from '@shared/components/presentation/reusable/sad-face/sad-face.component'
import { NewInvitationPreviewComponent } from '@shared/components/presentation/app-specific/user/new-invitation-preview/new-invitation-preview.component'
import { InvitationPreviewComponent } from '@shared/components/presentation/app-specific/user/invitation-preview/invitation-preview.component'
import { TableSkeletonComponent } from '@shared/components/presentation/reusable/loaders/table-skeleton/table-skeleton.component'
import { ConfirmationDialogComponent } from '@shared/components/presentation/reusable/confirmation-dialog/confirmation-dialog.component'
import { ActionButtonComponent } from '@shared/components/presentation/reusable/action-button/action-button.component'
import { TypeIconComponent } from '@shared/components/presentation/app-specific/type-icon/type-icon.component'
import { ReactiveFormsModule } from '@angular/forms'
import { NgLetDirective } from 'ng-let'
import { TabViewModule } from 'primeng/tabview'
import { ButtonModule } from 'primeng/button'
import { TableModule } from 'primeng/table'
import { MenuModule } from 'primeng/menu'
import { TagModule } from 'primeng/tag'
import { InputTextModule } from 'primeng/inputtext'

@NgModule({
  declarations: [UsersComponent, ActiveComponent, InvitationsComponent, InviteUserComponent, ChangeRolesComponent, ActiveOptionsComponent, InvitationOptionsComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    RoleCheckerComponent,
    InfoComponent,
    SadFaceComponent,
    NewInvitationPreviewComponent,
    InvitationPreviewComponent,
    TableSkeletonComponent,
    ConfirmationDialogComponent,
    ActionButtonComponent,
    TypeIconComponent,
    ReactiveFormsModule,
    NgLetDirective,
    TabViewModule,
    ButtonModule,
    TableModule,
    MenuModule,
    TagModule,
    InputTextModule,
  ],
})
export class UsersModule {}
