/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { ItemsRoutingModule } from './items-routing.module'

import { ItemsComponent } from './items.component'
import { ItemDetailSkeletonComponent } from './components/item-detail-skeleton/item-detail-skeleton.component'
import { ItemDetailComponent } from './components/item-detail/item-detail.component'
import { AdapterConnectionComponent } from './my-node-items/my-node-items-props/adapter-connection/adapter-connection.component'
import { ChangeConnectionButtonComponent } from './my-node-items/my-node-items-props/components/change-connection-button/change-connection-button.component'
import { ConnectionDetailComponent } from './my-node-items/my-node-items-props/connection-detail/connection-detail.component'
import { ConnectionUrlComponent } from './my-node-items/my-node-items-props/components/connection-url/connection-url.component'
import { CreateConnectionButtonComponent } from './my-node-items/my-node-items-props/components/create-connection-button/create-connection-button.component'
import { DeleteConnectionButtonComponent } from './my-node-items/my-node-items-props/components/delete-connection-button/delete-connection-button.component'
import { ParamsListComponent } from './components/params-list/params-list.component'
import { SelectAdapterComponent } from './my-node-items/my-node-items-props/components/select-adapter/select-adapter.component'
import { ItemConsumptionComponent } from './foreign-items/foreign-items-props/item-consumption/item-consumption.component'
import { ConsumptionDetailComponent } from './foreign-items/foreign-items-props/consumption-detail/consumption-detail.component'
import { FloatingBackButtonComponent } from '@shared/components/presentation/app-specific/detail/floating-back-button/floating-back-button.component'
import { SadFaceComponent } from '@shared/components/presentation/reusable/sad-face/sad-face.component'
import { MenuComponent } from '@shared/components/presentation/reusable/menu/menu/menu.component'
import { MenuLabelComponent } from '@shared/components/presentation/reusable/menu/menu-label/menu-label.component'
import { MenuItemComponent } from '@shared/components/presentation/reusable/menu/menu-item/menu-item.component'
import { MenuContentComponent } from '@shared/components/presentation/reusable/menu/menu-content/menu-content.component'
import { AdapterPreviewComponent } from '@shared/components/presentation/app-specific/adapter/adapter-preview/adapter-preview.component'
import { AdapterHelpComponent } from '@shared/components/presentation/app-specific/adapter/adapter-help/adapter-help.component'
import { ConfirmationDialogComponent } from '@shared/components/presentation/reusable/confirmation-dialog/confirmation-dialog.component'
import { ActionButtonComponent } from '@shared/components/presentation/reusable/action-button/action-button.component'
import { RoleCheckerComponent } from '@shared/components/smart/role-checker/role-checker.component'
import { TypeIconComponent } from '@shared/components/presentation/app-specific/type-icon/type-icon.component'
import { InfoComponent } from '@shared/components/presentation/reusable/info/info.component'
import { LongIdComponent } from '@shared/components/presentation/reusable/long-id/long-id.component'
import { CodeEditorComponent } from '@shared/components/presentation/reusable/inputs/code-editor/code-editor.component'
import { PropInfoComponent } from '@shared/components/presentation/app-specific/prop/prop-info/prop-info.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgLetDirective } from 'ng-let'
import { ButtonModule } from 'primeng/button'
import { MessageModule } from 'primeng/message'
import { SidebarModule } from 'primeng/sidebar'
import { TabViewModule } from 'primeng/tabview'
import { SkeletonModule } from 'primeng/skeleton'
import { TagModule } from 'primeng/tag'
import { InputTextModule } from 'primeng/inputtext'
import { FileUploadComponent } from '@shared/components/presentation/reusable/inputs/file-upload/file-upload.component'
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ProgressBarModule } from 'primeng/progressbar'
import { ConsumptionsComponent } from '@shared/components/smart/consumptions/consumptions.component';
import { PayloadComponent } from './my-node-items/my-node-items-props/components/payload/payload.component';
import { SelectPayloadComponent } from './my-node-items/my-node-items-props/components/select-payload/select-payload.component'
import { AutoCompleteModule } from 'primeng/autocomplete';
import { BodyComponent } from './components/body/body.component'
import { TooltipModule } from 'primeng/tooltip';
import { MyNodeItemsPropsComponent } from './my-node-items/my-node-items-props/my-node-items-props.component';
import { MyNodeItemsEventsComponent } from './my-node-items/my-node-items-events/my-node-items-events.component';
import { ForeignItemsEventsComponent } from './foreign-items/foreign-items-events/foreign-items-events.component';
import { ForeignItemsPropsComponent } from './foreign-items/foreign-items-props/foreign-items-props.component';
import { ItemSubscriptionComponent } from './foreign-items/foreign-items-events/item-subscription/item-subscription.component'
import { SelectButtonModule } from 'primeng/selectbutton';
import { EventSubscriptionsComponent } from './my-node-items/my-node-items-events/event-subscriptions/event-subscriptions.component';
import { TableModule } from 'primeng/table'
import { NodePreviewComponent } from '@shared/components/presentation/app-specific/node/node-preview/node-preview.component'
import { OrgPreviewComponent } from '@shared/components/presentation/app-specific/organisation/org-preview/org-preview.component'

@NgModule({
  declarations: [
    ItemsComponent,
    ItemDetailSkeletonComponent,
    ItemDetailComponent,
    AdapterConnectionComponent,
    SelectAdapterComponent,
    ChangeConnectionButtonComponent,
    DeleteConnectionButtonComponent,
    CreateConnectionButtonComponent,
    ParamsListComponent,
    ConnectionUrlComponent,
    ConnectionDetailComponent,
    ItemConsumptionComponent,
    ConsumptionDetailComponent,
    PayloadComponent,
    SelectPayloadComponent,
    BodyComponent,
    MyNodeItemsPropsComponent,
    MyNodeItemsEventsComponent,
    ForeignItemsEventsComponent,
    ForeignItemsPropsComponent,
    ItemSubscriptionComponent,
    EventSubscriptionsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ItemsRoutingModule,
    FloatingBackButtonComponent,
    SadFaceComponent,
    MenuComponent,
    MenuLabelComponent,
    MenuItemComponent,
    MenuContentComponent,
    AdapterPreviewComponent,
    AdapterHelpComponent,
    ConfirmationDialogComponent,
    ActionButtonComponent,
    RoleCheckerComponent,
    TypeIconComponent,
    InfoComponent,
    LongIdComponent,
    CodeEditorComponent,
    PropInfoComponent,
    NgLetDirective,
    ButtonModule,
    MessageModule,
    SidebarModule,
    TabViewModule,
    SkeletonModule,
    TagModule,
    InputTextModule,
    FileUploadComponent,
    InputTextareaModule,
    ProgressBarModule,
    ConsumptionsComponent,
    AutoCompleteModule,
    TooltipModule,
    SelectButtonModule,
    TableModule,
    NodePreviewComponent,
    OrgPreviewComponent,
  ],
})
export class ItemsModule {}
