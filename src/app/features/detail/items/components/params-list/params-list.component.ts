/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren } from '@angular/core'

interface _Param {
  value: string
  isNew: boolean
}

@Component({
  selector: 'app-params-list',
  templateUrl: './params-list.component.html',
  styleUrl: './params-list.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ParamsListComponent implements OnInit {
  @Input() params: string[] = []
  @Input() values: string[] = []
  @Input() editMode: boolean = true
  @Input() loading: boolean = false

  @Output() onSave: EventEmitter<string[]> = new EventEmitter()

  @ViewChildren('inputs') inputs?: QueryList<ElementRef> | undefined | null

  editParams: boolean = false

  _params!: _Param[]

  constructor(private _cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    this._params = this._resetParams()
  }

  addParam() {
    if (this.loading) {
      return
    }
    if (!this.editParams) {
      this.editParams = true
    }
    this._params.push({ value: '', isNew: true })
    this.values.push('')
    this._cd.detectChanges()
    if (this.inputs) {
      this.inputs.last.nativeElement.focus()
    }
  }

  async removeParam(index: number) {
    if (this.loading) {
      return
    }
    if (!this.editParams) {
      this.editParams = true
    }
    this._params.splice(index, 1)
    this.values.splice(index, 1)
    if (this._params.length == 0) {
      await this.save()
    }
  }

  cancel() {
    this._params = this._resetParams()
    this.editParams = false
  }

  async save() {
    this._filterEmptyParams()
    this.params = [...this._params.map((element) => element.value)]
    this.onSave.emit(this.params)
  }

  trackByFn(index: number, item: _Param) {
    return index
  }

  private _filterEmptyParams() {
    this._params = this._params.filter((element) => element.value.trim().length > 0)
  }

  private _resetParams() {
    return [
      ...this.params.map((element) => {
        return { value: element, isNew: false }
      }),
    ]
  }
}
