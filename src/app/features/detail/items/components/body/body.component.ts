/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrl: './body.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BodyComponent {

  @Input() contentType?: string
  @Input() body!: FormControl<string | null>
  @Input() file!: FormControl<File | null>
  @Input() text!: FormControl<string | null>

  get noContentType() {
    return this.contentType == undefined || this.contentType == 'not-set'
  }

  get isBody() {
    return this.contentType && this.contentType == 'application/json'
  }

  get isText() {
    return this.contentType && this.contentType == 'text/plain'
  }

  get isFile() {
    return !this.isBody && !this.isText
  }
}
