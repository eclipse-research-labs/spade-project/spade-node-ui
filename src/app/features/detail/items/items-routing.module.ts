/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { offlineGuard } from '@core/guards/offline.guard'
import { onlineGuard } from '@core/guards/online.guard'
import { userGuard } from '@core/guards/user.guard'
import { ItemsComponent } from './items.component'
import { MyNodeItemsPropsComponent } from './my-node-items/my-node-items-props/my-node-items-props.component'
import { MyNodeItemsEventsComponent } from './my-node-items/my-node-items-events/my-node-items-events.component'
import { ForeignItemsPropsComponent } from './foreign-items/foreign-items-props/foreign-items-props.component'
import { ForeignItemsEventsComponent } from './foreign-items/foreign-items-events/foreign-items-events.component'

const routes: Routes = [
  {
    path: 'my-node-items/online',
    canActivate: [userGuard.watch],
    component: ItemsComponent,
    children: [
      {
        path: ':nodeId',
        canActivate: [onlineGuard.watch],
        children: [
          {
            path: ':oid/props/:pid',
            component: MyNodeItemsPropsComponent,
          },
          {
            path: ':oid/events/:eid',
            component: MyNodeItemsEventsComponent,
          },
        ],
      },
    ],
  },
  {
    path: 'my-node-items/offline/:nodeId',
    canActivate: [offlineGuard.watch],
    component: ItemsComponent,
    children: [
      {
        path: ':oid/props/:pid',
        component: MyNodeItemsPropsComponent,
      },
    ],
  },
  {
    path: 'foreign-items',
    canActivate: [userGuard.watch],
    component: ItemsComponent,
    children: [
      {
        path: ':nodeId',
        canActivate: [onlineGuard.watch],
        children: [
          {
            path: ':oid/props/:pid',
            component: ForeignItemsPropsComponent,
          },
          {
            path: ':oid/events/:eid',
            component: ForeignItemsEventsComponent,
          },
        ]
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ItemsRoutingModule {}
