/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core'
import { Adapter } from '@core/models/adapter.model'
import { trigger, transition, style, animate } from '@angular/animations'
import { TabViewChangeEvent } from 'primeng/tabview'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { UserRole } from '@core/enums/user.enum'
import { MyNodeItemsPropsService } from '../my-node-items-props.service'

@UntilDestroy()
@Component({
  selector: 'app-adapter-connection',
  templateUrl: './adapter-connection.component.html',
  styleUrl: './adapter-connection.component.scss',
  animations: [
    trigger('myInsertRemoveTrigger', [
      transition(':enter', [style({ opacity: 0 }), animate('600ms', style({ opacity: 1 }))]),
      transition(':leave', [animate('100ms', style({ opacity: 0 }))]),
    ]),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdapterConnectionComponent implements OnInit {
  activeIndex: number = 0
  editParams: boolean = false
  editOptionalParams: boolean = false

  adapter?: Adapter | undefined | null

  constructor(private _myNodeService: MyNodeService, private _service: MyNodeItemsPropsService) {}

  async ngOnInit(): Promise<void> {
    this._listenForActiveIndexChange()
    this._listenForAdapterConnectionChange()
  }

  onTabChange(event: TabViewChangeEvent) {
    const index = event.index
    this._service.updateActiveIndex(index)
  }

  onTryAgain() {
    this._myNodeService.init()
  }

  private _loadAdapter() {
    const adid = this.adapterConnections[this.activeIndex]?.adid
    this.adapter = this._myNodeService.adapters.find((element) => element.adid == adid)
  }

  private _listenForAdapterConnectionChange() {
    this._service.adapterConnections$.pipe(untilDestroyed(this)).subscribe((value) => {
      this._loadAdapter()
    })
  }

  private _listenForActiveIndexChange() {
    this._service.activeIndex$.pipe(untilDestroyed(this)).subscribe((value) => {
      this.activeIndex = value
      this._loadAdapter()
    })
  }

  get item$() {
    return this._service.item$
  }

  get item() {
    return this._service.item!
  }

  get adapterConnections$() {
    return this._service.adapterConnections$
  }

  get adapterConnections() {
    return this._service.adapterConnections
  }

  get op() {
    return this.adapterConnections[this.activeIndex]?.op
  }

  get noConnections() {
    return this.adapterConnections.length <= 0
  }

  get initializing$() {
    return this._service.initializing$
  }

  get errorInitializing$() {
    return this._service.errorInitializing$
  }

  get admin() {
    return UserRole.ADMIN
  }

  get nodeOperator() {
    return UserRole.NODE_OPERATOR
  }
}
