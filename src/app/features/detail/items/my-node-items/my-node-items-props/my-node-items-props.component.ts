/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute, ParamMap } from '@angular/router'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { combineLatest, map } from 'rxjs'
import { MyNodeItemsPropsService } from './my-node-items-props.service'

@UntilDestroy()
@Component({
  selector: 'app-my-node-items-props',
  templateUrl: './my-node-items-props.component.html',
  styleUrl: './my-node-items-props.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [MyNodeItemsPropsService],
})
export class MyNodeItemsPropsComponent implements OnInit {
  private _oid?: string
  private _iid?: string

  constructor(
    private _service: MyNodeItemsPropsService,
    private _myNodeService: MyNodeService,
    private _router: Router,
    private _route: ActivatedRoute
  ) {}

  async ngOnInit(): Promise<void> {
    await this._myNodeService.init()
    this._listenForParamChange()
  }

  goBack() {
    this._router.navigateByUrl(`${this.backUrl}/items/${this._oid}`, { replaceUrl: true })
  }

  async onTryAgain() {
    await this._myNodeService.init(true)
    await this._service.init(this._oid, this._iid)
  }

  private _listenForParamChange() {
    this._route.paramMap.pipe(untilDestroyed(this)).subscribe((params) => this._loadItem(params))
  }

  private async _loadItem(params: ParamMap) {
    this._oid = params.get('oid') ?? undefined
    this._iid = params.get('pid') ?? undefined
    await this._service.init(this._oid, this._iid)
  }

  get item() {
    return this._service.item
  }

  get item$() {
    return this._service.item$
  }

  get initializing$() {
    return combineLatest([this._myNodeService.initializing$, this._service.initializing$]).pipe(
      map(([x0, x1]) => x0 || x1)
    )
  }

  get errorInitializing$() {
    return combineLatest([this._myNodeService.errorInitializing$, this._service.errorInitializing$]).pipe(
      map(([x0, x1]) => x0 || x1)
    )
  }

  get backUrl() {
    return `/my-node/${this.brokerState}/${this.nodeState}`
  }

  get brokerState() {
    return this._myNodeService.brokerState
  }

  get nodeState() {
    return this._myNodeService.nodeState
  }
}
