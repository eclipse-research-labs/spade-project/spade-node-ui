/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { AdaptersApiService } from '@core/api/modules/adapters'
import { ItemsApiService } from '@core/api/modules/items'
import { AdapterConnection, AdapterConnectionCreate } from '@core/models/adapter.model'
import { ItemWithPropertyWithKey, Item } from '@core/models/item.model'
import { BaseService } from '@core/services/base'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'

@Injectable()
export class MyNodeItemsPropsService extends BaseService {
  private _adapterConnections = new BehaviorSubject<AdapterConnection[]>([])
  private _activeIndex = new BehaviorSubject<number>(0)
  private _item = new BehaviorSubject<ItemWithPropertyWithKey | null>(null)

  adapterConnections$ = this._adapterConnections.asObservable()
  activeIndex$ = this._activeIndex.asObservable()
  item$ = this._item.asObservable()

  private _oid!: string
  private _iid!: string

  constructor(
    private _adapterApi: AdaptersApiService,
    private _itemsApi: ItemsApiService,
    private _myNodeService: MyNodeService
  ) {
    super()
  }

  async init(oid?: string, iid?: string) {
    await this.initApiWrapper(this._init(oid, iid))
  }

  async createAdapterConnection(data: AdapterConnectionCreate) {
    await this.loadApiWrapper(this._createAdapterConnection(data))
  }

  async deleteAdapterConnection(adidconn: string) {
    await this.loadApiWrapper(this._deleteAdapterConnection(adidconn))
  }

  async changeAdapterConnection(adidconn: string, op: string, adid: string) {
    await this.loadApiWrapper(this._changeAdapterConnection(adidconn, op, adid))
  }

  async updateAdapterConnectionParams(adidconn: string, params: string[]) {
    await this.loadApiWrapper(this._updateAdapterConnectionParams(adidconn, params))
  }

  async updateAdapterConnectionOptionalParams(adidconn: string, optionalParams: string[]) {
    await this.loadApiWrapper(this._updateAdapterConnectionOptionalParams(adidconn, optionalParams))
  }

  async updateAdapterConnectionPath(adidconn: string, path: string) {
    await this.loadApiWrapper(this._updateAdapterConnectionPath(adidconn, path))
  }

  async updateAdapterConnectionIncomingContentType(adidconn: string, contentType: string) {
    await this.loadApiWrapper(this._updateAdapterConnectionIncomingContentType(adidconn, contentType))
  }

  async updateAdapterConnectionOutgoingContentType(adidconn: string, contentType: string) {
    await this.loadApiWrapper(this._updateAdapterConnectionOutgoingContentType(adidconn, contentType))
  }

  private async _init(oid?: string, iid?: string) {
    if (!oid) {
      throw Error('oid missing')
    }
    if (!iid) {
      throw Error('iid missing')
    }
    this._oid = oid
    this._iid = iid
    await Promise.all([this._initItem(), this._initAdapterConnections()])
  }

  private async _initItem(): Promise<void> {
    const item = this._myNodeService.myItems.find((element) => element.oid === this._oid)
    this._item.next(this._mergeItemWithProp(item))
  }

  private _mergeItemWithProp(item: Item | undefined): ItemWithPropertyWithKey {
    const oid = this._oid
    const iid = this._iid
    if (!item) {
      throw Error(`Item with oid: ${oid} does not exists`)
    }
    const props = item.properties
    if (!props) {
      throw Error('Item has no properties')
    }
    const prop = props[iid]
    if (!prop) {
      throw Error(`Prop with iid: ${iid} does not exists`)
    }
    return {
      ...item,
      prop: {
        key: iid,
        ...prop,
      },
    }
  }

  private async _initAdapterConnections(): Promise<void> {
    const oid = this._oid.split(':').at(-1)!
    const iid = this._iid
    const connections = await firstValueFrom(this._adapterApi.getAdapterConnections(this.sbUrl, oid, iid).pipe(take(1)))
    this._updateAdapterConnections(connections, false)
  }

  private async _createAdapterConnection(data: AdapterConnectionCreate) {
    const newConnection = await firstValueFrom(this._adapterApi.createAdapterConnection(this.sbUrl, data).pipe(take(1)))
    await this._updateAdapterConnections([...this.adapterConnections, newConnection])
    this._updateActiveIndex(newConnection)
  }

  private async _deleteAdapterConnection(adidconn: string) {
    await firstValueFrom(this._adapterApi.deleteAdapterConnection(this.sbUrl, adidconn).pipe(take(1)))
    await this._updateAdapterConnections(this.adapterConnections.filter((element) => element.adidconn != adidconn))
    this._updateActiveIndex(this.adapterConnections[0])
  }

  private async _changeAdapterConnection(adidconn: string, op: string, adid: string) {
    const updatedAdapter = await firstValueFrom(
      this._adapterApi.changeAdapterConnection(this.sbUrl, adidconn, op, adid).pipe(take(1))
    )
    await this._updateAdapterConnections([
      ...this.adapterConnections.filter((element) => element.adidconn != adidconn),
      updatedAdapter,
    ])
    this._updateActiveIndex(updatedAdapter)
  }

  private async _updateAdapterConnectionParams(adidconn: string, params: string[]) {
    const updatedAdapter = await firstValueFrom(
      this._adapterApi.updateAdapterConnectionParams(this.sbUrl, adidconn, params).pipe(take(1))
    )
    await this._updateAdapterConnections([
      ...this.adapterConnections.filter((element) => element.adidconn != adidconn),
      updatedAdapter,
    ])
    this._updateActiveIndex(updatedAdapter)
  }

  private async _updateAdapterConnectionOptionalParams(adidconn: string, optionalParams: string[]) {
    const updatedAdapter = await firstValueFrom(
      this._adapterApi.updateAdapterConnectionOptionalParams(this.sbUrl, adidconn, optionalParams).pipe(take(1))
    )
    await this._updateAdapterConnections([
      ...this.adapterConnections.filter((element) => element.adidconn != adidconn),
      updatedAdapter,
    ])
    this._updateActiveIndex(updatedAdapter)
  }

  private async _updateAdapterConnectionPath(adidconn: string, path: string) {
    const updatedAdapter = await firstValueFrom(
      this._adapterApi.updateAdapterConnectionPath(this.sbUrl, adidconn, path).pipe(take(1))
    )
    this._updateAdapterConnections([
      ...this.adapterConnections.filter((element) => element.adidconn != adidconn),
      updatedAdapter,
    ])
    this._updateActiveIndex(updatedAdapter)
  }

  private async _updateAdapterConnectionIncomingContentType(adidconn: string, contentType: string) {
    const updatedAdapter = await firstValueFrom(
      this._adapterApi.updateAdapterConnectionIncomingContentType(this.sbUrl, adidconn, contentType).pipe(take(1))
    )
    this._updateAdapterConnections([
      ...this.adapterConnections.filter((element) => element.adidconn != adidconn),
      updatedAdapter,
    ])
    this._updateActiveIndex(updatedAdapter)
  }

  private async _updateAdapterConnectionOutgoingContentType(adidconn: string, contentType: string) {
    const updatedAdapter = await firstValueFrom(
      this._adapterApi.updateAdapterConnectionOutgoingContentType(this.sbUrl, adidconn, contentType).pipe(take(1))
    )
    this._updateAdapterConnections([
      ...this.adapterConnections.filter((element) => element.adidconn != adidconn),
      updatedAdapter,
    ])
    this._updateActiveIndex(updatedAdapter)
  }

  private async _updateAdapterConnections(adapterConnections: AdapterConnection[], updateItem: boolean = true) {
    if (updateItem) {
      const item = await firstValueFrom(this._itemsApi.getItem(this.sbUrl, this._oid).pipe(take(1)))
      this._item.next(this._mergeItemWithProp(item))
    }
    const sorted = adapterConnections.sort((a: AdapterConnection, b: AdapterConnection) => (a.op > b.op ? 1 : -1))
    this._adapterConnections.next(sorted)
  }

  private _updateActiveIndex(adapterConnection: AdapterConnection) {
    const index = this.adapterConnections.findIndex((element) => element.adidconn == adapterConnection.adidconn)
    if (index >= 0) {
      this.updateActiveIndex(index)
    } else {
      this.updateActiveIndex(0)
    }
  }

  updateActiveIndex(activeIndex: number) {
    this._activeIndex.next(activeIndex)
  }

  get activeIndex() {
    return this._activeIndex.value
  }

  get item() {
    return this._item.value
  }

  get adapterConnections() {
    return this._adapterConnections.value
  }

  get sbUrl() {
    return this._myNodeService.sbUrl
  }
}
