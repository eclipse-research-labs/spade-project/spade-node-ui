/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core'
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog'
import { SelectAdapterComponent } from '../select-adapter/select-adapter.component'
import { Adapter, AdapterConnection, AdapterConnectionCreate } from '@core/models/adapter.model'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { PropertyWithKey } from '@core/models/item.model'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { MyNodeItemsPropsService } from '../../my-node-items-props.service'

@UntilDestroy()
@Component({
  selector: 'app-create-connection-button',
  templateUrl: './create-connection-button.component.html',
  styleUrl: './create-connection-button.component.scss',
  providers: [DialogService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateConnectionButtonComponent implements OnInit {
  @Input() op?: string | undefined | null
  @Input() oid?: string | undefined | null
  @Input() prop?: PropertyWithKey | undefined | null
  @Input() disabled: boolean = false

  private _ref: DynamicDialogRef | undefined

  title!: string
  icon!: string
  desc!: string
  solo!: boolean

  constructor(private _dialogService: DialogService, private _service: MyNodeItemsPropsService, private _snackBar: SnackBarService) {}

  ngOnInit(): void {
    switch (this.op) {
      case 'WRITE':
        this.title = 'Write'
        this.icon = 'pi pi-upload'
        this.desc = 'For writing a values to a property'
        this.solo = this.prop?.forms?.some((element) => element.op == 'readproperty') ?? false
        break
      default:
        this.title = 'Read'
        this.icon = 'pi pi-download'
        this.desc = 'For reading a values from a property'
        this.solo = this.prop?.forms?.some((element) => element.op == 'writeproperty') ?? false
        break
    }
  }

  openSelectAdapter() {
    this._ref = this._dialogService.open(SelectAdapterComponent, {
      header: 'Select Adapter',
      data: {
        op: this.op,
        solo: this.solo,
      },
      height: '600px',
    })
    this._ref.onClose.pipe(untilDestroyed(this)).subscribe((data) => this._createAdapter(data))
  }

  private async _createAdapter(data: any) {
    if (!data) {
      return
    }
    const { op, adapter }: { op: string; adapter: Adapter } = data
    const oid = this.oid
    const iid = this.prop?.key
    if (op && adapter && oid && iid) {
      const newConnection: AdapterConnectionCreate = {
        oid,
        iid,
        op,
        interaction: 'property',
        adid: adapter.adid,
        optionalParams: [],
        params: [],
        method: op == 'READ' ? 'GET' : 'PUT',
        path: '/',
      }
      await this._service.createAdapterConnection(newConnection)
      this._snackBar.showSuccess('Adapter connection created')
    }
  }

  get adapterConnections(): AdapterConnection[] {
    return this._service.adapterConnections
  }

  get loading$() {
    return this._service.loading$
  }
}
