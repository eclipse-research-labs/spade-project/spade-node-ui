/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core'
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog'
import { SelectPayloadComponent } from '../select-payload/select-payload.component'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { resolveItemContentTypeIcon } from 'src/app/utils'
import { MyNodeItemsPropsService } from '../../my-node-items-props.service'

@UntilDestroy()
@Component({
  selector: 'app-payload',
  templateUrl: './payload.component.html',
  styleUrl: './payload.component.scss',
  providers: [DialogService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PayloadComponent implements OnInit {
  @Input() id?: string | undefined | null
  @Input() contentType?: string
  @Input() type!: 'Request' | 'Response'
  @Input() editMode: boolean = true

  private _ref: DynamicDialogRef | undefined

  contentTypeIcon?: string = undefined

  constructor(
    private _dialogService: DialogService,
    private _snackBar: SnackBarService,
    private _service: MyNodeItemsPropsService
  ) {}

  ngOnInit(): void {
    this.contentTypeIcon = resolveItemContentTypeIcon(this.contentType)
  }

  editPayload(loading: boolean) {
    if (!loading && this.editMode) {
      this._ref = this._dialogService.open(SelectPayloadComponent, {
        header: `Select ${this.type} Payload MIME type`,
        data: {
          type: this.type,
          contentType: this.contentType,
        },
      })
      this._ref.onClose.pipe(untilDestroyed(this)).subscribe((data) => this._updateConnection(data))
    }
  }

  private async _updateConnection(contentType?: string) {
    if (contentType && this.id) {
      if (this.type == 'Request') {
        await this._service.updateAdapterConnectionIncomingContentType(this.id, contentType)
        this._snackBar.showSuccess(`${this.type} payload updated`)
      }
      if (this.type == 'Response') {
        await this._service.updateAdapterConnectionOutgoingContentType(this.id, contentType)
        this._snackBar.showSuccess(`${this.type} payload updated`)
      }
    }
  }

  get noContentType() {
    return this.contentType == undefined || this.contentType == 'not-set'
  }

  get loading$() {
    return this._service.loading$
  }
}
