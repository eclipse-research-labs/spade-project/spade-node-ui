/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { FormControl, Validators } from '@angular/forms'
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog'
import { BehaviorSubject, Observable } from 'rxjs'
import { mimeTypes } from 'src/app/data'

type _MimeWithIcon = { icon: string; mime: string }

@Component({
  selector: 'app-select-payload',
  templateUrl: './select-payload.component.html',
  styleUrl: './select-payload.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectPayloadComponent {
  type!: 'Request' | 'Response'

  defaultActiveedIndex = 0

  allMimeTypes: _MimeWithIcon[] = []
  groupMimeTypes: Record<string, _MimeWithIcon[]> = {}

  fileMimeType = new FormControl<string>('', {
    validators: [Validators.required],
    updateOn: 'change',
  })

  customMimeType = new FormControl<string>('', {
    validators: [Validators.required],
    updateOn: 'change',
  })

  searchAllText = ''
  searchGroupText = ''

  private _allFilteredTypes!: BehaviorSubject<_MimeWithIcon[]>
  private _groupFilteredTypes!: Record<string, BehaviorSubject<_MimeWithIcon[]>>

  allFilteredTypes$!: Observable<_MimeWithIcon[]>
  groupFilteredTypes!: Record<string, Observable<_MimeWithIcon[]>>

  constructor(private _dialogRef: DynamicDialogRef, _config: DynamicDialogConfig) {
    this.type = _config.data.type
    this._initAllMimeTypes()
    this._initGroupMimeTypes()
  }

  close() {
    this._dialogRef.close()
  }

  selectType(type: string | null | undefined) {
    if (type) {
      this._dialogRef.close(type)
    }
  }

  searchAll(event: Event) {
    const text = this.searchAllText.toLowerCase()
    if (text.length <= 0) {
      this._allFilteredTypes.next(this.allMimeTypes)
    } else {
      this._allFilteredTypes.next(
        this.allMimeTypes.filter((element) => element.mime.toLocaleLowerCase().includes(text))
      )
    }
  }

  searchGroup(event: Event, mimeGroup: string) {
    const text = this.searchGroupText.toLowerCase()
    const groupControl = this._groupFilteredTypes[mimeGroup]
    const groupTypes = this.groupMimeTypes[mimeGroup]
    if (!groupControl || !groupTypes) return
    if (text.length <= 0) {
      groupControl.next(groupTypes)
    } else {
      groupControl.next(groupTypes.filter((element) => element.mime.toLocaleLowerCase().includes(text)))
    }
  }

  private _initAllMimeTypes() {
    this.allMimeTypes = Object.values(mimeTypes).reduce(
      (allTypes, group) => [
        ...allTypes,
        ...group.values.map((element) => {
          return { icon: group.icon, mime: element }
        }),
      ],
      this.allMimeTypes
    )
    this._allFilteredTypes = new BehaviorSubject<_MimeWithIcon[]>([...this.allMimeTypes])
    this.allFilteredTypes$ = this._allFilteredTypes.asObservable()
  }

  private _initGroupMimeTypes() {
    this._groupFilteredTypes = {}
    this.groupFilteredTypes = {}
    for (let [group, value] of Object.entries(mimeTypes)) {
      this.groupMimeTypes[group] = value.values.map((element) => {
        return { icon: value.icon, mime: element }
      })
      const control = new BehaviorSubject<_MimeWithIcon[]>([...this.groupMimeTypes[group]])
      this._groupFilteredTypes[group] = control
      this.groupFilteredTypes[group] = control.asObservable()
    }
  }

  get mimeTypes() {
    return mimeTypes
  }
}
