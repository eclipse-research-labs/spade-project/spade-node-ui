/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { Adapter, AdapterConnection } from '@core/models/adapter.model'
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog'
import { SelectAdapterComponent } from '../select-adapter/select-adapter.component'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { MyNodeItemsPropsService } from '../../my-node-items-props.service'

@UntilDestroy()
@Component({
  selector: 'app-change-connection-button',
  templateUrl: './change-connection-button.component.html',
  styleUrl: './change-connection-button.component.scss',
  providers: [DialogService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChangeConnectionButtonComponent implements OnInit, OnChanges {
  @Input() adapterConnection?: AdapterConnection | undefined | null

  private _ref: DynamicDialogRef | undefined

  adapter?: Adapter | undefined | null

  constructor(
    private _service: MyNodeItemsPropsService,
    private _snackBar: SnackBarService,
    private _dialogService: DialogService,
    private _myNodeService: MyNodeService
  ) {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    const con = changes.adapterConnection
    if (con?.currentValue?.id != con?.previousValue?.id) {
      this._initAdapter()
    }
  }

  openSelectAdapter() {
    const selectedReadAdapter = this.adapterConnection?.op == 'READ' ? this.adapter : null
    const selectedWriteAdapter = this.adapterConnection?.op == 'WRITE' ? this.adapter : null
    this._ref = this._dialogService.open(SelectAdapterComponent, {
      header: 'Select Adapter',
      data: {
        op: this.adapterConnection?.op,
        solo: true,
        selectedReadAdapter,
        selectedWriteAdapter,
      },
      height: '600px',
    })
    this._ref.onClose.pipe(untilDestroyed(this)).subscribe((data) => this._changeAdapter(data))
  }

  private async _changeAdapter(data: any) {
    if (!data) {
      return
    }
    const { op, adapter }: { op: string; adapter: Adapter } = data
    const id = this.adapterConnection?.adidconn
    const adid = adapter?.adid
    if (id && op && adid) {
      await this._service.changeAdapterConnection(id, op, adid)
      this._snackBar.showSuccess('Adapter connection changed')
    }
  }

  private _initAdapter() {
    this.adapter = this.adapters.find((element) => element.adid == this.adapterConnection?.adid)
  }

  get adapters() {
    return this._myNodeService.adapters
  }

  get loading$() {
    return this._service.loading$
  }
}
