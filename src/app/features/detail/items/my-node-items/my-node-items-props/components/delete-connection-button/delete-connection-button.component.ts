/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { ConfirmationService } from 'primeng/api'
import { MyNodeItemsPropsService } from '../../my-node-items-props.service'

@Component({
  selector: 'app-delete-connection-button',
  templateUrl: './delete-connection-button.component.html',
  styleUrl: './delete-connection-button.component.scss',
  providers: [ConfirmationService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteConnectionButtonComponent {
  constructor(private _service: MyNodeItemsPropsService, private _snackBar: SnackBarService, private _confirmationService: ConfirmationService) {}

  @Input() id?: string | undefined | null

  deleteConnection(event: Event) {
    this._confirmationService.confirm({
      target: event.target as EventTarget,
      header: 'Delete connection?',
      acceptLabel: 'Delete',
      rejectLabel: 'Cancel',
      acceptIcon: 'fa fa-trash-can',
      rejectIcon: 'pi pi-times',
      acceptButtonStyleClass: 'p-button-text p-button-danger',
      rejectButtonStyleClass: 'p-button-text p-button-secondary',
      accept: async () => {
        await this._deleteConnection()
      },
    })
  }

  async _deleteConnection() {
    if (this.id) {
      await this._service.deleteAdapterConnection(this.id)
      this._snackBar.showSuccess('Adapter connection deleted')
    }
  }

  get loading$() {
    return this._service.loading$
  }
}
