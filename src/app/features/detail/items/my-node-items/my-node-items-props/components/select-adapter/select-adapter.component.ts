/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { trigger, transition, style, animate } from '@angular/animations'
import { ChangeDetectionStrategy, Component } from '@angular/core'
import { Adapter } from '@core/models/adapter.model'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog'
import { Observable } from 'rxjs'

@Component({
  selector: 'app-select-adapter',
  templateUrl: './select-adapter.component.html',
  styleUrl: './select-adapter.component.scss',
  animations: [
    trigger('myInsertRemoveTrigger', [
      transition(':enter', [style({ opacity: 0 }), animate('600ms', style({ opacity: 1 }))]),
      transition(':leave', [animate('100ms', style({ opacity: 0 }))]),
    ]),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectAdapterComponent {
  op!: string
  solo!: boolean

  selectedReadAdapter?: Adapter | undefined | null
  selectedWriteAdapter?: Adapter | undefined | null

  constructor(private _ref: DynamicDialogRef, private _config: DynamicDialogConfig, private _myNodeService: MyNodeService) {
    this.op = _config.data.op ?? 'READ'
    this.solo = _config.data.solo ?? false
    this.selectedReadAdapter = _config.data.selectedReadAdapter
    this.selectedWriteAdapter = _config.data.selectedWriteAdapter
  }

  selectReadAdapter(adapter: Adapter) {
    this.selectedReadAdapter = adapter
    this._ref.close({ op: this.op, adapter })
  }

  selectWriteAdapter(adapter: Adapter) {
    this.selectedWriteAdapter = adapter
    this._ref.close({ op: this.op, adapter })
  }

  onChange(index: number) {
    this.op = index == 0 ? 'READ': 'WRITE'
  }

  get $adapters(): Observable<Adapter[]> {
    return this._myNodeService.adapters$
  }
}
