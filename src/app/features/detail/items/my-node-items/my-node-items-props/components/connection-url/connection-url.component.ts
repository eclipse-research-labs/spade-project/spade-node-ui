/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core'
import { Adapter } from '@core/models/adapter.model'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { MyNodeItemsPropsService } from '../../my-node-items-props.service'

@Component({
  selector: 'app-connection-url',
  templateUrl: './connection-url.component.html',
  styleUrl: './connection-url.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConnectionUrlComponent implements OnInit {
  @Input() adapter?: Adapter | undefined | null
  @Input() id?: string | undefined | null
  @Input() path?: string | undefined | null
  @Input() editMode: boolean = true

  edit: boolean = false
  _path: string = ''

  constructor(private _service: MyNodeItemsPropsService, private _snackBar: SnackBarService) {}

  ngOnInit(): void {
    this._path = this.path ?? ''
  }

  editUrl() {
    this.edit = true
  }

  cancel() {
    this._path = this.path ?? ''
    this.edit = false
  }

  async save() {
    if (!this.id) {
      return
    }
    let path = this._path
    if (!path.startsWith('/')) {
      path = '/' + path
    }
    await this._service.updateAdapterConnectionPath(this.id, path)
    this._snackBar.showSuccess('Path updated')
  }

  get baseAdapterBaseUrl(): string | undefined | null {
    const adapter = this.adapter
    return `${adapter?.protocol?.toLowerCase()}://${adapter?.host}:${adapter?.port}${adapter?.path ?? ''}`
  }

  get loading$() {
    return this._service.loading$
  }
}
