/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core'
import { FormControl, Validators } from '@angular/forms'
import { Adapter, AdapterConnection } from '@core/models/adapter.model'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { UserRole } from '@core/enums/user.enum'
import { ConsumptionService } from '@core/services/consumption/consumption.service'
import { UserService } from '@core/services/user/user.service'
import { Payload } from '@core/models/consumption.model'
import { MyNodeItemsPropsService } from '../my-node-items-props.service'

@Component({
  selector: 'app-connection-detail',
  templateUrl: './connection-detail.component.html',
  styleUrl: './connection-detail.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConnectionDetailComponent implements OnInit {
  @Input() adapter!: Adapter
  @Input() connection!: AdapterConnection

  paramValues!: string[]
  params?: Map<string, string>

  body = new FormControl('', {
    updateOn: 'change',
    validators: [Validators.required],
  })

  file = new FormControl<File | null>(null, {
    updateOn: 'change',
    validators: [Validators.required],
  })

  text = new FormControl('', {
    updateOn: 'change',
    validators: [Validators.required],
  })

  constructor(
    private _service: MyNodeItemsPropsService,
    private _consumptionService: ConsumptionService,
    private _myNodeService: MyNodeService,
    private _userService: UserService
  ) {}

  ngOnInit(): void {
    this.paramValues = Array.from(this.connection.params, (element) => '')
  }

  async updateParams(params: string[]) {
    await this._service.updateAdapterConnectionParams(this.connection.adidconn, params)
  }

  async consume() {
    this.params = new Map<string, string>()
    for (let i = 0; i < this.connection.params.length; i++) {
      const key = this.connection.params[i]
      const value = this.paramValues[i]
      this.params.set(key, value)
    }
    if (this.write) {
      if (this.inIsBody && this.body.valid) {
        this._consume({
          type: 'BODY',
          data: this.body.value!,
          contentType: this.connection.incomingContentType,
        })
      } else if (this.inIsFile && this.file.valid) {
        const file = this.file.value!
        this._consume({
          type: 'FILE',
          data: file,
          contentType: this.connection.incomingContentType,
        })
      } else if (this.inIsText && this.text.valid) {
        this._consume({
          type: 'TEXT',
          data: this.text.value!,
          contentType: this.connection.incomingContentType,
        })
      }
    } else {
      this._consume(undefined, this.connection.outgoingContentType)
    }
  }

  private _consume(payload?: Payload, contentType?: string) {
    this._consumptionService.addConsumption({
      consumptionUrl: this.consumptionUrl,
      item: { ...this.item, organisation: this.organisation },
      property: this.property,
      adapter: this.adapter,
      params: this.params,
      op: this.connection.op,
      contentType,
      payload,
    })
  }

  get loading$() {
    return this._service.loading$
  }

  get consumptionUrl() {
    return this._myNodeService.sbUrl + '/api/sb/consumption'
  }

  get item() {
    return this._service.item!
  }

  get property() {
    return this._service.item!.prop!
  }

  get admin() {
    return UserRole.ADMIN
  }

  get nodeOperator() {
    return UserRole.NODE_OPERATOR
  }

  get organisation() {
    return this._userService.user?.organisation
  }

  get read() {
    return this.connection.op == 'READ' || this.connection.op == 'readproperty'
  }

  get write() {
    return this.connection.op == 'WRITE' || this.connection.op == 'writeproperty'
  }

  get outgoingContentType() {
    return this.connection.outgoingContentType
  }

  get incomingContentType() {
    return this.connection.incomingContentType
  }

  get noOutContentType() {
    return this.outgoingContentType == undefined || this.outgoingContentType == 'not-set'
  }

  get noInContentType() {
    return this.incomingContentType == undefined || this.incomingContentType == 'not-set'
  }

  get inIsBody() {
    return this.incomingContentType && this.incomingContentType == 'application/json'
  }

  get inIsText() {
    return this.incomingContentType && this.incomingContentType == 'text/plain'
  }

  get inIsFile() {
    return !this.inIsBody && !this.inIsText
  }
}
