/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MyNodeItemsEventsService } from '../my-node-items-events.service';
import { NodeWithOwner } from '@core/models/node.model';

@Component({
  selector: 'app-event-subscriptions',
  templateUrl: './event-subscriptions.component.html',
  styleUrl: './event-subscriptions.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventSubscriptionsComponent {

  constructor(private _service: MyNodeItemsEventsService) { }

  get item$() {
    return this._service.item$
  }

  get nodes$() {
    return this._service.nodes$
  }

}
