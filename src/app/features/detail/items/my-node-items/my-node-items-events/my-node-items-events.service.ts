/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { EventsApiService } from '@core/api/modules/events'
import { NodesApiService } from '@core/api/modules/nodes'
import { Item, ItemWithEventWithKey } from '@core/models/item.model'
import { NodeWithOwner } from '@core/models/node.model'
import { BaseService } from '@core/services/base'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'

@Injectable()
export class MyNodeItemsEventsService extends BaseService {
  private _item = new BehaviorSubject<ItemWithEventWithKey | null>(null)
  private _nodes = new BehaviorSubject<NodeWithOwner[] | null>(null)

  item$ = this._item.asObservable()
  nodes$ = this._nodes.asObservable()

  private _oid!: string
  private _iid!: string

  constructor(
    private _myNodeService: MyNodeService,
    private _eventApi: EventsApiService,
    private _nodesApi: NodesApiService
  ) {
    super()
  }

  async init(oid?: string, iid?: string) {
    await this.initApiWrapper(this._init(oid, iid))
  }

  private async _init(oid?: string, iid?: string) {
    if (!oid) {
      throw Error('oid missing')
    }
    if (!iid) {
      throw Error('iid missing')
    }
    this._oid = oid
    this._iid = iid
    await Promise.all([this._initItem(), this._initEventSubscribers()])
  }

  private async _initItem(): Promise<void> {
    const item = this._myNodeService.myItems.find((element) => element.oid === this._oid)
    this._item.next(this._mergeItemWithEvent(item))
  }

  private _mergeItemWithEvent(item: Item | undefined): ItemWithEventWithKey {
    const oid = this._oid
    const iid = this._iid
    if (!item) {
      throw Error(`Item with oid: ${oid} does not exists`)
    }
    const events = item.events
    if (!events) {
      throw Error('Item has no events')
    }
    const event = events[iid]
    if (!event) {
      throw Error(`Event with iid: ${iid} does not exists`)
    }
    return {
      ...item,
      event: {
        key: iid,
        ...event,
      },
    }
  }

  private async _initEventSubscribers(): Promise<void> {
    const oid = this._oid.split(':').at(-1)!
    const iid = this._iid
    const subscribers = await firstValueFrom(this._eventApi.getEventSubscribers(this.sbUrl, oid, iid).pipe(take(1)))
    const nodes = await firstValueFrom(this._nodesApi.getAllNodes().pipe(take(1)))
    const filteredNodes = nodes.filter((node) => subscribers.includes(`urn:node:${node.nodeId}`))
    this._nodes.next(filteredNodes)
  }

  get item() {
    return this._item.value
  }

  get sbUrl() {
    return this._myNodeService.sbUrl
  }
}
