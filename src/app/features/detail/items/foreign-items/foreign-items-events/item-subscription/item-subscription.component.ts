/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { ItemWithIdentity, EventWithKey } from '@core/models/item.model'

@Component({
  selector: 'app-item-subscription',
  templateUrl: './item-subscription.component.html',
  styleUrl: './item-subscription.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemSubscriptionComponent {
  @Input() item!: ItemWithIdentity
  @Input() event!: EventWithKey

  stateOptions: any[] = [
    { label: 'Off', value: 'off' },
    { label: 'On', value: 'on' },
  ]

  value: string = 'off'

  get noForms() {
    return !this.event.forms || this.event.forms.length <= 0
  }
}
