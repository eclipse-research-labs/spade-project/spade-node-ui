/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ItemWithIdentity, EventWithKey, ItemWithEventWithKey } from '@core/models/item.model';
import { DiscoveryService } from '@core/services/discovery/discovery.service';
import { MyNodeService } from '@core/services/my-node/my-node.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-foreign-items-events',
  templateUrl: './foreign-items-events.component.html',
  styleUrl: './foreign-items-events.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ForeignItemsEventsComponent {
  private _oid: string | null = null
  private _eid: string | null = null

  item?: ItemWithIdentity
  event?: EventWithKey

  constructor(
    private _discoveryService: DiscoveryService,
    private _myNodeService: MyNodeService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _cd: ChangeDetectorRef
  ) {}

  async ngOnInit(): Promise<void> {
    await this._discoveryService.init()
    this._listenForParamsChange()
  }

  onTryAgain() {
    this._discoveryService.init(true)
  }

  goBack() {
    this._router.navigateByUrl(`${this.backUrl}/discovery/partners/${this._oid}`, { replaceUrl: true })
  }

  private _listenForParamsChange() {
    this._route.paramMap.pipe(untilDestroyed(this)).subscribe((params) => {
      this._oid = params.get('oid')
      this._eid = params.get('eid')
      this._loadInfo()
      this._cd.markForCheck()
    })
  }

  private _loadInfo() {
    const oid = this._oid
    const eid = this._eid
    if (oid && eid) {
      this.item = this.foreignItems.find((element) => element.oid === oid)
      if (this.item && this.item?.events) {
        this.event = {
          key: eid,
          ...this.item.events[eid],
        }
      }
    }
  }

  get foreignItems() {
    return this._discoveryService.foreignItems
  }

  get initializing$() {
    return this._discoveryService.initializing$
  }

  get initializing() {
    return this._discoveryService.initializing
  }

  get errorInitializing$() {
    return this._discoveryService.errorInitializing$
  }

  get errorInitializing() {
    return this._discoveryService.errorInitializing
  }

  get itemWithEventAndKey(): ItemWithEventWithKey {
    return { ...this.item!, event: this.event! }
  }

  get backUrl() {
    return `/my-node/${this.brokerState}/${this.nodeState}`
  }

  get brokerState() {
    return this._myNodeService.brokerState
  }

  get nodeState() {
    return this._myNodeService.nodeState
  }
}
