/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ItemWithIdentity, PropertyWithKey, ItemWithPropertyWithKey } from '@core/models/item.model';
import { DiscoveryService } from '@core/services/discovery/discovery.service';
import { MyNodeService } from '@core/services/my-node/my-node.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-foreign-items-props',
  templateUrl: './foreign-items-props.component.html',
  styleUrl: './foreign-items-props.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ForeignItemsPropsComponent implements OnInit {
  private _oid: string | null = null
  private _pid: string | null = null

  item?: ItemWithIdentity
  prop?: PropertyWithKey

  constructor(
    private _discoveryService: DiscoveryService,
    private _myNodeService: MyNodeService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _cd: ChangeDetectorRef
  ) {}

  async ngOnInit(): Promise<void> {
    await this._discoveryService.init()
    this._listenForParamsChange()
  }

  onTryAgain() {
    this._discoveryService.init(true)
  }

  goBack() {
    this._router.navigateByUrl(`${this.backUrl}/discovery/partners/${this._oid}`, { replaceUrl: true })
  }

  private _listenForParamsChange() {
    this._route.paramMap.pipe(untilDestroyed(this)).subscribe((params) => {
      this._oid = params.get('oid')
      this._pid = params.get('pid')
      this._loadInfo()
      this._cd.markForCheck()
    })
  }

  private _loadInfo() {
    const oid = this._oid
    const pid = this._pid
    if (oid && pid) {
      this.item = this.foreignItems.find((element) => element.oid === oid)
      if (this.item && this.item?.properties) {
        this.prop = {
          key: pid,
          ...this.item?.properties[pid],
        }
      }
    }
  }

  get foreignItems() {
    return this._discoveryService.foreignItems
  }

  get initializing$() {
    return this._discoveryService.initializing$
  }

  get initializing() {
    return this._discoveryService.initializing
  }

  get errorInitializing$() {
    return this._discoveryService.errorInitializing$
  }

  get errorInitializing() {
    return this._discoveryService.errorInitializing
  }

  get itemWithPropertyAndKey(): ItemWithPropertyWithKey {
    return { ...this.item!, prop: this.prop! }
  }

  get backUrl() {
    return `/my-node/${this.brokerState}/${this.nodeState}`
  }

  get brokerState() {
    return this._myNodeService.brokerState
  }

  get nodeState() {
    return this._myNodeService.nodeState
  }
}
