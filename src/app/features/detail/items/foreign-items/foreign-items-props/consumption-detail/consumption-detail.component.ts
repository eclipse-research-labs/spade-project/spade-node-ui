/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core'
import { FormControl, Validators } from '@angular/forms'
import { Payload } from '@core/models/consumption.model'
import { Form, ItemWithIdentity } from '@core/models/item.model'
import { ConsumptionService } from '@core/services/consumption/consumption.service'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { mimeTypes } from 'src/app/data'
import { resolveItemContentTypeIcon } from 'src/app/utils'

@Component({
  selector: 'app-consumption-detail',
  templateUrl: './consumption-detail.component.html',
  styleUrl: './consumption-detail.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConsumptionDetailComponent implements OnInit {
  @Input() item!: ItemWithIdentity
  @Input() iid!: string
  @Input() form!: Form

  resultVisible: boolean = false

  hrefVariables!: string[]
  paramValues!: string[]
  params?: Map<string, string>

  body = new FormControl('', {
    updateOn: 'change',
    validators: [Validators.required],
  })

  file = new FormControl<File | null>(null, {
    updateOn: 'change',
    validators: [Validators.required],
  })

  text = new FormControl('', {
    updateOn: 'change',
    validators: [Validators.required],
  })

  incomingContentTypeIcon?: string = undefined
  outgoingContentTypeIcon?: string = undefined

  constructor(private _myNodeService: MyNodeService, private _consumptionService: ConsumptionService) {}

  ngOnInit(): void {
    this.parseHrefVariables()
    this.paramValues = Array.from(this.hrefVariables, (_) => '')
    this.incomingContentTypeIcon = resolveItemContentTypeIcon(this.incomingContentType)
    this.outgoingContentTypeIcon = resolveItemContentTypeIcon(this.outgoingContentType)
  }

  // async consume() {
  //   this.params = new Map<string, string>()
  //   for (let i = 0; i < this.hrefVariables.length; i++) {
  //     const key = this.hrefVariables[i]
  //     const value = this.paramValues[i]
  //     this.params.set(key, value)
  //   }
  //   if (this.read) {
  //     this._consumptionService.addConsumption({
  //       consumptionUrl: this.consumptionUrl,
  //       item: this.item,
  //       property: this.property,
  //       params: this.params,
  //       op: this.form.op,
  //       contentType: this.property?.response?.contentType,
  //     })
  //   }
  //   if (this.write) {
  //     let payload: Payload | undefined = this.file.valid
  //       ? {
  //           type: 'FILE',
  //           data: this.file.value!,
  //         }
  //       : undefined

  //     this._consumptionService.addConsumption({
  //       consumptionUrl: this.consumptionUrl,
  //       item: this.item,
  //       property: this.property,
  //       params: this.params,
  //       op: this.form.op,
  //       payload
  //     })
  //   }
  // }

  async consume() {
    this.params = new Map<string, string>()
    for (let i = 0; i < this.hrefVariables.length; i++) {
      const key = this.hrefVariables[i]
      const value = this.paramValues[i]
      this.params.set(key, value)
    }
    if (this.write) {
      if (this.inIsBody && this.body.valid) {
        this._consume({
          type: 'BODY',
          data: this.body.value!,
          contentType: this.incomingContentType,
        })
      } else if (this.inIsFile && this.file.valid) {
        const file = this.file.value!
        this._consume({
          type: 'FILE',
          data: file,
          contentType: this.incomingContentType,
        })
      } else if (this.inIsText && this.text.valid) {
        this._consume({
          type: 'TEXT',
          data: this.text.value!,
          contentType: this.incomingContentType,
        })
      }
    } else {
      this._consume(undefined, this.outgoingContentType)
    }
  }

  private _consume(payload?: Payload, contentType?: string) {
    this._consumptionService.addConsumption({
      consumptionUrl: this.consumptionUrl,
      item: this.item,
      property: this.property,
      params: this.params,
      op: this.form.op,
      contentType,
      payload,
    })
  }

  parseHrefVariables() {
    const regexp = /{\?(.*?)}/
    const groups = regexp.exec(this.form.href)
    if (groups && groups[1]) {
      this.hrefVariables = groups[1].split(',')
    } else {
      this.hrefVariables = []
    }
  }

  get uriVariables() {
    return Object.keys(this.property.uriVariables ?? {})
  }

  get consumptionUrl() {
    return this._myNodeService.sbUrl + '/api/sb/consumption'
  }

  get property() {
    return { key: this.iid, ...this.item.properties![this.iid] }
  }

  get read() {
    return this.form.op == 'readproperty'
  }

  get write() {
    return this.form.op == 'writeproperty'
  }

  get outgoingContentType() {
    return this.form.response?.contentType
  }

  get incomingContentType() {
    return this.form.contentType
  }

  get noOutContentType() {
    return this.outgoingContentType == undefined
  }

  get noInContentType() {
    return this.incomingContentType == undefined
  }

  get inIsBody() {
    return this.incomingContentType && this.incomingContentType == 'application/json'
  }

  get inIsText() {
    return this.incomingContentType && this.incomingContentType == 'text/plain'
  }

  get inIsFile() {
    return !this.inIsBody && !this.inIsText
  }
}
