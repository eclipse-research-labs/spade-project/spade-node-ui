/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ItemWithIdentity, PropertyWithKey } from '@core/models/item.model';

@Component({
  selector: 'app-item-consumption',
  templateUrl: './item-consumption.component.html',
  styleUrl: './item-consumption.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemConsumptionComponent {
  @Input() item!: ItemWithIdentity
  @Input() prop!: PropertyWithKey

  activeIndex: number = 0

  get noForms() {
    return !this.prop.forms || this.prop.forms.length <= 0
  }
}
