/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, ElementRef, OnInit, ViewChild } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Status } from '@core/models/audit-trail.model'
import { AuditTrailsService } from '@core/services/audit-trails/audit-trails.service'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { FloatingBackButtonComponent } from '../../../../shared/components/presentation/app-specific/detail/floating-back-button/floating-back-button.component'
import { UserService } from '@core/services/user/user.service'

@Component({
  selector: 'app-org-audit-trail',
  templateUrl: './org-audit-trail.component.html',
  styleUrl: './org-audit-trail.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrgAuditTrailComponent implements OnInit {
  @ViewChild('detail', { static: false }) detail?: FloatingBackButtonComponent

  selectedStatus!: Status

  constructor(
    private _myNodeService: MyNodeService,
    private _userService: UserService,
    private _auditTrailsService: AuditTrailsService,
    private _router: Router,
    private _route: ActivatedRoute
  ) {}

  async ngOnInit(): Promise<void> {
    await this._myNodeService.init()
    this.selectedStatus =
      (this._route.snapshot.paramMap.get('status')?.toUpperCase() as Status | undefined) ?? Status.ALL
    await this._auditTrailsService.init(0, this.selectedStatus, false)
  }

  goBack() {
    this._router.navigateByUrl(`${this.backUrl}/items`, { replaceUrl: true })
  }

  async onPageChange(page: number) {
    this.detail?.detail?.nativeElement.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth',
    })
    await this._auditTrailsService.loadOrgPage(page, this.selectedStatus)
  }

  async onStatusChange(status: Status) {
    this.selectedStatus = status
    await this._auditTrailsService.init(0, this.selectedStatus, true)
  }

  async onRefresh() {
    await this._auditTrailsService.init(0, this.selectedStatus, true)
  }

  get auditTrails$() {
    return this._auditTrailsService.orgAuditTrails$
  }

  get user$() {
    return this._userService.user$
  }

  get backUrl() {
    return `/my-node/online/${this.nodeState}`
  }

  get nodeState() {
    return this._myNodeService.nodeState
  }
}
