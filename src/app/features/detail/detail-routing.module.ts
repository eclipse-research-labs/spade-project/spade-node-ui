/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { DetailComponent } from './detail.component'
import { DashboardComponent } from './dashboard/dashboard.component'
import { userGuard } from '@core/guards/user.guard'
import { onlineGuard } from '@core/guards/online.guard'
import { NodeAuditTrailComponent } from './audit-trail/node-audit-trail/node-audit-trail.component'
import { OrgAuditTrailComponent } from './audit-trail/org-audit-trail/org-audit-trail.component'
import { UserAuditTrailComponent } from './audit-trail/user-audit-trail/user-audit-trail.component'

const routes: Routes = [
  {
    path: 'items',
    component: DetailComponent,
    loadChildren: () => import('./items/items.module').then((m) => m.ItemsModule),
  },
  {
    path: 'contracts',
    component: DetailComponent,
    canActivate: [userGuard.watch],
    loadChildren: () => import('./contracts/contracts.module').then((m) => m.ContractsModule),
  },
  {
    path: 'audit-trails',
    canActivate: [userGuard.watch],
    children: [
      {
        path: 'node/:nodeId/:status',
        canActivate: [onlineGuard.watch],
        component: NodeAuditTrailComponent,
      },
      {
        path: 'org/:status',
        component: OrgAuditTrailComponent,
      },
      {
        path: 'user/:status',
        component: UserAuditTrailComponent,
      },
    ],
  },
  {
    path: 'dashboards',
    component: DetailComponent,
    children: [
      {
        path: ':dashboard',
        component: DashboardComponent,
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailRoutingModule {}
