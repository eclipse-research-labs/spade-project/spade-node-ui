/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { MenuItem } from 'primeng/api'
import { combineLatest, map, Observable } from 'rxjs'
import { ContractsService } from './contracts.service'
import { Item, ItemWithIdentity } from '@core/models/item.model'
import { DiscoveryService } from '@core/services/discovery/discovery.service'
import { UserRole } from '@core/enums/user.enum'
import { ContractAccessType } from '@core/enums/contract.enum'
import { UserService } from '@core/services/user/user.service'
import { ContractIdentity, NewContract } from '@core/models/contract.model'
import { ContractCreatorService } from '@core/services/contract-creator/contract-creator.service'

interface _MenuItem extends MenuItem {
  indicate?: boolean
  contractIdentities$: Observable<ContractIdentity[]>
}

@UntilDestroy()
@Component({
  selector: 'app-contracts',
  templateUrl: './contracts.component.html',
  styleUrl: './contracts.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ContractsService],
})
export class ContractsComponent implements OnInit {
  items!: _MenuItem[]

  item?: Item | ItemWithIdentity
  selectedItems: ItemWithIdentity[] = []

  private _oid?: string

  constructor(
    private _service: ContractsService,
    private _userService: UserService,
    private _myNodeService: MyNodeService,
    private _discoveryService: DiscoveryService,
    private _contractCreatorService: ContractCreatorService,
    private _route: ActivatedRoute,
    private _cd: ChangeDetectorRef,
    private _router: Router
  ) {}

  contractItem() {
    const newContract: NewContract = {
      targets: this.selectedItems,
      assignees: this.assignees,
      read: false,
      write: false,
      subscribe: false,
      accessType: this.accessType,
    }
    this._contractCreatorService.init(newContract)
    this._router.navigateByUrl(`contract-creator/${this._myNodeService.node?.nodeId}`)
  }

  goBack() {
    if (this._isMyItem) {
      this._router.navigateByUrl(`${this.backUrl}/items/${this._oid}`, { replaceUrl: true })
    } else {
      this._router.navigateByUrl(`${this.backUrl}/discovery/partners/${this._oid}`, { replaceUrl: true })
    }
  }

  async ngOnInit(): Promise<void> {
    this._oid = this._route.snapshot.paramMap.get('oid') ?? undefined
    await this._init()
    this._listenForParamChange()
  }

  private _listenForParamChange() {
    this._route.paramMap.pipe(untilDestroyed(this)).subscribe(async (params) => {
      this._oid = params.get('oid') ?? undefined
      await this._init()
    })
  }

  private async _init() {
    await this._myNodeService.init()
    if (this._isMyItem) {
      this.item = this._myNodeService.myItems.find((element) => element.oid == this._oid)
      if (this.item) {
        this.selectedItems = [{ ...this.item, organisation: this._userOrganisation }]
      }
    } else {
      await this._discoveryService.init()
      this.item = this._discoveryService.foreignItems.find((element) => element.oid == this._oid)
      if (this.item) {
        this.selectedItems = [this.item]
      }
    }
    await this._service.init(this._oid ?? '')
    this._initItems()
    this._listenForExigentContractsChange()
  }

  private _listenForExigentContractsChange() {
    this._myNodeService.exigentContracts$.pipe(untilDestroyed(this)).subscribe((_) => {
      this._initItems()
    })
  }

  private _initItems() {
    this.items = [
      {
        label: 'Active',
        icon: 'fa fa-file-signature',
        routerLink: `${this.url}/active`,
        contractIdentities$: this._service.activeContractIdentities$,
      },
      {
        label: 'Pending',
        icon: 'fa fa-file-signature',
        routerLink: `${this.url}/pending`,
        indicate: this._myNodeService.hasExigentContracts(this._oid),
        contractIdentities$: this._service.pendingContractIdentities$,
      },
      {
        label: 'Revoked',
        icon: 'fa fa-file-signature',
        routerLink: `${this.url}/revoked`,
        contractIdentities$: this._service.revokedContractIdentities$,
      },
    ]
    this._cd.markForCheck()
  }

  get privacyTooLow() {
    return this.item?.['SPADE:Privacy']?.Level != undefined && this.item?.['SPADE:Privacy']?.Level < 2
  }

  get initializing$() {
    return combineLatest([this._myNodeService.initializing$, this._service.initializing$]).pipe(
      map(([x0, x1]) => x0 || x1)
    )
  }

  get url() {
    return `/detail/contracts/my-node-contracts/${this.nodeState}/${this._oid}`
  }

  get backUrl() {
    return `/my-node/${this.brokerState}/${this.nodeState}`
  }

  get brokerState() {
    return this._myNodeService.brokerState
  }

  get nodeState() {
    return this._myNodeService.nodeState
  }

  get admin() {
    return UserRole.ADMIN
  }

  get nodeOperator() {
    return UserRole.NODE_OPERATOR
  }

  get assignees() {
    return this._isMyItem
      ? []
      : this._myNodeService.node && this._userOrganisation
      ? [{ ...this._myNodeService.node, owner: this._userOrganisation }]
      : []
  }
  get accessType() {
    return this._isMyItem ? ContractAccessType.GRANT : ContractAccessType.REQUEST
  }

  private get _isMyItem() {
    return this._myNodeService.myItems.some((element) => element.oid == this._oid)
  }

  private get _userOrganisation() {
    return this._userService.user?.organisation
  }
}
