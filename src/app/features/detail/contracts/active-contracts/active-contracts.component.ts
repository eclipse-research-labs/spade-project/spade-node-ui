/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { ContractsService } from '../contracts.service'
import { ContractIdentity } from '@core/models/contract.model'

@Component({
  selector: 'app-active-contracts',
  templateUrl: './active-contracts.component.html',
  styleUrl: './active-contracts.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActiveContractsComponent {
  constructor(private _service: ContractsService) {}

  onDownloadChunkOfIdentities(contractIdentities: ContractIdentity[]) {
    this._service.downloadContractIdentities(contractIdentities)
  }

  get contractIdentities$() {
    return this._service.activeContractIdentities$
  }

  get loading$() {
    return this._service.loading$
  }
}
