/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { ContractsRoutingModule } from './contracts-routing.module'

import { ContractsComponent } from './contracts.component'
import { ActiveContractsComponent } from './active-contracts/active-contracts.component'
import { PendingContractsComponent } from './pending-contracts/pending-contracts.component'
import { RevokedContractsComponent } from './revoked-contracts/revoked-contracts.component'
import { FloatingBackButtonComponent } from '@shared/components/presentation/app-specific/detail/floating-back-button/floating-back-button.component'
import { TableSkeletonComponent } from '@shared/components/presentation/reusable/loaders/table-skeleton/table-skeleton.component'
import { RoleCheckerComponent } from '@shared/components/smart/role-checker/role-checker.component'
import { InfoComponent } from '@shared/components/presentation/reusable/info/info.component'
import { FormsModule } from '@angular/forms'
import { NgLetDirective } from 'ng-let'
import { ButtonModule } from 'primeng/button'
import { TabMenuModule } from 'primeng/tabmenu'
import { ContractsListComponent } from '@shared/components/smart/contracts/contracts-list/contracts-list.component'
import { ActiveContractActionsComponent } from '@shared/components/smart/contracts/contract-actions/active-contract-actions/active-contract-actions.component'
import { PendingContractActionsComponent } from '@shared/components/smart/contracts/contract-actions/pending-contract-actions/pending-contract-actions.component'
import { RevokedContractActionsComponent } from '@shared/components/smart/contracts/contract-actions/revoked-contract-actions/revoked-contract-actions.component'

@NgModule({
  declarations: [ContractsComponent, ActiveContractsComponent, PendingContractsComponent, RevokedContractsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ContractsRoutingModule,
    FloatingBackButtonComponent,
    TableSkeletonComponent,
    RoleCheckerComponent,
    InfoComponent,
    NgLetDirective,
    ButtonModule,
    TabMenuModule,
    ContractsListComponent,
    ActiveContractActionsComponent,
    PendingContractActionsComponent,
    RevokedContractActionsComponent
  ],
})
export class ContractsModule {}
