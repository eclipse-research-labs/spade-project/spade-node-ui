/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { ContractIdentityStatus, ContractStatus } from '@core/enums/contract.enum'
import { Contract, ContractIdentity } from '@core/models/contract.model'
import { Item, ItemWithIdentity } from '@core/models/item.model'
import { BaseService } from '@core/services/base'
import { CacheService } from '@core/services/cache/cache.service'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { BehaviorSubject, Observable, map } from 'rxjs'
import { delay, isString } from 'src/app/utils'

@Injectable()
export class ContractsService extends BaseService {
  activeContractIdentities$!: Observable<ContractIdentity[]>
  pendingContractIdentities$!: Observable<ContractIdentity[]>
  revokedContractIdentities$!: Observable<ContractIdentity[]>

  constructor(private _myNodeService: MyNodeService, private _cacheService: CacheService) {
    super()
  }

  async init(oid: string) {
    await this.initApiWrapper(this._init(oid))
  }

  async _init(oid: string) {
    this.activeContractIdentities$ = this._myNodeService.contracts$.pipe(
      map((contracts) => this._initActiveContractIdentities(contracts, oid))
    )
    this.pendingContractIdentities$ = this._myNodeService.contracts$.pipe(
      map((contracts) => this._initPendingContractIdentities(contracts, oid))
    )
    this.revokedContractIdentities$ = this._myNodeService.contracts$.pipe(
      map((contracts) => this._initRevokedContractIdentities(contracts, oid))
    )
    await delay(10)
  }

  private _initActiveContractIdentities(contracts: Contract[], oid: string): ContractIdentity[] {
    return contracts
      .filter((contract) => _compareOids(contract.target, oid) && contract.status === ContractStatus.ACTIVE)
      .map((element) => this._initContractIdentity(element))
  }

  private _initPendingContractIdentities(contracts: Contract[], oid: string): ContractIdentity[] {
    return (
      contracts
        .filter(
          (contract) =>
            _compareOids(contract.target, oid) &&
            (contract.status === ContractStatus.WAITING_ON_ASSIGNEE ||
              contract.status === ContractStatus.WAITING_ON_ASSIGNER)
        )
        // .sort((e1, e2) => (this._myNodeService.isExigentContract(e1) ? -1 : this._myNodeService.isExigentContract(e2) ? 1 : 0))
        .map((element) => this._initContractIdentity(element))
    )
  }

  private _initRevokedContractIdentities(contracts: Contract[], oid: string): ContractIdentity[] {
    return contracts
      .filter((contract) => _compareOids(contract.target, oid) && contract.status === ContractStatus.REVOKED)
      .map((element) => this._initContractIdentity(element))
  }

  private _initContractIdentity(contract: Contract) {
    const targetStatus = new BehaviorSubject<ContractIdentityStatus>(ContractIdentityStatus.READY)
    const assigneeStatus = new BehaviorSubject<ContractIdentityStatus>(ContractIdentityStatus.READY)
    const assignerStatus = new BehaviorSubject<ContractIdentityStatus>(ContractIdentityStatus.READY)
    const identity: ContractIdentity = { contract, targetStatus, assigneeStatus, assignerStatus }
    return identity
  }

  downloadContractIdentities(contractIdentities: ContractIdentity[]) {
    for (const identity of contractIdentities) {
      this._downloadContractIdentity(identity)
    }
  }

  private async _downloadContractIdentity(identity: ContractIdentity) {
    await this._downloadTarget(identity)
    await this._downloadAssignee(identity)
    await this._downloadAssigner(identity)
  }

  private async _downloadTarget(identity: ContractIdentity) {
    try {
      if (identity.targetStatus.value != ContractIdentityStatus.READY) {
        return
      }
      const source = identity.contract.target
      if (!source) {
        throw Error('Failed to download the item')
      }
      if (isString(source)) {
        identity.targetStatus.next(ContractIdentityStatus.DOWNLOADING)
        const item = await this._cacheService.getItem(source, this._myNodeService.sbUrl)
        if (!item) {
          throw Error('Failed to download the item')
        }
        const cid = item['SPADE:organisation']?.['@id']
        if (!cid) {
          throw Error('Failed to download the items organisation')
        }
        const org = await this._cacheService.getOrganisation(cid)
        if (!org) {
          throw Error('Failed to download the items organisation')
        }
        identity.contract = { ...identity.contract, target: { ...item, organisation: org } }
        identity.targetStatus.next(ContractIdentityStatus.DONE)
      } else {
        const cid = source['SPADE:organisation']?.['@id']
        if (!cid) {
          throw Error('Failed to download the items organisation')
        }
        const org = await this._cacheService.getOrganisation(cid)
        if (!org) {
          throw Error('Failed to download the items organisation')
        }
        identity.contract.target = { ...source, organisation: org }
        identity.targetStatus.next(ContractIdentityStatus.DONE)
      }
    } catch (e) {
      console.error(e)
      identity.targetStatus.next(ContractIdentityStatus.FAILED)
    }
  }

  private async _downloadAssignee(identity: ContractIdentity) {
    try {
      if (identity.assigneeStatus.value != ContractIdentityStatus.READY) {
        return
      }
      const source = identity.contract.assignee
      if (!source) {
        throw Error('Failed to download the Broker')
      }
      if (isString(source)) {
        identity.assigneeStatus.next(ContractIdentityStatus.DOWNLOADING)
        const node = await this._cacheService.getNode(source)
        if (!node) {
          throw Error('Failed to download the Broker')
        }
        identity.contract = { ...identity.contract, assignee: { ...node } }
        identity.assigneeStatus.next(ContractIdentityStatus.DONE)
      } else {
        identity.assigneeStatus.next(ContractIdentityStatus.DONE)
      }
    } catch (e) {
      console.error(e)
      identity.assigneeStatus.next(ContractIdentityStatus.FAILED)
    }
  }

  private async _downloadAssigner(identity: ContractIdentity) {
    try {
      if (identity.assignerStatus.value != ContractIdentityStatus.READY) {
        return
      }
      const source = identity.contract.assigner
      if (!source) {
        throw Error('Failed to download the user')
      }
      if (isString(source)) {
        identity.assignerStatus.next(ContractIdentityStatus.DOWNLOADING)
        const user = await this._cacheService.getUser(source)
        if (!user) {
          throw Error('Failed to download the user')
        }
        identity.contract = { ...identity.contract, assigner: { ...user } }
        identity.assignerStatus.next(ContractIdentityStatus.DONE)
      } else {
        identity.assignerStatus.next(ContractIdentityStatus.DONE)
      }
    } catch (e) {
      console.error(e)
      identity.assignerStatus.next(ContractIdentityStatus.FAILED)
    }
  }
}

function _compareOids(item: string | ItemWithIdentity | Item | null, oid: string) {
  if (!item) return false
  const oid1 = oid.split(':').at(-1) ?? oid
  const oid2 = (isString(item) ? item : item.oid).split(':').at(-1) ?? item
  return oid1 == (isString(oid2) ? oid2 : oid2.oid)
}
