/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { DetailRoutingModule } from './detail-routing.module'
import { DetailComponent } from './detail.component'
import { AdaptersModule } from '@features/my-node/adapters/adapters.module'
import { DashboardComponent } from './dashboard/dashboard.component'
import { NodeAuditTrailComponent } from './audit-trail/node-audit-trail/node-audit-trail.component'
import { OrgAuditTrailComponent } from './audit-trail/org-audit-trail/org-audit-trail.component'
import { SanitizerPipe } from '@shared/pipes/sanitizer.pipe'
import { FloatingBackButtonComponent } from '@shared/components/presentation/app-specific/detail/floating-back-button/floating-back-button.component'
import { TableSkeletonComponent } from '@shared/components/presentation/reusable/loaders/table-skeleton/table-skeleton.component'
import { AuditTrailsTableComponent } from '@shared/components/smart/audit-trails/audit-trails-table/audit-trails-table.component'
import { NgLetDirective } from 'ng-let'
import { UserAuditTrailComponent } from './audit-trail/user-audit-trail/user-audit-trail.component'

@NgModule({
  declarations: [
    DetailComponent,
    DashboardComponent,
    NodeAuditTrailComponent,
    OrgAuditTrailComponent,
    UserAuditTrailComponent,
  ],
  imports: [
    CommonModule,
    DetailRoutingModule,
    AdaptersModule,
    SanitizerPipe,
    FloatingBackButtonComponent,
    TableSkeletonComponent,
    AuditTrailsTableComponent,
    NgLetDirective,
  ],
})
export class DetailModule {}
