/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { MyNodeService } from '@core/services/my-node/my-node.service'

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotFoundComponent implements OnInit {
  constructor(private _router: Router, private _myNodeService: MyNodeService) {}

  ngOnInit(): void {}

  goToHome() {
    this._router.navigateByUrl(this.url, { replaceUrl: true })
  }

  get url() {
    return `my-node/${this.brokerState}/${this.nodeState}/items`
  }

  get nodeState() {
    return this._myNodeService.nodeState
  }

  get brokerState() {
    return this._myNodeService.brokerState
  }
}
