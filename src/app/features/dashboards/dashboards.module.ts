/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { DashboardsRoutingModule } from './dashboards-routing.module'
import { DashboardsComponent } from './dashboards.component'
import { NgLetDirective } from 'ng-let'

@NgModule({
  declarations: [DashboardsComponent],
  imports: [CommonModule, DashboardsRoutingModule, NgLetDirective],
})
export class DashboardsModule {}
