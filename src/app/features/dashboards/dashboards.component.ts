/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, HostListener } from '@angular/core'
import { Router } from '@angular/router'

@Component({
  selector: 'app-dashboards',
  templateUrl: './dashboards.component.html',
  styleUrl: './dashboards.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardsComponent {

  constructor(private _router: Router) {}

  @HostListener('window:message', ['$event'])
  onMessage(event: any) {
    const data = event?.data
    if (data?.source === 'plotly-dashboard' && data?.key) {
      this._router.navigateByUrl('/detail/dashboards/' + data.key)
    }
  }
}
