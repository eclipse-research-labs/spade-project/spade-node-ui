/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { Router } from '@angular/router'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { ConfirmationService } from 'primeng/api'

@Component({
  selector: 'app-auth-card',
  templateUrl: './auth-card.component.html',
  styleUrl: './auth-card.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ConfirmationService],
})
export class AuthCardComponent {
  @Input() title!: string
  @Input() showClose: boolean = false
  @Input() routeOnBack?: string | null | undefined

  constructor(private _confirmationService: ConfirmationService, private _router: Router, private _myNodeService: MyNodeService) {}

  onBack() {
    if (this.routeOnBack) {
      this._router.navigateByUrl(this.routeOnBack)
    }
  }

  close() {
    this._myNodeService.setBasicAuth(null)
    this._router.navigateByUrl('/')
  }
}
