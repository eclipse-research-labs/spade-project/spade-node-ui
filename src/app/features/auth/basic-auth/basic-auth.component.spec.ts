/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicAuthComponent } from './basic-auth.component';

describe('BasicAuthComponent', () => {
  let component: BasicAuthComponent;
  let fixture: ComponentFixture<BasicAuthComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BasicAuthComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BasicAuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
