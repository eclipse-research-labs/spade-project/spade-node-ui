/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Component } from '@angular/core'
import { FormGroup, FormControl } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { encodeSBUrl } from 'src/app/utils'
import { BasicAuthService } from './basic-auth.service'

@Component({
  selector: 'app-basic-auth',
  templateUrl: './basic-auth.component.html',
  styleUrl: './basic-auth.component.scss',
  providers: [BasicAuthService],
})
export class BasicAuthComponent {
  authForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  })

  intro = false
  nodeAddress?: string

  constructor(private _service: BasicAuthService, private _myNodeService: MyNodeService, private _router: Router, private _activatedRoute: ActivatedRoute) {
    this._activatedRoute.queryParams.subscribe((params) => {
      this.intro = params['intro']
    })
  }

  async authenticate() {
    const address = this._myNodeService.broker?.address
    if (this.valid && address) {
      const username = this.authForm.value.username!
      const password = this.authForm.value.password!
      await this._service.authenticate(address, { username, password })
      this._router.navigateByUrl(`/my-node/offline/${encodeSBUrl(address)}`, { replaceUrl: true })
    }
  }

  get valid() {
    return this.authForm.valid
  }

  get loading$() {
    return this._service.loading$
  }

  get errorLoading$() {
    return this._service.errorLoading$
  }
}
