/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { NodesApiService } from '@core/api/modules/nodes'
import { BasicAuth } from '@core/models/user.model'
import { BaseService } from '@core/services/base'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { firstValueFrom, take } from 'rxjs'

@Injectable()
export class BasicAuthService extends BaseService {
  constructor(private _myNodeService: MyNodeService, private _nodesApi: NodesApiService) {
    super()
  }

  async authenticate(sbUrl: string, auth: BasicAuth) {
    await this.loadApiWrapper(this._authenticate(sbUrl, auth))
    this._myNodeService.setBasicAuth(auth)
  }

  private async _authenticate(sbUrl: string, auth: BasicAuth) {
    await firstValueFrom(this._nodesApi.testAuth(sbUrl, auth).pipe(take(1)))
  }
}
