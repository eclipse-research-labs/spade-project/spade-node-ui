/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { AuthRoutingModule } from './auth-routing.module'
import { AuthComponent } from './auth.component'
import { AuthCardComponent } from './components/auth-card/auth-card.component'
import { BasicAuthComponent } from './basic-auth/basic-auth.component'
import { InfoComponent } from '@shared/components/presentation/reusable/info/info.component'
import { ReactiveFormsModule } from '@angular/forms'
import { NgLetDirective } from 'ng-let'
import { ConfirmDialogModule } from 'primeng/confirmdialog'
import { MessageModule } from 'primeng/message'
import { PasswordModule } from 'primeng/password'
import { InputTextModule } from 'primeng/inputtext'

@NgModule({
  declarations: [AuthComponent, AuthCardComponent, BasicAuthComponent],
  imports: [CommonModule, ReactiveFormsModule, AuthRoutingModule, InfoComponent, NgLetDirective, ConfirmDialogModule, MessageModule, PasswordModule, InputTextModule],
})
export class AuthModule {}
