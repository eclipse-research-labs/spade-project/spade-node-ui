/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { ItemsApiService } from '@core/api/modules/items'
import { Item } from '@core/models/item.model'
import { Node } from '@core/models/node.model'
import { Organisation } from '@core/models/organisation.model'
import { User } from '@core/models/user.model'
import { firstValueFrom, take } from 'rxjs'
import { NodesApiService } from '@core/api/modules/nodes'
import { UsersApiService } from '@core/api/modules/users'
import { OrganisationApiService } from '@core/api/modules/organisations'

@Injectable({
  providedIn: 'root',
})
export class CacheService {
  private _nodesCache: Record<string, Node> = {}
  private _itemsCache: Record<string, Item> = {}
  private _usersCache: Record<string, User> = {}
  private _organisationsCache: Record<string, Organisation> = {}

  constructor(
    private _usersApiService: UsersApiService,
    private _nodesApiService: NodesApiService,
    private _itemsApiService: ItemsApiService,
    private _organisationsApiService: OrganisationApiService
  ) {}

  updateNodesCache(nodes: Node[]) {
    for (const node of nodes) {
      this._nodesCache[node.nodeId] = node
    }
  }

  updateItemsCache(items: Item[]) {
    for (const item of items) {
      this._itemsCache[item.oid] = item
    }
  }

  updateUsersCache(users: User[]) {
    for (const user of users) {
      this._usersCache[user.uid] = user
    }
  }

  updateOrganisationsCache(organisations: Organisation[]) {
    for (const organisation of organisations) {
      this._organisationsCache[organisation.cid] = organisation
    }
  }

  async getNode(nodeId: string, download: boolean = true) {
    let cached: Node | undefined = this._nodesCache[nodeId]
    if (cached) return cached
    if (download) {
      cached = await this._getNode(nodeId)
      if (cached) {
        this._nodesCache[nodeId] = cached
        return cached
      }
    }
    return undefined
  }

  async getItem(oid: string, sbUrl: string, download: boolean = true) {
    let cached: Item | undefined = this._itemsCache[oid]
    if (cached) return cached
    if (download) {
      cached = await this._getItem(oid, sbUrl)
      if (cached) {
        this._itemsCache[oid] = cached
        return cached
      }
    }
    return undefined
  }

  async getUser(uid: string, download: boolean = true) {
    let cached: User | undefined = this._usersCache[uid]
    if (cached) return cached
    if (download) {
      cached = await this._getUser(uid)
      if (cached) {
        this._usersCache[uid] = cached
        return cached
      }
    }
    return undefined
  }

  async getOrganisation(cid: string, download: boolean = true) {
    let cached: Organisation | undefined = this._organisationsCache[cid]
    if (cached) return cached
    if (download) {
      cached = await this._getOrganisation(cid)
      if (cached) {
        this._organisationsCache[cid] = cached
        return cached
      }
    }
    return undefined
  }

  private _getNode(nodeId: string) {
    return firstValueFrom(this._nodesApiService.getNode(nodeId).pipe(take(1)))
  }

  private _getItem(oid: string, sbUrl: string) {
    return firstValueFrom(this._itemsApiService.getItem(sbUrl, oid).pipe(take(1)))
  }

  private _getUser(uid: string) {
    return firstValueFrom(this._usersApiService.getUser(uid).pipe(take(1)))
  }

  private _getOrganisation(cid: string) {
    return firstValueFrom(this._organisationsApiService.getForeignOrganisation(cid).pipe(take(1)))
  }
}
