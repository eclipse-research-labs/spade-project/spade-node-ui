/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { TestBed } from '@angular/core/testing';

import { MyOrgService } from './my-org.service';

describe('MyOrgService', () => {
  let service: MyOrgService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyOrgService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
