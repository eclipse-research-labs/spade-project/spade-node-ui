/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { User } from '@core/models/user.model'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'
import { BaseService } from '../base'
import { OrganisationApiService } from '@core/api/modules/organisations'
import { ForeignOrganisation } from '@core/models/organisation.model'
import { AcceptPartnershipNotification, NewPartnershipNotification, Notification } from '@core/models/notification.model'
import { NotificationType } from '@core/enums/notification.enum'
import { Invitation } from '@core/models/invitation.model'

@Injectable({
  providedIn: 'root',
})
export class MyOrgService extends BaseService {

  private _loaded = false

  constructor(private _orgApi: OrganisationApiService) {
    super()
  }

  protected async reload(): Promise<void> {
    await this.initApiWrapper(this._init())
  }

  async init() {
    if (!this._loaded) {
      await this.initApiWrapper(this._init())
      this._loaded = true
    }
  }

  private async _init() {
    await Promise.all([this._initUsers(), this._initOrganisations(), this._initInvitations()])
  }

  // ############################################################################
  // ############################ ORGANISATIONS #################################

  private _organisations = new BehaviorSubject<ForeignOrganisation[]>([])

  organisations$ = this._organisations.asObservable()

  updateOrganisations(organisations: ForeignOrganisation[]) {
    this._organisations.next(organisations)
  }

  private async _initOrganisations() {
    const organisations = await this._getForeignOrganisaions()
    this._organisations.next(organisations)
  }

  private async _getForeignOrganisaions(): Promise<ForeignOrganisation[]> {
    // const org = await firstValueFrom(this._orgApi.getForeignOrganisations().pipe(take(1)))
    // return Array.from({length: 100}, (_, i) => org[0])
    return firstValueFrom(this._orgApi.getForeignOrganisations().pipe(take(1)))
  }

  private async _getForeignOrganisaion(cid: string): Promise<ForeignOrganisation> {
    return firstValueFrom(this._orgApi.getForeignOrganisation(cid).pipe(take(1)))
  }

  async updateOrgFromNotification(notification: Notification) {
    let payload
    let cid
    switch (notification.type) {
      case NotificationType.NEW_PARTNERSHIP_REQUEST:
        payload = JSON.parse(notification.payload!) as NewPartnershipNotification
        cid = payload.from.cid
        break
      case NotificationType.ACCEPT_PARTNERSHIP_REQUEST:
        payload = JSON.parse(notification.payload!) as AcceptPartnershipNotification
        cid = payload.by.cid
        break
      default:
        return
    }
    const org = await this._getForeignOrganisaion(cid)
    this._organisations.next([org, ...this.organisations.filter((el) => el.cid != org.cid)])
  }

  get organisations() {
    return this._organisations.value
  }

  // ############################################################################
  // ############################# INVITATIONS ##################################

  private _invitations = new BehaviorSubject<Invitation[]>([])

  invitations$ = this._invitations.asObservable()

  updateInvitations(invitations: Invitation[]) {
    this._invitations.next(invitations)
  }

  private async _initInvitations() {
    const invitations = await this._getCurrentOrgInvitations()
    this._invitations.next(invitations)
  }

  private async _getCurrentOrgInvitations(): Promise<Invitation[]> {
    return firstValueFrom(this._orgApi.getCurrentOrganisationInvitations().pipe(take(1)))
  }

  get invitations() {
    return this._invitations.value
  }

  // ############################################################################
  // ################################# USERS ####################################

  private _users = new BehaviorSubject<User[]>([])

  users$ = this._users.asObservable()

  updateUsers(users: User[]) {
    this._users.next(users)
  }

  private async _initUsers() {
    const users = await this._getCurrentOrgUsers()
    this._users.next(users)
  }

  private async _getCurrentOrgUsers(): Promise<User[]> {
    return firstValueFrom(this._orgApi.getCurrentOrganisationUsers().pipe(take(1)))
  }

  get users() {
    return this._users.value
  }
}
