/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { BaseService } from '../base'
import { Item } from '@core/models/item.model'
import { MarketplaceApiService } from '@core/api/modules/marketplace'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'
import { OrganisationApiService } from '@core/api/modules/organisations'
import { UserService } from '../user/user.service'
import { ForeignOrganisation } from '@core/models/organisation.model'

@Injectable({
  providedIn: 'root',
})
export class MarketplaceService extends BaseService {
  private _initialized = false

  private _services = new BehaviorSubject<Item[]>([])
  private _datasets = new BehaviorSubject<Item[]>([])
  private _lastUpdated = new BehaviorSubject<Date>(new Date())

  services$ = this._services.asObservable()
  datasets$ = this._datasets.asObservable()
  lastUpdated$ = this._lastUpdated.asObservable()

  constructor(
    private _marketplaceApi: MarketplaceApiService,
    private _organisationApi: OrganisationApiService,
    private _userService: UserService
  ) {
    super()
  }

  async init(force: boolean = false) {
    if ((!this._initialized || force) && !this.initializing) {
      await this.initApiWrapper(this._init())
      this._initialized = true
    }
  }

  private async _init() {
    const items = await this._getMarketplaceItems()
    const organisations = await this._getForeignOrganisations()
    this._services.next(items.services.map((item) => this._addOrganisation(item, organisations)))
    this._datasets.next(items.datasets.map((item) => this._addOrganisation(item, organisations)))
    this._lastUpdated.next(items.lastUpdated)
  }

  private _addOrganisation(item: Item, organisations: ForeignOrganisation[]) {
    const myOrg = this._userService.user?.organisation
    const cid = item['SPADE:organisation']?.['@id']?.replace('urn:organisation:', '')
    if (myOrg?.cid === cid) {
      return { ...item, organisation: myOrg }
    } else {
      const organisation = organisations.find((org) => org.cid === cid)
      return { ...item, organisation }
    }
  }

  private _getMarketplaceItems() {
    return firstValueFrom(this._marketplaceApi.getMarketplaceItems().pipe(take(1)))
  }

  private _getForeignOrganisations() {
    return firstValueFrom(this._organisationApi.getForeignOrganisations().pipe(take(1)))
  }

  get services() {
    return this._services.value
  }

  get datasets() {
    return this._datasets.value
  }

  get lastUpdated() {
    return this._lastUpdated.value
  }
}
