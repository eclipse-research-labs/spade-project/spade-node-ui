/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Inject, Injectable } from '@angular/core'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { BehaviorSubject, Observable, Subject } from 'rxjs'
import { Settings } from '@core/models/settings.model'
import { defaultSettings } from 'src/app/settings'
import { environment } from '@env'
import { stripLeadingAndTailingSlash } from 'src/app/utils'

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class MasterService {
  private _settings!: BehaviorSubject<Settings>
  private _loading = new BehaviorSubject<boolean>(false)
  private _error = new BehaviorSubject<string | null>(null)
  private _reloadApp: Subject<void> = new Subject()

  private _appUrl!: string

  settings$!: Observable<Settings>
  reloadApp$!: Observable<void>
  loading$!: Observable<boolean>
  error$!: Observable<string | null>

  constructor() {
    this._appUrl = stripLeadingAndTailingSlash(
      window.location.origin + '/' + stripLeadingAndTailingSlash(environment.prefixUrl)
    )
    this._init()
  }

  private _init() {
    var settings = JSON.parse(localStorage.getItem('settings') ?? 'null')
    if (!settings) {
      settings = defaultSettings
      localStorage.setItem('settings', JSON.stringify(settings))
    }
    this._settings = new BehaviorSubject<Settings>(settings)
    this._settings.pipe(untilDestroyed(this)).subscribe((value) => {
      localStorage.setItem('settings', JSON.stringify(value))
    })
    this.settings$ = this._settings.asObservable()
    this.reloadApp$ = this._reloadApp.asObservable()
    this.loading$ = this._loading.asObservable()
    this.error$ = this._error.asObservable()
  }

  updateSettings(settings: Settings) {
    this._settings.next(settings)
  }

  reloadApp() {
    this._reloadApp.next()
  }

  updateLoading(loading: boolean) {
    this._loading.next(loading)
  }

  updateError(error: string) {
    this._error.next(error)
  }

  get settings() {
    return this._settings.value
  }

  get loading() {
    return this._loading.value
  }

  get error() {
    return this._error.value
  }

  get appUrl() {
    return this._appUrl
  }
}
