/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Inject, Injectable } from '@angular/core'
import { BaseService } from '../base'
import { NodesApiService } from '@core/api/modules/nodes'
import { Node, DB } from '@core/models/node.model'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'
import { Item, ItemWithIdentity } from '@core/models/item.model'
import { ItemsApiService } from '@core/api/modules/items'
import { AdaptersApiService } from '@core/api/modules/adapters'
import { Adapter } from '@core/models/adapter.model'
import { Credential } from '@core/models/credentials.model'
import { BasicAuth } from '@core/models/user.model'
import { NodesService } from '../nodes/nodes.service'
import { HttpErrorResponse } from '@angular/common/http'
import { encodeSBUrl, isString } from 'src/app/utils'
import { CredentialsApiService } from '@core/api/modules/credentials'
import { Contract } from '@core/models/contract.model'
import { ContractsApiService } from '@core/api/modules/contracts'
import { ContractAccessType, ContractStatus } from '@core/enums/contract.enum'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { CacheService } from '../cache/cache.service'

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class MyNodeService extends BaseService {
  private _initialized = false

  constructor(
    private _nodesService: NodesService,
    private _cacheService: CacheService,
    private _nodesApiService: NodesApiService,
    private _itemsApiService: ItemsApiService,
    private _adaptersApiService: AdaptersApiService,
    private _contractsApiService: ContractsApiService,
    private _credentialsApiService: CredentialsApiService,
    @Inject('SOUTH_BOUND_URL') private _sbUrl: string
  ) {
    super()
    this._listenForContractsChange()
  }

  protected async reload() {
    await this.initApiWrapper(this._init())
  }

  async init(force: boolean = false) {
    if ((!this._initialized || force) && !this.initializing) {
      await this.initApiWrapper(this._init())
      this._initialized = true
    }
  }

  private async _init() {
    if (this.node || this.broker?.address) {
      await Promise.all([this._initItems(), this._initApaters(), this._initCredentials()])
      await this._initContracts()
    }
  }

  get sbUrl() {
    return this.node?.host ? `https://${this.node.host}` : this.broker!.address
  }

  // ############################################################################
  // ############################## NODE ########################################

  private _node = new BehaviorSubject<Node | null>(null)
  private _broker = new BehaviorSubject<DB | null>(null)
  private _basicAuth = new BehaviorSubject<BasicAuth | null>(null)
  private _online = new BehaviorSubject<boolean>(true)
  private _healthcheckError = new BehaviorSubject<string | undefined | null>(null)

  healthcheckError$ = this._healthcheckError.asObservable()
  basicAuth$ = this._basicAuth.asObservable()
  online$ = this._online.asObservable()
  broker$ = this._broker.asObservable()
  node$ = this._node.asObservable()

  async healthcheck(sbUrl?: string, failWhenDBNotOnline: boolean = false) {
    this._broker.next(null)
    this._healthcheckError.next(null)
    const address = sbUrl ?? this._sbUrl
    try {
      const healthcheck = await this.loadApiWrapper(this._healthcheck(address))
      if (failWhenDBNotOnline && !healthcheck.DB.online) {
        throw Error('DataBroker not online')
      }
      this._broker.next({ ...healthcheck.DB, address })
    } catch (e) {
      if (e instanceof HttpErrorResponse) {
        if (e.status === 0 && e.error instanceof ProgressEvent) {
          this._healthcheckError.next(address)
          return
        }
      }
      throw e
    }
  }

  async updateNodeBuildInfo() {
    const broker = this._broker.value
    const node = this._node.value
    if (broker && node && node.nodeId && (broker.version || broker.buildTime)) {
      await this.loadApiWrapper(this._updateNodeBuildInfo(node.nodeId, broker.version, broker.buildTime))
    }
  }

  private async _updateNodeBuildInfo(nodeId: string, version?: string, buildTime?: string) {
    return await firstValueFrom(this._nodesApiService.updateNodeBuildInfo(nodeId, version, buildTime).pipe(take(1)))
  }

  private async _healthcheck(sbUrl: string) {
    return await firstValueFrom(this._nodesApiService.healthcheck(sbUrl).pipe(take(1)))
  }

  addNode(node: Node) {
    this._nodesService.addNode(node)
    this._node.next(node)
  }

  selectNode(node: Node) {
    this._node.next(node)
  }

  setOnline(online: boolean) {
    this._online.next(online)
  }

  setBasicAuth(basicAuth: BasicAuth | null) {
    this._basicAuth.next(basicAuth)
  }

  clearHealthcheckError() {
    this._healthcheckError.next(null)
  }

  get healthcheckError() {
    return this._healthcheckError.value
  }

  get online() {
    return this._online.value
  }

  get node() {
    return this._node.value
  }

  get broker() {
    return this._broker.value
  }

  get basicAuth() {
    return this._basicAuth.value
  }

  get nodeState() {
    return this.online ? this.node?.nodeId : encodeSBUrl(this.broker?.address ?? '')
  }

  get brokerState() {
    return this.online ? 'online' : 'offline'
  }

  // ############################################################################
  // ############################### ITEMS ######################################

  private _myItems = new BehaviorSubject<Item[]>([])

  myItems$ = this._myItems.asObservable()

  private _newItems: string[] = []

  private async _initItems() {
    const items = await this._getMyItems()
    this._cacheService.updateItemsCache(items)
    this._myItems.next(
      items.sort((a, b) => {
        return a.title.localeCompare(b.title, undefined, { numeric: true, sensitivity: 'base' })
      })
    )
  }

  private async _getMyItems(): Promise<Item[]> {
    return firstValueFrom(this._itemsApiService.getMyNodeItems(this.sbUrl).pipe(take(1)))
  }

  isNewItem(item: Item) {
    return this._newItems.includes(item.oid)
  }

  removeFromNew(item: Item) {
    this._newItems = this._newItems.filter((oid) => oid != item.oid)
  }

  addToNew(item: Item) {
    if (!this.isNewItem(item)) {
      this._newItems.push(item.oid)
    }
  }

  updateMyItems(items: Item[]) {
    this._myItems.next(items)
  }

  get myItems() {
    return this._myItems.value
  }

  // ############################################################################
  // ############################# ADAPTERS #####################################

  private _adapters = new BehaviorSubject<Adapter[]>([])

  adapters$ = this._adapters.asObservable()

  private _newAdapters: string[] = []

  private async _initApaters() {
    const adapters = await this._getAdapters()
    this._adapters.next(adapters)
  }

  private async _getAdapters(): Promise<Adapter[]> {
    return await firstValueFrom(this._adaptersApiService.getAdapters(this.sbUrl).pipe(take(1)))
  }

  isNewAdapter(adapter: Adapter) {
    return this._newAdapters.includes(adapter.adid)
  }

  updateAdapters(adapters: Adapter[]) {
    this._adapters.next(adapters)
  }

  get adapters() {
    return this._adapters.value
  }

  // ############################################################################
  // ############################ CONTRACTS ######################################

  private _contracts = new BehaviorSubject<Contract[]>([])

  /** Exigent contracts are those that require interaction: accept | reject */
  private _exigentContracts = new BehaviorSubject<Record<string, Contract>>({})

  contracts$ = this._contracts.asObservable()
  exigentContracts$ = this._exigentContracts.asObservable()

  updateContracts(contracts: Contract[]) {
    this._contracts.next(contracts)
  }

  isExigentContract(contract: Contract) {
    return (
      this._exigentContracts.value[contract.policyId] !== undefined &&
      this._exigentContracts.value[contract.policyId] !== null
    )
  }

  hasExigentContracts(oid?: string): boolean {
    const keys = Object.keys(this._exigentContracts.value)
    if (oid) {
      return (
        keys.length > 0 &&
        keys.some((element) => {
          const target = this._exigentContracts.value[element].target
          if (!target) return false
          const id = isString(target) ? target : target.oid
          const id1 = id.split(':').at(-1) ?? id
          const id2 = oid.split(':').at(-1) ?? oid
          return id1 == id2
        })
      )
    }
    return keys.length > 0
  }

  private async _initContracts() {
    if (this.online) {
      const contracts = await this._getContracts()
      this._contracts.next(contracts)
    }
  }

  private async _getContracts(): Promise<Contract[]> {
    return await firstValueFrom(this._contractsApiService.getContracts(this.sbUrl).pipe(take(1)))
  }

  private _listenForContractsChange() {
    this.contracts$.pipe(untilDestroyed(this)).subscribe((_) => this._initExigentContracts(this._contracts.value))
  }

  private _initExigentContracts(contracts: Contract[]) {
    const exigentContracts: Record<string, Contract> = {}
    contracts.forEach((contract) => {
      if (this._isExigentContract(contract)) {
        exigentContracts[contract.policyId] = contract
      }
    })
    this._exigentContracts.next(exigentContracts)
  }

  private _isExigentContract(contract: Contract) {
    const target = contract.target
    if (!target) return false
    const urn = isString(target) ? target : (target as ItemWithIdentity | Item).oid
    const nodeId = urn.split(':').at(-2) ?? urn
    return (
      (contract.status === ContractStatus.WAITING_ON_ASSIGNEE ||
        contract.status === ContractStatus.WAITING_ON_ASSIGNER) &&
      ((nodeId == this.node!.nodeId && contract.accessType == ContractAccessType.REQUEST) ||
        (nodeId != this.node!.nodeId && contract.accessType == ContractAccessType.GRANT))
    )
  }

  get contracts() {
    return this._contracts.value
  }

  // ############################################################################
  // ############################## CREDENTIALS #################################

  private _credentials = new BehaviorSubject<Credential[]>([])

  credentials$ = this._credentials.asObservable()

  private async _initCredentials() {
    if (this.online) {
      const credentials = await this._getCredentials()
      this._credentials.next(credentials)
    }
  }

  private async _getCredentials(): Promise<Credential[]> {
    return await firstValueFrom(this._credentialsApiService.getCredentials(this.sbUrl).pipe(take(1)))
  }

  updateCredentials(credentials: Credential[]) {
    this._credentials.next(credentials)
  }

  get credentials() {
    return this._credentials.value
  }
}
