/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Inject, Injectable } from '@angular/core'
import { Notification } from '@core/models/notification.model'
import { KeycloakService } from 'keycloak-angular'
import { BehaviorSubject, defer, firstValueFrom, retry, take, timer } from 'rxjs'
import SockJS from 'sockjs-client/dist/sockjs.js'
import { BaseService } from '../base'
import { NotificationApiService } from '@core/api/modules/notifications'
import { MessageService } from 'primeng/api'
import { MyOrgService } from '../my-org/my-org.service'

const interval = 800

@Injectable({
  providedIn: 'root',
})
export class NotificationService extends BaseService {
  private _connection?: WebSocket

  private _notifications = new BehaviorSubject<Notification[]>([])
  private _newNotifications = new BehaviorSubject<string[]>([])

  notifications$ = this._notifications.asObservable()
  newNotifications$ = this._newNotifications.asObservable()

  constructor(
    @Inject('CC_URL') private _ccUrl: string,
    private _keycloak: KeycloakService,
    private _notificationApi: NotificationApiService,
    private _messageService: MessageService,
    private _myOrgService: MyOrgService
  ) {
    super()
  }

  public async initConnection() {
    await this._connect().catch((_) => {
      console.warn(`Connection to Notification server ${this._ccUrl}/ws failed`)
      if (!this._connection || this._connection.readyState !== WebSocket.OPEN) {
        this._reconnect()
      }
    })
    if (this._connection) {
      await this._connectUserClient()
      this._listenForNotifications()
    }
  }

  public async initNotifications() {
    const notifications = await this.initApiWrapper(this._getNotifications())
    this._notifications.next(notifications)
  }

  private _listenForNotifications() {
    this._connection!.onmessage = this._onmessage
  }

  private async _connectUserClient() {
    const token = await this._keycloak.getToken()
    if (!token) {
      return
    }
    if (this._connection!.readyState !== WebSocket.OPEN) {
      return
    }
    this._connection!.send(JSON.stringify({ token }))
  }

  private async _connect() {
    this._connection = new SockJS(`${this._ccUrl}/ws`)
    this._connection.onerror = this._onerror
    this._connection.onopen = this._onopen
    await this._waitForConnectionToEstablish()
    if (this._connection.readyState !== WebSocket.OPEN) {
      throw Error(`Failed to connect to Notification server ${this._ccUrl}/ws`)
    }
    await this._connectUserClient()
    console.log(`Connection to Notification server ${this._ccUrl}/ws established!`)
    await this.initNotifications()
  }

  private async _reconnect() {
    console.log('Attempting to reconnect')
    defer(() => this._connect())
      .pipe(
        retry({
          count: 10,
          delay: (_, index) => {
            console.log('retrying connection to Notification server for ' + index + ' time')
            const delay = Math.pow(2, index - 1) * interval
            return timer(delay)
          },
        })
      )
      .subscribe()
  }

  private _onerror = () => {
    this._connection!.close()
  }

  private _onopen = () => {
    this._connection!.onclose = this._onclose
  }

  private _onclose = () => {
    this._reconnect()
  }

  private _onmessage = (message: MessageEvent) => {
    const data = message.data
    const notification = JSON.parse(data) as Notification
    if (!this.notifications.some(element => element.notifId == notification.notifId)) {
      this._newNotifications.next([notification.notifId, ...this.newNotifications])
      this._notifications.next([notification, ...this.notifications])
      this._messageService.add({ key: 'notification', severity: 'info', detail: data })
      this._myOrgService.updateOrgFromNotification(notification)
    }
  }

  private async _waitForConnectionToEstablish() {
    await new Promise<void>((resolve, _) => {
      const timer = setInterval(() => {
        if (this._connection!.readyState !== WebSocket.CONNECTING) {
          clearInterval(timer)
          resolve()
        }
      }, 10)
    })
  }

  private async _getNotifications() {
    return await firstValueFrom(this._notificationApi.getMyNotifications().pipe(take(1)))
  }

  isNewNotification(notification: Notification) {
    return this._newNotifications.value.includes(notification.notifId)
  }

  markAllSeen() {
    this._newNotifications.next([])
  }

  markSeen(notification: Notification) {
    this._newNotifications.next([...this.newNotifications.filter((el) => el != notification.notifId)])
  }

  get hasNewNotifications() {
    return this._newNotifications.value.length > 0
  }

  get notifications() {
    return this._notifications.value
  }

  get newNotifications() {
    return this._newNotifications.value
  }

  get online() {
    return this._connection?.readyState == WebSocket.OPEN
  }
}
