/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { BaseService } from '../base'
import { NodesApiService } from '@core/api/modules/nodes'
import { AuditTrail, Status } from '@core/models/audit-trail.model'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'
import { MyNodeService } from '../my-node/my-node.service'
import { OrganisationApiService } from '@core/api/modules/organisations'
import { UsersApiService } from '@core/api/modules/users'
import { UserService } from '../user/user.service'
import { UserRole } from '@core/enums/user.enum'

@Injectable({
  providedIn: 'root',
})
export class AuditTrailsService extends BaseService {
  private _nodeAuditTrails = new BehaviorSubject<AuditTrail[]>([])
  private _orgAuditTrails = new BehaviorSubject<AuditTrail[]>([])
  private _userAuditTrails = new BehaviorSubject<AuditTrail[]>([])

  private _initialized = false

  nodeAuditTrails$ = this._nodeAuditTrails.asObservable()
  orgAuditTrails$ = this._orgAuditTrails.asObservable()
  userAuditTrails$ = this._userAuditTrails.asObservable()

  constructor(
    private _myNodeService: MyNodeService,
    private _userService: UserService,
    private _nodesApi: NodesApiService,
    private _orgApi: OrganisationApiService,
    private _userApi: UsersApiService
  ) {
    super()
  }

  async init(page?: number, status?: Status, force?: boolean) {
    if (!this._initialized || force) {
      await this.initApiWrapper(
        Promise.all([
          this._initNodeAuditTrails(page, status),
          this._initOrgAuditTrails(page, status),
          this._initUserAuditTrails(page, status),
        ])
      )
      this._initialized = true
    }
  }

  private async _initNodeAuditTrails(page?: number, status?: Status) {
    if (this.node || this.broker) {
      const nodeAuditTrails = await this.getNodeAuditTrails(page, status)
      this._nodeAuditTrails.next(nodeAuditTrails)
    }
  }

  private async _initOrgAuditTrails(page?: number, status?: Status) {
    if (this.user && this.user.organisation && this.user.roles.includes(UserRole.ADMIN)) {
      const orgAuditTrails = await this.getOrgAuditTrails(page, status)
      this._orgAuditTrails.next(orgAuditTrails)
    }
  }

  private async _initUserAuditTrails(page?: number, status?: Status) {
    const userAuditTrails = await this.getUserAuditTrails(page, status)
    this._userAuditTrails.next(userAuditTrails)
  }

  async loadNodePage(page: number, status?: Status) {
    await this.loadApiWrapper(this._loadNodePage(page, status))
  }

  async loadOrgPage(page: number, status?: Status) {
    await this.loadApiWrapper(this._loadOrgPage(page, status))
  }

  async loadUserPage(page: number, status?: Status) {
    await this.loadApiWrapper(this._loadUserPage(page, status))
  }

  private async _loadNodePage(page: number, status?: Status) {
    const nodeAuditTrails = await this.getNodeAuditTrails(page, status)
    this._nodeAuditTrails.next(nodeAuditTrails)
  }

  private async _loadOrgPage(page: number, status?: Status) {
    const orgAuditTrails = await this.getOrgAuditTrails(page, status)
    this._orgAuditTrails.next(orgAuditTrails)
  }

  private async _loadUserPage(page: number, status?: Status) {
    const userAuditTrails = await this.getUserAuditTrails(page, status)
    this._userAuditTrails.next(userAuditTrails)
  }

  async getNodeAuditTrails(page?: number, status?: Status) {
    return await firstValueFrom(
      this._nodesApi
        .getAuditTrails(this._myNodeService.sbUrl, page, status == Status.ALL ? undefined : status)
        .pipe(take(1))
    )
  }

  async getOrgAuditTrails(page?: number, status?: Status) {
    return await firstValueFrom(
      this._orgApi.getCurrentOrganisationAuditTrails(page, status == Status.ALL ? undefined : status).pipe(take(1))
    )
  }

  async getUserAuditTrails(page?: number, status?: Status) {
    return await firstValueFrom(
      this._userApi.getCurrentUserAuditTrails(page, status == Status.ALL ? undefined : status).pipe(take(1))
    )
  }

  get nodeAuditTrails() {
    return this._nodeAuditTrails.value
  }

  get orgAuditTrails() {
    return this._orgAuditTrails.value
  }

  get userAuditTrails() {
    return this._userAuditTrails.value
  }

  get user() {
    return this._userService.user
  }

  get node() {
    return this._myNodeService.node
  }

  get broker() {
    return this._myNodeService.broker
  }
}
