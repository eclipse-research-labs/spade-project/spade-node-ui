/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject'
import { MasterService } from './master/master.service'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { HttpErrorResponse } from '@angular/common/http'
import { inject } from '@angular/core'

@UntilDestroy()
export class BaseService {
  private _loading = new BehaviorSubject<boolean>(false)
  private _errorLoading = new BehaviorSubject<string | null>(null)
  private _initializing = new BehaviorSubject<boolean>(false)
  private _errorInitializing = new BehaviorSubject<string | null>(null)


  loading$ = this._loading.asObservable()
  initializing$ = this._initializing.asObservable()
  errorLoading$ = this._errorLoading.asObservable()
  errorInitializing$ = this._errorInitializing.asObservable()

  private _masterService = inject(MasterService)

  constructor() {
    this._listenForAppReload()
  }


  protected async initApiWrapper<T>(callbackPromise: Promise<T>) {
    this.updateInitializing(true)
    this.updateErrorInitializing(null)
    try {
      return await callbackPromise
    } catch (e: any) {
      if (e instanceof HttpErrorResponse) {
        this.updateErrorInitializing(e.message)
      } else {
        this.updateErrorInitializing(e.toString())
      }
      throw e
    } finally {
      this.updateInitializing(false)
    }
  }

  protected async loadApiWrapper<T>(callbackPromise: Promise<T>) {
    this.updateLoading(true)
    this.updateErrorLoading(null)
    try {
      return await callbackPromise
    } catch (e: any) {
      if (e instanceof HttpErrorResponse) {
        this.updateErrorLoading(e.message)
      } else {
        this.updateErrorLoading(e.toString())
      }
      throw e
    } finally {
      this.updateLoading(false)
    }
  }

  private _listenForAppReload() {
    this._masterService.reloadApp$.pipe(untilDestroyed(this)).subscribe(async () => {
      await this.reload()
    })
  }

  /**
   * This method should be overriden by child when there is some logic
   * to be executed after the call to {@link MasterService.reloadApp} is triggered.
   * Intended use case is "Try again" buttons.
   */
  protected async reload(): Promise<void> {}

  updateInitializing(initializing: boolean) {
    this._initializing.next(initializing)
  }

  updateLoading(loading: boolean) {
    this._loading.next(loading)
  }

  updateErrorInitializing(error: string | null) {
    this._errorInitializing.next(error)
  }

  updateErrorLoading(error: string | null) {
    this._errorLoading.next(error)
  }

  get initializing() {
    return this._initializing.value
  }

  get loading() {
    return this._loading.value
  }

  get errorInitializing() {
    return this._errorInitializing.value
  }

  get errorLoading() {
    return this._errorLoading.value
  }

  get appUrl() {
    return this._masterService.appUrl
  }
}
