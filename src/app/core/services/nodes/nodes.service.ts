/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { BaseService } from '../base'
import { Node } from '@core/models/node.model'
import { NodesApiService } from '@core/api/modules/nodes'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'
import { CacheService } from '../cache/cache.service'
import { parseNodeVersionFromVersionString } from 'src/app/utils'
import { environment } from '@env'

@Injectable({
  providedIn: 'root',
})
export class NodesService extends BaseService {
  private _nodeLatestVersion?: string
  private _nodes = new BehaviorSubject<Node[] | null>(null)

  nodes$ = this._nodes.asObservable()

  private _newNodes: string[] = []

  constructor(private _nodesApiService: NodesApiService, private _cacheService: CacheService) {
    super()
  }

  protected async reload() {
    await this.initApiWrapper(this._init())
  }

  async init() {
    if (!this.nodes) {
      await this.initApiWrapper(this._init())
    }
  }

  private async _init() {
    await this._initLatestVersion()
    await Promise.all([this._initNodes()])
  }

  private async _initLatestVersion() {
    try {
      this._nodeLatestVersion = await this._getLatestVersion()
    } catch (error) {
      console.warn('Failed to get info about the latest version of the Broker')
    }
  }

  private async _initNodes() {
    const nodes = (await this._getMyOrgNodes()).map((node) => {
      const isOutdated =
        environment.production && node.version && this._nodeLatestVersion
          ? parseNodeVersionFromVersionString(node.version) != this._nodeLatestVersion
          : false
      return {
        ...node,
        isOutdated,
        isOutdatedMessage: isOutdated
          ? "Data Broker is outdated. Some functionalities may not work as expected. Please run 'docker compose pull' to update the Broker."
          : undefined,
      }
    })
    this._cacheService.updateNodesCache(nodes)
    this._nodes.next(nodes)
  }

  private async _getLatestVersion(): Promise<string> {
    return await firstValueFrom(this._nodesApiService.getNodeLatestVersion().pipe(take(1)))
  }

  private async _getMyOrgNodes(): Promise<Node[]> {
    return await firstValueFrom(this._nodesApiService.getMyOrgNodes().pipe(take(1)))
  }

  addNode(node: Node) {
    this._newNodes.push(node.nodeId)
    this._nodes.next([...(this.nodes ?? []), node])
  }

  isNewNode(node: Node) {
    return this._newNodes.includes(node.nodeId)
  }

  removeFromNew(node: Node) {
    this._newNodes = this._newNodes.filter((nodeId) => nodeId != node.nodeId)
  }

  updateNodes(nodes: Node[]) {
    this._nodes.next(nodes)
  }

  get nodes() {
    return this._nodes.value
  }
}
