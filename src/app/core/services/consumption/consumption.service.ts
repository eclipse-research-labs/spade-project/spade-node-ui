/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { BaseService } from '../base'
import { ConsumptionWithStatusAndResult, Consumption } from '@core/models/consumption.model'
import { BehaviorSubject } from 'rxjs'
import { AdaptersApiService } from '@core/api/modules/adapters'
import { MyNodeService } from '../my-node/my-node.service'
import { HttpEventType, HttpResponse } from '@angular/common/http'
import { delay } from 'src/app/utils'

@Injectable({
  providedIn: 'root',
})
export class ConsumptionService extends BaseService {
  private _consumption = new BehaviorSubject<ConsumptionWithStatusAndResult[]>([])
  private _opened = new BehaviorSubject<boolean>(false)

  consumption$ = this._consumption.asObservable()
  opened$ = this._opened.asObservable()

  constructor(private _adapterApi: AdaptersApiService, private _myNodeService: MyNodeService) {
    super()
  }

  async addConsumption(consumption: Consumption) {
    const status = new BehaviorSubject<number>(0)
    const result = new BehaviorSubject<HttpResponse<any> | undefined>(undefined)
    const error = new BehaviorSubject<any | undefined>(undefined)
    const consumptionWitStatus: ConsumptionWithStatusAndResult = {
      ...consumption,
      status,
      status$: status.asObservable(),
      result,
      result$: result.asObservable(),
      error,
      error$: error.asObservable(),
    }
    this._consumption.next([consumptionWitStatus, ...this._consumption.value])
    // Await so the mouse click event in ConsumptionComponent which closes dialog on click outside is executed before this
    await delay(100)
    this._opened.next(true)
    this.triggerConsumption(consumptionWitStatus)
  }

  async triggerConsumption(consumption: ConsumptionWithStatusAndResult, delayBeforeTiggering?: number) {
    consumption.status.next(0)
    consumption.error.next(undefined)
    await delay(delayBeforeTiggering ?? 400)
    if (consumption.op == 'READ' || consumption.op == 'readproperty') {
      this._readProperty(consumption)
    }
    if (consumption.op == 'WRITE' || consumption.op == 'writeproperty') {
      this._writeProperty(consumption)
    }
  }

  clear() {
    this._consumption.next([])
  }

  open() {
    if (this._opened.value != true) {
      this._opened.next(true)
    }
  }

  close() {
    if (this._opened.value != false) {
      this._opened.next(false)
    }
  }

  private _readProperty(consumption: ConsumptionWithStatusAndResult) {
    const contentType = consumption.contentType
    const jsonApi = this._adapterApi.readJsonProperty
    const fileApi = this._adapterApi.readFileProperty
    const textApi = this._adapterApi.readTextProperty
    const consumptionApi = !contentType
      ? fileApi
      : contentType == 'application/json'
      ? jsonApi
      : contentType == 'text/plain'
      ? textApi
      : fileApi
    const test = consumptionApi
      .call(this._adapterApi, this.sbUrl, consumption.item.oid, consumption.property.key, consumption.params)
      .subscribe({
        next: (event: any) => {
          if (event && event.type === HttpEventType.DownloadProgress) {
            consumption.status.next(Math.round((100 * event.loaded) / event.total))
          }
          if (event && event.type === HttpEventType.Response) {
            consumption.status.next(100)
            consumption.result.next(event as HttpResponse<any>)
          }
        },
        error: (err: any) => {
          consumption.status.next(0)
          consumption.error.next(err)
        },
      })
  }

  private _writeProperty(consumption: ConsumptionWithStatusAndResult) {
    this._adapterApi
      .writeProperty(
        this.sbUrl,
        consumption.item.oid,
        consumption.property.key,
        consumption.payload?.data,
        consumption.params,
        consumption.payload?.contentType
      )
      .subscribe({
        next: (event: any) => {
          if (event && event.type === HttpEventType.UploadProgress) {
            consumption.status.next(Math.round((100 * event.loaded) / event.total))
          }
          if (event && event.type === HttpEventType.Response) {
            consumption.status.next(100)
            consumption.result.next(event as HttpResponse<any>)
          }
        },
        error: (err: any) => {
          consumption.status.next(0)
          consumption.error.next(err)
        },
      })
  }

  get sbUrl() {
    return this._myNodeService.online ? this._myNodeService.node!.host : this._myNodeService.broker!.address
  }

  get opened() {
    return this._opened.value
  }
}
