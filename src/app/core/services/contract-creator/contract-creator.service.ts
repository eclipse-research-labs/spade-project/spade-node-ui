/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { BaseService } from '../base'
import { HttpErrorResponse, HttpStatusCode } from '@angular/common/http'
import { ContractsApiService } from '@core/api/modules/contracts'
import { NodesApiService } from '@core/api/modules/nodes'
import { ContractRequestStatus, ContractAccessType, ContractStatus } from '@core/enums/contract.enum'
import { Contract, NewContract } from '@core/models/contract.model'
import { ItemWithIdentity } from '@core/models/item.model'
import { NodeWithOwner } from '@core/models/node.model'
import { Organisation } from '@core/models/organisation.model'
import { User } from '@core/models/user.model'
import { TreeNodeWithStatus } from '@core/types/contract.types'
import { TreeNode } from 'primeng/api'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'
import { MyNodeService } from '../my-node/my-node.service'
import { UserService } from '../user/user.service'
import { v4 as uuidv4 } from 'uuid'
import { chunkArray, delay, parseNodeVersionFromVersionString } from 'src/app/utils'
import { environment } from '@env'

@Injectable({
  providedIn: 'root',
})
export class ContractCreatorService extends BaseService {
  private _assigneeCandidates = new BehaviorSubject<NodeWithOwner[]>([])
  private _assigneeCandidatesTree: TreeNode[] = []
  private _contracts = new BehaviorSubject<Contract[]>([])
  private _contractsTree: TreeNodeWithStatus[] = []
  private _newContracts?: NewContract

  private _hasFailed = false

  constructor(
    private _myNodeService: MyNodeService,
    private _userService: UserService,
    private _nodesApiService: NodesApiService,
    private _contractsApi: ContractsApiService
  ) {
    super()
  }

  init(newContracts: NewContract) {
    this._newContracts = newContracts
  }

  clear() {
    this._newContracts = undefined
  }

  initContracts() {
    if (this._newContracts?.accessType) {
      this._generateContracts()
      this._generateContractsTree()
    }
  }

  private _generateContracts() {
    const contracts: Contract[] = []
    if (this._newContracts) {
      this._newContracts.assignees.forEach((assignee) => {
        this._newContracts!.targets.forEach((target) => {
          const assigner = this.assigner
          const read = this._newContracts!.read
          const write = this._newContracts!.write
          const subscribe = this._newContracts!.subscribe
          const accessType = this._newContracts!.accessType!
          const newContract = _generateContract(assignee, assigner, target, read, write, subscribe, accessType)
          contracts.push(newContract)
        })
      })
      this._contracts.next(contracts)
    }
  }

  private _generateContractsTree() {
    this._contractsTree = []
    for (const contract of this._contracts.value) {
      const node = contract.assignee as NodeWithOwner
      const owner = node.owner
      const orgTreeNode = this._contractsTree.find(
        (element) => (element.data as Organisation | undefined)?.cid == owner.cid
      )
      if (orgTreeNode) {
        const nodeTreeNode = orgTreeNode.children?.find(
          (element) => (element.data as NodeWithOwner | undefined)?.nodeId == node.nodeId
        )
        if (nodeTreeNode) {
          nodeTreeNode.children ??= []
          nodeTreeNode.children.push(_generateContractTreeContractNode(contract))
        } else {
          orgTreeNode.children ??= []
          orgTreeNode.children.push(_generateNodeTreeContractNode(node, contract))
        }
      } else {
        this._contractsTree.push(_generateOrgTreeContractNode(owner, node, contract))
      }
    }
  }

  async initAssigneeCandidates() {
    await this.initApiWrapper(this._initAssigneesCandidates())
    this._generateAssigneesCandidatesTree()
  }

  private async _initAssigneesCandidates() {
    const myPartnersNodes = await this._getAssigneesCandidates()
    let nodeLatestVersion: string | null | undefined = undefined
    try {
      nodeLatestVersion = await this._getLatestVersion()
    } catch (error) {
      console.warn('Failed to get info about the latest version of the Broker')
    }
    if (nodeLatestVersion) {
      this._assigneeCandidates.next(
        myPartnersNodes.map((node) => {
          const isOutdated =
            environment.production && node.version && nodeLatestVersion
              ? parseNodeVersionFromVersionString(node.version) != nodeLatestVersion
              : false
          return {
            ...node,
            isOutdated,
            isOutdatedMessage: isOutdated
              ? 'Data Broker is outdated. Some functionalities may not work as expected. Please contact node owner.'
              : undefined,
          }
        })
      )
    } else {
      this._assigneeCandidates.next(myPartnersNodes)
    }
  }

  private async _getAssigneesCandidates() {
    return await firstValueFrom(this._nodesApiService.getMyOrgPartnersNodes().pipe(take(1)))
  }

  private async _getLatestVersion(): Promise<string> {
    return await firstValueFrom(this._nodesApiService.getNodeLatestVersion().pipe(take(1)))
  }

  private _generateAssigneesCandidatesTree() {
    this._assigneeCandidatesTree = []
    for (const node of this._assigneeCandidates.value) {
      const owner = node.owner
      const orgTreeNode = this._assigneeCandidatesTree.find(
        (element) => (element.data as Organisation | undefined)?.cid == owner.cid
      )
      if (orgTreeNode) {
        orgTreeNode.children ??= []
        orgTreeNode.children.push(_generateNodeTreeAssigneeCandidateNode(node, this._isChecked(node)))
      } else {
        this._assigneeCandidatesTree.push(_generateOrgTreeAssigneeCandidateNode(owner, node, this._isChecked(node)))
      }
    }
  }

  private _isChecked(node: NodeWithOwner) {
    return this._newContracts?.assignees.some((element) => element.nodeId == node.nodeId) ?? false
  }

  async sendContracts(status?: ContractRequestStatus) {
    this._hasFailed = false
    await this.loadApiWrapper(this._sendContracts(status))
  }

  private async _sendContracts(status?: ContractRequestStatus) {
    await Promise.allSettled(this._contractsTree.map((element) => this._sendOrgContracts(element, status)))
  }

  private async _sendOrgContracts(orgNode: TreeNodeWithStatus, status?: ContractRequestStatus) {
    orgNode.control.next(ContractRequestStatus.SENDING)
    let failed = undefined
    let conflict = undefined
    try {
      await Promise.all(
        ((orgNode.children ?? []) as TreeNodeWithStatus[]).map((element) => this._sendNodeContracts(element, status))
      )
    } catch (e) {
      if (e instanceof HttpErrorResponse && e.status == HttpStatusCode.Conflict) {
        conflict = e
      } else {
        failed = e
      }
    }
    if (failed) {
      orgNode.control.next(ContractRequestStatus.FAILED)
      throw failed
    } else if (conflict) {
      orgNode.control.next(ContractRequestStatus.CONFLICT)
      throw conflict
    } else {
      orgNode.control.next(ContractRequestStatus.DONE)
    }
  }

  private async _sendNodeContracts(nodeNode: TreeNodeWithStatus, status?: ContractRequestStatus) {
    nodeNode.control.next(ContractRequestStatus.SENDING)
    const contracts = (nodeNode.children ?? []) as TreeNodeWithStatus[]
    contracts.forEach((element) => {
      element.control.next(ContractRequestStatus.SENDING)
    })
    let failed = undefined
    let conflict = undefined
    for (const chunk of chunkArray(contracts)) {
      try {
        await Promise.all((chunk as TreeNodeWithStatus[]).map((element) => this._sendContract(element, status)))
      } catch (e) {
        if (e instanceof HttpErrorResponse && e.status == HttpStatusCode.Conflict) {
          conflict = e
        } else {
          failed = e
        }
      }
      await delay(500)
    }
    if (failed) {
      nodeNode.control.next(ContractRequestStatus.FAILED)
      throw failed
    } else if (conflict) {
      nodeNode.control.next(ContractRequestStatus.CONFLICT)
      throw conflict
    } else {
      nodeNode.control.next(ContractRequestStatus.DONE)
    }
  }

  private async _sendContract(contractNode: TreeNodeWithStatus, status?: ContractRequestStatus) {
    if (status && contractNode.control.value != status) return
    try {
      // contractNode.control.next(ContractRequestStatus.SENDING)
      const contract = contractNode.data as Contract | undefined
      if (contract) {
        await firstValueFrom(
          this._contractsApi.sendContractRequest(this._myNodeService.sbUrl, contract.policy).pipe(take(1))
        )
      }
      contractNode.control.next(ContractRequestStatus.DONE)
    } catch (e) {
      if (e instanceof HttpErrorResponse && e.status == HttpStatusCode.Conflict) {
        contractNode.control.next(ContractRequestStatus.CONFLICT)
      } else {
        this._hasFailed = true
        contractNode.control.next(ContractRequestStatus.FAILED)
      }
      throw e
    }
  }

  get contracts() {
    return this._contracts.value
  }

  get contractsTree() {
    return this._contractsTree
  }

  get assigneeCandidates() {
    return this._assigneeCandidates.value
  }

  get assigneeCandidatesTree() {
    return this._assigneeCandidatesTree
  }

  get assigner() {
    return this._userService.user!
  }

  get newContracts() {
    return this._newContracts
  }

  get read() {
    return this._newContracts?.read ?? false
  }

  get write() {
    return this._newContracts?.write ?? false
  }

  get subscribe() {
    return this._newContracts?.subscribe ?? false
  }

  get accessType() {
    return this._newContracts?.accessType
  }

  get url() {
    return `/contract-creator/${this._myNodeService.node!.nodeId}`
  }

  get selectAssignees() {
    return this._newContracts?.accessType == ContractAccessType.GRANT
  }
  get selectTargets() {
    return this._newContracts?.accessType == undefined || this._newContracts.accessType == null
  }

  get hasFailed() {
    return this._hasFailed
  }

  set assignees(value: NodeWithOwner[]) {
    if (this._newContracts) {
      this._newContracts.assignees = value
    }
  }

  set read(value: boolean) {
    if (this._newContracts) {
      this._newContracts.read = value
    }
  }

  set write(value: boolean) {
    if (this._newContracts) {
      this._newContracts.write = value
    }
  }

  set subscribe(value: boolean) {
    if (this._newContracts) {
      this._newContracts.subscribe = value
    }
  }
}

function _generateContract(
  assignee: NodeWithOwner,
  assigner: User,
  target: ItemWithIdentity,
  read: boolean,
  write: boolean,
  subscribe: boolean,
  accessType: ContractAccessType
): Contract {
  return {
    policyId: uuidv4(),
    assignee,
    assigner,
    target,
    targetType: 'ITEM',
    status:
      accessType == ContractAccessType.GRANT ? ContractStatus.WAITING_ON_ASSIGNEE : ContractStatus.WAITING_ON_ASSIGNER,
    accessType,
    policy: {
      '@type': 'Policy',
      '@context': 'http://www.w3.org/ns/odrl.jsonld',
      permission: [
        ...(read
          ? [
              {
                action: 'read',
                target: target.oid,
                assignee: `urn:node:${assignee.nodeId}`,
                assigner: `urn:user:${assigner.uid}`,
              },
            ]
          : []),
        ...(write
          ? [
              {
                action: 'write',
                target: target.oid,
                assignee: `urn:node:${assignee.nodeId}`,
                assigner: `urn:user:${assigner.uid}`,
              },
            ]
          : []),
        ...(subscribe
          ? [
              {
                action: 'subscribe',
                target: target.oid,
                assignee: `urn:node:${assignee.nodeId}`,
                assigner: `urn:user:${assigner.uid}`,
              },
            ]
          : []),
      ],
    },
    created: new Date(),
    updated: new Date(),
  }
}

function _generateOrgTreeContractNode(org: Organisation, node: NodeWithOwner, contract: Contract): TreeNodeWithStatus {
  const status = new BehaviorSubject(ContractRequestStatus.READY)
  return {
    data: org,
    checked: false,
    expanded: true,
    children: [_generateNodeTreeContractNode(node, contract)],
    control: status,
    status$: status.asObservable(),
  }
}

function _generateNodeTreeContractNode(node: NodeWithOwner, contract: Contract): TreeNodeWithStatus {
  const status = new BehaviorSubject(ContractRequestStatus.READY)
  return {
    data: node,
    checked: false,
    expanded: true,
    children: [_generateContractTreeContractNode(contract)],
    control: status,
    status$: status.asObservable(),
  }
}

function _generateContractTreeContractNode(contract: Contract): TreeNodeWithStatus {
  const status = new BehaviorSubject(ContractRequestStatus.READY)
  return {
    data: contract,
    checked: false,
    expanded: true,
    control: status,
    status$: status.asObservable(),
  }
}

function _generateOrgTreeAssigneeCandidateNode(org: Organisation, node: NodeWithOwner, checked: boolean): TreeNode {
  return {
    data: org,
    children: [_generateNodeTreeAssigneeCandidateNode(node, checked)],
  }
}

function _generateNodeTreeAssigneeCandidateNode(node: NodeWithOwner, checked: boolean): TreeNode {
  return {
    data: node,
    checked,
  }
}
