/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { BaseService } from '../base'
import { ItemsApiService } from '@core/api/modules/items'
import { ItemWithIdentity } from '@core/models/item.model'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'
import { MyNodeService } from '../my-node/my-node.service'
import { UserService } from '../user/user.service'
import { OrganisationApiService } from '@core/api/modules/organisations'
import { ForeignOrganisation } from '@core/models/organisation.model'
import { CacheService } from '../cache/cache.service'

@Injectable({
  providedIn: 'root',
})
export class DiscoveryService extends BaseService {
  private _initialized = false

  constructor(
    private _userService: UserService,
    private _myNodeService: MyNodeService,
    private _itemsApi: ItemsApiService,
    private _orgApi: OrganisationApiService,
    private _cacheService: CacheService
  ) {
    super()
  }

  protected async reload() {
    await this.initApiWrapper(this._init())
  }

  async init(force: boolean = false) {
    if ((!this._initialized || force) && !this.initializing) {
      await this.initApiWrapper(this._init())
      this._initialized = true
    }
  }

  private async _init() {
    if (this._myNodeService.online) {
      await this._initPartnerOrganisations()
      await Promise.all([this._initPartnerItems(), this._initMyOrgItems()])
    }
  }

  get foreignItems() {
    return [...this._partnerItems.value, ...this._myOrgItems.value]
  }

  // ############################################################################
  // ######################## PARTNER ORGANISATIONS #############################

  private _partnerOrganisations = new BehaviorSubject<ForeignOrganisation[]>([])

  partnerOrganisations$ = this._partnerOrganisations.asObservable()

  private async _initPartnerOrganisations() {
    const organisations = await this._getPartnerOrganisations()
    this._cacheService.updateOrganisationsCache(organisations)
    this._partnerOrganisations.next(organisations)
  }

  private async _getPartnerOrganisations() {
    return firstValueFrom(this._orgApi.getPartnerOrganisations().pipe(take(1)))
  }

  get partnerOrganisations() {
    return this._partnerOrganisations.value
  }

  // ############################################################################
  // ########################### PARTNER ITEMS ##################################

  private _partnerItems = new BehaviorSubject<ItemWithIdentity[]>([])

  partnerItems$ = this._partnerItems.asObservable()

  private async _initPartnerItems() {
    const nodeId = this._myNodeService.node?.nodeId
    const cid = this._userService.user?.organisation?.cid
    if (nodeId && cid) {
      const items = await this._getPartnerItems(nodeId, cid)
      this._cacheService.updateItemsCache(items)
      const itemsWithIdentity = items.map((item) => {
        const partnerCid = item['SPADE:organisation']?.['@id'].replace('urn:organisation:', '')
        const partnerOrganisation = this.partnerOrganisations.find((org) => org.cid == partnerCid)
        return {
          ...item,
          organisation: partnerOrganisation,
        }
      })
      this._partnerItems.next(
        itemsWithIdentity.sort((a, b) => {
          return a.title.localeCompare(b.title, undefined, { numeric: true, sensitivity: 'base' })
        })
      )
    }
  }

  private async _getPartnerItems(nodeId: string, cid: string) {
    return firstValueFrom(this._itemsApi.getPartnerItems(this._myNodeService.sbUrl, nodeId, cid).pipe(take(1)))
  }

  get partnerItems() {
    return this._partnerItems.value
  }

  // ############################################################################
  // ############################ MY ORG ITEMS ##################################

  private _myOrgItems = new BehaviorSubject<ItemWithIdentity[]>([])

  myOrgItems$ = this._myOrgItems.asObservable()

  private async _initMyOrgItems() {
    const nodeId = this._myNodeService.node?.nodeId
    const organisation = this._userService.user?.organisation
    const cid = organisation?.cid
    if (nodeId && cid) {
      const items = await this._getMyOrgItems(nodeId, cid)
      this._cacheService.updateItemsCache(items)
      const itemsWithIdentity = items.map((item) => {
        return {
          ...item,
          organisation,
        }
      })
      this._myOrgItems.next(
        itemsWithIdentity.sort((a, b) => {
          return a.title.localeCompare(b.title, undefined, { numeric: true, sensitivity: 'base' })
        })
      )
    }
  }

  private async _getMyOrgItems(nodeId: string, cid: string) {
    return firstValueFrom(this._itemsApi.getMyOrgItems(this._myNodeService.sbUrl, nodeId, cid).pipe(take(1)))
  }

  get myOrgItems() {
    return this._myOrgItems.value
  }
}
