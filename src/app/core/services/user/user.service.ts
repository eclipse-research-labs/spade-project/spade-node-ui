/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { BaseService } from '../base'
import { BehaviorSubject, Subscription, firstValueFrom, map, take } from 'rxjs'
import { User } from '@core/models/user.model'
import { UsersApiService } from '@core/api/modules/users'
import { OrganisationApiService } from '@core/api/modules/organisations'
import { KeycloakEventType, KeycloakService } from 'keycloak-angular'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { UserRole } from '@core/enums/user.enum'
import { CacheService } from '../cache/cache.service'

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class UserService extends BaseService {
  private _user = new BehaviorSubject<User | null>(null)
  private _refreshTokenSubscription?: Subscription

  user$ = this._user.asObservable()
  checkRoles$ = this.user$.pipe(
    untilDestroyed(this),
    map((element) => this._checkRolesClosure())
  )

  constructor(
    private _userApi: UsersApiService,
    private _keycloakService: KeycloakService,
    private _cacheService: CacheService
  ) {
    super()
  }

  public async loadUser() {
    const user = await this.initApiWrapper(this._getCurrentUser())
    this._cacheService.updateUsersCache([user])
    if (user.organisation) {
      this._cacheService.updateOrganisationsCache([user.organisation])
    }
    this._user.next(user)
  }

  updateUser(user: User) {
    this._user.next(user)
  }

  listenForTokenExpiration() {
    if (!this._refreshTokenSubscription) {
      this._refreshTokenSubscription = this._keycloakService.keycloakEvents$
        .pipe(untilDestroyed(this))
        .subscribe((event) => {
          if (event.type == KeycloakEventType.OnTokenExpired) {
            this._keycloakService.updateToken().then((success) => {
              if (!success) {
                this._keycloakService.login()
              }
            })
          }
        })
    }
  }

  async logout(redirectUri?: string) {
    if (this.isLoggedIn) {
      await this.loadApiWrapper(this._logout(this.appUrl + (redirectUri ?? '')))
      this._user.next(null)
    }
  }

  async login(redirectUri?: string) {
    if (this.keycloakInitialized && !this._keycloakService.isLoggedIn()) {
      await this._keycloakService.login({
        redirectUri: this.appUrl + (redirectUri ?? ''),
      })
      this.listenForTokenExpiration()
    }
  }

  private async _getCurrentUser(): Promise<User> {
    return await firstValueFrom(this._userApi.getCurrentUser().pipe(take(1)))
  }

  private async _logout(redirectUri?: string) {
    await this._keycloakService.logout(redirectUri)
  }

  private _checkRolesClosure() {
    const userRoles = this.user?.roles ?? []
    return function checkRoles(roles: UserRole[]): boolean {
      return userRoles.some((element) => roles.includes(element))
    }
  }

  get user() {
    return this._user.value
  }

  get keycloakInitialized() {
    const kc = this._keycloakService.getKeycloakInstance()
    return kc !== undefined && kc !== null
  }

  get isLoggedIn() {
    return this._keycloakService.getKeycloakInstance() && this._keycloakService.isLoggedIn()
  }
}
