/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserService } from './user.service';

let service: UserService;

beforeEach(() => {
  TestBed.configureTestingModule({ providers: [UserService], imports: [HttpClientTestingModule] });
});

it('should create', () => {
  service = TestBed.inject(UserService);
  expect(service).toBeTruthy();
});
