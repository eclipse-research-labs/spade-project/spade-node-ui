/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { MessageService } from 'primeng/api'

@Injectable({
  providedIn: 'root',
})
export class SnackBarService {
  constructor(private _messageService: MessageService) {}
  showError(message: string) {
    this._messageService.add({ key: 'toast', severity: 'error', summary: 'Error', detail: message })
  }

  showSuccess(message: string) {
    this._messageService.add({ key: 'toast', severity: 'success', summary: 'Success', detail: message })
  }

  showInfo(message: string) {
    this._messageService.add({ key: 'toast', severity: 'info', summary: 'Info', detail: message })
  }

  showWarning(message: string) {
    this._messageService.add({ key: 'toast', severity: 'warn', summary: 'Warning', detail: message })
  }
}
