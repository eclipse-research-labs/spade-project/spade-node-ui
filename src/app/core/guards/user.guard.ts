/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot } from '@angular/router'
import { User } from '@core/models/user.model'
import { inject } from '@angular/core'
import { MasterService } from '@core/services/master/master.service'
import { NotificationService } from '@core/services/notification/notification.service'
import { UserService } from '@core/services/user/user.service'
import { NodesService } from '@core/services/nodes/nodes.service'
import { MarketplaceService } from '@core/services/marketplace/marketplace.service'

const _homeUrl = '/'
const _onboardingUrl = _homeUrl + 'onboarding'
const _createJoinOrgUrl = _onboardingUrl + '/organisation'

class UserGuard {
  private _router!: Router
  private _userService!: UserService
  private _marketplaceService!: MarketplaceService
  private _nodesService!: NodesService
  private _masterService!: MasterService
  private _notificationService!: NotificationService

  private _injectServices() {
    this._router = inject(Router)
    this._userService = inject(UserService)
    this._marketplaceService = inject(MarketplaceService)
    this._nodesService = inject(NodesService)
    this._masterService = inject(MasterService)
    this._notificationService = inject(NotificationService)
  }

  watch: CanActivateFn = async (route, state) => {
    this._injectServices()
    try {
      await this._authenticate(state)
      await this._loadUser(state)
      const user = this._userService.user
      if (!user!.organisation) {
        // Allow the navigation when route is one of onboarding create org routes (user is in process of createing org)
        if (state.url === _createJoinOrgUrl) {
          return this._checkRoles(route, user!)
        }
        // Navigate to onboarding create/join org otherwise
        this._router.navigateByUrl(_createJoinOrgUrl)
        return false
      }
      await this._loadNodes(state)
      this._startNotificationService(state)
      this._listenForTokenExpiration(state)
      return true
    } catch (e) {
      console.error(e)
      return false
    } finally {
      this._stopLoading()
    }
  }

  private async _authenticate(state: RouterStateSnapshot) {
    if (!this._userService.isLoggedIn) {
      this._startLoading()
      try {
        await this._userService.login(state.url)
        if (!this._userService.isLoggedIn) {
          throw Error('User not logged in')
        }
      } catch (e) {
        this._router.navigateByUrl(_homeUrl)
        throw e
      }
    }
  }

  private async _loadUser(state: RouterStateSnapshot) {
    if (!this._userService.user) {
      this._startLoading()
      try {
        await this._userService.loadUser()
        if (!this._userService.user) {
          throw Error('User is ' + this._userService.user)
        }
      } catch (e) {
        await this._userService.logout()
        throw e
      }
    }
  }

  private async _loadNodes(state: RouterStateSnapshot) {
    try {
      if (!this._nodesService.nodes) {
        this._startLoading()
        await this._nodesService.init()
      }
    } catch (e) {
      await this._userService.logout()
      throw e
    }
  }

  private _checkRoles(route: ActivatedRouteSnapshot, user: User): boolean {
    const requiredRoles = route.data.roles
    // When no roles are required to access this route allow to navigate
    if (!Array.isArray(requiredRoles) || requiredRoles.length === 0) {
      return true
    }
    // When roles are required check if user has the permission
    if (!requiredRoles.every((role) => user!.roles.includes(role))) {
      this._router.navigateByUrl(_homeUrl)
      return false
    }
    return true
  }

  private _startNotificationService(state: RouterStateSnapshot) {
    if (!this._notificationService.online) {
      this._notificationService.initConnection()
    }
  }

  private _listenForTokenExpiration(state: RouterStateSnapshot) {
    this._userService.listenForTokenExpiration()
  }

  private _startLoading() {
    if (!this._masterService.loading) {
      this._masterService.updateLoading(true)
    }
  }

  private _stopLoading() {
    if (this._masterService.loading) {
      this._masterService.updateLoading(false)
    }
  }
}

/**
 * Guard ensures that the user is authenticated, has an organisation, and the notification service is up and running.
 */
export const userGuard = new UserGuard()
