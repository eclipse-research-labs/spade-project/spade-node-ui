/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { inject } from '@angular/core'
import { CanActivateFn, Router } from '@angular/router'
import { ContractCreatorService } from '@core/services/contract-creator/contract-creator.service'
import { MyNodeService } from '@core/services/my-node/my-node.service'

class ContractCreatorGuard {
  private _router!: Router
  private _myNodeService!: MyNodeService
  private _contractCreatorService!: ContractCreatorService

  private _injectServices() {
    this._router = inject(Router)
    this._myNodeService = inject(MyNodeService)
    this._contractCreatorService = inject(ContractCreatorService)
  }

  watch: CanActivateFn = async (route, state) => {
    this._injectServices()
    if (!this._contractCreatorService.newContracts) {
      this._router.navigateByUrl(`/my-node/online/${this._myNodeService.node?.nodeId}/items`)
      return false
    }
    return true
  }
}

export const contractCreatorGuard = new ContractCreatorGuard()
