/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { inject } from '@angular/core'
import { CanActivateFn, Router } from '@angular/router'
import { MasterService } from '@core/services/master/master.service'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { NodesService } from '@core/services/nodes/nodes.service'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'

const _homeUrl = '/'
const _nodesUrl = _homeUrl + 'nodes'

class OnlineGuard {
  private _router!: Router
  private _nodesService!: NodesService
  private _myNodeService!: MyNodeService
  private _masterService!: MasterService
  private _snackBarService!: SnackBarService

  private _injectServices() {
    this._router = inject(Router)
    this._nodesService = inject(NodesService)
    this._myNodeService = inject(MyNodeService)
    this._masterService = inject(MasterService)
    this._snackBarService = inject(SnackBarService)
  }

  watch: CanActivateFn = async (route, state) => {
    const nodeId = route.params['nodeId'] as string | undefined | null

    if (!nodeId || nodeId.length == 0) {
      this._router.navigateByUrl(_nodesUrl)
      return false
    }

    this._injectServices()

    this._myNodeService.setOnline(true)

    const node = this._myNodeService.node
    const broker = this._myNodeService.broker
    if (!node || !broker || node.nodeId != nodeId || node.nodeId != broker.nodeId) {
      try {
        await this._initNode(nodeId)
        return true
      } catch (e) {
        console.error(e)
        this._router.navigateByUrl(_nodesUrl)
        return false
      } finally {
        this._stopLoading()
      }
    }

    return true
  }

  private async _initNode(nodeId: string) {
    const candidate = this._findCandidate(nodeId)

    if (!candidate) {
      throw Error(`Could not find a Node with id: ${nodeId}`)
    }

    this._startLoading()

    try {
      const host = candidate.host
      await this._myNodeService.healthcheck(host)
      const broker = this._myNodeService.broker!

      if (!broker.online) {
        throw Error(`Broker with host: ${host} is offline`)
      }
      if (candidate.nodeId != broker.nodeId) {
        throw Error(`Broker with id: ${broker.nodeId} does not match id: ${candidate.id}`)
      }
    } catch (e) {
      this._snackBarService.showWarning('Unable to establish communication with the Data Broker')
      throw e
    }

    this._myNodeService.selectNode(candidate)
    try {
      await this._myNodeService.updateNodeBuildInfo()
    } catch (e) {
      console.warn('Unable to update node build info', e)
    }
  }

  private _findCandidate(nodeId: string) {
    return this._nodesService.nodes?.find((element) => element.nodeId == nodeId)
  }

  private _startLoading() {
    if (!this._masterService.loading) {
      this._masterService.updateLoading(true)
    }
  }

  private _stopLoading() {
    if (this._masterService.loading) {
      this._masterService.updateLoading(false)
    }
  }
}

export const onlineGuard = new OnlineGuard()
