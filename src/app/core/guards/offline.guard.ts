/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { inject } from '@angular/core'
import { CanActivateFn, Router } from '@angular/router'
import { MasterService } from '@core/services/master/master.service'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { decodeSBUrl, encodeSBUrl } from 'src/app/utils'

const _homeUrl = '/'
const _authUrl = _homeUrl + 'auth/'
const _basicAuthUrl = _authUrl + 'basic'
const _onboardingUrl = _homeUrl + 'onboarding/'
const _connectToNodeUrl = _onboardingUrl + 'connect-to-node'

class OfflineGuard {
  private _router!: Router
  private _myNodeService!: MyNodeService
  private _masterService!: MasterService

  private _injectServices() {
    this._router = inject(Router)
    this._myNodeService = inject(MyNodeService)
    this._masterService = inject(MasterService)
  }

  watch: CanActivateFn = async (route, state) => {
    const address = route.params['nodeAddress'] as string | undefined | null

    if (address == undefined || address == null || address.length == 0 || address == 'undefined' || address == 'null') {
      this._router.navigateByUrl(_connectToNodeUrl)
      return false
    }

    const sbUrl = decodeSBUrl(address)
    const basicAuthUrl = `${_basicAuthUrl}/${address}`

    this._injectServices()

    this._myNodeService.setOnline(false)

    const broker = this._myNodeService.broker

    if (!broker || broker.address != sbUrl) {
      try {
        await this._initNode(sbUrl)
        this._router.navigateByUrl(basicAuthUrl)
        return false
      } catch (e) {
        console.error(e)
        this._router.navigateByUrl(_connectToNodeUrl)
        return false
      } finally {
        this._stopLoading()
      }
    }
    if (!this._myNodeService.basicAuth) {
      if (state.url === basicAuthUrl) {
        return true
      } else {
        this._router.navigateByUrl(basicAuthUrl)
        return false
      }
    }
    return true
  }

  private async _initNode(sbUrl: string) {
    this._startLoading()

    await this._myNodeService.healthcheck(sbUrl)
    const broker = this._myNodeService.broker!

    if (broker.online) {
      throw Error(`Broker with host: ${sbUrl} is online`)
    }
    if (sbUrl != broker.address) {
      throw Error(`Broker with address: ${broker.address} does not match address: ${sbUrl}`)
    }
  }

  private _startLoading() {
    if (!this._masterService.loading) {
      this._masterService.updateLoading(true)
    }
  }

  private _stopLoading() {
    if (this._masterService.loading) {
      this._masterService.updateLoading(false)
    }
  }
}

export const offlineGuard = new OfflineGuard()
