/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { HttpErrorResponse, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http'

import { Injectable } from '@angular/core'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { Observable, throwError } from 'rxjs'
import { catchError, finalize, map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root',
})
export class HTTPReqResInterceptor implements HttpInterceptor {
  constructor(private _myNodeService: MyNodeService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const newReq = req.clone({
      url: req.url,
      setHeaders: this._resolveAuthorization(req.headers),
    })
    return next.handle(newReq).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          event = event.clone({ body: event.body?.message })
        }
        return event
      }),
      catchError((err) => this._error(err)),
      finalize(() => {})
    )
  }

  private _resolveAuthorization(headers: HttpHeaders) {
    if (headers.has('Authorization')) {
      return undefined
    }
    const basicAuth = this._myNodeService.basicAuth
    if (basicAuth) {
      return {
        Authorization: `BASIC ${btoa(`${basicAuth.username}:${basicAuth.password}`)}`,
      }
    }
    return undefined
  }

  private _error(err: any) {
    if (err instanceof HttpErrorResponse) {
      const error = err.error
      console.error(`Backend returned code ${err.status}`)
      console.error(error)
    } else {
      console.error(err.toString())
    }
    return throwError(() => err)
  }
}
