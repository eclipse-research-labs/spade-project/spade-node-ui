/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { NgModule, Optional, SkipSelf } from '@angular/core'
import { ToastComponent } from './components/toast/toast.component'
import { ToastModule } from 'primeng/toast'

import { SplashComponent } from './components/splash/splash.component'
import { ErrorComponent } from './components/error/error.component'
import { NotificationComponent } from '@shared/components/smart/notifications/notification/notification.component'
import { AcceptSslComponent } from '@shared/components/smart/accept-ssl/accept-ssl.component'
import { DividerComponent } from '@shared/components/presentation/reusable/divider/divider.component'
import { NgLetDirective } from 'ng-let';

@NgModule({
  declarations: [ToastComponent, SplashComponent, ErrorComponent],
  imports: [CommonModule, ToastModule, NotificationComponent, AcceptSslComponent, DividerComponent, NgLetDirective],
  exports: [ToastComponent, SplashComponent, ErrorComponent],
  providers: [],
})
export class CoreModule {
  /* make sure CoreModule is imported only by the AppModule and noone else */
  constructor(@Optional() @SkipSelf() presentInParent: CoreModule) {
    if (presentInParent) {
      throw new Error('CoreModule is already loaded. Import only in AppModule')
    }
  }
}
