/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

export enum ContractRequestStatus {
  READY = 'READY',
  SENDING = 'SENDING',
  DONE = 'DONE',
  FAILED = 'FAILED',
  CONFLICT = 'CONFLICT',
}

export enum ContractIdentityStatus {
  READY = 'READY',
  DOWNLOADING = 'DOWNLOADING',
  DONE = 'DONE',
  FAILED = 'FAILED',
}

export enum ContractStatus {
  ACTIVE = 'ACTIVE',
  WAITING_ON_ASSIGNEE = 'WAITING_ON_ASSIGNEE',
  WAITING_ON_ASSIGNER = 'WAITING_ON_ASSIGNER',
  REVOKED = 'REVOKED',
}

export enum ContractAccessType {
  REQUEST = 'REQUEST',
  GRANT = 'GRANT',
}
