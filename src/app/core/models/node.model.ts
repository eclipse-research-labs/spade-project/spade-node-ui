/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Organisation } from './organisation.model'

export interface Node {
  id: number
  nodeId: string
  name: string
  host: string
  status: string
  publicKey?: string
  version?: string
  buildTime?: string
  isOutdated?: boolean
  isOutdatedMessage?: string
  lastUpdated: string
  created: string
}

export interface NodeWithOwner extends Node {
  owner: Organisation
}

export interface Healthcheck {
  DB: DB
  POSTGRES: Postgres
  REDIS: Postgres
}

export interface DB {
  nodeId: string
  status: string
  security: boolean
  online: boolean
  address: string
  version?: string
  buildTime?: string
}

export interface Postgres {
  status: string
  security: boolean
}

export interface DNSCheck {
  active: boolean
}

export interface CerthPayload {
  cid: string
  nodeId: string
  host: string
  country: string
}

export interface CSR {
  csr: string
}

export interface Certh {
  certificate: string
  serial_number: string
  response_format: string
  certificate_chain: string[]
  certificate_pem: string
}
