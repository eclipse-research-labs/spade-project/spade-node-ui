/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { UserRole } from '@core/enums/user.enum'
import { Node } from './node.model'
import { Organisation } from './organisation.model'

export interface User {
  id: number
  uid: string
  email: string
  givenName: string
  familyName: string
  occupation?: string
  organisation?: Organisation
  roles: UserRole[]
  nodes: Node[]
  lastUpdated: string
  created: string
}

export interface BasicAuth {
  username: string
  password: string
}
