/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

export interface Adapter {
  adid: string
  name: string
  type: string
  host: string
  port: string
  path: string
  color: string
  icon: string
  protocol: string
  security: string
  securitySchema: SecuritySchema
  credentials: Credentials
  adapterConnections: string[]
  description?: string
}

export interface Credentials {
  [key: string]: string
}

export interface SecuritySchema {
  name: string
  in: string
}

export interface AdapterConnection {
  adidconn: string
  oid: string
  iid: string
  interaction: string
  op: string
  path: string
  adid: string
  params: string[]
  optionalParams: string[]
  method: string
  incomingContentType?: string
  outgoingContentType?: string
}

export interface AdapterConnectionCreate {
  oid: string
  iid: string
  interaction: string
  op: string
  adid: string
  optionalParams: string[]
  params: string[]
  method: string
  path: string
  incomingContentType?: string
  outgoingContentType?: string
}
