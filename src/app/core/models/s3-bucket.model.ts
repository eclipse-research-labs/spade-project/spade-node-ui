/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

export interface S3Bucket {
  ListBucketResult: ListBucketResult
}

export interface ListBucketResult {
  $: Empty
  Name: string[]
  Prefix: string[]
  Marker: string[]
  MaxKeys: string[]
  IsTruncated: string[]
  Contents: Content[]
}

export interface Empty {
  xmlns: string
}

export interface Content {
  Key: string[]
  LastModified: Date[]
  ETag: string[]
  Size: string[]
  Owner: Owner[]
  StorageClass: string[]
}

export interface Owner {
  ID: string[]
  DisplayName: string[]
}
