/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Organisation } from './organisation.model'

export interface Item {
  id: string
  oid: string
  base: string
  title: string
  '@context': Array<ContextClass | string>
  security: string[]
  securityDefinitions: SecurityDefinitions
  '@type'?: string | string[]
  adapterId?: string
  'SPADE:node'?: SPADEIdentity
  properties?: Properties
  events?: Events
  description?: string
  'geo:location'?: GeoLocation
  registration?: Registration
  'SPADE:Privacy'?: SPADEPrivacy
  'SPADE:organisation'?: SPADEIdentity
  'dct:license'?: string
  'dct:licenseHolder'?: string
  'dct:creator'?: string
  'dct:hasVersion'?: string
  links?: ItemLink[]
}

export interface ItemWithIdentity extends Item {
  organisation?: Organisation
}

export interface ItemWithPropertyWithKey extends Item {
  prop?: PropertyWithKey
}

export interface ItemWithEventWithKey extends Item {
  event?: EventWithKey
}

export interface SPADEIdentity {
  '@id': string
}

export interface SPADEPrivacy {
  Level: number
  Caption: string
}

export interface ContextClass {
  [key: string]: string
}

export interface GeoLocation {
  'geo:lat': string
  'geo:long': string
}

export interface Properties {
  [key: string]: Property
}

export interface Events {
  [key: string]: Event
}

export interface Property {
  title: string
  description?: string
  '@type'?: string
  unit?: string
  'om:hasUnit'?: string
  readOnly?: boolean
  type: string
  forms?: Form[]
  uriVariables?: UriVariable
}

export interface Event {
  title: string
  forms?: Form[]
}

export interface PropertyResponse {
  contentType?: string
}

export interface UriVariable {
  [key: string]: string
}

export interface PropertyWithKey extends Property {
  key: string
}

export interface EventWithKey extends Event {
  key: string
}

export interface Form {
  op: string
  href: string
  'htv:methodName'?: string
  contentType?: string
  response?: PropertyResponse
}

export interface Registration {
  created: string
  modified: string
}

export interface SecurityDefinitions {
  nosec_sc: NosecSc
}

export interface NosecSc {
  scheme: string
}

export interface ItemLink {
  rel: string
  href: string
}

// Converts JSON strings to/from your types
export class Convert {
  public static toItems(json: string): Item[] {
    return JSON.parse(json)
  }

  public static itemsToJson(value: Item[]): string {
    return JSON.stringify(value)
  }
}

export interface ItemType {
  id: string
  title: string
  icon: string
  color: string
}

export interface PropUnitDataType {
  name?: string
  symbol?: string
}

export interface MIMEType {
  icon: string
  values: string[]
}

export interface SPADEPrivacyWithMetadata extends SPADEPrivacy {
  description: string
  icon: string
}

export interface MarketplaceItems {
  lastUpdated: Date
  services: ItemWithIdentity[]
  datasets: ItemWithIdentity[]
}
