/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

export interface Openroute {
  features: Feature[]
  bbox: number[]
}

export interface Feature {
  type: string
  geometry: Geometry
  properties: Properties
  bbox?: number[]
}

export interface Geometry {
  type: string
  coordinates: number[]
}

export interface Properties {
  id: string
  layer?: string
  name?: string
  accuracy?: string
  country?: string
  country_a?: string
  region?: string
  region_a?: string
  county?: string
  locality?: string
  continent?: string
  label?: string
  gid?: string
  county_a?: string
  borough?: string
  borough_a?: string
  housenumber?: string
  street?: string
  postalcode?: string
  macroregion?: string
  macroregion_a?: string
  localadmin?: string
  neighbourhood?: string
}
