/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ContractAccessType, ContractIdentityStatus, ContractStatus } from '@core/enums/contract.enum'
import { Item, ItemWithIdentity } from './item.model'
import { NodeWithOwner, Node } from './node.model'
import { User } from './user.model'
import { BehaviorSubject } from 'rxjs'

export interface NewContract {
  targets: ItemWithIdentity[]
  assignees: NodeWithOwner[]
  read: boolean
  write: boolean
  subscribe: boolean
  accessType?: ContractAccessType
}

export interface Contract {
  policyId: string
  assignee: string | NodeWithOwner | Node | null
  assigner: string | User | null
  target: string | ItemWithIdentity | Item | null
  targetType: string
  status: ContractStatus
  accessType: ContractAccessType
  policy: Policy
  created: Date
  updated: Date
  originalContract?: Contract
}

export interface Policy {
  uid?: string
  '@type': string
  '@context': string
  permission: Permission[]
}

export interface Permission {
  action: string
  target: string
  assignee: string
  assigner: string
}

export interface ContractIdentity {
  contract: Contract
  targetStatus: BehaviorSubject<ContractIdentityStatus>
  assigneeStatus: BehaviorSubject<ContractIdentityStatus>
  assignerStatus: BehaviorSubject<ContractIdentityStatus>
}