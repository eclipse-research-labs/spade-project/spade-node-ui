/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

export interface AuditTrail {
  id: string
  ts?: string
  requester?: string
  origin?: string
  target?: string
  purpose?: Purpose
  method?: Method
  path?: string
  info?: string
  status?: Status
  statusreason?: string
}

export enum Purpose {
  REGISTRY = 'REGISTRY',
  DISCOVERY = 'DISCOVERY',
  CONSUMPTION = 'CONSUMPTION',
  ADMIN = 'ADMIN',
  MANAGEMENT = 'MANAGEMENT',
  CONTRACT = 'CONTRACT',
  UNDEFINED = 'UNDEFINED',
  ORGANISATION = 'ORGANISATION',
  INVITATION = 'INVITATION',
  USER = 'USER',
  NODE = 'NODE',
  PARTNERSHIP = 'PARTNERSHIP',
}

export enum Method {
  GET = 'GET',
  PUT = 'PUT',
  POST = 'POST',
  DELETE = 'DELETE',
}

export enum Status {
  ALL = 'ALL',
  NOT_SET = 'NOT_SET',
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR',
  REJECTED = 'REJECTED',
}
