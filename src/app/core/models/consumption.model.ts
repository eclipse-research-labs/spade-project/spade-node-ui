/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { BehaviorSubject, Observable } from 'rxjs'
import { Adapter } from './adapter.model'
import { ItemWithIdentity, PropertyWithKey } from './item.model'
import { HttpResponse } from '@angular/common/http'

export interface Consumption {
  consumptionUrl: string
  item: ItemWithIdentity
  property: PropertyWithKey
  op: string
  adapter?: Adapter
  params?: Map<string, string>
  payload?: Payload
  contentType?: string
}

export interface Payload {
  type: 'BODY' | 'FILE' | 'TEXT'
  data: string | File
  contentType?: string
}

export interface ConsumptionWithStatusAndResult extends Consumption {
  status: BehaviorSubject<number>
  status$: Observable<number>
  result: BehaviorSubject<HttpResponse<any> | undefined>
  result$: Observable<HttpResponse<any> | undefined>
  error: BehaviorSubject<any | undefined>
  error$: Observable<any | undefined>
}
