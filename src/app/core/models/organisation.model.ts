/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Partnership } from './partnership.model'

export interface Organisation {
  id: number
  cid: string
  name: string
  location: string
  countryCode: string
  businessId?: string
  avatar?: string
  description?: string
  lastUpdated: string
  created: string
}

export interface OrganisationCreate {
  name: string
  location: string
  countryCode: string
  businessId?: string
  avatar?: string
  description?: string
}

export class ForeignOrganisation implements Organisation {
  id!: number
  cid!: string
  name!: string
  location!: string
  countryCode!: string
  businessId?: string | undefined
  avatar?: string | undefined
  description?: string | undefined
  lastUpdated!: string
  created!: string
  ingressPartnerships?: Partnership[]
  egressPartnerships?: Partnership[]

  get isPartner() {
    return (
      (this.egressPartnership != undefined && this.egressPartnership.status == 'approved') || (this.ingressPartnership != undefined && this.ingressPartnership.status == 'approved')
    )
  }

  get isPendingPartner() {
    return (
      (this.egressPartnership != undefined && this.egressPartnership.status == 'pending') || (this.ingressPartnership != undefined && this.ingressPartnership.status == 'pending')
    )
  }

  get isIngressPendingPartner() {
    return this.egressPartnership != undefined && this.egressPartnership.status == 'pending'
  }

  get egressPartnership() {
    return this.egressPartnerships?.at(0)
  }

  get ingressPartnership() {
    return this.ingressPartnerships?.at(0)
  }
}
