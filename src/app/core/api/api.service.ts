/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { HttpClient, HttpErrorResponse, HttpStatusCode } from '@angular/common/http'
import { inject, Injectable } from '@angular/core'
import { Observable, delay, catchError, throwError, of, retry } from 'rxjs'

export interface RetryOprions {
    when?: HttpStatusCode
}

@Injectable({ providedIn: 'root' })
export class ApiService {
  private readonly _maxRetries = 3

  protected _http!: HttpClient

  constructor() {
    this._http = inject(HttpClient)
  }

  protected retryRequest<T>(observable: Observable<T>, options?: RetryOprions): Observable<T> {
    return observable.pipe(
      retry({
        count: this._maxRetries,
        resetOnSuccess: true,
        delay: (error, retryCount) => {
          if (options?.when) {
            if (error instanceof HttpErrorResponse && error.status === 425) {
              return of(error).pipe(delay(retryCount * 1000))
            }
            return throwError(() => error)
          }
          return of(error).pipe(delay(retryCount * 1000))
        },
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(() => error)
      })
    )
  }
}
