/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { HttpStatusCode } from '@angular/common/http'

import { Injectable } from '@angular/core'
import { Contract, Policy } from '@core/models/contract.model'
import { Observable } from 'rxjs'
import { prependHttps } from 'src/app/utils'
import { ApiService } from '../api.service'

const _contractUrl = '/api/sb/contract'

@Injectable({ providedIn: 'root' })
export class ContractsApiService extends ApiService {
  constructor() {
    super()
  }

  getContracts(sbUrl: string): Observable<Contract[]> {
    return this.retryRequest(
      this._http.get<Contract[]>(`${prependHttps(sbUrl) + _contractUrl}`, {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }

  sendContractRequest(sbUrl: string, policy: Policy): Observable<void> {
    return this.retryRequest(
      this._http.put<void>(`${prependHttps(sbUrl) + _contractUrl}`, policy, {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }

  acceptContract(sbUrl: string, policyId: string): Observable<void> {
    return this.retryRequest(
      this._http.post<void>(
        `${prependHttps(sbUrl) + _contractUrl}` + `/${policyId}` + '/approve',
        {},
        {
          headers: {
            accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  rejectContract(sbUrl: string, policyId: string): Observable<void> {
    return this.retryRequest(
      this._http.delete<void>(`${prependHttps(sbUrl) + _contractUrl}` + `/${policyId}` + '/reject', {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }

  deleteContract(sbUrl: string, policyId: string): Observable<void> {
    return this.retryRequest(
      this._http.delete<void>(`${prependHttps(sbUrl) + _contractUrl}` + `/${policyId}`, {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }
}
