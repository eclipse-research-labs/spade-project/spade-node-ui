/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Inject, Injectable } from '@angular/core'
import { User } from '@core/models/user.model'
import { map, Observable } from 'rxjs'
import { ApiService } from '../api.service'
import { Status, AuditTrail } from '@core/models/audit-trail.model'

const _userUrl = '/api/ui/v1/users'

@Injectable({ providedIn: 'root' })
export class UsersApiService extends ApiService {
  constructor(@Inject('CC_URL') private _ccUrl: string) {
    super()
  }

  getUser(uid: string): Observable<User> {
    return this._http.get<User>(this._ccUrl + _userUrl + '/' + uid, {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }

  getCurrentUser(): Observable<User> {
    return this._http.get<User>(this._ccUrl + _userUrl + '/my', {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }

  getCurrentUserAuditTrails(page?: number, status?: Status): Observable<AuditTrail[]> {
    return this._http
      .get<AuditTrail[]>(
        this._ccUrl + _userUrl + '/my/audit-trails' + `?page=${page ?? 0}${status ? '&status=' + status : ''}`,
        {
          headers: {
            accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }
      )
      .pipe(
        map((values) =>
          values.map((value) => {
            if (typeof value.info === 'string') {
              value.info = JSON.parse(value.info)
            }
            return value
          })
        )
      )
  }

  updateUserRoles(uid: string, roles: string[]): Observable<User> {
    return this._http.put<User>(
      this._ccUrl + _userUrl + '/' + uid,
      { roles, organisation: null },
      {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }
    )
  }
}
