/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Inject, Injectable } from '@angular/core'
import { map, Observable, switchMap } from 'rxjs'
import { ApiService } from '../api.service'
import { parseXml } from 'src/app/utils'
import { S3Bucket } from '@core/models/s3-bucket.model'

const _spadetdsUrl = '/spadetds/'

@Injectable({ providedIn: 'root' })
export class TdCatalogApiService extends ApiService {
  constructor(@Inject('S3_URL') private _s3url: string) {
    super()
  }

  listObjects(): Observable<S3Bucket | unknown> {
    return this._http
      .get(this._s3url + _spadetdsUrl, {
        headers: {
          accept: 'text/xml',
          'Content-Type': 'text/xml',
        },
        responseType: 'text',
      })
      .pipe(switchMap(async (xml) => (await parseXml(xml)) as S3Bucket))
  }

  getObject(name: string): Observable<any> {
    return this._http.get<any>(this._s3url + _spadetdsUrl + '/' + name, {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }
}
