/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Inject, Injectable } from '@angular/core'
import { Openroute } from '@core/models/geolocation.model'
import { Observable } from 'rxjs'
import { ApiService } from '../api.service'

const _geolocationUrl = '/api/ui/v1/geolocation/'

@Injectable({ providedIn: 'root' })
export class GeolocationApiService extends ApiService {
  constructor(@Inject('CC_URL') private _ccUrl: string) {
    super()
  }

  autocomplete(text: string): Observable<Openroute> {
    return this._http.get<Openroute>(this._ccUrl + _geolocationUrl + 'autocomplete' + '?text=' + text, {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }
}
