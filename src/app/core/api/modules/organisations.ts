/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Inject, Injectable } from '@angular/core'
import { Invitation } from '@core/models/invitation.model'
import { ForeignOrganisation, Organisation, OrganisationCreate } from '@core/models/organisation.model'
import { User } from '@core/models/user.model'
import { Observable, map } from 'rxjs'
import { ApiService } from '../api.service'
import { AuditTrail, Status } from '@core/models/audit-trail.model'

const _organisationUrl = '/api/ui/v1/organisations'

@Injectable({ providedIn: 'root' })
export class OrganisationApiService extends ApiService {
  constructor(@Inject('CC_URL') private _ccUrl: string) {
    super()
  }

  createOrganisation(data: OrganisationCreate): Observable<Organisation> {
    return this._http.post<Organisation>(this._ccUrl + _organisationUrl + '/my', data, {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }

  getOrganisationFromInvitation(invitation: string): Observable<Organisation> {
    return this._http.get<Organisation>(this._ccUrl + _organisationUrl + '/invitations/' + invitation, {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }

  joinOrganisation(invitation: string): Observable<Organisation> {
    return this._http.post<Organisation>(
      this._ccUrl + _organisationUrl + `/invitations/` + invitation + '/join',
      undefined,
      {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }
    )
  }

  getCurrentOrganisationUsers(): Observable<User[]> {
    return this._http.get<User[]>(this._ccUrl + _organisationUrl + '/my/users', {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }

  getCurrentOrganisationAuditTrails(page?: number, status?: Status): Observable<AuditTrail[]> {
    return this._http
      .get<AuditTrail[]>(
        this._ccUrl + _organisationUrl + '/my/audit-trails' + `?page=${page ?? 0}${status ? '&status=' + status : ''}`,
        {
          headers: {
            accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }
      )
      .pipe(
        map((values) =>
          values.map((value) => {
            if (typeof value.info === 'string') {
              value.info = JSON.parse(value.info)
            }
            return value
          })
        )
      )
  }

  getCurrentOrganisationInvitations(): Observable<Invitation[]> {
    return this._http.get<Invitation[]>(this._ccUrl + _organisationUrl + '/my/invitations', {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }

  sendCurrentOrganisationInvitations(emails: string[]): Observable<Invitation[]> {
    return this._http.post<Invitation[]>(
      this._ccUrl + _organisationUrl + '/my/invitations',
      { emails },
      {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }
    )
  }

  renewCurrentOrganisationInvitation(code: string): Observable<Invitation> {
    return this._http.put<Invitation>(
      this._ccUrl + _organisationUrl + '/my/invitations/' + code,
      {},
      {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }
    )
  }

  deleteCurrentOrganisationInvitation(code: string): Observable<void> {
    return this._http.delete<void>(this._ccUrl + _organisationUrl + '/my/invitations/' + code, {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }

  removeCurrentOrganisationUser(uid: string): Observable<void> {
    return this._http.delete<void>(this._ccUrl + _organisationUrl + '/my/users/' + uid, {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }

  getForeignOrganisations(): Observable<ForeignOrganisation[]> {
    return this._http
      .get<ForeignOrganisation[]>(this._ccUrl + _organisationUrl + '/foreign', {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
      .pipe(map((values) => values.map((value) => Object.assign(new ForeignOrganisation(), value))))
  }

  getForeignOrganisation(cid: string): Observable<ForeignOrganisation> {
    return this._http
      .get<ForeignOrganisation>(this._ccUrl + _organisationUrl + '/foreign/' + cid, {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
      .pipe(map((value) => Object.assign(new ForeignOrganisation(), value)))
  }

  getPartnerOrganisations(): Observable<ForeignOrganisation[]> {
    return this._http
      .get<ForeignOrganisation[]>(this._ccUrl + _organisationUrl + '/my/partners', {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
      .pipe(map((values) => values.map((value) => Object.assign(new ForeignOrganisation(), value))))
  }
}
