/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Inject, Injectable } from '@angular/core'
import { Partnership } from '@core/models/partnership.model'
import { Observable } from 'rxjs'
import { ApiService } from '../api.service'

const _partnershipsUrl = '/api/ui/v1/partnerships'

@Injectable({ providedIn: 'root' })
export class PartnershipApiService extends ApiService {
  constructor(@Inject('CC_URL') private _ccUrl: string) {
    super()
  }

  createPartnership(cid: string): Observable<Partnership> {
    return this._http.post<Partnership>(
      this._ccUrl + _partnershipsUrl + '/my',
      { cid },
      {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }
    )
  }

  acceptPartnership(partId: string): Observable<Partnership> {
    return this._http.put<Partnership>(this._ccUrl + _partnershipsUrl + '/' + partId, {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }

  cancelPartnership(partId: string): Observable<void> {
    return this._http.delete<void>(this._ccUrl + _partnershipsUrl + '/' + partId, {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }
}
