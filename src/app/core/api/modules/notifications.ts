/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Inject, Injectable } from '@angular/core'
import { Notification } from '@core/models/notification.model'
import { Observable } from 'rxjs'
import { ApiService } from '../api.service'

const _notificationsUrl = '/api/ui/v1/notifications'

@Injectable({ providedIn: 'root' })
export class NotificationApiService extends ApiService {
  constructor(@Inject('CC_URL') private _ccUrl: string) {
    super()
  }

  getMyNotifications(): Observable<Notification[]> {
    return this._http.get<Notification[]>(this._ccUrl + _notificationsUrl + '/my', {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }
}
