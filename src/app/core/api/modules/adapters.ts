/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { HttpStatusCode } from '@angular/common/http'

import { Injectable } from '@angular/core'
import { Adapter, AdapterConnection, AdapterConnectionCreate } from '@core/models/adapter.model'
import { Observable } from 'rxjs'
import { prependHttps } from 'src/app/utils'
import { ApiService } from '../api.service'

const _adapterDiscoveryUrl = '/api/sb/adapter/discovery'
const _adapterConnectionUrl = '/api/sb/adapterConnection'
const _propertyConsumptionUrl = '/api/sb/consumption/properties'

@Injectable({ providedIn: 'root' })
export class AdaptersApiService extends ApiService {
  constructor() {
    super()
  }

  getAdapters(sbUrl: string): Observable<Adapter[]> {
    return this.retryRequest(
      this._http.post<Adapter[]>(prependHttps(sbUrl) + _adapterDiscoveryUrl, '$', {
        headers: {
          accept: 'application/json',
          'Content-Type': 'text/plain',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }

  getAdapterConnections(sbUrl: string, oid: string, iid: string): Observable<AdapterConnection[]> {
    return this.retryRequest(
      this._http.post<AdapterConnection[]>(
        prependHttps(sbUrl) + _adapterConnectionUrl + '/discovery',
        `$[*] ? (@.oid == "${oid}" && @.iid == "${iid}")`,
        {
          headers: {
            accept: 'application/json',
            'Content-Type': 'text/plain',
          },
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  resolveAdapterConnections(sbUrl: string, adid: string): Observable<AdapterConnection[]> {
    return this.retryRequest(
      this._http.post<AdapterConnection[]>(
        prependHttps(sbUrl) + _adapterConnectionUrl + '/discovery',
        `$[*] ? (@.adid == "${adid}")`,
        {
          headers: {
            accept: 'application/json',
            'Content-Type': 'text/plain',
          },
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  createAdapterConnection(sbUrl: string, data: AdapterConnectionCreate): Observable<AdapterConnection> {
    return this.retryRequest(
      this._http.post<AdapterConnection>(prependHttps(sbUrl) + _adapterConnectionUrl, data, {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }

  deleteAdapterConnection(sbUrl: string, adidconn: string): Observable<void> {
    return this.retryRequest(
      this._http.delete<void>(`${prependHttps(sbUrl) + _adapterConnectionUrl}?adidconn=${adidconn}`, {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }

  changeAdapterConnection(sbUrl: string, adidconn: string, op: string, adid: string): Observable<AdapterConnection> {
    return this.retryRequest(
      this._http.put<AdapterConnection>(
        `${prependHttps(sbUrl) + _adapterConnectionUrl}`,
        { adidconn, op, adid },
        {
          headers: {
            accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  updateAdapterConnectionParams(sbUrl: string, adidconn: string, params: string[]): Observable<AdapterConnection> {
    return this.retryRequest(
      this._http.put<AdapterConnection>(
        `${prependHttps(sbUrl) + _adapterConnectionUrl}`,
        { adidconn, params },
        {
          headers: {
            accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  updateAdapterConnectionOptionalParams(
    sbUrl: string,
    adidconn: string,
    optionalParams: string[]
  ): Observable<AdapterConnection> {
    return this.retryRequest(
      this._http.put<AdapterConnection>(
        `${prependHttps(sbUrl) + _adapterConnectionUrl}`,
        { adidconn, optionalParams },
        {
          headers: {
            accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  updateAdapterConnectionPath(sbUrl: string, adidconn: string, path: string): Observable<AdapterConnection> {
    return this.retryRequest(
      this._http.put<AdapterConnection>(
        `${prependHttps(sbUrl) + _adapterConnectionUrl}`,
        { adidconn, path },
        {
          headers: {
            accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  updateAdapterConnectionIncomingContentType(
    sbUrl: string,
    adidconn: string,
    incomingContentType: string
  ): Observable<AdapterConnection> {
    return this.retryRequest(
      this._http.put<AdapterConnection>(
        `${prependHttps(sbUrl) + _adapterConnectionUrl}`,
        { adidconn, incomingContentType },
        {
          headers: {
            accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  updateAdapterConnectionOutgoingContentType(
    sbUrl: string,
    adidconn: string,
    outgoingContentType: string
  ): Observable<AdapterConnection> {
    return this.retryRequest(
      this._http.put<AdapterConnection>(
        `${prependHttps(sbUrl) + _adapterConnectionUrl}`,
        { adidconn, outgoingContentType },
        {
          headers: {
            accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  readJsonProperty(sbUrl: string, oid: string, pid: string, params?: Map<string, string>): Observable<any> {
    return this.retryRequest(
      this._http.get(
        `${prependHttps(sbUrl) + _propertyConsumptionUrl}/${oid}/${pid}` +
          (params !== undefined && params.size > 0
            ? '?' +
              Array.from(params.keys())
                .filter((element) => params!.get(element) && params!.get(element)!.length > 0)
                .map((element) => `${element}=${params!.get(element)}`)
                .join('&')
            : ''),
        {
          reportProgress: true,
          observe: 'events',
          responseType: 'json',
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  readFileProperty(sbUrl: string, oid: string, pid: string, params?: Map<string, string>): Observable<any> {
    return this.retryRequest(
      this._http.get(
        `${prependHttps(sbUrl) + _propertyConsumptionUrl}/${oid}/${pid}` +
          (params !== undefined && params.size > 0
            ? '?' +
              Array.from(params.keys())
                .filter((element) => params!.get(element) && params!.get(element)!.length > 0)
                .map((element) => `${element}=${params!.get(element)}`)
                .join('&')
            : ''),
        {
          reportProgress: true,
          observe: 'events',
          responseType: 'blob',
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  readTextProperty(sbUrl: string, oid: string, pid: string, params?: Map<string, string>): Observable<any> {
    return this.retryRequest(
      this._http.get(
        `${prependHttps(sbUrl) + _propertyConsumptionUrl}/${oid}/${pid}` +
          (params !== undefined && params.size > 0
            ? '?' +
              Array.from(params.keys())
                .filter((element) => params!.get(element) && params!.get(element)!.length > 0)
                .map((element) => `${element}=${params!.get(element)}`)
                .join('&')
            : ''),
        {
          reportProgress: true,
          observe: 'events',
          responseType: 'text',
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  writeProperty(
    sbUrl: string,
    oid: string,
    pid: string,
    payload?: string | FormData | File,
    params?: Map<string, string>,
    contentType?: string
  ): Observable<any> {
    const keys = params?.keys()
    return this.retryRequest(
      this._http.put<any>(
        `${prependHttps(sbUrl) + _propertyConsumptionUrl}/${oid}/${pid}` +
          (keys !== undefined
            ? '?' +
              Array.from(keys)
                .filter((element) => params!.get(element) && params!.get(element)!.length > 0)
                .map((element) => `${element}=${params!.get(element)}`)
                .join('&')
            : ''),
        payload,
        {
          reportProgress: true,
          observe: 'events',
          headers: contentType
            ? {
                'Content-Type': contentType,
              }
            : undefined,
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }
}

type Test = 'json' | 'text' | 'blob' | 'arraybuffer'
