/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { HttpStatusCode } from '@angular/common/http'

import { Inject, Injectable } from '@angular/core'
import { Certh, CerthPayload, CSR, DNSCheck, Healthcheck, Node, NodeWithOwner } from '@core/models/node.model'
import { AuditTrail, Status } from '@core/models/audit-trail.model'
import { BasicAuth } from '@core/models/user.model'
import { map, Observable } from 'rxjs'
import { prependHttps } from 'src/app/utils'
import { ApiService } from '../api.service'

const _adminUrl = '/api/sb/admin'
const _nodesUrl = '/api/ui/v1/nodes'

@Injectable({ providedIn: 'root' })
export class NodesApiService extends ApiService {
  constructor(@Inject('CC_URL') private _ccUrl: string) {
    super()
  }

  getAllNodes(): Observable<NodeWithOwner[]> {
    return this._http.get<NodeWithOwner[]>(this._ccUrl + _nodesUrl + '/all', {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }

  getNode(nodeId: string): Observable<NodeWithOwner> {
    return this._http.get<NodeWithOwner>(this._ccUrl + _nodesUrl + '/' + nodeId, {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }

  getMyOrgNodes(): Observable<Node[]> {
    return this._http.get<Node[]>(this._ccUrl + _nodesUrl + '/my', {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }

  getMyOrgPartnersNodes(): Observable<NodeWithOwner[]> {
    return this._http.get<NodeWithOwner[]>(this._ccUrl + _nodesUrl + '/my/partners', {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }

  checkDNS(host: string): Observable<DNSCheck> {
    return this._http.post<DNSCheck>(
      this._ccUrl + _nodesUrl + '/check-dns',
      { host },
      {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }
    )
  }

  testAuth(host: string, auth?: BasicAuth): Observable<void> {
    return this._http.get<void>(prependHttps(host) + _adminUrl + '/test-auth', {
      headers: {
        accept: 'application/json',
        'Content-Type': 'text/plain',
        Authorization: auth ? `BASIC ${btoa(`${auth.username}:${auth.password}`)}` : '',
      },
    })
  }

  healthcheck(sbUrl: string): Observable<Healthcheck> {
    return this.retryRequest(
      this._http.get<Healthcheck>(prependHttps(sbUrl) + _adminUrl + '/healthcheck', {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }

  registerNode(name: string, host: string): Observable<Node> {
    return this._http.post<Node>(
      this._ccUrl + _nodesUrl + '/my',
      { name, host },
      {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }
    )
  }

  removeNode(nodeId: string): Observable<void> {
    return this._http.delete<void>(this._ccUrl + _nodesUrl + '/my/' + nodeId, {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }

  enablePlatformMode(data: CerthPayload, sbUrl: string, auth?: BasicAuth): Observable<CSR> {
    return this.retryRequest(
      this._http.post<CSR>(prependHttps(sbUrl) + _adminUrl + '/platformcert/enable', data, {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: auth ? `BASIC ${btoa(`${auth.username}:${auth.password}`)}` : '',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }

  disablePlatformMode(sbUrl: string): Observable<void> {
    return this.retryRequest(
      this._http.get<void>(prependHttps(sbUrl) + _adminUrl + '/platformcert/disable', {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }

  signMyNodeCert(csr: CSR): Observable<Certh> {
    return this._http.post<Certh>(this._ccUrl + _nodesUrl + '/my/sign-cert', csr, {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }

  sendCert(certh: Certh, sbUrl: string, auth?: BasicAuth): Observable<void> {
    return this.retryRequest(
      this._http.post<void>(
        prependHttps(sbUrl) + _adminUrl + '/platformcert/cert',
        { certificate: certh.certificate },
        {
          headers: {
            accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: auth ? `BASIC ${btoa(`${auth.username}:${auth.password}`)}` : '',
          },
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  updateNodeBuildInfo(nodeId: string, version?: string, buildTime?: string): Observable<Node> {
    return this._http.put<Node>(
      this._ccUrl + _nodesUrl + '/my/' + nodeId + '/build-info',
      { version, buildTime },
      {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }
    )
  }

  getAuditTrails(sbUrl: string, page?: number, status?: Status): Observable<AuditTrail[]> {
    return this.retryRequest(
      this._http.get<AuditTrail[]>(
        prependHttps(sbUrl) + _adminUrl + '/audit-trail' + `?page=${page ?? 0}${status ? '&status=' + status : ''}`,
        {
          headers: {
            accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  getNodeLatestVersion(): Observable<string> {
    return this._http.get<string>(this._ccUrl + _nodesUrl + '/latest-version', {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }
}
