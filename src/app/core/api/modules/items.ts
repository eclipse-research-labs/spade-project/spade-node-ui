/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { Item } from '@core/models/item.model'
import { map, Observable } from 'rxjs'
import { isArray, prependHttps } from 'src/app/utils'
import { ApiService } from '../api.service'
import { HttpStatusCode } from '@angular/common/http'

const _discoveryUrl = '/api/sb/discovery'
const _itemsRegistrationsUrl = '/api/sb/registrations'
const _itemsInfrastructureUrl = '/api/sb/infrastructure'

@Injectable({ providedIn: 'root' })
export class ItemsApiService extends ApiService {
  constructor() {
    super()
  }

  getItem(sbUrl: string, oid: string): Observable<Item | undefined> {
    return this.retryRequest(
      this._http
        .get<Item[] | Item>(prependHttps(sbUrl) + _discoveryUrl + '/td/' + oid, {
          headers: {
            accept: 'application/json',
            'Content-Type': 'text/plain',
          },
        })
        .pipe(map((res) => (isArray<typeof res>(res) ? res.at(0) : res))),
      { when: HttpStatusCode.TooEarly }
    )
  }

  getMyNodeItems(sbUrl: string): Observable<Item[]> {
    return this.retryRequest(
      this._http.post<Item[]>(prependHttps(sbUrl) + _discoveryUrl, '$', {
        headers: {
          accept: 'application/json',
          'Content-Type': 'text/plain',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }

  getPartnerItems(sbUrl: string, nodeId: string, cid: string): Observable<Item[]> {
    return this.retryRequest(
      this._http.post<Item[]>(
        prependHttps(sbUrl) + _discoveryUrl + '/remote',
        `$[*] ? (@."SPADE:node"."@id" != "urn:node:${nodeId}" && @."SPADE:organisation"."@id" != "urn:organisation:${cid}")`,
        {
          headers: {
            accept: 'application/json',
            'Content-Type': 'text/plain',
          },
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  getMyOrgItems(sbUrl: string, nodeId: string, cid: string): Observable<Item[]> {
    return this.retryRequest(
      this._http.post<Item[]>(
        prependHttps(sbUrl) + _discoveryUrl + '/remote',
        `$[*] ? (@."SPADE:node"."@id" != "urn:node:${nodeId}" && @."SPADE:organisation"."@id" == "urn:organisation:${cid}")`,
        {
          headers: {
            accept: 'application/json',
            'Content-Type': 'text/plain',
          },
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  createItem(sbUrl: string, td: Object): Observable<Item> {
    return this.retryRequest(
      this._http.post<Item>(prependHttps(sbUrl) + _itemsRegistrationsUrl, td, {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }

  deleteItem(sbUrl: string, oid: string): Observable<void> {
    return this.retryRequest(
      this._http.delete<void>(`${prependHttps(sbUrl) + _itemsRegistrationsUrl}?oid=${oid}`, {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }

  changeItemPrivacy(sbUrl: string, oid: string, privacy: string): Observable<void> {
    return this.retryRequest(
      this._http.put<void>(
        prependHttps(sbUrl) + _itemsInfrastructureUrl + '/item/' + oid + '/privacy',
        {
          privacy,
        },
        {
          headers: {
            accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  changeItemsPrivacy(sbUrl: string, oids: string[], privacy: string): Observable<void> {
    return this.retryRequest(
      this._http.put<void>(
        prependHttps(sbUrl) + _itemsInfrastructureUrl + '/item/bulkPrivacy',
        oids.map((oid) => {
          return {
            oid,
            privacy,
          }
        }),
        {
          headers: {
            accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }
}
