/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { HttpStatusCode } from '@angular/common/http'

import { Injectable } from '@angular/core'
import { Credential, Secret } from '@core/models/credentials.model'
import { Observable } from 'rxjs'
import { prependHttps } from 'src/app/utils'
import { ApiService } from '../api.service'

const _credentialsUrl = '/api/sb/admin/credentials'

@Injectable({ providedIn: 'root' })
export class CredentialsApiService extends ApiService {
  constructor() {
    super()
  }

  getCredentials(sbUrl: string): Observable<Credential[]> {
    return this.retryRequest(
      this._http.get<Credential[]>(prependHttps(sbUrl) + _credentialsUrl, {
        headers: {
          accept: 'application/json',
          'Content-Type': 'text/plain',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }

  createCredential(sbUrl: string, permissions: string[], ttl: number): Observable<Secret> {
    return this.retryRequest(
      this._http.post<Secret>(
        prependHttps(sbUrl) + _credentialsUrl,
        {},
        {
          params: { ttl, permissions: permissions.map((element) => element.toUpperCase()).join(',') },
          headers: {
            accept: 'application/json',
            'Content-Type': 'text/plain',
          },
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  renewCredential(sbUrl: string, clientId: string, ttl: number): Observable<Secret> {
    return this.retryRequest(
      this._http.put<Secret>(
        prependHttps(sbUrl) + _credentialsUrl + '/' + clientId,
        {},
        {
          params: { ttl },
          headers: {
            accept: 'application/json',
            'Content-Type': 'text/plain',
          },
        }
      ),
      { when: HttpStatusCode.TooEarly }
    )
  }

  deleteCredential(sbUrl: string, clientId: string): Observable<void> {
    return this.retryRequest(
      this._http.delete<void>(prependHttps(sbUrl) + _credentialsUrl + '/' + clientId, {
        headers: {
          accept: 'application/json',
          'Content-Type': 'text/plain',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }
}
