/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { HttpStatusCode } from '@angular/common/http'

import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { prependHttps } from 'src/app/utils'
import { ApiService } from '../api.service'

const _subscriberUrl = '/api/sb/subscriber'
const _eventsUrl = _subscriberUrl + '/events'

@Injectable({ providedIn: 'root' })
export class EventsApiService extends ApiService {
  constructor() {
    super()
  }

  getEventSubscribers(sbUrl: string, oid: string, eid: string): Observable<string[]> {
    return this.retryRequest(
      this._http.get<any[]>(`${prependHttps(sbUrl) + _eventsUrl}/${oid}/${eid}`, {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }

  subscribeToEvent(sbUrl: string, oid: string, eid: string): Observable<void> {
    return this.retryRequest(
      this._http.post<any>(`${prependHttps(sbUrl) + _eventsUrl}/${oid}/${eid}`, {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }

  unsubscribeFromEvent(sbUrl: string, oid: string, eid: string): Observable<any> {
    return this.retryRequest(
      this._http.delete<any>(`${prependHttps(sbUrl) + _eventsUrl}/${oid}/${eid}`, {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }),
      { when: HttpStatusCode.TooEarly }
    )
  }
}
