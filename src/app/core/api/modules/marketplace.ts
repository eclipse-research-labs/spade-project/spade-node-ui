/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Inject, Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from '../api.service'
import { Item, MarketplaceItems } from '@core/models/item.model'

@Injectable({ providedIn: 'root' })
export class MarketplaceApiService extends ApiService {
  constructor(@Inject('MARKETPLACE_URL') private _marketplaceUrl: string) {
    super()
  }

  getMarketplaceItems(): Observable<MarketplaceItems> {
    return this._http.get<MarketplaceItems>(this._marketplaceUrl + '/all', {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
  }
}
