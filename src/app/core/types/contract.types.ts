/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ContractRequestStatus } from '@core/enums/contract.enum'
import { Contract } from '@core/models/contract.model'
import { Item, ItemWithIdentity } from '@core/models/item.model'
import { Node, NodeWithOwner } from '@core/models/node.model'
import { User } from '@core/models/user.model'
import { TreeNode } from 'primeng/api'
import { BehaviorSubject, Observable } from 'rxjs'

export type TargetType = string | ItemWithIdentity | Item | null | 'DOWNLOADING'
export type AssigneeType = string | NodeWithOwner | Node | null | 'DOWNLOADING'
export type AssignerType = string | User | null | 'DOWNLOADING'
export type DownloadIdentityStatus = 'DOWNLOADING' | 'DOWNLOADED'

export interface TreeNodeWithStatus extends TreeNode {
  control: BehaviorSubject<ContractRequestStatus>
  status$: Observable<ContractRequestStatus>
}


