/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ItemType } from '@core/models/item.model'
import { itemTypes, mimeTypes, propDataTypes } from './data'
import xml2js from 'xml2js'

export function deepEqual(object1: any, object2: any) {
  const keys1 = Object.keys(object1)
  const keys2 = Object.keys(object2)
  if (keys1.length !== keys2.length) {
    return false
  }
  for (const key of keys1) {
    const val1 = object1[key]
    const val2 = object2[key]
    const areObjects = isObject(val1) && isObject(val2)
    if ((areObjects && !deepEqual(val1, val2)) || (!areObjects && val1 !== val2)) {
      return false
    }
  }
  return true
}

export function isObject(value: any): value is object | Object {
  return value !== undefined && value !== null && (typeof value === 'object' || value instanceof Object)
}

export function isString(value: any): value is string | String {
  return value !== undefined && value !== null && (typeof value === 'string' || value instanceof String)
}

export function isArray<T>(value: any): value is Array<T> {
  return value !== undefined && value !== null && value instanceof Array
}

export function isBlob(value: any): value is Blob {
  return value !== undefined && value !== null && isObject(value) && value.constructor.name === 'Blob'
}

export function isFile(value: any): value is File {
  return value !== undefined && value !== null && isObject(value) && value.constructor.name === 'File'
}

export function delay(time: number) {
  return new Promise((resolve) => setTimeout(resolve, time))
}

export function inflect(num: number, zero: string, one: string, many: string) {
  switch (num) {
    case 0:
      return zero
    case 1:
      return one
    default:
      return many
  }
}

export function resolveItemType(itemType?: string | string[]): ItemType {
  if (!itemType) {
    return itemTypes[0]
  }
  function findType(itemType: any) {
    if (isString(itemType)) {
      const split = itemType.split(':').at(-1)
      return itemTypes.find((element) => element.title === split) ?? itemTypes[0]
    }
    return itemTypes[0]
  }
  if (Array.isArray(itemType)) {
    itemType = itemType[0]
    return findType(itemType)
  }
  return findType(itemType)
}

export function resolveItemContentTypeIcon(contentType?: string): string | undefined {
  if (contentType) {
    const mimeGroup = contentType.split('/').at(0)
    if (mimeGroup) {
      const mimes = mimeTypes[mimeGroup]
      if (mimes) {
        return mimes.values.some((element) => element == contentType) ? mimes.icon : undefined
      }
    }
  }
  return undefined
}

export function getPropUnitDataTypeSymbol(unitDataType?: string): string | undefined {
  if (!unitDataType) {
    return undefined
  }
  return propDataTypes.find((element) => element.name === unitDataType)?.symbol
}

export function getBase64(blob: Blob): Promise<string> {
  const temporaryFileReader = new FileReader()
  return new Promise((resolve, reject) => {
    temporaryFileReader.onerror = () => {
      temporaryFileReader.abort()
      reject(new DOMException('Problem parsing input file.'))
    }
    temporaryFileReader.onload = () => {
      resolve(temporaryFileReader.result as string)
    }
    temporaryFileReader.readAsDataURL(blob)
  })
}

export function stripLeadingAndTailingSlash(url: string) {
  if (url.startsWith('/')) {
    url = url.substring(1)
  }
  if (url.endsWith('/')) {
    url = url.substring(0, url.length - 1)
  }
  return url
}

export function prependHttps(url: string) {
  if (!url.startsWith('https://')) {
    return 'https://' + url
  }
  return url
}

export function encodeSBUrl(url: string) {
  const encoded = btoa(url)
  return encoded.replaceAll('=', '!')
}

export function decodeSBUrl(url: string) {
  const parsed = url.replaceAll('!', '=')
  return atob(parsed)
}

export function parseNodeHost(host: string): string {
  const noSlash = stripLeadingAndTailingSlash(host)
  // Remove protocol
  return noSlash.replace(/(^\w+:|^)\/\//, '')
}

export function applyLocalTimezone(utcDate: Date | string | number) {
  const today = new Date()
  const date = new Date(utcDate)
  return new Date(-1 * today.getTimezoneOffset() * 60000 + date.getTime())
}

export function removeLocalTimezone(utcDate: Date | string | number) {
  const today = new Date()
  const date = new Date(utcDate)
  return new Date(today.getTimezoneOffset() * 60000 + date.getTime())
}

const _ICONS: Map<number, string> = new Map([
  [0, 'fa fa-eye'],
  [1, 'fa fa-house-user'],
  [2, 'fa-regular fa-handshake'],
  [3, 'pi pi-globe'],
])

export function levelToPrivacyIcon(level: number) {
  return _ICONS.get(level)
}

export async function parseXml(xmlString: string) {
  return await new Promise((resolve, reject) =>
    xml2js.parseString(xmlString, (err, jsonData) => {
      if (err) {
        reject(err)
      }
      resolve(jsonData)
    })
  )
}

export function chunkArray<T>(array: T[], chunkSize?: number) {
  const chunks = []
  const size = chunkSize ?? 100
  for (let i = 0; i < array.length; i += size) {
    const chunk = array.slice(i, i + size)
    chunks.push(chunk)
  }
  return chunks
}


export function parseNodeVersionFromVersionString(version: string) {
  const match = version.match(/(\d+\.\d+\.\d+)/)
  return match ? match[0] : null
}