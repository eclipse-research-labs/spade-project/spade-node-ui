/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { MasterService } from '@core/services/master/master.service'
import { environment } from '@env'
import { KeycloakService } from 'keycloak-angular'

export function initializeKeycloak(keycloakService: KeycloakService, masterService: MasterService, appUrl: string) {
  return () =>
    keycloakService
      .init({
        config: environment.kcConfig,
        initOptions: {
          onLoad: 'check-sso',
          silentCheckSsoRedirectUri: masterService.appUrl + '/assets/silent-check-sso.html',
        },
        bearerExcludedUrls: [
          '/api/sb/admin/test-auth',
          '/api/sb/admin/platformcert/enable',
          '/api/sb/admin/platformcert/cert',
          environment.s3url,
        ],
      })
      .catch((e) => {
        masterService.updateError('Failed to initialize keycloak')
      })
}
