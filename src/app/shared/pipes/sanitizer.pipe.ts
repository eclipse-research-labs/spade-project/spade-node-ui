/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Pipe, PipeTransform } from '@angular/core'
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser'

@Pipe({
  name: 'sanitizer',
  standalone: true,
})
export class SanitizerPipe implements PipeTransform {
  constructor(protected _sanitizer: DomSanitizer) {}

  transform(value: string, type: string): SafeHtml | SafeStyle | SafeScript | SafeUrl | SafeResourceUrl {
    switch (type) {
      case 'html':
        return this._sanitizer.bypassSecurityTrustHtml(value)
      case 'style':
        return this._sanitizer.bypassSecurityTrustStyle(value)
      case 'script':
        return this._sanitizer.bypassSecurityTrustScript(value)
      case 'url':
        return this._sanitizer.bypassSecurityTrustUrl(value)
      case 'resourceUrl':
        return this._sanitizer.bypassSecurityTrustResourceUrl(value)
      default:
        return this._sanitizer.bypassSecurityTrustHtml(value)
    }
  }
}
