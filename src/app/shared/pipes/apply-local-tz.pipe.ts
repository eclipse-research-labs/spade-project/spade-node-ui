/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Pipe, PipeTransform } from '@angular/core'
import { applyLocalTimezone } from 'src/app/utils'

@Pipe({
  name: 'applyLocalTzPipe',
  standalone: true,
})
export class ApplyLocalTzPipe implements PipeTransform {
  transform(value: Date | string | number): Date {
    return applyLocalTimezone(value)
  }
}
