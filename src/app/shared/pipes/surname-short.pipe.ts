/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'surnameShort',
  standalone: true,
})
export class SurnameShortPipe implements PipeTransform {

  transform(surname: string | undefined | null): string {
    if (surname && surname[0]) {
      return `${surname[0].toUpperCase()}.`
    }
    return '';
  }

}
