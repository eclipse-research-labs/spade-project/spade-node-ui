/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { Contract } from '@core/models/contract.model'
import { ItemWithIdentity } from '@core/models/item.model'
import { NodeWithOwner } from '@core/models/node.model'
import { User } from '@core/models/user.model'
import { TagModule } from 'primeng/tag'
import { isString } from 'src/app/utils'
import { TypeIconComponent } from '../../type-icon/type-icon.component'
import { SkeletonModule } from 'primeng/skeleton'

@Component({
  selector: 'app-contract-preview',
  templateUrl: './contract-preview.component.html',
  styleUrl: './contract-preview.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, TypeIconComponent, TagModule, SkeletonModule]
})
export class ContractPreviewComponent {
  @Input() contract!: Contract
  @Input() loadingTarget: boolean = false
  @Input() loadingAssignee: boolean = false
  @Input() loadingAssigner: boolean = false
  @Input() isNew = false

  get target() {
    const target = this.contract.target
    if(isString(target)){ 
      return target
    }
    if (!target) return target
    const hasOrganisation = 'organisation' in (target ?? {})
    const org = hasOrganisation ? (target as ItemWithIdentity).organisation?.name : target['SPADE:organisation']?.['@id']
    return `${target.title} <span class="text-text-300 font-normal">(${org})`
  }

  get assignee() {
    const assignee = this.contract.assignee
    if (isString(assignee)) {
      return assignee
    }
    if (!assignee) return assignee
    const hasOwner = 'owner' in (assignee ?? {})
    const owner = hasOwner ? (assignee as NodeWithOwner).owner.name : assignee.host
    return `${assignee.name} <span class="text-text-300">(${owner})`
  }

  get assigner() {
    const assigner = this.contract.assigner
    if (isString(assigner)) {
      return assigner
    }
    if (!assigner) return assigner
    const assignerOrganisation = (this.contract.assigner as User).organisation
    const user = `${assigner?.givenName} ${assigner?.familyName}`
    return `${user} <span class="text-text-300">(${assignerOrganisation ? assignerOrganisation.name: 'Unknown org'})</span>`
  }

  get assignerEmail() {
    const assigner = this.contract.assigner
    if (isString(assigner)) {
      return null
    }
    return assigner?.email
  }

  get readPermission() {
    return this.contract.policy.permission.some(element => element.action == 'read')
  }

  get writePermission() {
    return this.contract.policy.permission.some(element => element.action == 'write')
  }

  get subscribePermission() {
    return this.contract.policy.permission.some(element => element.action == 'subscribe')
  }
}
