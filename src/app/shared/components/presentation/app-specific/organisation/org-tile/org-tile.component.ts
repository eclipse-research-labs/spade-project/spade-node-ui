/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, ContentChild, Input, TemplateRef } from '@angular/core'
import { ForeignOrganisation } from '@core/models/organisation.model'

@Component({
  selector: 'app-org-tile',
  templateUrl: './org-tile.component.html',
  styleUrl: './org-tile.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule],
})
export class OrgTileComponent {
  @Input() org!: ForeignOrganisation

  @ContentChild('trailing') trailing: TemplateRef<any> | undefined
}
