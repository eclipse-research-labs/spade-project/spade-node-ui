/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'
import { ForeignOrganisation } from '@core/models/organisation.model'
import { TypeIconComponent } from '../../type-icon/type-icon.component'

@Component({
  selector: 'app-org-preview',
  templateUrl: './org-preview.component.html',
  styleUrl: './org-preview.component.scss',
  standalone: true,
  imports: [CommonModule, TypeIconComponent],
})
export class OrgPreviewComponent {
  @Input() org!: ForeignOrganisation
}
