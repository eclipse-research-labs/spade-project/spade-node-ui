/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core'
import { Adapter } from '@core/models/adapter.model'
import { inflect } from 'src/app/utils'
import { CommonModule } from '@angular/common'
import { TypeIconComponent } from '../../type-icon/type-icon.component'

@Component({
  selector: 'app-adapter-preview',
  templateUrl: './adapter-preview.component.html',
  styleUrl: './adapter-preview.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, TypeIconComponent]
})
export class AdapterPreviewComponent implements OnInit {
  @Input() adapter!: Adapter
  @Input() showConnectionCount: boolean = true

  conNum!: string

  constructor() {}

  ngOnInit(): void {
    const num = this.adapter.adapterConnections?.length ?? 0
    this.conNum = num + ' ' + inflect(num, 'connections', 'connection', 'connections')
  }
}
