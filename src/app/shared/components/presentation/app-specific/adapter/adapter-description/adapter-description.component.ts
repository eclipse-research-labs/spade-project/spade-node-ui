/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges, ViewEncapsulation } from '@angular/core'
import { Adapter } from '@core/models/adapter.model'
import { SanitizerPipe } from '@shared/pipes/sanitizer.pipe'

@Component({
  selector: 'app-adapter-description',
  templateUrl: './adapter-description.component.html',
  styleUrl: './adapter-description.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule, SanitizerPipe],
})
export class AdapterDescriptionComponent implements OnInit, OnChanges {
  text: string = ''

  @Input() adapter!: Adapter

  ngOnInit(): void {
    this._loadDescription()
  }

  ngOnChanges(changes: SimpleChanges): void {
    this._loadDescription()
  }

  private _loadDescription() {
    this.text = atob(this.adapter.description ?? '')
  }

  editMode: boolean = false
}

// PushAdapter
// <p class="ql-align-justify">This adapter is used to store the last value in the <a href="https://redis.com/why-redis-cloud/?utm_source=google&amp;utm_medium=cpc&amp;utm_term=redis&amp;utm_campaign=redis360-brand-emea-19645427181&amp;utm_content=&amp;gad_source=1&amp;gclid=Cj0KCQiAtaOtBhCwARIsAN_x-3KNxYpcqXjrCkpZDXe9L_kkkwx1hbh2eKT8n3KPyJ_C_n3ftfpNlxQaAgIoEALw_wcB" rel="noopener noreferrer" target="_blank">Redis</a> object database.</p><p class="ql-align-justify"><em>Please keep in mind that database is part of this Node.</em></p><p class="ql-align-justify"><br></p><p class="ql-align-justify"><strong>How the adapter works?</strong></p><p class="ql-align-justify">The values ​​are stored as <em>{key:value}</em> pairs in local embedded database. Only one value is remebered, so any new value stored under the same key will overwrite the previous one.</p><p class="ql-align-justify"><br></p><p class="ql-align-justify"><strong>Accepted values</strong>: Json, String, Number</p><p class="ql-align-justify"><br></p><p class="ql-align-justify"><strong>How to setup?</strong></p><ol><li class="ql-align-justify">navigate to item and select item's property</li><li class="ql-align-justify">setup the connection between the property and the adapter</li><li class="ql-align-justify">provide the key as last part of the <em>Base url</em></li><li class="ql-align-justify">under this key values ​​will be read or written to the db</li></ol>

// TSAdapter
// <p class="ql-align-justify">This adapter is used to store the series of values in the <a href="https://redis.com/why-redis-cloud/?utm_source=google&amp;utm_medium=cpc&amp;utm_term=redis&amp;utm_campaign=redis360-brand-emea-19645427181&amp;utm_content=&amp;gad_source=1&amp;gclid=Cj0KCQiAtaOtBhCwARIsAN_x-3KNxYpcqXjrCkpZDXe9L_kkkwx1hbh2eKT8n3KPyJ_C_n3ftfpNlxQaAgIoEALw_wcB" rel="noopener noreferrer" target="_blank">Redis</a> object database.</p><p class="ql-align-justify"><em>Please keep in mind that database is part of this Node.</em></p><p class="ql-align-justify"><br></p><p class="ql-align-justify"><strong>How the adapter works?</strong></p><p class="ql-align-justify">The values ​​are stored as Time series Data Points using <a href="https://redis.io/docs/data-types/timeseries/" rel="noopener noreferrer" target="_blank">RedisTimeSeries</a> module in local embedded database. When saving data, you must submit it in the body with the following signature:</p><pre class="ql-syntax ql-align-justify" spellcheck="false">{
//   "name": "string",
//   "ts": "timestemp",
//   "value": "json | string | number"
// }
// </pre><p class="ql-align-justify"><br></p><p class="ql-align-justify">Data can be retrieved from database using <em>timestamp</em> ranges specified by <em>tsini</em> and <em>tsend</em> query parameters.</p><p class="ql-align-justify"><br></p><p class="ql-align-justify"><strong>Accepted values</strong>:</p><p class="ql-align-justify">Json, String, Number</p><p class="ql-align-justify"><br></p><p class="ql-align-justify"><strong>How to setup?</strong></p><ol><li class="ql-align-justify">navigate to item and select item's property</li><li class="ql-align-justify">setup the connection between the property and the adapter</li><li class="ql-align-justify">add the <em>tsini</em> and <em>tsend</em> query parameters</li><li class="ql-align-justify">saving data to the db is done using the correct body described above</li></ol>

// DummyAdapter
// <p class="ql-align-justify">This adapter is used for testing purposes only<em>.</em></p><p class="ql-align-justify"><br></p><p class="ql-align-justify"><strong>How the adapter works?</strong></p><p class="ql-align-justify">The adapter just returns a 200 message when it's online.</p><p class="ql-align-justify"><br></p><p class="ql-align-justify"><strong>Accepted values</strong>:</p><p class="ql-align-justify">Json, String, Number</p><p class="ql-align-justify"><br></p><p class="ql-align-justify"><strong>How to setup?</strong></p><ol><li class="ql-align-justify">navigate to item and select item's property</li><li class="ql-align-justify">setup the connection between the property and the adapter</li></ol>
