/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Adapter } from '@core/models/adapter.model';
import { DialogModule } from 'primeng/dialog';
import { AdapterDescriptionComponent } from '../adapter-description/adapter-description.component';
import { TypeIconComponent } from '../../type-icon/type-icon.component';

@Component({
  selector: 'app-adapter-help',
  templateUrl: './adapter-help.component.html',
  styleUrl: './adapter-help.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, DialogModule, AdapterDescriptionComponent, TypeIconComponent]
})
export class AdapterHelpComponent {
  @Input() adapter!: Adapter

  showHelp: boolean = false

}
