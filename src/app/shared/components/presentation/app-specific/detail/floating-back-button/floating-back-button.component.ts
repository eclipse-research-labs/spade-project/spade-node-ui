/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ContentChild, ElementRef, EventEmitter, HostListener, NgZone, Output, TemplateRef, ViewChild } from '@angular/core'
import { ButtonModule } from 'primeng/button'

@Component({
  selector: 'app-floating-back-button',
  templateUrl: './floating-back-button.component.html',
  styleUrl: './floating-back-button.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, ButtonModule]
})
export class FloatingBackButtonComponent implements AfterViewInit {
  @ContentChild('scrolled') scrolled?: TemplateRef<any>
  
  @Output() buttonClicked: EventEmitter<Event> = new EventEmitter()

  @ViewChild('detail', { static: false }) detail?: ElementRef

  backButtonPosFixed: boolean = false
  scrolledVisible: boolean = false

  constructor(private _zone: NgZone, private _cd: ChangeDetectorRef) {}

  ngAfterViewInit(): void {
    if (this.detail) {
      this.detail.nativeElement.addEventListener('scroll', (event: Event) => {
        if (this.detail?.nativeElement?.scrollTop > 20) {
          this.backButtonPosFixed = true
        } else {
          this.backButtonPosFixed = false
        }
        if (this.detail?.nativeElement?.scrollTop > 68) {
          this.scrolledVisible = true
        } else {
          this.scrolledVisible = false
        }
        this._cd.markForCheck()
      })
    }
  }

  onButtonClick(event: Event) {
    this.buttonClicked.emit(event)
  }
}
