/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, ContentChild, EventEmitter, Input, Output, TemplateRef } from '@angular/core'
import { RouterModule } from '@angular/router'
import { ButtonModule } from 'primeng/button'

@Component({
  selector: 'app-onboarding-card',
  templateUrl: './onboarding-card.component.html',
  styleUrl: './onboarding-card.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, RouterModule, ButtonModule],
})
export class OnboardingCardComponent {
  @ContentChild('beforeTitle') beforeTitle: TemplateRef<any> | undefined

  @Input() title!: string
  @Input() showClose: boolean = false
  @Input() wide: boolean = false
  @Input() routeOnBack?: string | null | undefined
  @Output() onClose: EventEmitter<void> = new EventEmitter()

  constructor() {}

  close() {
    this.onClose.emit()
  }
}
