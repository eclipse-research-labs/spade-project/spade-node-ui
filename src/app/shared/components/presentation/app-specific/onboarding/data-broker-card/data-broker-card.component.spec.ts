/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataBrokerCardComponent } from './data-broker-card.component';

describe('DataBrokerCardComponent', () => {
  let component: DataBrokerCardComponent;
  let fixture: ComponentFixture<DataBrokerCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DataBrokerCardComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DataBrokerCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
