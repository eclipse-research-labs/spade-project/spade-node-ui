/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core'
import { Item, ItemType } from '@core/models/item.model'
import { Organisation } from '@core/models/organisation.model'
import { resolveItemType, inflect } from 'src/app/utils'
import { TypeIconComponent } from '../../type-icon/type-icon.component'
import { TagModule } from 'primeng/tag'

@Component({
  selector: 'app-item-preview',
  templateUrl: './item-preview.component.html',
  styleUrls: ['./item-preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, TypeIconComponent, TagModule],
})
export class ItemPreviewComponent implements OnInit, OnChanges {
  @Input() item!: Item
  @Input() organisation?: Organisation
  @Input() isNew = false
  @Input() showPropCount = true
  @Input() indicate = false

  propNum!: string
  type!: ItemType

  constructor() {}

  ngOnInit(): void {
    this._initItemMeta()
  }

  ngOnChanges(changes: SimpleChanges): void {
    this._initItemMeta()
  }

  private _initItemMeta() {
    const num = Object.keys(this.item.properties ?? {}).length
    this.propNum = num + ' ' + inflect(num, 'properties', 'property', 'properties')
    this.type = resolveItemType(this.item['@type'])
  }
}
