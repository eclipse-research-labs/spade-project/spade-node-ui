/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { Item, EventWithKey } from '@core/models/item.model'
import { ButtonModule } from 'primeng/button'

@Component({
  selector: 'app-event-preview',
  standalone: true,
  imports: [CommonModule, ButtonModule],
  templateUrl: './event-preview.component.html',
  styleUrl: './event-preview.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventPreviewComponent {
  @Input() item!: Item
  @Input() event!: EventWithKey
}
