/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { ItemWithIdentity } from '@core/models/item.model'
import { TypeIconComponent } from '../../type-icon/type-icon.component'

@Component({
  selector: 'app-dataset-preview',
  standalone: true,
  imports: [CommonModule, TypeIconComponent],
  templateUrl: './dataset-preview.component.html',
  styleUrl: './dataset-preview.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DatasetPreviewComponent {
  @Input() dataset!: ItemWithIdentity
}
