/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { ItemWithIdentity } from '@core/models/item.model'
import { TypeIconComponent } from '../../type-icon/type-icon.component'
import { CommonModule } from '@angular/common'
import { ButtonModule } from 'primeng/button'
import { DialogModule } from 'primeng/dialog'

@Component({
  selector: 'app-service-preview',
  standalone: true,
  imports: [
    CommonModule,
    TypeIconComponent,
    ButtonModule,
    DialogModule,
  ],
  templateUrl: './service-preview.component.html',
  styleUrl: './service-preview.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServicePreviewComponent {
  @Input() service!: ItemWithIdentity

  detailVisible: boolean = false
}
