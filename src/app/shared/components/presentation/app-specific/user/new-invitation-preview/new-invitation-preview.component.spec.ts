/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewInvitationPreviewComponent } from './new-invitation-preview.component';

describe('NewInvitationPreviewComponent', () => {
  let component: NewInvitationPreviewComponent;
  let fixture: ComponentFixture<NewInvitationPreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NewInvitationPreviewComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NewInvitationPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
