/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { MyOrgService } from '@core/services/my-org/my-org.service'
import { TypeIconComponent } from '../../type-icon/type-icon.component'
import { TagModule } from 'primeng/tag'

@Component({
  selector: 'app-new-invitation-preview',
  templateUrl: './new-invitation-preview.component.html',
  styleUrl: './new-invitation-preview.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, TypeIconComponent, TagModule],
})
export class NewInvitationPreviewComponent {
  @Input() email!: string

  constructor(private _myOrgService: MyOrgService) {}

  get isOld() {
    return this._myOrgService.invitations.some((element) => element.email == this.email)
  }
}
