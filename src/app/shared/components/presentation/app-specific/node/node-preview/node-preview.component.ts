/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { Node } from '@core/models/node.model'
import { TypeIconComponent } from '../../type-icon/type-icon.component'
import { TagModule } from 'primeng/tag'
import { ApplyLocalTzPipe } from '@shared/pipes/apply-local-tz.pipe'
import { TooltipModule } from 'primeng/tooltip'

@Component({
  selector: 'app-node-preview',
  templateUrl: './node-preview.component.html',
  styleUrl: './node-preview.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, TypeIconComponent, ApplyLocalTzPipe, TagModule, TooltipModule],
})
export class NodePreviewComponent {
  @Input() node!: Node
  @Input() isNew = false
  @Input() darkMode = false
}
