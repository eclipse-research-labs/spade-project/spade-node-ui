/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { Item, PropertyWithKey } from '@core/models/item.model'
import { PropInfoComponent } from '../prop-info/prop-info.component'

@Component({
  selector: 'app-prop-preview',
  templateUrl: './prop-preview.component.html',
  styleUrl: './prop-preview.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, PropInfoComponent]
})
export class PropPreviewComponent {
  @Input() item!: Item
  @Input() prop!: PropertyWithKey
}
