/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core'
import { ContextClass, Item, PropertyWithKey } from '@core/models/item.model'
import { getPropUnitDataTypeSymbol, isString, stripLeadingAndTailingSlash } from 'src/app/utils'

@Component({
  selector: 'app-prop-info',
  templateUrl: './prop-info.component.html',
  styleUrl: './prop-info.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule],
})
export class PropInfoComponent implements OnInit {
  @Input() item!: Item
  @Input() prop!: PropertyWithKey
  @Input() hideOverflownDescription = true

  hideDesc = true

  type?: string
  typeContext?: string
  typeUrl?: string

  dataUnits?: string
  dataUnitsContext?: string
  dataUnitsUrl?: string

  ngOnInit(): void {
    this._parseType()
    this._parseDataUnits()
  }

  toggleInfo() {
    this.hideDesc = !this.hideDesc
  }

  propUnitDataTypeSymbol(unitDataType?: string): string | undefined {
    return getPropUnitDataTypeSymbol(unitDataType)
  }

  isEllipsisActive(e: HTMLElement) {
    return e.offsetWidth < e.scrollWidth
  }

  openUrl(url?: string) {
    if (url) window.open(url, '_blank')
  }

  private _parseType() {
    const type = this.prop['@type']
    if (type) {
      if (type.includes(':')) {
        const [key, propType] = type.split(':')
        ;[this.typeContext, this.type] = [this._parseUrlFromContextKey(key), propType]
      } else {
        ;[this.typeContext, this.type] = [this._baseContext as string, type]
      }
      if (this.typeContext && this.type) {
        this.typeUrl = `${stripLeadingAndTailingSlash(this.typeContext)}#${this.type}`
      }
    }
  }

  private _parseDataUnits() {
    const dataUnits = this.prop.unit ?? this.prop['om:hasUnit']
    if (dataUnits) {
      if (dataUnits.includes(':')) {
        const [key, propDataUnit] = dataUnits.split(':')
        ;[this.dataUnitsContext, this.dataUnits] = [this._parseUrlFromContextKey(key), propDataUnit]
      } else {
        ;[this.dataUnitsContext, this.dataUnits] = [this._baseContext as string, dataUnits]
      }
      if (this.dataUnitsContext && this.dataUnits) {
        this.dataUnitsUrl = `${stripLeadingAndTailingSlash(this.dataUnitsContext)}/${this.dataUnits}`
      }
    }
  }

  private _parseUrlFromContextKey(key: string) {
    const context = this.item['@context'].find((element) => !isString(element)) as ContextClass | undefined
    if (!context) return undefined
    else return context[key]
  }

  private get _baseContext() {
    return this.item['@context'].find((element) => isString(element))
  }
}
