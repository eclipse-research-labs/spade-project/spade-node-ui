/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core'

@Component({
  selector: 'app-type-icon',
  templateUrl: './type-icon.component.html',
  styleUrls: ['./type-icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule]
})
export class TypeIconComponent implements OnInit {
  @Input() size?: number
  @Input() color?: string
  @Input() icon?: string

  constructor() {}

  ngOnInit(): void {}

  get _color(): string {
    return this.color ?? '#DDB593'
  }

  get _icon(): string {
    return this.icon ?? 'fa fa-cube'
  }
}
