/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { RouterModule } from '@angular/router'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { encodeSBUrl } from 'src/app/utils'

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, RouterModule]
})
export class LogoComponent implements OnInit {
  constructor(private _myNodeService: MyNodeService) {}

  ngOnInit(): void {}

  get routerLink() {
    return this._myNodeService.online ? '/marketplace/services' : `/my-node/offline/${encodeSBUrl(this._myNodeService.broker?.address ?? '')}/items`
  }
}
