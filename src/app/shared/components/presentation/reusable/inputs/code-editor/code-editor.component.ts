/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, Input, ViewChild } from '@angular/core'
import { NG_VALUE_ACCESSOR } from '@angular/forms'
import { ACE_CONFIG, AceComponent, AceConfigInterface, AceModule } from 'ngx-ace-wrapper'
import 'brace'
import 'brace/mode/json'
import 'brace/mode/sh'
// import 'brace/mode/html'
// import 'brace/mode/xml'
// import 'brace/mode/css'
// import 'brace/mode/javascript'
import 'brace/theme/cobalt'
import 'brace/ext/searchbox'
import { CommonModule } from '@angular/common'
import { PlatformCopyComponent } from '../../platform-copy/platform-copy.component'

const DEFAULT_ACE_CONFIG: AceConfigInterface = {
  tabSize: 2,
  showPrintMargin: false,
}

@Component({
  selector: 'app-code-editor',
  templateUrl: './code-editor.component.html',
  styleUrls: ['./code-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: CodeEditorComponent,
    },
    {
      provide: ACE_CONFIG,
      useValue: DEFAULT_ACE_CONFIG,
    },
  ],
  standalone: true,
  imports: [CommonModule, AceModule, PlatformCopyComponent],
})
export class CodeEditorComponent {
  @Input() fontSize: number = 14
  @Input() readOnly: boolean = false
  @Input() showGutter: boolean = true
  @Input() mode?: string
  @Input() allowCopy: boolean = false

  @ViewChild('editor') editor?: AceComponent

  config: AceConfigInterface = {}

  value: string = ''

  touched = false

  disabled = false

  constructor() {}

  onChange = (value: string) => {}

  onTouched = () => {}

  writeValue(value: string) {
    this.value = value
  }

  registerOnChange(onChange: any) {
    this.onChange = onChange
  }

  registerOnTouched(onTouched: any) {
    this.onTouched = onTouched
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched()
      this.touched = true
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled
  }
}
