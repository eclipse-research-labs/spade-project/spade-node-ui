/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ContentChild, TemplateRef, ViewEncapsulation } from '@angular/core'
import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormsModule } from '@angular/forms'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { FileSelectEvent, FileUploadModule } from 'primeng/fileupload'
import { CodeEditorComponent } from '../code-editor/code-editor.component'
import { PlatformCopyComponent } from '../../platform-copy/platform-copy.component'

@Component({
  selector: 'app-td-upload',
  templateUrl: './td-upload.component.html',
  styleUrls: ['./td-upload.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: TdUploadComponent,
    },
  ],
  standalone: true,
  imports: [CommonModule, FormsModule, CodeEditorComponent, PlatformCopyComponent, FileUploadModule],
})
export class TdUploadComponent implements ControlValueAccessor {
  @ContentChild('actions') actions: TemplateRef<any> | undefined

  tdFile: File | null = null

  uploaded: boolean = false

  over: boolean = false

  touched = false

  disabled = false

  content: any

  constructor(private _snackBar: SnackBarService, private _cd: ChangeDetectorRef) {}

  onChange = (tdFile: File | null) => {}

  onTouched = () => {}

  writeValue(tdFile: File | null) {
    this.tdFile = tdFile
  }

  registerOnChange(onChange: any) {
    this.onChange = onChange
  }

  registerOnTouched(onTouched: any) {
    this.onTouched = onTouched
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched()
      this.touched = true
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled
  }

  async onFileUpload(event: FileSelectEvent) {
    if (!this.disabled) {
      const files = event.files
      if (files.length > 0) {
        this.tdFile = files[0] ?? null
        this.content = await this.tdFile?.text()
        this.uploaded = true
        this.onChange(this.tdFile)
        this._snackBar.showInfo('TD File Loaded')
        this._cd.markForCheck()
      }
    }
  }

  clear() {
    this.tdFile = null
    this.uploaded = false
    this.over = false
    this.onChange(this.tdFile)
    this._cd.markForCheck()
  }
}
