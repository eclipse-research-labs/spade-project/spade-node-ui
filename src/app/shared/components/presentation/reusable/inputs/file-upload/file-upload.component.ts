/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewEncapsulation } from '@angular/core'
import { NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { FileSelectEvent, FileUploadModule } from 'primeng/fileupload'

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrl: './file-upload.component.scss',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: FileUploadComponent,
    },
  ],
  standalone: true,
  imports: [CommonModule, FileUploadModule, ReactiveFormsModule],
})
export class FileUploadComponent {
  file: File | null = null

  over: boolean = false

  touched = false

  disabled = false

  constructor(private _snackBar: SnackBarService, private _cd: ChangeDetectorRef) {}

  onChange = (file: File | null) => {}

  onTouched = () => {}

  writeValue(file: File | null) {
    this.file = file
  }

  registerOnChange(onChange: any) {
    this.onChange = onChange
  }

  registerOnTouched(onTouched: any) {
    this.onTouched = onTouched
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched()
      this.touched = true
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled
  }

  async onFileUpload(event: FileSelectEvent) {
    if (!this.disabled) {
      const files = Array.from(event.files)
      if (files.length > 0) {
        this.file = files.at(0) ?? null
        this.onChange(this.file)
        this._snackBar.showInfo('File Loaded')
        this._cd.markForCheck()
      }
    }
  }

  change() {
    var input = document.createElement('input')
    input.type = 'file'
    input.hidden = true
    input.onchange = (event) => {
      const files = (event.target as HTMLInputElement | undefined)?.files
      if (files && files.length > 0) {
        this.file = files[0]
        this.onChange(this.file)
        this._snackBar.showInfo('File Changed')
        this._cd.markForCheck()
      }
    }
    input.click()
    input.remove()
  }

  clear() {
    this.file = null
    this.over = false
    this.onChange(this.file)
    this._snackBar.showInfo('File Removed')
    this._cd.markForCheck()
  }
}
