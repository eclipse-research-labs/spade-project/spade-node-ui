/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { SideNavDivider, SideNavItem } from '@shared/models/side-nav.model'
import { SideNavSectionComponent } from './nav-section/nav-section.component'
import { SideNavButtonComponent } from './nav-button/nav-button.component'

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrl: './side-nav.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, SideNavSectionComponent, SideNavButtonComponent],
})
export class SideNavComponent {
  @Input() items: (SideNavItem | SideNavDivider)[] = []

  isItem(item: SideNavItem | SideNavDivider): SideNavItem | null {
    return 'label' in item ? null: item as SideNavItem
  }

  isDivider(item: SideNavItem | SideNavDivider): SideNavDivider | null {
    return 'label' in item ? item as SideNavDivider: null
  }
}
