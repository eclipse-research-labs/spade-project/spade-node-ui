/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core'
import { Router, RouterModule } from '@angular/router'

@Component({
  selector: 'app-nav-button',
  templateUrl: './nav-button.component.html',
  styleUrls: ['./nav-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, RouterModule],
})
export class SideNavButtonComponent implements OnInit {
  @Input() path: string = ''
  @Input() name: string | undefined
  @Input() icon: string | undefined
  @Input() symbol: string | undefined
  @Input() paddingLeft: string | undefined
  @Input() marginTop: string | undefined
  @Input() indicate: boolean = false

  constructor(private _router: Router) {}

  ngOnInit(): void {}

  navigate(): void {
    this._router.navigateByUrl(this.path)
  }

  get isActive(): boolean {
    return this._router.url.startsWith(this.path) || this._router.url.replaceAll(':', '%3A').startsWith(this.path)
  }
}
