/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-section',
  templateUrl: './nav-section.component.html',
  styleUrls: ['./nav-section.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule],
})
export class SideNavSectionComponent implements OnInit {
  @Input() path!: string
  @Input() name: string | undefined
  @Input() icon: string | undefined
  @Input() marginTop: string | undefined

  folded: boolean = true

  constructor(private _router: Router) {}

  ngOnInit(): void {
    this.folded = !this._router.url.startsWith(this.path)
  }
}
