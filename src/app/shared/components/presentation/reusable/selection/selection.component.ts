/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild,
} from '@angular/core'
import { delay, inflect } from 'src/app/utils'
import { SelectionSkeletonComponent } from '../loaders/selection-skeleton/selection-skeleton.component'
import { FormsModule } from '@angular/forms'
import { CheckboxModule } from 'primeng/checkbox'
import { InputTextModule } from 'primeng/inputtext'
import { VirtualScrollerSettings } from '@shared/models/selection.model'
import { CdkVirtualScrollViewport, ScrollingModule } from '@angular/cdk/scrolling'

@Component({
  selector: 'app-selection',
  templateUrl: './selection.component.html',
  styleUrl: './selection.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, FormsModule, SelectionSkeletonComponent, CheckboxModule, InputTextModule, ScrollingModule],
})
export class SelectionComponent<T> implements OnInit, OnChanges {
  @ContentChild('preview') preview: TemplateRef<any> | undefined
  @ContentChild('action') action: TemplateRef<any> | undefined
  @ContentChild('detail') detail: TemplateRef<any> | undefined
  @ContentChild('leading') leading: TemplateRef<any> | undefined
  @ContentChild('selectionAction') selectionAction: TemplateRef<any> | undefined
  @ContentChild('onError') onError: TemplateRef<any> | undefined
  @ContentChild('onEmpty') onEmpty: TemplateRef<any> | undefined

  @Input() items?: T[]
  @Input() search?: (value: string) => T[]
  @Input() defaultPicked?: T
  @Input() loading: boolean = false
  @Input() selectable: boolean = false
  @Input() error: string | null = null
  @Input() inflectCount?: (count: number) => string
  @Input() virtualScrollerSettings?: VirtualScrollerSettings

  @Output() onChange: EventEmitter<T> = new EventEmitter()
  @Output() onSelectionChange: EventEmitter<T[]> = new EventEmitter()

  @ViewChild('virtualScroller', { static: false }) virtualScroller?: CdkVirtualScrollViewport

  pickedItem?: T

  itemsToDisplay!: T[]
  selectedItems!: T[]
  length!: string

  searchText: string = ''
  allSelected = false

  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {
    this._init()
    delay(1).then(() => {
      this._scrollToDefaultPicked()
    })
  }

  ngOnInit(): void {
    this.selectedItems = []
    this._init()
  }

  private _init() {
    this.itemsToDisplay = this.items ?? []
    this.pickedItem = this.defaultPicked
    this.selectedItems = this.selectedItems?.filter((element) => this.items?.includes(element))
    this._calcLength()
  }

  private _scrollToDefaultPicked() {
    if (this.defaultPicked && this.items && this.virtualScroller) {
      const index = this.items.findIndex((element) => element === this.defaultPicked)
      if (index >= 0) {
        this.virtualScroller.scrollToIndex(index)
      }
    }
  }

  onSearchChange(value: string) {
    if (value.length > 2 && this.search) {
      const searchItems = this.search(value)
      if (this.itemsToDisplay.length != searchItems.length) {
        this._clearSelection()
      }
      this.itemsToDisplay = searchItems
      this._calcLength()
    } else {
      if (this.itemsToDisplay != this.items && this.items) {
        this.itemsToDisplay = this.items
        this._calcLength()
        this._clearSelection()
      }
    }
  }

  pickItem(item: any) {
    if (this.pickedItem !== item) {
      this.pickedItem = item
      this.onChange.emit(item)
    }
  }

  toggleAll() {
    if (this._allSelected) {
      this._clearSelection()
    } else {
      this.selectedItems = [...this.itemsToDisplay]
      this.allSelected = true
      this.onSelectionChange.emit(this.selectedItems)
    }
  }

  toggleOne() {
    if (this._allSelected && !this.allSelected) {
      this.allSelected = true
    }

    if (!this._allSelected && this.allSelected) {
      this.allSelected = false
    }
    this.onSelectionChange.emit(this.selectedItems)
  }

  private _clearSelection() {
    this.selectedItems = []
    this.allSelected = false
    this.onSelectionChange.emit([])
  }

  private _calcLength() {
    const n = this.itemsToDisplay.length
    if (this.inflectCount) {
      this.length = this.inflectCount(n)
    } else {
      this.length = inflect(n, '0 items', n + ' item', n + ' items')
    }
  }

  private get _allSelected() {
    return this.itemsToDisplay.every((element) => this.selectedItems!.includes(element))
  }

  get disabled() {
    return !this.items || this.items.length <= 0
  }
}
