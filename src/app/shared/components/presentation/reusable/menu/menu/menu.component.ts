/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { AfterContentChecked, AfterContentInit, ChangeDetectionStrategy, Component, ContentChild, ContentChildren, EventEmitter, Input, OnInit, Output, QueryList, TemplateRef } from '@angular/core';
import { Observable, startWith, delay, map } from 'rxjs';
import { MenuItemComponent } from '../menu-item/menu-item.component';
import { CommonModule } from '@angular/common';

type Type = 'horizontal' | 'vertical'

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule],
})
export class MenuComponent implements AfterContentInit, AfterContentChecked {

  @ContentChild('leading') leading: TemplateRef<any> | undefined
  @ContentChild('trailing') trailing: TemplateRef<any> | undefined

  @ContentChildren(MenuItemComponent)
  items?: QueryList<MenuItemComponent>;

  menuItems$?: Observable<MenuItemComponent[]>;

  activeItem?: MenuItemComponent;

  @Input() type: Type = 'horizontal'
  @Input() defaultActiveIndex: number = 0
  @Input() theme: 'light' | 'dark' = 'light'

  @Output() onChange: EventEmitter<number> = new EventEmitter();

  constructor() {}

  ngAfterContentInit(): void {
    if (this.items) {
      this.activeItem = this.items.get(this.defaultActiveIndex)
      this.menuItems$ = this.items.changes
      .pipe(startWith(""))
      .pipe(delay(0))
      .pipe(map(() => this.items!.toArray()));
      this.activeItem = this.items.get(this.defaultActiveIndex)
    }
    
  }

  ngAfterContentChecked() {
    if (!this.activeItem) {
      const first = this.items!.first
      if (first) {
        Promise.resolve().then(() => {
          this.activeItem = first;
        });
      }
    }
  }

  selectItem(menuItem: MenuItemComponent) {
    if (this.activeItem === menuItem) {
      return;
    }
    if (this.activeItem) {
      this.activeItem.isActive = false;
    }
    this.activeItem = menuItem;
    menuItem.isActive = true;
    if (this.items) {
      for (let i = 0; i < this.items?.length; i++) {
        if (this.items.get(i) === menuItem) {
          this.onChange.emit(i)
          return
        }
      }
    }
    
  }

  get light() {
    return this.theme == 'light'
  }
  
  get dark() {
    return this.theme == 'dark'
  }
}
