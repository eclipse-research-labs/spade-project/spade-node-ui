/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, ContentChild, Input, OnInit } from '@angular/core'
import { MenuContentComponent } from '../menu-content/menu-content.component'
import { MenuLabelComponent } from '../menu-label/menu-label.component'
import { CommonModule } from '@angular/common'

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule],
})
export class MenuItemComponent implements OnInit {
  @Input() label!: string

  @Input() isActive: boolean = false

  @ContentChild(MenuContentComponent) bodyComponent!: MenuContentComponent

  @ContentChild(MenuLabelComponent) labelComponent!: MenuLabelComponent

  constructor() {}

  ngOnInit(): void {}
}
