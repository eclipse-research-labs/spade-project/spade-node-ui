/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, OnInit, TemplateRef, ViewChild } from '@angular/core'

@Component({
  selector: 'app-menu-label',
  templateUrl: './menu-label.component.html',
  styleUrls: ['./menu-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule],
})
export class MenuLabelComponent implements OnInit {
  @ViewChild(TemplateRef)
  labelContent!: TemplateRef<any>

  constructor() {}

  ngOnInit(): void {}
}
