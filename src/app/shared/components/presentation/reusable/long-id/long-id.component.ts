/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CopyToClipboardDirective } from '@shared/directives/copy-to-clipboard.directive';

@Component({
  selector: 'app-long-id',
  templateUrl: './long-id.component.html',
  styleUrls: ['./long-id.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, CopyToClipboardDirective],
})
export class LongIdComponent {
  @Input() id!: string
  @Input() copyMessage?: string
  @Input() width?: number

  hidden: boolean = true

  constructor() { }

  ngOnInit(): void {
  }

  toggleVisibility(event: Event) {
    event.stopPropagation()
    this.hidden = !this.hidden
  }
}
