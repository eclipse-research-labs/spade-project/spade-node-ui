/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, ContentChild, Input, TemplateRef, ViewChild } from '@angular/core';
import { OverlayPanel, OverlayPanelModule } from 'primeng/overlaypanel';

@Component({
  selector: 'app-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, OverlayPanelModule],
})
export class TooltipComponent {

  @ContentChild('overlay') overlay: TemplateRef<any> | undefined

  @Input() width: number | undefined
  @Input() height: number | undefined
  @Input() top: number | undefined
  @Input() left: number | undefined

  @ViewChild('op') op!: OverlayPanel

  showOp(event: Event) {
    this.op.show(event)
  }

  hideOp(event: Event) {
    this.op.hide()
  }
}
