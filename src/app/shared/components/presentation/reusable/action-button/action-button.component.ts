/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, ContentChild, EventEmitter, Input, Output, TemplateRef } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { CheckboxModule } from 'primeng/checkbox'

@Component({
  selector: 'app-action-button',
  templateUrl: './action-button.component.html',
  styleUrl: './action-button.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, FormsModule, CheckboxModule],
})
export class ActionButtonComponent {
  @Input() title!: string
  @Input() icon?: string
  @Input() loading: boolean = false
  @Input() disabled: boolean = false
  @Input() selectable: boolean = false
  @Input() selected: boolean = false
  @Input() size: 'normal' | 'small' = 'normal'

  @ContentChild('description') description?: TemplateRef<any>

  @Output() onClick: EventEmitter<void> = new EventEmitter()

  buttonClicked() {
    if (!this.loading && !this.disabled) {
      this.onClick.emit()
    }
  }
}
