/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, ContentChild, EventEmitter, HostListener, Input, NgZone, Output, SimpleChanges, TemplateRef } from '@angular/core'
import { delay } from 'src/app/utils'

@Component({
  selector: 'app-overlay',
  templateUrl: './overlay.component.html',
  styleUrls: ['./overlay.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule],
})
export class OverlayComponent {
  @ContentChild('content') content: TemplateRef<any> | undefined

  @Input() visible: boolean = false

  @Output() visibleChange = new EventEmitter<boolean>()
  @Output() closingChange = new EventEmitter<boolean>()

  closing: boolean = false
  isOpen: boolean = false

  constructor(private _zone: NgZone) {}

  ngOnChanges(changes: SimpleChanges) {
    const visible = changes.visible.currentValue
    if ((this.isOpen && !visible) || (!this.isOpen && visible)) {
      this.isOpen = visible
      const appWrapper = document.getElementById('app-wrapper')
      if (appWrapper?.style) {
        const scrollbarWidth = window.innerWidth - document.body.clientWidth + 'px'
        appWrapper.style.overflow = visible ? 'hidden' : ''
        appWrapper.style.marginRight = visible ? scrollbarWidth : ''
      }
    }
  }

  async close(event: Event) {
    await this._zone.run(async () => {
      event.stopPropagation()
      this.closing = true
      this.closingChange.emit(this.closing)
      await delay(180)
      this.visible = false
      this.closing = false
      this.visibleChange.emit(this.visible)
      this.closingChange.emit(this.closing)
    })
  }

  @HostListener('document:keydown.escape', ['$event'])
  onKeydownHandler(event: KeyboardEvent) {
    this._zone.run(() => {
      this.close(event)
    })
  }
}
