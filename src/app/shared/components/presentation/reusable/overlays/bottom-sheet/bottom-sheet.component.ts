/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, ContentChild, EventEmitter, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { OverlayComponent } from '../overlay.component';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';


@Component({
  selector: 'app-bottom-sheet',
  templateUrl: './bottom-sheet.component.html',
  styleUrls: ['./bottom-sheet.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, OverlayComponent, ButtonModule],
})
export class BottomSheetComponent {

  @ContentChild('header') header: TemplateRef<any> | undefined
  
  @Input()  visible: boolean = false;
  @Output() visibleChange = new EventEmitter<boolean>();

  @ViewChild('ol') ol!: OverlayComponent

  closing: boolean = false

  onVisibleChange(value: boolean) {
    this.visible = value
    this.visibleChange.emit(this.visible)
  }

  onClosingChange(value: boolean) {
    this.closing = value
  }

  close(event: Event) {
    this.ol.close(event)
  }
}
