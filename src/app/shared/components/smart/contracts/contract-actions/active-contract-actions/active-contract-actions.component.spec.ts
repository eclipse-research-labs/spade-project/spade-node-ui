/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveContractActionsComponent } from './active-contract-actions.component';

describe('ActiveContractActionsComponent', () => {
  let component: ActiveContractActionsComponent;
  let fixture: ComponentFixture<ActiveContractActionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ActiveContractActionsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ActiveContractActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
