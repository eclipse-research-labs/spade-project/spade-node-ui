/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { CodeEditorComponent } from '@shared/components/presentation/reusable/inputs/code-editor/code-editor.component'
import { SpinnerComponent } from '@shared/components/presentation/reusable/loaders/spinner/spinner.component'
import { ButtonModule } from 'primeng/button'
import { MenuModule } from 'primeng/menu'
import { SidebarModule } from 'primeng/sidebar'
import { ActiveContractActionsService } from './active-contract-actions.service'
import { Contract } from '@core/models/contract.model'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { MenuItem } from 'primeng/api'
import { NgLetDirective } from 'ng-let'
import { isString } from 'src/app/utils'

@Component({
  selector: 'app-active-contract-actions',
  standalone: true,
  imports: [
    CommonModule,
    SpinnerComponent,
    ButtonModule,
    MenuModule,
    SidebarModule,
    CodeEditorComponent,
    FormsModule,
    NgLetDirective,
  ],
  templateUrl: './active-contract-actions.component.html',
  styleUrl: './active-contract-actions.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ActiveContractActionsService],
})
export class ActiveContractActionsComponent implements OnInit, OnChanges {
  @Input() contract!: Contract

  showContract = false

  contractJson: string = ''

  options: MenuItem[] | undefined = [
    {
      label: 'Details',
      items: [
        {
          label: 'Show Contract',
          icon: 'fa fa-code',
          command: (e) => {
            const event = e.originalEvent
            if (event) {
              this.showContract = true
            }
          },
        },
      ],
    },
    {
      label: 'Settings',
      items: [
        {
          label: 'Revoke',
          icon: 'fa fa-trash-can',
          command: (e) => {
            const event = e.originalEvent
            if (event) {
              this.rejectContract()
            }
          },
        },
      ],
    },
  ]

  constructor(private _service: ActiveContractActionsService, private _snackBar: SnackBarService) {}

  ngOnInit(): void {
    this._initContractJson()
  }

  ngOnChanges(changes: SimpleChanges): void {
    this._initContractJson()
  }

  private _initContractJson() {
    if (this.contract) {
      this.contractJson = JSON.stringify(
        { ...this.contract, target: this.target, assignee: this.assignee, assigner: this.assigner },
        null,
        2
      )
    }
  }

  get target() {
    return isString(this.contract.target) ? this.contract.target : this.contract.target?.oid
  }

  get assignee() {
    return isString(this.contract.assignee) ? this.contract.assignee : `urn:node:${this.contract.assignee?.nodeId}`
  }

  get assigner() {
    return isString(this.contract.assigner) ? this.contract.assigner : `urn:user:${this.contract.assigner?.uid}`
  }

  async rejectContract() {
    await this._service.rejectContract(this.contract)
    this._snackBar.showSuccess('Contract revoked')
  }

  get loading$() {
    return this._service.loading$
  }
}
