/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { ContractsApiService } from '@core/api/modules/contracts'
import { ContractStatus } from '@core/enums/contract.enum'
import { Contract } from '@core/models/contract.model'
import { BaseService } from '@core/services/base'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { firstValueFrom, take } from 'rxjs'

@Injectable({
  providedIn: 'root',
})
export class ActiveContractActionsService extends BaseService {
  
  constructor(private _myNodeService: MyNodeService, private _contractsApiService: ContractsApiService) {
    super()
  }

  async rejectContract(contract: Contract) {
    await this.loadApiWrapper(this._rejectContract(contract.policyId))
    contract.status = ContractStatus.REVOKED
    this._myNodeService.updateContracts(
      this._myNodeService.contracts.map((element) => (element.policyId == contract.policyId ? contract : element))
    )
  }

  private async _rejectContract(policyId: string) {
    return firstValueFrom(this._contractsApiService.rejectContract(this._myNodeService.sbUrl, policyId).pipe(take(1)))
  }
}
