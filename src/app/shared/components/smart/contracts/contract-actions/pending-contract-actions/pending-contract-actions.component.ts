/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { Contract } from '@core/models/contract.model'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { CodeEditorComponent } from '@shared/components/presentation/reusable/inputs/code-editor/code-editor.component'
import { SpinnerComponent } from '@shared/components/presentation/reusable/loaders/spinner/spinner.component'
import { NgLetDirective } from 'ng-let'
import { MenuItem } from 'primeng/api'
import { ButtonModule } from 'primeng/button'
import { MenuModule } from 'primeng/menu'
import { SidebarModule } from 'primeng/sidebar'
import { PendingContractActionsService } from './pending-contract-actions.service'
import { isString } from 'src/app/utils'

@Component({
  selector: 'app-pending-contract-actions',
  standalone: true,
  imports: [
    CommonModule,
    SpinnerComponent,
    ButtonModule,
    MenuModule,
    SidebarModule,
    CodeEditorComponent,
    FormsModule,
    NgLetDirective,
  ],
  templateUrl: './pending-contract-actions.component.html',
  styleUrl: './pending-contract-actions.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [PendingContractActionsService],
})
export class PendingContractActionsComponent implements OnInit, OnChanges {
  @Input() contract!: Contract

  showContract = false

  contractJson: string = ''

  options: MenuItem[] | undefined = [
    {
      label: 'Details',
      items: [
        {
          label: 'Show Contract',
          icon: 'fa fa-code',
          command: (e) => {
            const event = e.originalEvent
            if (event) {
              this.showContract = true
            }
          },
        },
      ],
    },
    {
      label: 'Settings',
      items: [
        {
          label: 'Revoke',
          icon: 'fa fa-trash-can',
          command: (e) => {
            const event = e.originalEvent
            if (event) {
              this.rejectContract()
            }
          },
        },
      ],
    },
  ]

  constructor(
    private _service: PendingContractActionsService,
    private _snackBar: SnackBarService,
    private _myNodeService: MyNodeService
  ) {}

  ngOnInit(): void {
    this._initContractJson()
  }

  ngOnChanges(changes: SimpleChanges): void {
    this._initContractJson()
  }

  private _initContractJson() {
    if (this.contract) {
      this.contractJson = JSON.stringify(
        { ...this.contract, target: this.target, assignee: this.assignee, assigner: this.assigner },
        null,
        2
      )
    }
  }

  get target() {
    return isString(this.contract.target) ? this.contract.target : this.contract.target?.oid
  }

  get assignee() {
    return isString(this.contract.assignee) ? this.contract.assignee : `urn:node:${this.contract.assignee?.nodeId}`
  }

  get assigner() {
    return isString(this.contract.assigner) ? this.contract.assigner : `urn:user:${this.contract.assigner?.uid}`
  }

  async acceptContract() {
    await this._service.acceptContract(this.contract)
    this._snackBar.showSuccess('Contract accepted')
  }

  async rejectContract() {
    await this._service.rejectContract(this.contract)
    this._snackBar.showSuccess('Contract rejected')
  }

  async deleteContract() {
    await this._service.deleteContract(this.contract)
    this._snackBar.showSuccess('Contract deleted')
  }

  get isExigentContract() {
    return this._myNodeService.isExigentContract(this.contract)
  }

  get loading$() {
    return this._service.loading$
  }
}
