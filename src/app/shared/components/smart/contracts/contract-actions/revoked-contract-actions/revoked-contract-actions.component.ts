/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { Contract } from '@core/models/contract.model'
import { CodeEditorComponent } from '@shared/components/presentation/reusable/inputs/code-editor/code-editor.component'
import { MenuItem } from 'primeng/api'
import { ButtonModule } from 'primeng/button'
import { MenuModule } from 'primeng/menu'
import { SidebarModule } from 'primeng/sidebar'
import { isString } from 'src/app/utils'

@Component({
  selector: 'app-revoked-contract-actions',
  standalone: true,
  imports: [CommonModule, ButtonModule, SidebarModule, MenuModule, CodeEditorComponent, FormsModule],
  templateUrl: './revoked-contract-actions.component.html',
  styleUrl: './revoked-contract-actions.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RevokedContractActionsComponent implements OnInit, OnChanges {
  @Input() contract!: Contract

  showContract = false

  contractJson: string = ''

  options: MenuItem[] | undefined = [
    {
      label: 'Details',
      items: [
        {
          label: 'Show Contract',
          icon: 'fa fa-code',
          command: (e) => {
            const event = e.originalEvent
            if (event) {
              this.showContract = true
            }
          },
        },
      ],
    },
  ]

  ngOnInit(): void {
    this._initContractJson()
  }

  ngOnChanges(changes: SimpleChanges): void {
    this._initContractJson()
  }

  private _initContractJson() {
    if (this.contract) {
      this.contractJson = JSON.stringify(
        { ...this.contract, target: this.target, assignee: this.assignee, assigner: this.assigner },
        null,
        2
      )
    }
  }

  get target() {
    return isString(this.contract.target) ? this.contract.target : this.contract.target?.oid
  }

  get assignee() {
    return isString(this.contract.assignee) ? this.contract.assignee : `urn:node:${this.contract.assignee?.nodeId}`
  }

  get assigner() {
    return isString(this.contract.assigner) ? this.contract.assigner : `urn:user:${this.contract.assigner?.uid}`
  }
}
