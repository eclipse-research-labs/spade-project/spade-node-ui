/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild,
} from '@angular/core'
import { UserRole } from '@core/enums/user.enum'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { ContractPreviewComponent } from '@shared/components/presentation/app-specific/contract/contract-preview/contract-preview.component'
import { SadFaceComponent } from '@shared/components/presentation/reusable/sad-face/sad-face.component'
import { NgLetDirective } from 'ng-let'
import { TableModule } from 'primeng/table'
import { TagModule } from 'primeng/tag'
import { RoleCheckerComponent } from '../../role-checker/role-checker.component'
import { ContractStatusComponent } from '../contract-status/contract-status.component'
import { CheckboxModule } from 'primeng/checkbox'
import { FormsModule } from '@angular/forms'
import { CdkVirtualScrollViewport, ScrollingModule } from '@angular/cdk/scrolling'
import { ContractIdentity } from '@core/models/contract.model'
import { ContractIdentityStatus } from '@core/enums/contract.enum'

const DOWNLOAD_SIZE = 20

@Component({
  selector: 'app-contracts-list',
  standalone: true,
  imports: [
    CommonModule,
    RoleCheckerComponent,
    ContractPreviewComponent,
    ContractStatusComponent,
    SadFaceComponent,
    TableModule,
    NgLetDirective,
    TagModule,
    CheckboxModule,
    FormsModule,
    ScrollingModule,
  ],
  templateUrl: './contracts-list.component.html',
  styleUrl: './contracts-list.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContractsListComponent implements OnInit, AfterViewInit {
  @Input() contractIdentities!: ContractIdentity[]
  @Input() emptyMessage!: string
  @Input() selectable: boolean = false
  @Input() selection?: ContractIdentity[]

  @ContentChild('options') options?: TemplateRef<any>
  @ContentChild('selectionAction') selectionAction?: TemplateRef<any>

  @ViewChild('virtualScroller') virtualScroller?: CdkVirtualScrollViewport

  @Output() onSelectionChange: EventEmitter<ContractIdentity[]> = new EventEmitter()
  @Output() onDownloadChunkOfIdentities: EventEmitter<ContractIdentity[]> = new EventEmitter()

  selectedItems!: string[]
  allSelected = false

  itemSize = 190.75

  private _scrollIndex = 0

  ngOnInit(): void {
    this.selectedItems = this.selection?.map((element) => element.contract.policyId) ?? []
    this._initContracts()
    this.allSelected = this._allSelected
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.selectedItems = this.selection?.map((element) => element.contract.policyId) ?? []
    this._initContracts()
    this.allSelected = this._allSelected
  }

  ngAfterViewInit(): void {
    this._listenForScrollEnd()
  }

  constructor(private _myNodeService: MyNodeService) {}

  toggleAll() {
    if (this._allSelected) {
      this._clearSelection()
    } else {
      this.allSelected = true
      this.selectedItems = this.contractIdentities.map((element) => element.contract.policyId)
      this.onSelectionChange.emit(this.contractIdentities)
    }
  }

  toggleOne() {
    if (this._allSelected && !this.allSelected) {
      this.allSelected = true
    }

    if (!this._allSelected && this.allSelected) {
      this.allSelected = false
    }
    this.onSelectionChange.emit(
      this.contractIdentities.filter((element) =>
        this.selectedItems.some((policyId) => element.contract.policyId == policyId)
      )
    )
  }
  scrollhandler(index: number) {
    this._scrollIndex = index
  }

  loadingIdentity(status?: ContractIdentityStatus) {
    return (
      status == undefined ||
      status == null ||
      status == ContractIdentityStatus.DOWNLOADING ||
      status == ContractIdentityStatus.READY
    )
  }

  private _listenForScrollEnd() {
    if (this.virtualScroller) {
      this.virtualScroller.elementRef.nativeElement.onscrollend = (event) => {
        this._downloadChunkOfIdentities(this._scrollIndex)
      }
    }
  }

  private _clearSelection() {
    this.selectedItems = []
    this.allSelected = false
    this.onSelectionChange.emit([])
  }

  private get _allSelected() {
    return this.contractIdentities.length <= 0
      ? false
      : this.contractIdentities.every((element) =>
          this.selectedItems.some((policyId) => policyId == element.contract.policyId)
        )
  }

  private _initContracts() {
    const initialDownload = this.contractIdentities.slice(0, DOWNLOAD_SIZE)
    this.onDownloadChunkOfIdentities.emit(initialDownload)
    this._downloadChunkOfIdentities(0)
  }

  private _downloadChunkOfIdentities(indexFrom: number) {
    const initialDownload = this.contractIdentities.slice(indexFrom, indexFrom + DOWNLOAD_SIZE)
    this.onDownloadChunkOfIdentities.emit(initialDownload)
  }

  get containerHeight() {
    return this.contractIdentities.length * this.itemSize
  }

  get url() {
    return `/detail/contracts/my-node-contracts/${this.nodeId}`
  }

  get nodeId() {
    return this._myNodeService.node!.nodeId
  }

  get admin() {
    return UserRole.ADMIN
  }

  get nodeOperator() {
    return UserRole.NODE_OPERATOR
  }
}
