/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core'
import { ContractStatus } from '@core/enums/contract.enum'
import { UserRole } from '@core/enums/user.enum'
import { Contract } from '@core/models/contract.model'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { RoleCheckerComponent } from '../../role-checker/role-checker.component'
import { InfoComponent } from '@shared/components/presentation/reusable/info/info.component'
import { TagModule } from 'primeng/tag'

@Component({
  selector: 'app-contract-status',
  templateUrl: './contract-status.component.html',
  styleUrl: './contract-status.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, RoleCheckerComponent, InfoComponent, TagModule],
})
export class ContractStatusComponent implements OnInit, OnChanges {
  @Input() contract!: Contract

  severity!: 'success' | 'info' | 'warning' | 'danger'
  status!: 'ACTIVE' | 'REVOKED' | 'PENDING YOUR SIGNATURE' | 'PENDING' | 'WAITING_ON_ASSIGNEE' | 'WAITING_ON_ASSIGNER'
  isExigentContract!: boolean

  constructor(private _myNodeService: MyNodeService) {}

  ngOnChanges(changes: SimpleChanges): void {
    this._init()
  }

  ngOnInit(): void {
    this._init()
  }

  private _init() {
    this.isExigentContract = this._isExigentContract()
    this.severity = this._severity()
    this.status = this._status()
  }

  private _isExigentContract() {
    return this._myNodeService.isExigentContract(this.contract)
  }

  private _severity() {
    switch (this.contract.status) {
      case ContractStatus.ACTIVE:
        return 'success'
      case ContractStatus.REVOKED:
        return 'danger'
      case ContractStatus.WAITING_ON_ASSIGNEE:
        return 'warning'
      case ContractStatus.WAITING_ON_ASSIGNER:
        return 'warning'
      default:
        return 'info'
    }
  }

  private _status() {
    switch (this.contract.status) {
      case ContractStatus.ACTIVE:
        return 'ACTIVE'
      case ContractStatus.REVOKED:
        return 'REVOKED'
      case ContractStatus.WAITING_ON_ASSIGNEE:
        return this.isExigentContract ? 'PENDING YOUR SIGNATURE': 'PENDING'
      case ContractStatus.WAITING_ON_ASSIGNER:
        return this.isExigentContract ? 'PENDING YOUR SIGNATURE': 'PENDING'
      default:
        return this.contract.status
    }
  }

  get admin() {
    return UserRole.ADMIN
  }

  get nodeOperator() {
    return UserRole.NODE_OPERATOR
  }
}
