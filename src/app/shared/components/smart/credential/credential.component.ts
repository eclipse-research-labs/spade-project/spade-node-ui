/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog'
import { FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms'
import { CredentialService } from './credential.service'
import { SecretComponent } from './secret/secret.component'
import { Credential } from '@core/models/credentials.model'
import { CommonModule } from '@angular/common'
import { ButtonModule } from 'primeng/button'
import { InputNumberModule } from 'primeng/inputnumber'
import { RadioButtonModule } from 'primeng/radiobutton'
import { MultiSelectModule } from 'primeng/multiselect';
import { NgLetDirective } from 'ng-let'

interface _Range {
  name: string
  key: number
}

@Component({
  selector: 'app-credential',
  templateUrl: './credential.component.html',
  styleUrl: './credential.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, ButtonModule, MultiSelectModule, InputNumberModule, RadioButtonModule, NgLetDirective],
  providers: [CredentialService],
})
export class CredentialComponent {
  permissions = ['ADAPTER', 'SERVICE', 'LOCAL_EVENT_LISTENER']
  ranges: _Range[] = [
    { name: 'Week', key: 604800 },
    { name: 'Month', key: 2629746 },
    { name: 'Year', key: 31556952 },
    // { name: 'Custom', key: -1 },
  ]

  credential?: Credential

  formGroup!: FormGroup

  ref: DynamicDialogRef | undefined
  secretRef: DynamicDialogRef | undefined

  useAuthHeader: boolean = false

  constructor(private _ref: DynamicDialogRef, private _dialogService: DialogService, private _service: CredentialService, private _config: DynamicDialogConfig) {
    this.credential = _config.data?.credential
    this.useAuthHeader = _config.data?.useAuthHeader ?? false
  }

  ngOnInit() {
    this.formGroup = new FormGroup({
      selectedPermissions: new FormControl(this.credential?.permissions, { validators: [Validators.required], updateOn: 'change' }),
      selectedRange: new FormControl(this.ranges[0], { validators: [Validators.required], updateOn: 'change' }),
      customRange: new FormControl(1, { updateOn: 'change' }),
    })
  }

  async generate() {
    if (this.formGroup.valid) {
      const selectedPermissions = this.formGroup.controls.selectedPermissions.value
      const selectedRange = this.formGroup.controls.selectedRange.value
      const customRange = this.formGroup.controls.customRange.value * 60 * 60 * 24
      const ttl = selectedRange.name == 'Custom' ? customRange : selectedRange.key
      if (this.credential) {
        await this._service.renewOldCredential(this.credential, ttl)
      } else {
        await this._service.createNewCredential(selectedPermissions, ttl)
      }
      const secret = this._service.secret
      this.secretRef = this._dialogService.open(SecretComponent, {
        header: 'Generated Credential',
        width: '600px',
        data: {
          secret,
          useAuthHeader: this.useAuthHeader
        },
      })
      this.secretRef.onClose.subscribe((data) => {
        this._ref.close(data)
      })
    }
  }

  get loading$() {
    return this._service.loading$
  }
}
