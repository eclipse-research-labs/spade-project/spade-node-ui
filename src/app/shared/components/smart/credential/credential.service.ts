/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { CredentialsApiService } from '@core/api/modules/credentials'
import { Secret, Credential } from '@core/models/credentials.model'
import { BaseService } from '@core/services/base'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { UserService } from '@core/services/user/user.service'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'
import { removeLocalTimezone } from 'src/app/utils'

@Injectable()
export class CredentialService extends BaseService {
  private _secret = new BehaviorSubject<Secret | null>(null)

  secret$ = this._secret.asObservable()

  constructor(private _credentialApiService: CredentialsApiService, private _myNodeService: MyNodeService, private _userService: UserService) {
    super()
  }

  async createNewCredential(permissions: string[], ttl: number) {
    const secret = await this.loadApiWrapper(this._createNewCredential(permissions, ttl))
    const clientid = secret.clientId
    const uid = this._userService.user?.uid
    if (uid && clientid) {
      const credential: Credential = {
        uid,
        clientid,
        ttl,
        permissions,
        created: removeLocalTimezone(new Date()).toUTCString(),
      }
      this._myNodeService.updateCredentials([...this._myNodeService.credentials, credential])
    }
    this._secret.next(secret)
  }

  async renewOldCredential(credential: Credential, ttl: number) {
    const secret = await this.loadApiWrapper(this._renewOldCredential(credential, ttl))
    const clientid = secret.clientId
    const candidate = this._myNodeService.credentials.find((element) => element.clientid == clientid)
    if (candidate) {
      candidate.ttl = ttl
      candidate.created = removeLocalTimezone(new Date()).toUTCString()
      const credential: Credential = {
        uid: candidate.uid,
        clientid: candidate.clientid,
        ttl,
        permissions: candidate.permissions,
        created: removeLocalTimezone(new Date()).toUTCString(),
      }
      this._myNodeService.updateCredentials(this._myNodeService.credentials.map((element) => (element.clientid == clientid ? credential : element)))
    }
    this._secret.next(secret)
  }

  private async _createNewCredential(permissions: string[], ttl: number) {
    const sbUrl = this._myNodeService.sbUrl
    return await firstValueFrom(this._credentialApiService.createCredential(sbUrl, permissions, ttl).pipe(take(1)))
  }

  private async _renewOldCredential(credential: Credential, ttl: number) {
    const sbUrl = this._myNodeService.sbUrl
    return await firstValueFrom(this._credentialApiService.renewCredential(sbUrl, credential.clientid, ttl).pipe(take(1)))
  }

  get secret() {
    return this._secret.value
  }
}
