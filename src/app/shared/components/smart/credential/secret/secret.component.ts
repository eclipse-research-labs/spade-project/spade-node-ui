/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component } from '@angular/core'
import { Secret } from '@core/models/credentials.model'
import { LongIdComponent } from '@shared/components/presentation/reusable/long-id/long-id.component'
import { CopyToClipboardDirective } from '@shared/directives/copy-to-clipboard.directive'
import { ButtonModule } from 'primeng/button'
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog'

@Component({
  selector: 'app-secret',
  templateUrl: './secret.component.html',
  styleUrl: './secret.component.scss',
  standalone: true,
  imports: [CommonModule, LongIdComponent, CopyToClipboardDirective, ButtonModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SecretComponent {
  secret!: Secret
  authorizationHeader!: string
  useAuthHeader: boolean = false

  constructor(private _config: DynamicDialogConfig, private _ref: DynamicDialogRef) {
    this.secret = _config.data.secret
    this.useAuthHeader = _config.data.useAuthHeader
    this.authorizationHeader = `BASIC ${btoa(`${this.secret.clientId}:${this.secret.secret}`)}`
  }

  useCredentials() {
    this._ref.close(this.authorizationHeader)
  }
}
