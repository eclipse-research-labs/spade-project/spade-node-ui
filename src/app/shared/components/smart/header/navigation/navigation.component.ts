/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MyNodeService } from '@core/services/my-node/my-node.service';
import { HeaderNavButtonComponent } from './header-nav-button/header-nav-button.component';
import { NgLetDirective } from 'ng-let';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrl: './navigation.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, HeaderNavButtonComponent, NgLetDirective],
})
export class NavigationComponent {

  constructor(private _myNodeService: MyNodeService) {}

  get node$() {
    return this._myNodeService.node$
  }

}
