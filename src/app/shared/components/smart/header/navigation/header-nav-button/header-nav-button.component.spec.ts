/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderNavButtonComponent } from './header-nav-button.component';

describe('HeaderNavButtonComponent', () => {
  let component: HeaderNavButtonComponent;
  let fixture: ComponentFixture<HeaderNavButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderNavButtonComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(HeaderNavButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
