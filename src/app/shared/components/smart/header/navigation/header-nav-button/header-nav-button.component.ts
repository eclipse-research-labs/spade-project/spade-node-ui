/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, ContentChild, Input, TemplateRef } from '@angular/core'
import { Router } from '@angular/router'

@Component({
  selector: 'app-header-nav-button',
  templateUrl: './header-nav-button.component.html',
  styleUrl: './header-nav-button.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule],
})
export class HeaderNavButtonComponent {
  @ContentChild('icon') icon!: TemplateRef<any>

  @Input() label!: string
  @Input() link!: string
  @Input() activeUrls?: string[]

  constructor(private _router: Router) {}

  navigate() {
    this._router.navigateByUrl(this.link)
  }

  get active() {
    if (this.activeUrls) {
      for (const url of this.activeUrls) {
        if (this._router.url.startsWith(url)) {
          return true
        }
      }
    }
    return this._router.url.startsWith(this.link)
  }
}
