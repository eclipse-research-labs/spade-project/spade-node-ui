/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { UserService } from '@core/services/user/user.service'
import { LogoComponent } from '@shared/components/presentation/app-specific/logo/logo.component'
import { NavigationComponent } from './navigation/navigation.component'
import { UserProfileComponent } from '../user-profile/user-profile.component'
import { NotificationsComponent } from '../notifications/notifications.component'
import { NgLetDirective } from 'ng-let'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, LogoComponent, NavigationComponent, NotificationsComponent, UserProfileComponent, NgLetDirective],
})
export class HeaderComponent implements OnInit {
  constructor(private _userService: UserService) {}

  ngOnInit(): void {}

  get user$() {
    return this._userService.user$
  }

  get isLoggedIn() {
    return this._userService.isLoggedIn
  }
}
