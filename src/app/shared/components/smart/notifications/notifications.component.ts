/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core'
import { NotificationService } from '@core/services/notification/notification.service'
import { TopPanelComponent } from '@shared/components/presentation/reusable/overlays/top-panel/top-panel.component'
import { SadFaceComponent } from '@shared/components/presentation/reusable/sad-face/sad-face.component'
import { NotificationComponent } from './notification/notification.component'
import { ButtonModule } from 'primeng/button'
import { SkeletonModule } from 'primeng/skeleton'
import { MessageModule } from 'primeng/message'
import { NgLetDirective } from 'ng-let'
import { MessageService } from 'primeng/api'

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrl: './notifications.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, TopPanelComponent, NotificationComponent, SadFaceComponent, ButtonModule, SkeletonModule, MessageModule, NgLetDirective],
})
export class NotificationsComponent {
  visible: boolean = false

  @ViewChild('panel') panel!: TopPanelComponent

  constructor(private _notificationService: NotificationService, private _messageService: MessageService) {}

  close(event: Event) {
    this.panel.close(event)
  }

  openPanel() {
    this.visible = true
    this._messageService.clear('notification')
  }

  closePanel() {
    this.visible = false
  }

  onTryAgain() {
    this._notificationService.initNotifications()
  }

  markAllSeen() {
    this._notificationService.markAllSeen()
  }

  get indicate() {
    return this._notificationService.hasNewNotifications
  }

  get notifications$() {
    return this._notificationService.notifications$
  }

  get newNotifications$() {
    return this._notificationService.newNotifications$
  }

  get initializing$() {
    return this._notificationService.initializing$
  }

  get errorInitializing$() {
    return this._notificationService.errorInitializing$
  }
}
