/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { Notification, NewPartnershipNotification, AcceptPartnershipNotification, HumanReadableNotification } from '@core/models/notification.model'
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog'
import { PartnershipComponent } from '../../partnership/partnership.component'
import { NotificationService } from '@core/services/notification/notification.service'
import { NotificationType } from '@core/enums/notification.enum'
import { CommonModule } from '@angular/common'
import { NgLetDirective } from 'ng-let'

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrl: './notification.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DialogService],
  standalone: true,
  imports: [CommonModule, NgLetDirective],
})
export class NotificationComponent implements OnInit {
  @Input() notification!: Notification
  @Output() onSelect = new EventEmitter<Event>()

  parsedNotification!: HumanReadableNotification

  ngOnInit(): void {
    this._parseNotification()
  }

  ref: DynamicDialogRef | undefined

  constructor(private _dialogService: DialogService, private _notificationService: NotificationService) {}

  select(event: MouseEvent) {
    switch (this.notification.type) {
      case NotificationType.NEW_PARTNERSHIP_REQUEST:
        this._newPartnershipRequest(event)
        return
      case NotificationType.ACCEPT_PARTNERSHIP_REQUEST:
        this._acceptPartnershipRequest(event)
        return
      default:
        return
    }
  }

  markSeen() {
    this._notificationService.markSeen(this.notification)
  }

  isNewNotification() {
    return this._notificationService.isNewNotification(this.notification)
  }

  private _newPartnershipRequest(event: MouseEvent) {
    const payload = JSON.parse(this.notification.payload!) as NewPartnershipNotification
    const from = payload.from
    this.ref = this._dialogService.open(PartnershipComponent, {
      header: 'Partnership',
      data: {
        cid: from.cid,
      },
    })
    this.markSeen()
    this.onSelect.emit(event)
  }

  private _acceptPartnershipRequest(event: MouseEvent) {
    const payload = JSON.parse(this.notification.payload!) as AcceptPartnershipNotification
    const by = payload.by
    this.ref = this._dialogService.open(PartnershipComponent, {
      header: 'Partnership',
      data: {
        cid: by.cid,
      },
    })
    this.markSeen()
    this.onSelect.emit(event)
  }

  private _parseNotification() {
    let type: string
    let description: string
    let payload

    switch (this.notification.type) {
      case NotificationType.NEW_PARTNERSHIP_REQUEST:
        type = 'New Partnership Request'
        break
      case NotificationType.ACCEPT_PARTNERSHIP_REQUEST:
        type = 'Partnership Request Accepted'
        break
      default:
        type = 'Unknown Notification Type'
    }

    switch (this.notification.type) {
      case NotificationType.NEW_PARTNERSHIP_REQUEST:
        payload = JSON.parse(this.notification.payload!) as NewPartnershipNotification
        description = 'from: ' + payload.from.name
        break
      case NotificationType.ACCEPT_PARTNERSHIP_REQUEST:
        payload = JSON.parse(this.notification.payload!) as AcceptPartnershipNotification
        description = 'by: ' + payload.by.name
        break
      default:
        description = 'No detail'
    }

    this.parsedNotification = { type, description }
  }

  get newNotifications$() {
    return this._notificationService.newNotifications$
  }
}
