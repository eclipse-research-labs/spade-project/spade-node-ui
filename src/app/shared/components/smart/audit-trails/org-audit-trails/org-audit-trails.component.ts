/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Status } from '@core/models/audit-trail.model'
import { AuditTrailsService } from '@core/services/audit-trails/audit-trails.service'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { NgLetDirective } from 'ng-let'
import { ButtonModule } from 'primeng/button'
import { AuditTrailsTableComponent } from '../audit-trails-table/audit-trails-table.component'

@Component({
  selector: 'app-org-audit-trails',
  standalone: true,
  imports: [CommonModule, ButtonModule, AuditTrailsTableComponent, NgLetDirective],
  templateUrl: './org-audit-trails.component.html',
  styleUrl: './org-audit-trails.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrgAuditTrailsComponent implements OnInit {
  private _status?: Status

  constructor(
    private _auditTrailsService: AuditTrailsService,
    private _myNodeService: MyNodeService,
    private _router: Router
  ) {}

  async ngOnInit(): Promise<void> {
    await this._auditTrailsService.init(0, Status.ALL, true)
  }

  showMore() {
    this._router.navigateByUrl(`${this.url}/${this._status ?? 'ALL'}`)
  }

  async onStatusChange(status: Status) {
    this._status = status
    await this._auditTrailsService.init(0, this._status, true)
  }

  async onRefresh() {
    await this._auditTrailsService.init(0, this._status, true)
  }

  get url() {
    return `/detail/audit-trails/org`
  }

  get nodeState() {
    return this._myNodeService.nodeState
  }

  get orgAuditTrails$() {
    return this._auditTrailsService.orgAuditTrails$
  }

  get initializing$() {
    return this._auditTrailsService.initializing$
  }

  get loading$() {
    return this._auditTrailsService.loading$
  }
}
