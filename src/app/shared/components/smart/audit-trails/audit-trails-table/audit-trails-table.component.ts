/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { AuditTrail, Purpose, Status } from '@core/models/audit-trail.model'
import { AuditTrailsService } from '@core/services/audit-trails/audit-trails.service'
import { TableSkeletonComponent } from '@shared/components/presentation/reusable/loaders/table-skeleton/table-skeleton.component'
import { LongIdComponent } from '@shared/components/presentation/reusable/long-id/long-id.component'
import { SadFaceComponent } from '@shared/components/presentation/reusable/sad-face/sad-face.component'
import { ApplyLocalTzPipe } from '@shared/pipes/apply-local-tz.pipe'
import { NgLetDirective } from 'ng-let'
import { ButtonModule } from 'primeng/button'
import { SkeletonModule } from 'primeng/skeleton'
import { TableModule, TablePageEvent } from 'primeng/table'

@Component({
  selector: 'app-audit-trails-table',
  templateUrl: './audit-trails-table.component.html',
  styleUrl: './audit-trails-table.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    CommonModule,
    TableSkeletonComponent,
    LongIdComponent,
    SadFaceComponent,
    ApplyLocalTzPipe,
    ButtonModule,
    TableModule,
    SkeletonModule,
    NgLetDirective,
  ],
})
export class AuditTrailsTableComponent implements OnInit {
  @Input() compact = false
  @Input() defaultSelectedStatus?: Status
  @Input() auditTrails: AuditTrail[] = []
  @Output() onStatusChange = new EventEmitter<Status>()
  @Output() onPageChange = new EventEmitter<number>()
  @Output() onRefresh = new EventEmitter()

  filterOptions!: Status[]
  selectedStatus!: Status

  totalRecords = 10000

  constructor(private _auditTrailsService: AuditTrailsService) {}

  ngOnInit(): void {
    this.filterOptions = [Status.ALL, Status.ERROR, Status.SUCCESS, Status.REJECTED]
    this.selectedStatus = this.selectedStatus ?? this.defaultSelectedStatus ?? Status.ALL
  }

  isMissing(value: string | undefined | null) {
    return !value || value.length <= 0 || value == 'UNDEFINED' || value == 'NOT_SET' || value == 'ANONYMOUS'
  }

  statusIcon(status: Status) {
    switch (status) {
      case Status.SUCCESS:
        return 'fa-solid fa-circle-check'
      case Status.REJECTED:
        return 'fa-solid fa-circle-xmark'
      case Status.ERROR:
        return 'fa-solid fa-circle-exclamation'
      default:
        return 'fa-solid fa-circle'
    }
  }

  statusColor(status: Status) {
    switch (status) {
      case Status.SUCCESS:
        return '#277d26'
      case Status.REJECTED:
        return '#f2b619'
      case Status.ERROR:
        return '#ff3a3a'
      default:
        return '#a4a4a4'
    }
  }

  purposeIcon(purpose: Purpose) {
    switch (purpose) {
      case Purpose.ADMIN:
        return 'fa-solid fa-user-gear'
      case Purpose.CONTRACT:
        return 'fa-solid fa-file-signature'
      case Purpose.REGISTRY:
        return 'fa-solid fa-cube'
      case Purpose.DISCOVERY:
        return 'fa-solid fa-compass'
      case Purpose.MANAGEMENT:
        return 'fa-solid fa-gear'
      case Purpose.CONSUMPTION:
        return 'fa-solid fa-database'
      case Purpose.ORGANISATION:
        return 'fa-solid fa-house-user'
      case Purpose.INVITATION:
        return 'fa-solid fa-envelope'
      case Purpose.USER:
        return 'fa-solid fa-user'
      case Purpose.NODE:
        return 'fa-solid fa-circle-nodes'
      case Purpose.PARTNERSHIP:
        return 'fa-solid fa-handshake'
      default:
        return 'fa-solid fa-circle'
    }
  }

  async refresh() {
    this.onRefresh.emit()
  }

  onPage(event: TablePageEvent) {
    if (event) {
      const first = event.first
      const page = first / 25
      this.onPageChange.emit(page)
    }
  }

  async toggleFilter(status: Status) {
    if (this.selectedStatus == status) return
    this.selectedStatus = status
    this.onStatusChange.emit(this.selectedStatus)
  }

  get initializing$() {
    return this._auditTrailsService.initializing$
  }

  get loading$() {
    return this._auditTrailsService.loading$
  }
}
