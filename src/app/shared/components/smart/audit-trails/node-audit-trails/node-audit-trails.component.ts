/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Status } from '@core/models/audit-trail.model'
import { AuditTrailsService } from '@core/services/audit-trails/audit-trails.service'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { AuditTrailsTableComponent } from '../audit-trails-table/audit-trails-table.component'
import { ButtonModule } from 'primeng/button'
import { NgLetDirective } from 'ng-let'

@Component({
  selector: 'app-node-audit-trails',
  templateUrl: './node-audit-trails.component.html',
  styleUrl: './node-audit-trails.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, AuditTrailsTableComponent, ButtonModule, NgLetDirective],
})
export class NodeAuditTrailsComponent implements OnInit {
  private _status?: Status

  constructor(
    private _auditTrailsService: AuditTrailsService,
    private _myNodeService: MyNodeService,
    private _router: Router
  ) {}

  async ngOnInit(): Promise<void> {
    await this._auditTrailsService.init(0, Status.ALL, true)
  }

  showMore() {
    this._router.navigateByUrl(`${this.url}/${this._status ?? 'ALL'}`)
  }

  async onStatusChange(status: Status) {
    this._status = status
    await this._auditTrailsService.init(0, this._status, true)
  }

  async onRefresh() {
    await this._auditTrailsService.init(0, this._status, true)
  }

  get url() {
    return `/detail/audit-trails/node/${this.nodeState}`
  }

  get nodeState() {
    return this._myNodeService.nodeState
  }

  get nodeAuditTrails$() {
    return this._auditTrailsService.nodeAuditTrails$
  }

  get initializing$() {
    return this._auditTrailsService.initializing$
  }

  get loading$() {
    return this._auditTrailsService.loading$
  }
}
