/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component } from '@angular/core'
import { Node } from '@core/models/node.model'
import { User } from '@core/models/user.model'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { UserService } from '@core/services/user/user.service'
import { Observable } from 'rxjs'
import { prependHttps } from 'src/app/utils'
import { ButtonModule } from 'primeng/button'
import { CopyToClipboardDirective } from '@shared/directives/copy-to-clipboard.directive'
import { ApplyLocalTzPipe } from '@shared/pipes/apply-local-tz.pipe'
import { TagModule } from 'primeng/tag'
import { TooltipModule } from 'primeng/tooltip'

@Component({
  selector: 'app-online',
  templateUrl: './online.component.html',
  styleUrl: './online.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, CopyToClipboardDirective, ApplyLocalTzPipe, ButtonModule, TagModule, TooltipModule],
})
export class OnlineComponent {
  constructor(private _myNodeService: MyNodeService, private _userService: UserService) {}

  openNodeHost(host: string) {
    window.open(prependHttps(host), '_blank')
  }

  get node$(): Observable<Node | null> {
    return this._myNodeService.node$
  }

  get user$(): Observable<User | null> {
    return this._userService.user$
  }
}
