/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component } from '@angular/core'
import { Router } from '@angular/router'
import { User } from '@core/models/user.model'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { UserService } from '@core/services/user/user.service'
import { ButtonModule } from 'primeng/button'
import { Observable } from 'rxjs'
import { encodeSBUrl } from 'src/app/utils'

@Component({
  selector: 'app-offline',
  templateUrl: './offline.component.html',
  styleUrl: './offline.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, ButtonModule],
})
export class OfflineComponent {
  constructor(private _userService: UserService, private _myNodeService: MyNodeService, private _router: Router) {}

  connectNode() {
    this._router.navigateByUrl(`/onboarding/connect-node/${encodeSBUrl(this._myNodeService.broker?.address ?? '')}`)
  }

  get user$(): Observable<User | null> {
    return this._userService.user$
  }
}
