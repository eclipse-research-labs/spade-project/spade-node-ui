/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component } from '@angular/core'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { CommonModule } from '@angular/common'
import { OnlineComponent } from './online/online.component'
import { OfflineComponent } from './offline/offline.component'
import { NgLetDirective } from 'ng-let'
import { NodeAuditTrailsComponent } from '../audit-trails/node-audit-trails/node-audit-trails.component'

@Component({
  selector: 'app-node',
  templateUrl: './node.component.html',
  styleUrl: './node.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, OnlineComponent, OfflineComponent, NodeAuditTrailsComponent, NgLetDirective],
})
export class NodeComponent {
  constructor(private _myNodeService: MyNodeService) {}

  get broker$() {
    return this._myNodeService.broker$
  }

  get node$() {
    return this._myNodeService.node$
  }

  get online() {
    return this._myNodeService.online
  }
}
