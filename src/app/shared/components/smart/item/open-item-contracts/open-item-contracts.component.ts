/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Item } from '@core/models/item.model';
import { MyNodeService } from '@core/services/my-node/my-node.service';
import { NgLetDirective } from 'ng-let';

@Component({
  selector: 'app-open-item-contracts',
  standalone: true,
  imports: [CommonModule, NgLetDirective],
  templateUrl: './open-item-contracts.component.html',
  styleUrl: './open-item-contracts.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OpenItemContractsComponent {

  @Input() item!: Item

  constructor(private _myNodeService: MyNodeService, private _router: Router) {}

  openContracts() {
    this._router.navigateByUrl(`detail/contracts/my-node-contracts/${this.nodeState}/${this.item?.oid}/active`)
  }

  get hasExigentContracts() {
    return this._myNodeService.hasExigentContracts(this.item.oid)
  }

  get exigentContracts$() {
    return this._myNodeService.exigentContracts$
  }

  get nodeState() {
    return this._myNodeService.nodeState
  }

}
