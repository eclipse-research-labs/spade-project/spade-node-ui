/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { ItemsApiService } from '@core/api/modules/items'
import { Item, SPADEPrivacy } from '@core/models/item.model'
import { BaseService } from '@core/services/base'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'

@Injectable()
export class ChangePrivacyService extends BaseService {
  private _item = new BehaviorSubject<Item | undefined>(undefined)

  item$ = this._item.asObservable()

  constructor(private _myNodeService: MyNodeService, private _itemsApi: ItemsApiService) {
    super()
  }

  init(item: Item) {
    this._item.next(item)
  }

  public async changeItemPrivacy(privacy: SPADEPrivacy) {
    const item = this._item.value
    if (item) {
      await this.loadApiWrapper(this._changeItemPrivacy(item.oid, privacy.Caption))
      item['SPADE:Privacy'] = privacy
      const index = this._myNodeService.myItems.findIndex((element) => element.oid == item.oid)
      if (index >= 0) {
        this._myNodeService.myItems[index]['SPADE:Privacy'] = privacy
        this._myNodeService.updateMyItems(this._myNodeService.myItems)
      }
      this._item.next(item)
    }
  }

  private async _changeItemPrivacy(oid: string, privacy: string) {
    await firstValueFrom(
      this._itemsApi.changeItemPrivacy(this._myNodeService.sbUrl, oid, privacy.toUpperCase()).pipe(take(1))
    )
  }

  get item() {
    return this._item.value
  }
}
