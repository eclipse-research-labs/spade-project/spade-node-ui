/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core'
import { Item, SPADEPrivacy } from '@core/models/item.model'
import { ChangePrivacyService } from './change-privacy.service'
import { CommonModule } from '@angular/common'
import { levelToPrivacyIcon } from 'src/app/utils'
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog'
import { ChangePrivacyDialogComponent } from './change-privacy-dialog/change-privacy-dialog.component'
import { NgLetDirective } from 'ng-let'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'

@UntilDestroy()
@Component({
  selector: 'app-change-privacy',
  templateUrl: './change-privacy.component.html',
  styleUrl: './change-privacy.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, NgLetDirective],
  providers: [ChangePrivacyService, DialogService],
})
export class ChangePrivacyComponent implements OnInit, OnChanges {
  @Input() item!: Item

  constructor(private _dialogService: DialogService, private _service: ChangePrivacyService) {}

  ngOnChanges(changes: SimpleChanges): void {
    this._service.init(this.item)
  }

  ngOnInit(): void {
    this._service.init(this.item)
  }

  ref: DynamicDialogRef | undefined

  changePrivacy() {
    this.ref = this._dialogService.open(ChangePrivacyDialogComponent, {
      header: 'Change privacy',
    })
    this.ref.onClose.pipe(untilDestroyed(this)).subscribe(element => {
      if (element) {
        this._service.init(element)
      }
    })
  }

  get privacyIcon() {
    return levelToPrivacyIcon(this.item['SPADE:Privacy']?.Level ?? 0)
  }

  get item$() {
    return this._service.item$
  }
}
