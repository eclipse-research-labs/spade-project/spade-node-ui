/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UserRole } from '@core/enums/user.enum';
import { Item, SPADEPrivacy, SPADEPrivacyWithMetadata } from '@core/models/item.model';
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service';
import { ActionButtonComponent } from '@shared/components/presentation/reusable/action-button/action-button.component';
import { InfoComponent } from '@shared/components/presentation/reusable/info/info.component';
import { RoleCheckerComponent } from '@shared/components/smart/role-checker/role-checker.component';
import { NgLetDirective } from 'ng-let';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { ChangePrivacyService } from '../change-privacy.service';
import { itemPrivacies } from 'src/app/data';

@Component({
  selector: 'app-change-privacy-dialog',
  standalone: true,
  imports: [CommonModule, RoleCheckerComponent, RoleCheckerComponent, ActionButtonComponent, InfoComponent, NgLetDirective],
  templateUrl: './change-privacy-dialog.component.html',
  styleUrl: './change-privacy-dialog.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChangePrivacyDialogComponent {
  item!: Item
  level = 0

  constructor(
    private _service: ChangePrivacyService,
    private _snackBar: SnackBarService,
    private _ref: DynamicDialogRef
  ) {
    this.item = this._service.item!
    this.level = this.item['SPADE:Privacy']?.Level ?? 0
  }

  async changePrivacy(privacy: SPADEPrivacyWithMetadata) {
    this.level = privacy.Level
    await this._service.changeItemPrivacy(privacy)
    this._snackBar.showSuccess('Privacy updated')
    this._ref.close(this.item)
  }

  get loading$() {
    return this._service.loading$
  }

  get admin() {
    return UserRole.ADMIN
  }

  get nodeOperator() {
    return UserRole.NODE_OPERATOR
  }

  get privacies() {
    return itemPrivacies
  }
}
