/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component } from '@angular/core'
import { SPADEPrivacyWithMetadata } from '@core/models/item.model'
import { ActionButtonComponent } from '@shared/components/presentation/reusable/action-button/action-button.component'
import { DynamicDialogRef } from 'primeng/dynamicdialog'
import { itemPrivacies } from 'src/app/data'
import { BulkChangePrivacyService } from '../bulk-change-privacy.service'
import { NgLetDirective } from 'ng-let'

@Component({
  selector: 'app-bulk-change-privacy-dialog',
  standalone: true,
  imports: [CommonModule, ActionButtonComponent, NgLetDirective],
  templateUrl: './bulk-change-privacy-dialog.component.html',
  styleUrl: './bulk-change-privacy-dialog.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BulkChangePrivacyDialogComponent {
  level = 0

  constructor(private _service: BulkChangePrivacyService, private _ref: DynamicDialogRef) {
  }

  async changePrivacy(privacy: SPADEPrivacyWithMetadata) {
    this.level = privacy.Level
    await this._service.changePrivacy(privacy)
    this._ref.close()
  }

  get loading$() {
    return this._service.loading$
  }

  get privacies() {
    return itemPrivacies
  }
}
