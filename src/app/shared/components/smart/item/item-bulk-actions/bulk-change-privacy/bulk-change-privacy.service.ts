/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { ItemsApiService } from '@core/api/modules/items'
import { Item, SPADEPrivacy } from '@core/models/item.model'
import { BaseService } from '@core/services/base'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'
import { delay } from 'src/app/utils'

@Injectable()
export class BulkChangePrivacyService extends BaseService {
  selectedItems: Item[] = []
  private _items = new BehaviorSubject<Item[]>([])

  items$ = this._items.asObservable()

  constructor(private _myNodeService: MyNodeService, private _itemsApiService: ItemsApiService) {
    super()
  }

  init(items: Item[]) {
    this._items.next(items)
  }

  async changePrivacy(privacy: SPADEPrivacy) {
    await this.loadApiWrapper(this._changePrivacy(privacy.Caption))
    const serviceItems = this._items.value.map((element) => {
      if (this.selectedItems.some((item) => item.oid == element.oid)) {
        element['SPADE:Privacy'] = privacy
      }
      return element
    })
    this._items.next([...serviceItems])
    const myNodeItems = this._myNodeService.myItems.map((element) => {
      if (this.selectedItems.some((item) => item.oid == element.oid)) {
        element['SPADE:Privacy'] = privacy
      }
      return element
    })
    this._myNodeService.updateMyItems([...myNodeItems])
  }

  private async _changePrivacy(privacy: string) {
    return await firstValueFrom(
      this._itemsApiService
        .changeItemsPrivacy(
          this._myNodeService.sbUrl,
          this.selectedItems.map((element) => element.oid),
          privacy.toUpperCase()
        )
        .pipe(take(1))
    )
  }
}
