/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core'
import { ItemPreviewComponent } from '@shared/components/presentation/app-specific/item/item-preview/item-preview.component'
import { ChangePrivacyComponent } from '../../change-privacy/change-privacy.component'
import { ScrollingModule } from '@angular/cdk/scrolling'
import { Item } from '@core/models/item.model'
import { CheckboxModule } from 'primeng/checkbox'
import { FormsModule } from '@angular/forms'
import { ButtonModule } from 'primeng/button'
import { inflect, levelToPrivacyIcon } from 'src/app/utils'
import { BulkChangePrivacyService } from './bulk-change-privacy.service'
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog'
import { BulkChangePrivacyDialogComponent } from './bulk-change-privacy-dialog/bulk-change-privacy-dialog.component'
import { InfoComponent } from '@shared/components/presentation/reusable/info/info.component'
import { ProgressBarModule } from 'primeng/progressbar'
import { map, Observable } from 'rxjs'
import { MyNodeService } from '@core/services/my-node/my-node.service'

@Component({
  selector: 'app-bulk-change-privacy',
  standalone: true,
  imports: [
    CommonModule,
    ItemPreviewComponent,
    ChangePrivacyComponent,
    ScrollingModule,
    CheckboxModule,
    FormsModule,
    ButtonModule,
    InfoComponent,
    ProgressBarModule,
  ],
  templateUrl: './bulk-change-privacy.component.html',
  styleUrl: './bulk-change-privacy.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [BulkChangePrivacyService, DialogService],
})
export class BulkChangePrivacyComponent implements OnInit, OnChanges {
  @Input() items!: Item[]
  @Input() preselectAll: boolean = false

  allSelected = false

  ref: DynamicDialogRef | undefined

  status = 20

  constructor(
    private _service: BulkChangePrivacyService,
    private _dialogService: DialogService
  ) {}

  ngOnInit(): void {
    this._init()
  }

  ngOnChanges(changes: SimpleChanges): void {
    this._init()
  }

  toggleAll() {
    if (this._allSelected) {
      this._clearSelection()
    } else {
      this._service.selectedItems = [...this.items]
      this.allSelected = true
    }
  }

  toggleOne() {
    if (this._allSelected && !this.allSelected) {
      this.allSelected = true
    }

    if (!this._allSelected && this.allSelected) {
      this.allSelected = false
    }
  }

  changePrivacy() {
    this.ref = this._dialogService.open(BulkChangePrivacyDialogComponent, {
      header: 'Change privacy',
    })
  }

  get items$() {
    return this._service.items$
  }

  get selectedItems() {
    return this._service.selectedItems
  }

  set selectedItems(items: Item[]) {
    this._service.selectedItems = items
  }

  get label() {
    return `Change ${inflect(
      this._service.selectedItems.length,
      '0 items',
      '1 item',
      `${this._service.selectedItems.length > 100 ? '100+' : this._service.selectedItems.length} items`
    )}`
  }

  privacyIcon(item: Item) {
    return levelToPrivacyIcon(item['SPADE:Privacy']?.Level ?? 0)
  }

  private _init() {
    this._service.init(this.items)
    if (this.preselectAll) {
      this._service.selectedItems = [...this.items]
      this.allSelected = true
    }
  }

  private _clearSelection() {
    this._service.selectedItems = []
    this.allSelected = false
  }

  private get _allSelected() {
    return this.items.every((element) => this._service.selectedItems!.includes(element))
  }
}
