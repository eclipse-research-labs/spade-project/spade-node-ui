/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core'
import { ItemPreviewComponent } from '@shared/components/presentation/app-specific/item/item-preview/item-preview.component'
import { ButtonModule } from 'primeng/button'
import { DialogModule } from 'primeng/dialog'
import { NG_VALUE_ACCESSOR } from '@angular/forms'
import { Router } from '@angular/router'
import { ContractAccessType } from '@core/enums/contract.enum'
import { NewContract } from '@core/models/contract.model'
import { Item } from '@core/models/item.model'
import { NodeWithOwner } from '@core/models/node.model'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { levelToPrivacyIcon, inflect } from 'src/app/utils'
import { MenuItem } from 'primeng/api'
import { MenuModule } from 'primeng/menu'
import { BulkChangePrivacyComponent } from './bulk-change-privacy/bulk-change-privacy.component'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { UserRole } from '@core/enums/user.enum'
import { UserService } from '@core/services/user/user.service'
import { ContractCreatorService } from '@core/services/contract-creator/contract-creator.service'

@UntilDestroy()
@Component({
  selector: 'app-item-bulk-actions',
  standalone: true,
  imports: [CommonModule, ButtonModule, DialogModule, MenuModule, BulkChangePrivacyComponent],
  templateUrl: './item-bulk-actions.component.html',
  styleUrl: './item-bulk-actions.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: ItemBulkActionsComponent,
    },
  ],
})
export class ItemBulkActionsComponent implements OnInit {
  @Input() assignees: NodeWithOwner[] = []
  @Input() accessType: ContractAccessType = ContractAccessType.REQUEST
  @Input() allowChangePrivacy: boolean = true

  options: MenuItem[] | undefined

  value: Item[] = []

  touched = false

  disabled = false

  lowPrivacy = false
  lowPrivacyItems: Item[] = []

  changePrivacy = false
  changePrivacyItems: Item[] = []

  private _authorized = true

  constructor(
    private _contractCreatorService: ContractCreatorService,
    private _myNodeService: MyNodeService,
    private _userService: UserService,
    private _cd: ChangeDetectorRef,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this._initOptions()
    this._listenForUserChange()
  }

  onChange = (value: Item[]) => {}

  onTouched = () => {}

  writeValue(value: Item[]) {
    this.value = value ?? []
    if (this.value.length <= 0 && !this.disabled) {
      this.disabled = true
    }
    if (this.value.length > 0 && this.disabled) {
      this.disabled = false
    }
  }

  registerOnChange(onChange: any) {
    this.onChange = onChange
  }

  registerOnTouched(onTouched: any) {
    this.onTouched = onTouched
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched()
      this.touched = true
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled
  }

  changePrivacyOfItems() {
    this.changePrivacyItems = [...this.value]
    if (this.changePrivacyItems.length > 0) {
      this.changePrivacy = true
      this._cd.detectChanges()
    }
  }

  contractItems() {
    this.lowPrivacyItems = this.value.filter(
      (element) => element['SPADE:Privacy']?.Level != undefined && element['SPADE:Privacy']?.Level < 2
    )
    if (this.lowPrivacyItems.length > 0) {
      this.lowPrivacy = true
      this._cd.detectChanges()
    } else {
      const newContract: NewContract = {
        targets: this.value,
        assignees: this.assignees,
        read: false,
        write: false,
        subscribe: false,
        accessType: this.accessType,
      }
      this._contractCreatorService.init(newContract)
      this._router.navigateByUrl(`contract-creator/${this._myNodeService.node?.nodeId}`)
    }
  }

  getPrivacyIcon(item: Item) {
    return levelToPrivacyIcon(item['SPADE:Privacy']?.Level ?? 0)
  }

  private _listenForUserChange() {
    this._userService.user$.pipe(untilDestroyed(this)).subscribe((user) => {
      this._authorized = user?.roles.includes(UserRole.ADMIN) || user?.roles.includes(UserRole.NODE_OPERATOR) || false
      this._initOptions()
    })
  }

  private _initOptions() {
    const actions: MenuItem[] = [
      {
        label: 'Contract',
        icon: 'fa fa-file-signature',
        disabled: !this._authorized,
        tooltipOptions: {
          appendTo: 'body',
          tooltipPosition: 'right',
          tooltipLabel: 'Insufficient role to contract items',
          disabled: this._authorized,
        },
        command: (e) => {
          const event = e.originalEvent
          if (event) {
            this.contractItems()
          }
        },
      },
    ]
    if (this.allowChangePrivacy) {
      actions.push({
        label: 'Change Privacy',
        icon: 'fa fa-eye',
        disabled: !this._authorized,
        tooltipOptions: {
          appendTo: 'body',
          tooltipPosition: 'right',
          tooltipLabel: 'Insufficient role to change privacy',
          disabled: this._authorized,
        },
        command: (e) => {
          const event = e.originalEvent
          if (event) {
            this.changePrivacyOfItems()
          }
        },
      })
    }
    this.options = [
      {
        label: 'Actions',
        items: actions,
      },
    ]
  }

  get label() {
    return `Edit ${inflect(
      this.value.length,
      '0 items',
      '1 item',
      `${this.value.length > 100 ? '100+' : this.value.length} items`
    )}`
  }

  get contractsDisabled() {
    return this.lowPrivacyItems.some((element) => !element['SPADE:Privacy'] || element['SPADE:Privacy'].Level < 2)
  }
}
