/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MyNodeService } from '@core/services/my-node/my-node.service';
import { ButtonModule } from 'primeng/button';
import { prependHttps } from 'src/app/utils';

@Component({
  selector: 'app-accept-ssl',
  templateUrl: './accept-ssl.component.html',
  styleUrl: './accept-ssl.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, ButtonModule],
})
export class AcceptSslComponent {
  @Input() message?: string
  
  constructor(private _myNodeService: MyNodeService) {}

  async acceptSSL() {
    const address = this._myNodeService.healthcheckError
    if (address) {
      window.open(prependHttps(address) + '/api/sb/admin/healthcheck', '_blank')
    }
  }
}
