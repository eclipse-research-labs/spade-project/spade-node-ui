/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core'
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog'
import { FilterComponent } from './filter/filter.component'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { Filter } from '@shared/models/filter-items.model'
import { Organisation } from '@core/models/organisation.model'
import { CommonModule } from '@angular/common'

@UntilDestroy()
@Component({
  selector: 'app-filter-items',
  templateUrl: './filter-items.component.html',
  styleUrl: './filter-items.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DialogService],
  standalone: true,
  imports: [CommonModule],
})
export class FilterItemsComponent {
  @Input() filter: Filter | null = null
  @Input() organisations: Organisation[] = []

  @Output() onFilterChange: EventEmitter<Filter | null> = new EventEmitter()

  private _ref: DynamicDialogRef | undefined

  constructor(private _dialogService: DialogService, private _cd: ChangeDetectorRef) {}

  openFilterDialog() {
    this._ref = this._dialogService.open(FilterComponent, {
      header: 'Filter Items',
      height: '700px',
      data: {
        filter: this.filter,
        organisations: this.organisations,
      },
    })
    this._ref.onClose.pipe(untilDestroyed(this)).subscribe((res) => {
      const clear = res?.clear
      const filter = res?.filter
      if (clear || (filter && filter.organisations.length <= 0 && filter.itemTypes.length <= 0)) {
        this.clearFilter()
        return
      }
      if (filter) {
        this.setFilter(filter)
        return
      }
    })
  }

  setFilter(filter: Filter) {
    this.filter = filter
    this._cd.markForCheck()
    this.onFilterChange.emit(this.filter)
    return
  }

  clearFilter() {
    this.filter = null
    this._cd.markForCheck()
    this.onFilterChange.emit(null)
    return
  }

  get filterActive() {
    return this.filter != undefined || this.filter != null
  }
}
