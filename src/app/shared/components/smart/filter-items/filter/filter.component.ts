/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { ItemType } from '@core/models/item.model'
import { ForeignOrganisation } from '@core/models/organisation.model'
import { OrgTileComponent } from '@shared/components/presentation/app-specific/organisation/org-tile/org-tile.component'
import { MenuContentComponent } from '@shared/components/presentation/reusable/menu/menu-content/menu-content.component'
import { MenuItemComponent } from '@shared/components/presentation/reusable/menu/menu-item/menu-item.component'
import { MenuLabelComponent } from '@shared/components/presentation/reusable/menu/menu-label/menu-label.component'
import { MenuComponent } from '@shared/components/presentation/reusable/menu/menu/menu.component'
import { SadFaceComponent } from '@shared/components/presentation/reusable/sad-face/sad-face.component'
import { Filter } from '@shared/models/filter-items.model'
import { ButtonModule } from 'primeng/button'
import { CheckboxModule } from 'primeng/checkbox'
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog'
import { itemTypes } from 'src/app/data'

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrl: './filter.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    MenuComponent,
    MenuItemComponent,
    MenuLabelComponent,
    MenuContentComponent,
    OrgTileComponent,
    SadFaceComponent,
    ButtonModule,
    CheckboxModule,
  ],
})
export class FilterComponent {
  constructor(private _ref: DynamicDialogRef, private _config: DynamicDialogConfig) {}

  organisations: ForeignOrganisation[] = []
  selectedOrganisations: ForeignOrganisation[] = []

  types: ItemType[] = itemTypes
  selectedTypes: ItemType[] = []

  async ngOnInit(): Promise<void> {
    const filter = this._config.data.filter as Filter | null
    this.organisations = this._config.data?.organisations ?? []
    this.selectedOrganisations = filter?.organisations ?? []
    this.selectedTypes = filter?.itemTypes ?? []
  }

  filter() {
    this._ref.close({ clear: false, filter: { organisations: this.selectedOrganisations, itemTypes: this.selectedTypes } })
  }

  clear() {
    this._ref.close({ clear: true })
  }
}
