/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core'
import { UserService } from '@core/services/user/user.service'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { Router } from '@angular/router'
import { CommonModule } from '@angular/common'
import { BottomSheetComponent } from '@shared/components/presentation/reusable/overlays/bottom-sheet/bottom-sheet.component'
import { NodeComponent } from '../node/node.component'
import { TypeIconComponent } from '@shared/components/presentation/app-specific/type-icon/type-icon.component'
import { NgLetDirective } from 'ng-let'
import { OrganisationComponent } from '../organisation/organisation.component'

@Component({
  selector: 'app-app-status',
  standalone: true,
  imports: [
    CommonModule,
    BottomSheetComponent,
    NodeComponent,
    OrganisationComponent,
    TypeIconComponent,
    NgLetDirective,
  ],
  templateUrl: './app-status.component.html',
  styleUrl: './app-status.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppStatusComponent implements OnInit {
  visible: boolean = false
  solo: boolean = false

  nodeActive = false
  orgActive = false

  constructor(
    private _cd: ChangeDetectorRef,
    private _myNodeService: MyNodeService,
    private _userService: UserService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.nodeActive = this._myNodeService.online && this._myNodeService.node != undefined
    this.orgActive =
      !this.nodeActive && this._userService.user != undefined && this._userService.user.organisation != undefined
  }

  showDialog() {
    this.visible = true
    this._cd.detectChanges()
  }

  hideDialog() {
    this.visible = false
    this._cd.detectChanges()
  }

  changeNode(event: Event) {
    event.stopPropagation()
    this._router.navigateByUrl('/nodes')
  }

  selectOrg(event: Event) {
    if (this.online && this.user && this.user.organisation) {
      this.orgActive = true
      this.nodeActive = false
    }
  }

  selectNode(event: Event) {
    if ((this.online && this.node) || (!this.online && this.broker)) {
      this.nodeActive = true
      this.orgActive = false
    }
  }

  get broker$() {
    return this._myNodeService.broker$
  }

  get broker() {
    return this._myNodeService.broker
  }

  get node$() {
    return this._myNodeService.node$
  }

  get node() {
    return this._myNodeService.node
  }

  get user$() {
    return this._userService.user$
  }

  get user() {
    return this._userService.user
  }

  get online() {
    return this._myNodeService.online
  }
}
