/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, ContentChild, Input, TemplateRef } from '@angular/core';
import { UserRole } from '@core/enums/user.enum';
import { UserService } from '@core/services/user/user.service';
import { NgLetDirective } from 'ng-let';

@Component({
  selector: 'app-role-checker',
  templateUrl: './role-checker.component.html',
  styleUrl: './role-checker.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, NgLetDirective],
})
export class RoleCheckerComponent {

  @Input() allowedRoles: UserRole[] = []

  @ContentChild(TemplateRef) templateRef?: TemplateRef<any>;

  constructor(private _userService: UserService) {}

  get checkRoles$() {
    return this._userService.checkRoles$
  }

}
