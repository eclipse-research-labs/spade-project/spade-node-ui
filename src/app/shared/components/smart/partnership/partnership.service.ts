/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Injectable } from '@angular/core'
import { OrganisationApiService } from '@core/api/modules/organisations'
import { PartnershipApiService } from '@core/api/modules/partnerships'
import { ForeignOrganisation } from '@core/models/organisation.model'
import { Partnership } from '@core/models/partnership.model'
import { BaseService } from '@core/services/base'
import { MyOrgService } from '@core/services/my-org/my-org.service'
import { BehaviorSubject, firstValueFrom, take } from 'rxjs'

@Injectable()
export class PartnershipService extends BaseService {
  private _org = new BehaviorSubject<ForeignOrganisation | null>(null)

  organisation$ = this._org.asObservable()

  constructor(private _organisationsApiService: OrganisationApiService, private _partnershipApiService: PartnershipApiService, private _myOrgService: MyOrgService) {
    super()
  }

  async initPartnership(cid: string) {
    const org = await this.initApiWrapper(this._getForeignOrganisation(cid))
    this._org.next(org)
  }

  async createPartnership() {
    const org = this.organisation
    if (org) {
      const part = await this.loadApiWrapper(this._createPartnership(this.organisation))
      org.ingressPartnerships ??= []
      org.ingressPartnerships.push(part)
      this._updatePartners(org)
    }
  }

  async acceptPartnership() {
    const org = this.organisation
    const part = org?.egressPartnership
    if (org && part) {
      const updated = await this.loadApiWrapper(this._acceptPartnership(org, part))
      org.egressPartnerships ??= []
      org.egressPartnerships = [updated, ...org.egressPartnerships.filter((p) => p.partId != part.partId)]
      this._updatePartners(org)
    }
  }

  async cancelPartnership() {
    const org = this.organisation
    const part = org?.egressPartnership ?? org?.ingressPartnership
    if (org && part) {
      await this.loadApiWrapper(this._cancelPartnership(org, part))
      org.egressPartnerships ??= []
      org.ingressPartnerships ??= []
      org.egressPartnerships = org.egressPartnerships.filter((p) => p.partId != part.partId)
      org.ingressPartnerships = org.ingressPartnerships.filter((p) => p.partId != part.partId)
      this._updatePartners(org)
    }
  }

  private async _getForeignOrganisation(cid: string) {
    return await firstValueFrom(this._organisationsApiService.getForeignOrganisation(cid).pipe(take(1)))
  }

  private async _createPartnership(org: ForeignOrganisation) {
    return await firstValueFrom(this._partnershipApiService.createPartnership(org.cid).pipe(take(1)))
  }

  private async _acceptPartnership(org: ForeignOrganisation, part: Partnership) {
    return await firstValueFrom(this._partnershipApiService.acceptPartnership(part.partId).pipe(take(1)))
  }

  private async _cancelPartnership(org: ForeignOrganisation, part: Partnership) {
    await firstValueFrom(this._partnershipApiService.cancelPartnership(part.partId).pipe(take(1)))
  }

  private _updatePartners(org: ForeignOrganisation) {
    const partners = this._myOrgService.organisations
    const updated = [org, ...partners.filter((el) => org.cid != el.cid)]
    this._myOrgService.updateOrganisations(updated)
  }

  get organisation() {
    return this._org.value
  }
}
