/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'
import { PartnershipService } from './partnership.service'
import { UserRole } from '@core/enums/user.enum'
import { CommonModule } from '@angular/common'
import { SpinnerComponent } from '@shared/components/presentation/reusable/loaders/spinner/spinner.component'
import { RoleCheckerComponent } from '../role-checker/role-checker.component'
import { ButtonModule } from 'primeng/button'
import { InfoComponent } from '@shared/components/presentation/reusable/info/info.component'
import { NgLetDirective } from 'ng-let'

@Component({
  selector: 'app-partnership',
  templateUrl: './partnership.component.html',
  styleUrl: './partnership.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [PartnershipService],
  standalone: true,
  imports: [CommonModule, InfoComponent, SpinnerComponent, RoleCheckerComponent, ButtonModule, NgLetDirective],
})
export class PartnershipComponent implements OnInit {
  constructor(private _config: DynamicDialogConfig, private _ref: DynamicDialogRef, private _snackBar: SnackBarService, private _service: PartnershipService) {}

  async ngOnInit(): Promise<void> {
    await this._service.initPartnership(this._config.data!.cid)
  }

  async createPartnership() {
    await this._service.createPartnership()
    this._snackBar.showSuccess('Partnership request sent')
    this._ref.close()
  }

  async acceptPartnership() {
    await this._service.acceptPartnership()
    this._snackBar.showSuccess('Partnership established')
    this._ref.close()
  }

  async cancelPartnership() {
    const wasPartner = this.organisation?.isPartner
    const wasIngressPendingPartner = this.organisation?.isIngressPendingPartner
    await this._service.cancelPartnership()
    this._snackBar.showSuccess(`Partnership ${wasPartner ? '' : 'request'} ${wasIngressPendingPartner ? 'declined' : 'canceled'}`)
    this._ref.close()
  }

  get errorInitializing$() {
    return this._service.errorInitializing$
  }

  get errorLoading$() {
    return this._service.errorLoading$
  }

  get organisation$() {
    return this._service.organisation$
  }

  get organisation() {
    return this._service.organisation
  }

  get initilizing$() {
    return this._service.initializing$
  }

  get loading$() {
    return this._service.loading$
  }

  get admin() {
    return UserRole.ADMIN
  }
}
