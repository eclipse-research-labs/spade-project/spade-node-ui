/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { Router } from '@angular/router'
import { BasicAuth, User } from '@core/models/user.model'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { UserService } from '@core/services/user/user.service'
import { ButtonModule } from 'primeng/button'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrl: './profile.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, ButtonModule],
})
export class ProfileComponent {
  @Input() user?: User
  @Input() basicAuth?: BasicAuth

  constructor(private _userService: UserService, private _myNodeService: MyNodeService, private _router: Router) {}

  async logout() {
    if (this._myNodeService.online) {
      await this._userService.logout()
    } else {
      this._myNodeService.setBasicAuth(null)
      this._router.navigateByUrl('/')
    }
  }

  get loading$() {
    return this._userService.loading$
  }
}
