/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core'
import { MyNodeService } from '@core/services/my-node/my-node.service'
import { UserService } from '@core/services/user/user.service'
import { TopPanelComponent } from '@shared/components/presentation/reusable/overlays/top-panel/top-panel.component'
import { ProfileComponent } from './profile/profile.component'
import { SurnameShortPipe } from '@shared/pipes/surname-short.pipe'
import { NgLetDirective } from 'ng-let'

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrl: './user-profile.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, TopPanelComponent, ProfileComponent, SurnameShortPipe, NgLetDirective],
})
export class UserProfileComponent {
  profileVisible = false

  constructor(private _userService: UserService, private _myNodeService: MyNodeService, private _cd: ChangeDetectorRef) {}

  openProfile(event: Event) {
    event.stopPropagation()
    this.profileVisible = true
    this._cd.detectChanges()
  }

  get basicAuth$() {
    return this._myNodeService.basicAuth$
  }

  get isLoggedIn() {
    return this._userService.isLoggedIn
  }

  get user$() {
    return this._userService.user$
  }
}
