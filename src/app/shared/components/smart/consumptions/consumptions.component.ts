/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component } from '@angular/core'
import { ConsumptionService } from '@core/services/consumption/consumption.service'
import { ConsumptionComponent } from './consumption/consumption.component'
import { ButtonModule } from 'primeng/button'
import { NgLetDirective } from 'ng-let'
import { SadFaceComponent } from '@shared/components/presentation/reusable/sad-face/sad-face.component'

@Component({
  selector: 'app-consumptions',
  standalone: true,
  imports: [CommonModule, ConsumptionComponent, ButtonModule, NgLetDirective, SadFaceComponent],
  templateUrl: './consumptions.component.html',
  styleUrl: './consumptions.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConsumptionsComponent {
  constructor(private _consumptionService: ConsumptionService) {}

  open() {
    this._consumptionService.open()
  }

  close() {
    this._consumptionService.close()
  }

  closeIfOpen() {
    if (this._consumptionService.opened) {
      this._consumptionService.close()
    }
  }

  clear() {
    this._consumptionService.clear()
  }

  get consumption$() {
    return this._consumptionService.consumption$
  }

  get opened$() {
    return this._consumptionService.opened$
  }
}
