/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { HttpErrorResponse, HttpResponse, HttpStatusCode } from '@angular/common/http'
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { ConsumptionWithStatusAndResult } from '@core/models/consumption.model'
import { ConsumptionService } from '@core/services/consumption/consumption.service'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { CodeEditorComponent } from '@shared/components/presentation/reusable/inputs/code-editor/code-editor.component'
import mime from 'mime'
import { NgLetDirective } from 'ng-let'
import { ButtonModule } from 'primeng/button'
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog'
import { MenuModule } from 'primeng/menu'
import { MessagesModule } from 'primeng/messages'
import { ProgressBarModule } from 'primeng/progressbar'
import { SidebarModule } from 'primeng/sidebar'
import { TagModule } from 'primeng/tag'
import { TooltipModule } from 'primeng/tooltip'
import { isFile, isBlob, isString, prependHttps } from 'src/app/utils'
import { CredentialComponent } from '../../credential/credential.component'

const _propertyConsumptionUrl = '/api/sb/consumption/properties'

@UntilDestroy()
@Component({
  selector: 'app-consumption',
  standalone: true,
  imports: [
    CommonModule,
    ProgressBarModule,
    TagModule,
    ButtonModule,
    NgLetDirective,
    SidebarModule,
    CodeEditorComponent,
    FormsModule,
    TooltipModule,
    MenuModule,
    MessagesModule,
  ],
  templateUrl: './consumption.component.html',
  styleUrl: './consumption.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DialogService],
})
export class ConsumptionComponent implements OnInit {
  @Input() consumption!: ConsumptionWithStatusAndResult

  ref: DynamicDialogRef | undefined

  showBody = false
  body?: string

  showError = false
  error?: string

  showCurl = false
  curl?: string

  authHeader?: string

  options = [
    {
      label: 'Try Again',
      icon: 'pi pi-refresh',
      tooltipOptions: {
        appendTo: 'body',
      },
      command: (e: any) => {
        const event = e.originalEvent
        if (event) {
          this._tryAgain()
        }
      },
    },
    {
      label: 'Show cURL',
      icon: 'fa fa-code',
      command: (e: any) => {
        const event = e.originalEvent
        if (event) {
          this.showCurl = true
        }
      },
    },
  ]

  constructor(
    private _consumptionService: ConsumptionService,
    private _dialogService: DialogService,
    private _cd: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this._listenForConsumptionResult()
    this._listenForConsumptionError()
    this._generateApi()
  }

  downloadBody(response: HttpResponse<any>) {
    const body = response.body
    const data = this._contentToBlob(body)
    const contentType = mime.getExtension(this.consumption.contentType ?? data.type)
    if (contentType) this._download(data, `${this.fileName}.${contentType}`)
    else this._download(data, `${this.fileName}`)
  }

  downloadError(error: any) {
    const contentType = mime.getExtension('application/json')!
    const data = this._contentToBlob(error)
    this._download(data, `${this.fileName}-error.${contentType}`)
  }

  openCredentials() {
    this.ref = this._dialogService.open(CredentialComponent, {
      header: 'New Credential',
      data: {
        useAuthHeader: true,
      },
    })
    this.ref.onClose.subscribe((data) => {
      this._generateApi(data)
      this._cd.markForCheck()
    })
  }

  private _contentToBlob(body: any) {
    if (isBlob(body)) {
      return body
    }
    let content: any = body
    if (isString(content)) {
      try {
        content = JSON.parse(content as string)
        content = JSON.stringify(content, undefined, 2)
      } catch (e) {
        console.warn('Body is not a valid JSON. Skipping formatting...')
      }
    } else {
      try {
        content = JSON.stringify(content, undefined, 2)
      } catch (e) {
        console.warn('Body is not a valid JSON. Skipping formatting...')
        content = content.toString()
      }
    }
    const payload = Array.from(content as string, (char) => char.charCodeAt(0))
    return new Blob([new Uint8Array(payload)])
  }

  private _download(blob: Blob, name: string) {
    var link = document.createElement('a')
    link.hidden = true
    link.download = name
    link.href = window.URL.createObjectURL(blob)
    link.click()
    link.remove()
  }

  private _listenForConsumptionResult() {
    this.consumption.result$.pipe(untilDestroyed(this)).subscribe((val) => {
      if (val && this.isJsonOrText(val)) {
        this.body = isString(val.body) ? (val.body as string) : JSON.stringify(val.body, null, 2)
      }
    })
  }

  private _listenForConsumptionError() {
    this.consumption.error$.pipe(untilDestroyed(this)).subscribe((val) => {
      if (val) {
        this.error = JSON.stringify(val, null, 2)
      }
    })
  }

  private _generateApi(authHeader?: string) {
    const url = this._generateUrl()
    if (url) {
      const method = this.read ? 'GET' : 'PUT'
      const contentType = this.write ? this.consumption.payload?.contentType : undefined
      const accept = this.read ? this.consumption.contentType : undefined
      const payload = this.consumption.payload?.data
      let curl = `curl -L -H 'Authorization: ${authHeader ?? '{ Generated credentials }'}' \\`
      if (contentType) {
        curl = `${curl}\n     -H \'Content-type: ${contentType}\' \\`
      }
      // if (accept) {
      //   curl = `${curl}\n     -H \'Accept: ${accept}\' \\`
      // }
      curl = `${curl}\n     -X ${method} \\`
      curl = `${curl}\n     \'${url}\'`
      if (this.write && payload) {
        if (isFile(payload)) {
          curl = `${curl} \\\n     -d \'{ File as binary data }\'`
        } else {
          curl = `${curl} \\\n     -d \\\n\'${payload}\'`
        }
      }
      this.curl = curl
    }
  }

  private _generateUrl() {
    const sbUrl = this._consumptionService.sbUrl
    const oid = this.consumption.item.oid
    const pid = this.consumption.property.key
    const params = this.consumption.params
    return (
      `${prependHttps(sbUrl) + _propertyConsumptionUrl}/${oid}/${pid}` +
      (params !== undefined && params.size > 0
        ? '?' +
          Array.from(params.keys())
            .filter((element) => params!.get(element) && params!.get(element)!.length > 0)
            .map((element) => `${element}=${params!.get(element)}`)
            .join('&')
        : '')
    )
  }

  private _tryAgain() {
    this._consumptionService.triggerConsumption(this.consumption, 1200)
  }

  isJsonOrText(result?: HttpResponse<any> | null) {
    if (!result) return false
    const contentTypeHeader = result.headers.get('Content-Type')
    if (!contentTypeHeader) return false
    const contentTypes = contentTypeHeader.split(',')
    return contentTypes.some((element) => element == 'application/json' || element.startsWith('text/'))
  }

  unauthorized(err: any) {
    return err instanceof HttpErrorResponse && err.status == HttpStatusCode.Unauthorized
  }

  payloadTooLarge(err: any) {
    return err instanceof HttpErrorResponse && err.status == HttpStatusCode.PayloadTooLarge
  }

  get error$() {
    return this.consumption.error$
  }

  get result$() {
    return this.consumption.result$
  }

  get status$() {
    return this.consumption.status$
  }

  get read() {
    return this.consumption.op == 'READ' || this.consumption.op == 'readproperty'
  }

  get write() {
    return this.consumption.op == 'WRITE' || this.consumption.op == 'writeproperty'
  }

  get fileName() {
    return `${this.consumption.item.title}-${this.consumption.property.key}`
  }
}
