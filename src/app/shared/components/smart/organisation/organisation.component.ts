/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component } from '@angular/core'
import { NgLetDirective } from 'ng-let'
import { OrgAuditTrailsComponent } from '../audit-trails/org-audit-trails/org-audit-trails.component'
import { UserService } from '@core/services/user/user.service'
import { ButtonModule } from 'primeng/button'
import { CopyToClipboardDirective } from '@shared/directives/copy-to-clipboard.directive'
import { UserRole } from '@core/enums/user.enum'
import { UserAuditTrailsComponent } from '../audit-trails/user-audit-trails/user-audit-trails.component'

@Component({
  selector: 'app-organisation',
  standalone: true,
  imports: [
    CommonModule,
    OrgAuditTrailsComponent,
    UserAuditTrailsComponent,
    CopyToClipboardDirective,
    ButtonModule,
    NgLetDirective,
  ],
  templateUrl: './organisation.component.html',
  styleUrl: './organisation.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrganisationComponent {
  constructor(private _userService: UserService) {}

  get isAdmin() {
    return this._userService.user?.roles.includes(UserRole.ADMIN)
  }

  get user$() {
    return this._userService.user$
  }
}
