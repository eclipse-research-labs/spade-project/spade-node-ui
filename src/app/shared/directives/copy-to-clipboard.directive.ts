/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Directive, HostListener, Input } from '@angular/core'
import { SnackBarService } from '@core/services/snack-bar/snack-bar.service'

@Directive({
  selector: '[appCopyToClipboard]',
  standalone: true
})
export class CopyToClipboardDirective {
  @Input() textToCopy: string | null | undefined
  @Input() copyMessage?: string

  constructor(private _snackbar: SnackBarService) {}

  @HostListener('click', ['$event'])
  copyToClipboard(event: Event) {
    event.stopPropagation()
    if (this.textToCopy) {
      navigator.clipboard.writeText(this.textToCopy)
      this._snackbar.showSuccess(this.copyMessage ?? 'Copied to clipboard')
    }
  }
}
