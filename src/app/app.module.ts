/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { APP_INITIALIZER, NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { HTTPReqResInterceptor } from '@core/interceptors/http-req-res.interceptor'
import { environment } from '@env'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { MessageService } from 'primeng/api'
import { FaIconLibrary } from '@fortawesome/angular-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http'
import { CoreModule } from '@core/core.module'
import { NotificationService } from '@core/services/notification/notification.service'
import { MasterService } from '@core/services/master/master.service'
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular'
import { initializeKeycloak } from './initializers'
import { NodesService } from '@core/services/nodes/nodes.service'
import { UserService } from '@core/services/user/user.service'
import { CacheService } from '@core/services/cache/cache.service'
import { MarketplaceModule } from './features/marketplace/marketplace.module'

@NgModule({
  declarations: [AppComponent],
  imports: [
    CoreModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    KeycloakAngularModule,
    MarketplaceModule,
  ],
  providers: [
    { provide: 'CC_URL', useValue: environment.ccUrl },
    { provide: 'S3_URL', useValue: environment.s3url },
    { provide: 'NORTH_BOUND_URL', useValue: environment.northBoundUrl },
    { provide: 'SOUTH_BOUND_URL', useValue: environment.southBoundUrl },
    { provide: 'MARKETPLACE_URL', useValue: environment.marketplaceUrl },
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService, MasterService],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HTTPReqResInterceptor,
      multi: true,
    },
    NotificationService,
    MessageService,
    MasterService,
    UserService,
    NodesService,
    CacheService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas, far)
  }
}
