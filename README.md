<!--- *************************************************************************
      *Copyright (C) 2024 bAvenir
      *
      *This program and the accompanying materials are made
      *available under the terms of the Eclipse Public License 2.0
      *which is available at https://www.eclipse.org/legal/epl-2.0/
      *
      *SPDX-License-Identifier: EPL-2.0
      ************************************************************************* --->

# SPADE-Node-UI

SPADE-Node-UI is an Angular application for managing Data Broker (Node) instances. It enables seamless control of your infrastructure by allowing you to register Data Brokers, register Thing Descriptions (Items), manage collaboration rules, and setup and test data consumption across both local and remote systems.

## Getting Started

This is an Angular application, and follows the standard process:

1. Install dependencies:
    ```bash
    npm install
    ```

2. Provide the necessary configuration in the `src/app/environments/environment.ts` file.

3. Start the application:
    ```bash
    npm start
    ```

> **Note:** Make sure to update the file with your environment-specific configurations before starting the application.

## Deploy

Build the application for production and deploy to dockerhub:

```bash
./build&publish.sh
```

> **Note:** Application container will be restarted automatically.

## Contributing

ToDo

Generate feature module:
```
ng g module features/{module_name} --module=app --routing
```

Generate features component:
```
ng g component features/{module_name}/{component_path}  --module features/{module_path}/{module_name}.module.ts
```

Generate shared smart component:
```
ng g component shared/components/smart/{component_path}  --standalone
```

Generate shared presentation reusable component:
```
ng g component shared/components/presentation/reusable/{component_path}  --standalone
```

Generate shared presentation app specific component:
```
ng g component shared/components/presentation/app-specific/{component_path}  --standalone
```

Generate core component:
```
ng g component core/components/{component_name}  --module core/core.module.ts
```

Generate core service:
```
ng g service core/services/{service_name}/{service_name}
```

Generate feature service:
```
ng g service features/{module_name}/{component_path}/{service_name}/{service_name}
```

## Who do I talk to?

Developed by bAvenir:
* Matej Kokol - [matej.kokol@bavenir.eu](mailto:matej.kokol@bavenir.eu)
* Jorge Almela - [jorge.almela@bavenir.eu](mailto:jorge.almela@bavenir.eu)
* Peter Drahovsky - [peter.drahovsky@bavenir.eu](mailto:peter.drahovsky@bavenir.eu)

## License

Copyright (C) 2024 bAvenir

This program and the accompanying materials are made
available under the terms of the Eclipse Public License 2.0
which is available at https://www.eclipse.org/legal/epl-2.0/

SPDX-License-Identifier: EPL-2.0

This program is based on templates originally licensed under
the MIT License from [angular-material-starter-template](https://github.com/sardapv/angular-material-starter-template)
