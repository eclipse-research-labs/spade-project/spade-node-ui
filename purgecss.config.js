/**
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

module.exports = {
    content: ['./dist/*.{html,js}'],
  
    defaultExtractor: (content) => {
     /*
      * this is taken from tailwind's sourcecode to prevent used tailwind classes with specail characters like 
      * @ - \ / to prevent in post-build purge script
      * */        

      const broadMatches = content.match(/[^<>"'`\s]*[^<>"'`\s:]/g) || [];
      const innerMatches = content.match(/[^<>"'`\s.()]*[^<>"'`\s.():]/g) || [];

      return broadMatches.concat(innerMatches);
    },
  
    css: ['./dist/*.css'],
    options: {
      safelist: [], // add if any special exceptions
    },
    output: './dist/',
  };