#!/bin/bash
# Copyright (C) 2024 bAvenir
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0

USAGE="$(basename "$0") [ -h ] [ -v version]
-- Build and publish image to docker registry
-- Flags:
      -h  shows help
      -v  version [ i.e. 1.0, 2.2,... ]"

# BUILD PLATFORMS 
PLATFORMS=linux/amd64,linux/arm64,linux/arm/v7
VERSION=0
LATEST=0 # If 1 build image with latest tag

# Default configuration
ENV=dev
# REGISTRY=hub.docker.com
IMAGE_NAME=bavenir/databroker-ui

# Get configuration
while getopts 'hd:v:l' OPTION; do
case "$OPTION" in
    h)
    echo "$USAGE"
    exit 0
    ;;
    v)
    VERSION="$OPTARG"
    ;;
    l)
    LATEST="1";
    ;;
esac
done

# Update to VERSION if any
if [ ${VERSION} != 0 ]
then
    ENV=${VERSION}
    GIT_ENV=${VERSION}
    LATEST=1
    echo Do you wish to continue pushing version ${VERSION} ?
    select yn in "Yes" "No"; do
    case $yn in
        Yes ) echo Updating image version!; break;;
        No ) echo Aborting...; exit;;
    esac
    done
fi

# Start build
echo Build and push image ${IMAGE_NAME} with tag ${ENV}

# Do login
docker login

# Compile ts into js
npm run build --  --output-path=dist --output-hashing=all --base-href=/

# PurgeCSS removes some of the valid css. Will look into it in future or remove PurgeCSS from project
# npm run purgecss

if [ $? != 0 ] 
then
    echo "Error building angular app"
    say 'Error building angular app'
    exit 1;
fi


# Multiarch builder
docker buildx use multiplatform

# Build images & push to private registry

if [ ${LATEST} == 1 ] 
then
    docker buildx build --platform ${PLATFORMS} \
                        --tag ${IMAGE_NAME}:${ENV} \
                        --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') \
                        --build-arg BUILD_VERSION=${VERSION} \
                        -f deployment/Dockerfile . --push
    # Pull local arch version
    docker pull ${IMAGE_NAME}:${ENV}
    # docker pull ${GIT_REGISTRY}/${GIT_IMAGE_NAME}:${GIT_ENV} # Build latest when new version
    # docker pull ${GIT_REGISTRY}/${GIT_IMAGE_NAME}:latest # Build latest when new version

else 
     # with latest tag 
    docker buildx build --platform ${PLATFORMS} \
                    --tag ${IMAGE_NAME}:${ENV} \
                    --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') \
                    --build-arg BUILD_VERSION=${VERSION} \
                    -f deployment/Dockerfile . --push
     # Pull local arch version
    docker pull ${IMAGE_NAME}:${ENV}
fi
# END
echo Build and publish ended successfully!
say 'Done!'
